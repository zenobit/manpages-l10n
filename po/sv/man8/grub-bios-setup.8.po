# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-07-22 15:08+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-BIOS-SETUP"
msgstr "GRUB-BIOS-SETUP"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "February 2023"
msgstr "februari 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r456.g65bc45963-1"
msgstr "GRUB 2:2.06.r456.g65bc45963-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrationsverktyg"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-bios-setup - set up a device to boot using GRUB"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-bios-setup> [I<\\,OPTION\\/>...] I<\\,DEVICE\\/>"
msgstr "B<grub-bios-setup> [I<\\,FLAGGA\\/>...] I<\\,ENHET\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Set up images to boot from DEVICE."
msgstr "Ställ in avbilder till att starta från DEVICE."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"You should not normally run this program directly.  Use grub-install instead."
msgstr ""
"Normalt kör du inte detta program direkt.  Använd B<grub-install>(8) "
"istället."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-a>, B<--allow-floppy>"
msgstr "B<-a>, B<--allow-floppy>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"make the drive also bootable as floppy (default for fdX devices). May break "
"on some BIOSes."
msgstr ""
"gör enheten startbar som floppy (standard för fdX-enheter). Kan gå sönder på "
"en del BIOS."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-b>, B<--boot-image>=I<\\,FILE\\/>"
msgstr "B<-b>, B<--boot-image>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "use FILE as the boot image [default=boot.img]"
msgstr "använd FIL som startavbildning [standard=boot.img]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>, B<--core-image>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--core-image>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "use FILE as the core image [default=core.img]"
msgstr "använd FIL som kärnavbild [standard=core.img]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,KATALOG\\/>"

#. type: Plain text
#: archlinux
msgid "use GRUB files in the directory DIR [default=//boot/grub]"
msgstr "använd GRUB-filer i katalogen KAT [standard=//boot/grub]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "install even if problems are detected"
msgstr "installera även om problem uppstår"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the device map [default=//boot/grub/device.map]"
msgstr "använd FIL som enhetsmappning [standard=//boot/grub/device.map]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--no-rs-codes>"
msgstr "B<--no-rs-codes>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Do not apply any reed-solomon codes when embedding core.img. This option is "
"only available on x86 BIOS targets."
msgstr ""
"Verkställ inte några reed-solomon-koder när core.img bäddas in. Detta "
"alternativ är bara tillgängligt på x86 BIOS."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>, B<--skip-fs-probe>"
msgstr "B<-s>, B<--skip-fs-probe>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "do not probe for filesystems in DEVICE"
msgstr "leta inte efter filsystem i ENHET"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print verbose messages."
msgstr "skriv ut informativa meddelanden."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "visa denna hjälplista"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "ge ett kort användningsmeddelande"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "skriv ut programversion"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriska eller valfria argument till långa flaggor är också "
"obligatoriska eller valfria för motsvarande korta flaggor."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "DEVICE must be an OS device (e.g. I<\\,/dev/sda\\/>)."
msgstr "ENHET måste vara en OS-enhet (exempelvis I<\\,/dev/sda\\/>)."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"
msgstr "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-bios-setup> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-bios-setup> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-bios-setup> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-bios-setup> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-bios-setup>"
msgstr "B<info grub-bios-setup>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "November 2022"
msgstr "november 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u5"
msgstr "GRUB 2.06-3~deb11u5"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "use GRUB files in the directory DIR [default=/boot/grub]"
msgstr "använd GRUB-filer i katalogen KATALOG [standard=/boot/grub]"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr "använd FIL som enhetsmappning [standard=/boot/grub/device.map]"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-8"
msgstr "GRUB 2.06-8"
