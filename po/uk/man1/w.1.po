# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-03 16:03+0200\n"
"PO-Revision-Date: 2022-04-13 16:33+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "W"
msgstr "W"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "May 2012"
msgstr "травень 2012 року"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: opensuse-leap-15-5
msgid "w - Show who is logged on and what they are doing."
msgstr ""
"w — програма для показу списку тих, хто працює у системі, та даних щодо "
"того, які дії вони виконують."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<w> [I<options>] I<user> [...]"
msgstr "B<w> [I<параметри>] I<користувач> [...]"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<w> displays information about the users currently on the machine, and "
"their processes.  The header shows, in this order, the current time, how "
"long the system has been running, how many users are currently logged on, "
"and the system load averages for the past 1, 5, and 15 minutes."
msgstr ""
"B<w> виводить дані щодо поточних користувачів системи та процесів, "
"власниками яких вони є. У заголовку виведених даних наведено: поточний час, "
"тривалість роботи системи, кількість користувачів, які працюють у системі, "
"та середнє навантаження на систему за останні 1, 5 та 15 хвилин."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The following entries are displayed for each user: login name, the tty name, "
"the remote host, login time, idle time, JCPU, PCPU, and the command line of "
"their current process."
msgstr ""
"Для кожного користувача буде показано такі дані: назву облікового запису, "
"назву tty, віддалений вузол, час входу до системи, час бездіяльності, JCPU, "
"PCPU та рядок команди поточного процесу."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The JCPU time is the time used by all processes attached to the tty.  It "
"does not include past background jobs, but does include currently running "
"background jobs."
msgstr ""
"Час JCPU є часом, використаним усіма процесами, пов’язаними із терміналом "
"tty. До нього не включено час виконання завершених фонових завдань, але "
"включено час виконання поточних фонових завдань."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The PCPU time is the time used by the current process, named in the \"what\" "
"field."
msgstr ""
"Час PCPU є часом, використаним поточним процесом, назву якого наведено у "
"полі «what»."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COMMAND-LINE OPTIONS"
msgstr "ПАРАМЕТРИ КОМАНДНОГО РЯДКА"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-h>, B<--no-header>"
msgstr "B<-h>, B<--no-header>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Don't print the header."
msgstr "Не виводити заголовок."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-u>, B<--no-current>"
msgstr "B<-u>, B<--no-current>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Ignores the username while figuring out the current process and cpu times.  "
"To demonstrate this, do a \"su\" and do a \"w\" and a \"w -u\"."
msgstr ""
"Ігнорувати ім’я користувача під час визначення поточного процесу та часу "
"використання процесора. Побачити різницю можна після виконання команди «su» "
"у даних, виведених командами «w» та «w -u»."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-s>, B<--short>"
msgstr "B<-s>, B<--short>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Use the short format.  Don't print the login time, JCPU or PCPU times."
msgstr ""
"Використовувати скорочений формат. Не виводити даних щодо часу входу до "
"системи та часових параметрів JCPU або PCPU."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-n>, B<--no-truncat>"
msgstr "B<-n>, B<--no-truncat>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Do not truncate the output format. This option might become renamed in "
"future versions."
msgstr ""
"Не обрізати формат виведення. Назву цього параметра може бути змінено у "
"майбутніх версіях."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-f>, B<--from>"
msgstr "B<-f>, B<--from>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Toggle printing the B<from> (remote hostname) field.  The default as "
"released is for the B<from> field to not be printed, although your system "
"administrator or distribution maintainer may have compiled a version in "
"which the B<from> field is shown by default."
msgstr ""
"Увімкнути виведення поля B<з> (назви віддаленого вузла). У типовому варіанті "
"програми поле B<з> не виводиться. Втім, адміністратор вашої системи або "
"супровідник дистрибутива може зібрати версію, у якій типово вміст поля B<з> "
"виводиться."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-i>, B<--ip-addr>"
msgstr "B<-i>, B<--ip-addr>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display IP address instead of hostname for B<from> field."
msgstr "Виводити у полі B<from> IP-адресу замість назви вузла."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information."
msgstr "Показати інформацію щодо версії."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-o>, B<--old-style>"
msgstr "B<-o>, B<--old-style>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Old style output.  Prints blank space for idle times less than one minute."
msgstr ""
"Виведення даних у старому стилі. Виводить пробіл для часів бездіяльності, "
"які тривають менше за одну хвилину."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<user >"
msgstr "B<user >"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Show information about the specified user only."
msgstr "Вивести дані лише щодо вказаного користувача."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "ENVIRONMENT"
msgstr "СЕРЕДОВИЩЕ"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "PROCPS_USERLEN"
msgstr "PROCPS_USERLEN"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Override the default width of the username column.  Defaults to 8."
msgstr ""
"Перевизначити типову ширину стовпчика імені користувача. Типовою є ширина у "
"8 символів."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "PROCPS_FROMLEN"
msgstr "PROCPS_FROMLEN"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Override the default width of the from column.  Defaults to 16."
msgstr ""
"Перевизначити типову ширину стовпчика джерела (from). Типовою є ширина у 16 "
"символів."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛИ"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "information about who is currently logged on"
msgstr "Дані щодо тих, хто зараз працює у системі."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "I</proc>"
msgstr "I</proc>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "process information"
msgstr "дані щодо процесу"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"
msgstr "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<w> was re-written almost entirely by Charles Blake, based on the version "
"by E<.UR greenfie@\\:gauss.\\:rutgers.\\:edu> Larry Greenfield E<.UE> and E<."
"UR johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE>"
msgstr ""
"B<w> майже повністю переписано з «нуля» Charles Blake на основі версії, "
"створеної E<.UR greenfie@\\:gauss.\\:rutgers.\\:edu> Larry Greenfield E<.UE> "
"та E<.UR johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE>"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Про вади, будь ласка, повідомляйте на адресу E<.UR procps@freelists.org> E<."
"UE>"
