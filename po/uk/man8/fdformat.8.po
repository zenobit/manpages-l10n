# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:49+0100\n"
"PO-Revision-Date: 2022-03-27 15:17+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "FDFORMAT"
msgstr "FDFORMAT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "June 2020"
msgstr "червень 2020 року"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "fdformat - low-level format a floppy disk"
msgstr "fdformat — низькорівневе форматування дискети"

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bullseye
msgid "B<fdformat> [options]I< device>"
msgstr "B<fdformat> [параметри]I< пристрій>"

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<fdformat> does a low-level format on a floppy disk.  I<device> is usually "
"one of the following (for floppy devices the major = 2, and the minor is "
"shown for informational purposes only):"
msgstr ""
"B<fdformat> виконує низькорівневе форматування дискети. Значенням аргументу "
"I<пристрій>, зазвичай, є одне з таких значень (для пристроїв для читання "
"дискет основним є 2, а допоміжний показано лише для інформування):"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid ""
"/dev/fd0d360  (minor = 4)\n"
"/dev/fd0h1200 (minor = 8)\n"
"/dev/fd0D360  (minor = 12)\n"
"/dev/fd0H360  (minor = 12)\n"
"/dev/fd0D720  (minor = 16)\n"
"/dev/fd0H720  (minor = 16)\n"
"/dev/fd0h360  (minor = 20)\n"
"/dev/fd0h720  (minor = 24)\n"
"/dev/fd0H1440 (minor = 28)\n"
msgstr ""
"/dev/fd0d360  (minor = 4)\n"
"/dev/fd0h1200 (minor = 8)\n"
"/dev/fd0D360  (minor = 12)\n"
"/dev/fd0H360  (minor = 12)\n"
"/dev/fd0D720  (minor = 16)\n"
"/dev/fd0H720  (minor = 16)\n"
"/dev/fd0h360  (minor = 20)\n"
"/dev/fd0h720  (minor = 24)\n"
"/dev/fd0H1440 (minor = 28)\n"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"/dev/fd1d360  (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360  (minor = 13)\n"
"/dev/fd1H360  (minor = 13)\n"
"/dev/fd1D720  (minor = 17)\n"
"/dev/fd1H720  (minor = 17)\n"
"/dev/fd1h360  (minor = 21)\n"
"/dev/fd1h720  (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"
msgstr ""
"/dev/fd1d360  (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360  (minor = 13)\n"
"/dev/fd1H360  (minor = 13)\n"
"/dev/fd1D720  (minor = 17)\n"
"/dev/fd1H720  (minor = 17)\n"
"/dev/fd1h360  (minor = 21)\n"
"/dev/fd1h720  (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"The generic floppy devices, /dev/fd0 and /dev/fd1, will fail to work with "
"B<fdformat> when a non-standard format is being used, or if the format has "
"not been autodetected earlier.  In this case, use B<setfdprm>(8)  to load "
"the disk parameters."
msgstr ""
"Типові пристрої для дискет, I</dev/fd0> і I</dev/fd1>, не працюватимуть з "
"B<fdformat>, якщо використовується нестандартний формат або якщо формат не "
"було автоматично виявлено раніше. Якщо ви маєте справу з одним з цих "
"випадків, скористайтеся B<setfdprm>(8) для завантаження параметрів дискети."

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<-f>, B<--from> I<N>"
msgstr "B<-f>, B<--from> I<N>"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "Start at the track I<N> (default is 0)."
msgstr "Почати з доріжки I<N> (типовою є доріжка 0)."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<-t>, B<--to> I<N>"
msgstr "B<-t>, B<--to> I<N>"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "Stop at the track I<N>."
msgstr "Зупинитися на доріжці I<N>."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<-r>, B<--repair> I<N>"
msgstr "B<-r>, B<--repair> I<N>"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "Try to repair tracks failed during the verification (max I<N> retries)."
msgstr ""
"Спробувати виправити доріжки з помилками, виявлені під час перевірки (до "
"I<N> повторів)."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<-n>, B<--no-verify>"
msgstr "B<-n>, B<--no-verify>"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "Skip the verification that is normally performed after the formatting."
msgstr "Пропустити верифікацію, яка, зазвичай, виконується після форматування."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ПРИМІТКИ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This utility does not handle USB floppy disk drives. Use B<ufiformat>(8)  "
"instead."
msgstr ""
"Ця програма не працює із пристроями USB для дискет. Скористайтеся замість "
"неї B<ufiformat>(8)."

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: Plain text
#: debian-bullseye
msgid "Werner Almesberger (almesber@nessie.cs.id.ethz.ch)"
msgstr "Werner Almesberger (almesber@nessie.cs.id.ethz.ch)"

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "B<fd>(4), B<emkfs>(8), B<mkfs>(8), B<setfdprm>(8), B<ufiformat>(8)"
msgstr "B<fd>(4), B<emkfs>(8), B<mkfs>(8), B<setfdprm>(8), B<ufiformat>(8)"

#. type: SH
#: debian-bullseye fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bullseye
msgid ""
"The fdformat command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"Програма fdformat є частиною пакунка util-linux і доступна за адресою "
"https://www.kernel.org/pub/linux/utils/util-linux/."

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "B<fdformat> [options] I<device>"
msgstr "B<fdformat> [параметри] I<пристрій>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"B<fdformat> does a low-level format on a floppy disk. I<device> is usually "
"one of the following (for floppy devices the major = 2, and the minor is "
"shown for informational purposes only):"
msgstr ""
"B<fdformat> виконує низькорівневе форматування дискети. Значенням аргументу "
"I<пристрій>, зазвичай, є одне з таких значень (для пристроїв для читання "
"дискет основним є 2, а допоміжний показано лише для інформування):"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid ""
"/dev/fd1d360 (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360 (minor = 13)\n"
"/dev/fd1H360 (minor = 13)\n"
"/dev/fd1D720 (minor = 17)\n"
"/dev/fd1H720 (minor = 17)\n"
"/dev/fd1h360 (minor = 21)\n"
"/dev/fd1h720 (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"
msgstr ""
"/dev/fd1d360 (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360 (minor = 13)\n"
"/dev/fd1H360 (minor = 13)\n"
"/dev/fd1D720 (minor = 17)\n"
"/dev/fd1H720 (minor = 17)\n"
"/dev/fd1h360 (minor = 21)\n"
"/dev/fd1h720 (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"The generic floppy devices, I</dev/fd0> and I</dev/fd1>, will fail to work "
"with B<fdformat> when a non-standard format is being used, or if the format "
"has not been autodetected earlier. In this case, use B<setfdprm>(8) to load "
"the disk parameters."
msgstr ""
"Типові пристрої для дискет, I</dev/fd0> і I</dev/fd1>, не працюватимуть з "
"B<fdformat>, якщо використовується нестандартний формат або якщо формат не "
"було автоматично виявлено раніше. Якщо ви маєте справу з одним з цих "
"випадків, скористайтеся B<setfdprm>(8) для завантаження параметрів дискети."

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"This utility does not handle USB floppy disk drives. Use B<ufiformat>(8) "
"instead."
msgstr ""
"Ця програма не працює із пристроями USB для дискет. Скористайтеся замість "
"неї B<ufiformat>(8)."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"The B<fdformat> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<fdformat> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
