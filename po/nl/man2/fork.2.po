# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-02-20 20:06+0100\n"
"PO-Revision-Date: 2023-02-12 19:47+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch - The Netherlands <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 42.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "fork"
msgstr "fork"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "fork - create a child process"
msgstr "fork - maak een kind-proces"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<pid_t fork(void);>\n"
msgstr "B<pid_t fork(void);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fork>()  creates a new process by duplicating the calling process.  The "
"new process is referred to as the I<child> process.  The calling process is "
"referred to as the I<parent> process."
msgstr ""
"B<fork>() maakt een nieuw proces aan door het huidige proces te dupliceren. "
"Het nieuwe proces wordt het kind-proces genoemd. Het huidige proces wordt "
"het I<ouder> proces genoemd."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child process and the parent process run in separate memory spaces.  At "
"the time of B<fork>()  both memory spaces have the same content.  Memory "
"writes, file mappings (B<mmap>(2)), and unmappings (B<munmap>(2))  performed "
"by one of the processes do not affect the other."
msgstr ""
"Het kind-proces en het ouder-proces draaien in aparte geheugen ruimten. "
"Tijdens de B<fork>() hebben beide geheugen gebieden dezelfde inhoud. "
"Schrijven naar geheugen, afbeelden  (B<mmap>(2)) en vrijgeven  "
"(B<munmap>(2)) van bestanden door een van de processen beïnvloeden het "
"andere niet."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child process is an exact duplicate of the parent process except for the "
"following points:"
msgstr ""
"Het kind-proces is een exact duplicaat van het ouder-proces behalve de "
"volgende punten:"

#. type: IP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child has its own unique process ID, and this PID does not match the ID "
"of any existing process group (B<setpgid>(2))  or session."
msgstr ""
"Het kind heeft zijn eigen unieke proces ID en dit PID komt niet overeen met "
"enig  ID van een van de bestaande proces groepen ( (B<setpgid>(2))  of "
"sessie."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The child's parent process ID is the same as the parent's process ID."
msgstr ""
"Het ouder proces ID van het kind is hetzelfde als het proces ID van de ouder."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child does not inherit its parent's memory locks (B<mlock>(2), "
"B<mlockall>(2))."
msgstr ""
"Het kind erft het slot op het geheugen niet van de ouder (B<mlock>(2), "
"B<mlockall>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Process resource utilizations (B<getrusage>(2))  and CPU time counters "
"(B<times>(2))  are reset to zero in the child."
msgstr ""
"Gebruik van bronnen  (B<getrusage>(2))  en de CPU tijd tellers (B<times>(2)) "
"worden op nul gezet in het kind."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child's set of pending signals is initially empty (B<sigpending>(2))."
msgstr ""
"De verzameling van in afwachting zijnde signalen in kind is leeg "
"(B<sigpending>(2)). "

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child does not inherit semaphore adjustments from its parent "
"(B<semop>(2))."
msgstr "Het kind erft geen seinpaal aanpassingen van zijn ouder (B<semop>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child does not inherit process-associated record locks from its parent "
"(B<fcntl>(2)).  (On the other hand, it does inherit B<fcntl>(2)  open file "
"description locks and B<flock>(2)  locks from its parent.)"
msgstr ""
"Het kind erft geen geassocieerde dossier sloten van zijn ouder B<fcntl>(2)). "
"(Aan de andere kant erft het wel B<fnctl>(2) de open bestandsindicator "
"sloten en de B<flock>(2) van zijn ouder.)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child does not inherit timers from its parent (B<setitimer>(2), "
"B<alarm>(2), B<timer_create>(2))."
msgstr ""
"Het kind erft geen timers van zijn ouder (B<setitimer>(2), B<alarm>(2), "
"B<timer_create>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child does not inherit outstanding asynchronous I/O operations from its "
"parent (B<aio_read>(3), B<aio_write>(3)), nor does it inherit any "
"asynchronous I/O contexts from its parent (see B<io_setup>(2))."
msgstr ""
"Het kind erft geen uitstaande asynchrone Invoer/Uitvoer operatie van zijn "
"ouder  (B<aio_read>(3), B<aio_write>(3)), noch erft het enige asynchrone "
"Invoer/Uitvoer contexts van zijn ouder (zie B<io_setup>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The process attributes in the preceding list are all specified in POSIX.1.  "
"The parent and child also differ with respect to the following Linux-"
"specific process attributes:"
msgstr ""
"De proces attributen in de voorafgaande lijst zijn allemaal gespecificeerd "
"in POSIX.1. De ouder en het kind verschillen op met betrekking tot de "
"volgende Linux-specifieke proces attributen:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child does not inherit directory change notifications (dnotify)  from "
"its parent (see the description of B<F_NOTIFY> in B<fcntl>(2))."
msgstr ""
"Het kind erft geen meldingen over verandering in mappen (dnotify) van zijn "
"ouder (zie de beschrijving van B<F_NOTIFY> in B<fcntl>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<prctl>(2)  B<PR_SET_PDEATHSIG> setting is reset so that the child does "
"not receive a signal when its parent terminates."
msgstr ""
"De B<prctl>(2)  B<PR_SET_PDEATHSIG> wordt teruggezet zodat het kind geen "
"signaal ontvangt wanneer zijn ouder stopt."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The default timer slack value is set to the parent's current timer slack "
"value.  See the description of B<PR_SET_TIMERSLACK> in B<prctl>(2)."
msgstr ""
"De standaard waarde van de timer speling wordt gezet op de huidige waarde "
"van de ouder. Zie de beschrijving van B<PR_SET_TIMERSLACK> in B<prctl>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Memory mappings that have been marked with the B<madvise>(2)  "
"B<MADV_DONTFORK> flag are not inherited across a B<fork>()."
msgstr ""
"De geheugen indelingen die werden gemarkeerd met de B<madvise>(2)  "
"B<MADV_DONTFORK> vlag worden niet overgeërfd langs B<fork>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Memory in address ranges that have been marked with the B<madvise>(2)  "
"B<MADV_WIPEONFORK> flag is zeroed in the child after a B<fork>().  (The "
"B<MADV_WIPEONFORK> setting remains in place for those address ranges in the "
"child.)"
msgstr ""
"Geheugen in de adres gebieden die werden gemarkeerd met de B<madvise>(2)  "
"B<MADV_WIPEONFORK> vlag wordt  met 0-gevuld in het kind na de B<fork>().  "
"(DeB<MADV_WIPEONFORK> instelling blijft van kracht voor deze adres gebieden "
"in het kind)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The termination signal of the child is always B<SIGCHLD> (see B<clone>(2))."
msgstr ""
"Het beëindiging signaal van het kind is altijd B<SIGCHLD> (zie B<clone>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The port access permission bits set by B<ioperm>(2)  are not inherited by "
"the child; the child must turn on any bits that it requires using "
"B<ioperm>(2)."
msgstr ""
"De toegangsrechten bits van de poort gezet door B<ioperm>(2)  worden niet "
"geërfd door het kind; het kind moet de benodigde bits zelf aan zetten met "
"B<ioperm>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Note the following further points:"
msgstr "Let op de volgende punten:"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "The child process is created with a single thread\\(emthe one that called "
#| "B<fork>().  The entire virtual address space of the parent is replicated "
#| "in the child, including the states of mutexes, condition variables, and "
#| "other pthreads objects; the use of B<pthread_atfork>(3)  may be helpful "
#| "for dealing with problems that this can cause."
msgid ""
"The child process is created with a single thread\\[em]the one that called "
"B<fork>().  The entire virtual address space of the parent is replicated in "
"the child, including the states of mutexes, condition variables, and other "
"pthreads objects; the use of B<pthread_atfork>(3)  may be helpful for "
"dealing with problems that this can cause."
msgstr ""
"Het kind-proces wordt gemaakt met een enkele thread\\emdie die B<fork>() "
"aanriep. De complete virtuele adresruimte van de ouder wordt  gekopieerd in "
"het kind, inclusief de statussen van de seinpalen, de conditie variabelen, "
"en andere pthread objecten; het gebruik van B<pthread_atfork>(3)  kan nuttig "
"zijn tijdens het afhandelen van problemen die dit kunnen veroorzaken."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"After a B<fork>()  in a multithreaded program, the child can safely call "
"only async-signal-safe functions (see B<signal-safety>(7))  until such time "
"as it calls B<execve>(2)."
msgstr ""
"Na een B<fork>() in een multithreaded programma, kan het kind zonder "
"problemen asynchrone-signaal-veilige functies aanroepen (zie B<signal-"
"safety>(7))  totdat het moment dat  B<execve>(2) wordt aangeroepen. "

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child inherits copies of the parent's set of open file descriptors.  "
"Each file descriptor in the child refers to the same open file description "
"(see B<open>(2))  as the corresponding file descriptor in the parent.  This "
"means that the two file descriptors share open file status flags, file "
"offset, and signal-driven I/O attributes (see the description of B<F_SETOWN> "
"and B<F_SETSIG> in B<fcntl>(2))."
msgstr ""
"Het kind erft kopieën van de verzameling open bestandsindicatoren van de "
"ouder. Elke bestandsindicator in het kind wijst naar dezelfde open "
"bestandsindicator (zie B<open>(2)) als de daarmee overeenkomende "
"bestandsindicator in de ouder. Dit betekent dat de twee open "
"bestandsindicatoren dezelfde open bestandsstatus vlaggen delen, de "
"bestandspositie en de signaal-gedreven Invoer/Uitvoer attributen (zie de "
"beschrijving van B<F_SETOWN> en B<F_SETSIG> in B<fcntl>(2))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child inherits copies of the parent's set of open message queue "
"descriptors (see B<mq_overview>(7)).  Each file descriptor in the child "
"refers to the same open message queue description as the corresponding file "
"descriptor in the parent.  This means that the two file descriptors share "
"the same flags (I<mq_flags>)."
msgstr ""
"Het kind erft kopieën van de verzameling open bericht rij-indicatoren van de "
"ouder (zie B<mq_overview>(7)). Elke bestandsindicator in het kind wijst naar "
"dezelfde open bericht rij-indicator als de daarmee overeenkomende "
"bestandsindicator in de ouder. Dit betekent dat de twee bestandsindicatoren "
"dezelfde vlaggen I<mq_flags>) delen."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child inherits copies of the parent's set of open directory streams (see "
"B<opendir>(3)).  POSIX.1 says that the corresponding directory streams in "
"the parent and child I<may> share the directory stream positioning; on Linux/"
"glibc they do not."
msgstr ""
"Het kind erft kopieën van de verzameling open map stromen van de ouder (zie "
"B<opendir>(3)).  POSIX.1 schrijft dat de overeenkomende map stromen in de "
"ouder en het kind dezelfde map stroom posities I<mogen> delen; op Linux/"
"glibc doen ze dit niet."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, the PID of the child process is returned in the parent, and 0 is "
"returned in the child.  On failure, -1 is returned in the parent, no child "
"process is created, and I<errno> is set to indicate the error."
msgstr ""
"Bij succes wordt de PID van het kind-proces teruggegeven aan het ouder-"
"proces en wordt 0 teruggegeven in het kind-proces. Bij falen wordt -1 "
"teruggegeven aan het ouder-proces, er wordt geen kind-proces gemaakt, en "
"I<errno> wordt overeenkomstig gezet."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#.  NOTE! The following should match the description in pthread_create(3)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A system-imposed limit on the number of threads was encountered.  There are "
"a number of limits that may trigger this error:"
msgstr ""
"Een door het systeem bepaalde limiet op het aantal threads werd "
"tegengekomen. Er zijn verschillende limieten die deze fout kunnen oproepen:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"the B<RLIMIT_NPROC> soft resource limit (set via B<setrlimit>(2)), which "
"limits the number of processes and threads for a real user ID, was reached;"
msgstr ""
"de B<RLIMIT_NPROC> zachte bron limiet (ingesteld via B<setrlimit>(2)), die "
"het aantal processen en threads voor het echte gebruikers ID limiteert, werd "
"bereikt;"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"the kernel's system-wide limit on the number of processes and threads, I</"
"proc/sys/kernel/threads-max>, was reached (see B<proc>(5));"
msgstr ""
"de systeem-brede limiet van de kernel op het aantal processen en threads,  "
"I</proc/sys/kernel/threads-max>, werd bereikt (zie B<proc>(5));"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"the maximum number of PIDs, I</proc/sys/kernel/pid_max>, was reached (see "
"B<proc>(5)); or"
msgstr ""
"het maximaal aantal PID´s, I</proc/sys/kernel/pid_max>, werd bereikt (zie "
"B<proc>(5)); of"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"the PID limit (I<pids.max>)  imposed by the cgroup \"process number\" (PIDs) "
"controller was reached."
msgstr ""
"de PID limiet  (I<pids.max>)  opgelegd door de cgroup \"proces nummer\" (PID"
"´s) controller werd bereikt."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The caller is operating under the B<SCHED_DEADLINE> scheduling policy and "
"does not have the reset-on-fork flag set.  See B<sched>(7)."
msgstr ""
"De aanroeper werkt onder het B<SCHED_DEADLINE> agendabeheer beleid en heeft "
"niet de reset-on-fork vlag gezet. Zie B<sched>(7)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fork>()  failed to allocate the necessary kernel structures because memory "
"is tight."
msgstr ""
"B<fork>() faalde in het vrijmaken van de nodige kernel structuren omdat er "
"te weinig geheugen was. "

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"An attempt was made to create a child process in a PID namespace whose "
"\"init\" process has terminated.  See B<pid_namespaces>(7)."
msgstr ""
"Een poging werd gedaan om een kind-proces aan te maken in de PID naamruimte "
"wiens \"init\" proces werd beëindigd.  Zie B<pid_namespaces>(7)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#.  e.g., arm (optionally), blackfin, c6x, frv, h8300, microblaze, xtensa
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fork>()  is not supported on this platform (for example, hardware without "
"a Memory-Management Unit)."
msgstr ""
"B<fork>() is niet ondersteund op dit platform (bij voorbeeld: hardware "
"zonder een Memory-Management Unit)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ERESTARTNOINTR> (since Linux 2.6.17)"
msgstr "B<ERESTARTNOINTR> (sinds Linux 2.6.17)"

#.  commit 4a2c7a7837da1b91468e50426066d988050e4d56
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"System call was interrupted by a signal and will be restarted.  (This can be "
"seen only during a trace.)"
msgstr ""
"De systeem aanroep werd onderbroken door een signaal en wordt geherstart. "
"(Dit kan alleen geobserveerd worden tijdens een trace.)"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Under Linux, B<fork>()  is implemented using copy-on-write pages, so the "
"only penalty that it incurs is the time and memory required to duplicate the "
"parent's page tables, and to create a unique task structure for the child."
msgstr ""
"Onder Linux is B<fork>() geïmplementeerd door gebruik te maken van copy-on-"
"write pagina´s, de enige straf die dit mee brengt is de benodigde tijd en "
"hoeveelheid geheugen die nodig is om de pagina tabellen van de ouder te "
"dupliceren, en om een unieke taak structuur in het kind te maken."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "C library/kernel verschillen"

#.  nptl/sysdeps/unix/sysv/linux/fork.c
#.  and does some magic to ensure that getpid(2) returns the right value.
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Since glibc 2.3.3, rather than invoking the kernel's B<fork>()  system call, "
"the glibc B<fork>()  wrapper that is provided as part of the NPTL threading "
"implementation invokes B<clone>(2)  with flags that provide the same effect "
"as the traditional system call.  (A call to B<fork>()  is equivalent to a "
"call to B<clone>(2)  specifying I<flags> as just B<SIGCHLD>.)  The glibc "
"wrapper invokes any fork handlers that have been established using "
"B<pthread_atfork>(3)."
msgstr ""
"Vanaf glibc 2.3.3, in plaats van het aanroepen van de B<fork>() systeem "
"aanroep, roept de glibc B<fork>() omwikkel functie, die voorzien is als "
"onderdeel van de NPTL threading implementatie, B<clone>(2) aan met vlaggen "
"die hetzelfde effect geven als de traditionele systeem aanroep. (Een aanroep "
"van B<fork>() is equivalent met de aanroep van B<clone>(2) waarbij de "
"I<flags> gespecificeerd wordt als precies B<SIGCHLD>.)  De glibc functie "
"roept enig vork afhandelaars aan  die werden vastgesteld door "
"B<pthread_atfork>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VOORBEELDEN"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "See B<pipe>(2)  and B<wait>(2)  for more examples."
msgstr "Zie B<pipe>(2) en B<wait>(2) voor meer voorbeelden."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    pid_t pid;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    pid_t pid;\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (signal(SIGCHLD, SIG_IGN) == SIG_ERR) {\n"
"        perror(\"signal\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    pid = fork();\n"
"    switch (pid) {\n"
"    case -1:\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    case 0:\n"
"        puts(\"Child exiting.\");\n"
"        exit(EXIT_SUCCESS);\n"
"    default:\n"
"        printf(\"Child is PID %jd\\en\", (intmax_t) pid);\n"
"        puts(\"Parent exiting.\");\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
"}\n"
msgstr ""
"    if (signal(SIGCHLD, SIG_IGN) == SIG_ERR) {\n"
"        perror(\"signal\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    pid = fork();\n"
"    switch (pid) {\n"
"    case -1:\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    case 0:\n"
"        puts(\"Kind verlaten.\");\n"
"        exit(EXIT_SUCCESS);\n"
"    default:\n"
"        printf(\"Kind is PID %jd\\en\", (intmax_t) pid);\n"
"        puts(\"Ouder verlaten.\");\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
"}\n"

#. #-#-#-#-#  archlinux: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: fork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<execve>(2), B<exit>(2), B<setrlimit>(2), B<unshare>(2), "
"B<vfork>(2), B<wait>(2), B<daemon>(3), B<pthread_atfork>(3), "
"B<capabilities>(7), B<credentials>(7)"
msgstr ""
"B<clone>(2), B<execve>(2), B<exit>(2), B<setrlimit>(2), B<unshare>(2), "
"B<vfork>(2), B<wait>(2), B<daemon>(3), B<pthread_atfork>(3), "
"B<capabilities>(7), B<credentials>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "FORK"
msgstr "FORK"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 juni 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<pid_t fork(void);>"
msgstr "B<pid_t fork(void);>"

#. type: IP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The child process is created with a single thread\\(emthe one that called "
"B<fork>().  The entire virtual address space of the parent is replicated in "
"the child, including the states of mutexes, condition variables, and other "
"pthreads objects; the use of B<pthread_atfork>(3)  may be helpful for "
"dealing with problems that this can cause."
msgstr ""
"Het kind-proces wordt gemaakt met een enkele thread\\emdie die B<fork>() "
"aanriep. De complete virtuele adresruimte van de ouder wordt  gekopieerd in "
"het kind, inclusief de statussen van de seinpalen, de conditie variabelen, "
"en andere pthread objecten; het gebruik van B<pthread_atfork>(3)  kan nuttig "
"zijn tijdens het afhandelen van problemen die dit kunnen veroorzaken."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, the PID of the child process is returned in the parent, and 0 is "
"returned in the child.  On failure, -1 is returned in the parent, no child "
"process is created, and I<errno> is set appropriately."
msgstr ""
"Bij succes wordt de PID van het kind-proces teruggegeven aan het ouder-"
"proces en wordt 0 teruggegeven in het kind-proces. Bij falen wordt -1 "
"teruggegeven aan het ouder-proces, er wordt geen kind-proces gemaakt, en "
"I<errno> wordt overeenkomstig gezet."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#.  nptl/sysdeps/unix/sysv/linux/fork.c
#.  and does some magic to ensure that getpid(2) returns the right value.
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Since version 2.3.3, rather than invoking the kernel's B<fork>()  system "
"call, the glibc B<fork>()  wrapper that is provided as part of the NPTL "
"threading implementation invokes B<clone>(2)  with flags that provide the "
"same effect as the traditional system call.  (A call to B<fork>()  is "
"equivalent to a call to B<clone>(2)  specifying I<flags> as just "
"B<SIGCHLD>.)  The glibc wrapper invokes any fork handlers that have been "
"established using B<pthread_atfork>(3)."
msgstr ""
"In plaats van het aanroepen van de kernel´ B<fork>() systeem aanroep, roept "
"vanaf versie 2.3.3 de glibc B<fork>() omwikkel functie, die voorzien is als "
"onderdeel van de NPTL threading implementatie, B<clone>(2) aan met vlaggen "
"die hetzelfde effect geven als de traditionele systeem aanroep. (Een aanroep "
"van B<fork>() is equivalent met de aanroep van B<clone>(2) waarbij de "
"I<flags> gespecificeerd wordt als precies B<SIGCHLD>.)  De glibc functie "
"roept enig vork afhandelaars aan  die werden vastgesteld door "
"B<pthread_atfork>(3) te gebruiken."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "See B<pipe>(2)  and B<wait>(2)."
msgstr "Zie B<pipe>(2) en B<wait>(2)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.10 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "VOORBEELD"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 december 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pagina's 6.02"

#. type: IP
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"
