# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-02-26 08:02+0100\n"
"PO-Revision-Date: 2023-02-28 19:01+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "UKIFY"
msgstr "UKIFY"

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#. type: TH
#: archlinux
#, no-wrap
msgid "ukify"
msgstr "ukify"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid "ukify - Combine kernel and initrd into a signed Unified Kernel Image"
msgstr ""
"ukify - Kernel und Initrd in ein einziges, vereinigtes Kernelabbild "
"kombinieren"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid "B</usr/lib/systemd/ukify> I<LINUX> I<INITRD>... [OPTIONS...]"
msgstr "B</usr/lib/systemd/ukify> I<LINUX> I<INITRD>… [OPTIONEN…]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux
msgid ""
"Note: this command is experimental for now\\&. While it is intended to "
"become a regular component of systemd, it might still change in behaviour "
"and interface\\&."
msgstr ""
"Hinweis: Dieser Befehl ist derzeit experimentell\\&. Es ist zwar geplant, "
"dass er eine normale Komponente von Systemd wird, aber sein Verhalten und "
"seine Schnittstellen können sich noch ändern\\&."

#. type: Plain text
#: archlinux
msgid ""
"B<ukify> is a tool that combines a kernel and an initrd with a UEFI boot "
"stub to create a \\m[blue]B<Unified Kernel Image "
"(UKI)>\\m[]\\&\\s-2\\u[1]\\d\\s+2 \\(em a PE binary that can be executed by "
"the firmware to start the embedded linux kernel\\&. See B<systemd-stub>(7)  "
"for details about the stub\\&."
msgstr ""
"B<ukify> ist ein Werkzeug, das einen Kernel und eine Initrd mit einem UEFI-"
"Startrumpf kombiniert, um ein \\m[blue]B<Vereinigtes Kernelabbild "
"(UKI)>\\m[]\\&\\s-2\\u[1]\\d\\s+2 zu erstellen \\(en ein PE-Programm, das "
"durch die Firmware ausgeführt werden kann, um einen eingebetteten Linux-"
"Kernel zu starten\\&. Siehe B<systemd-stub>(7) für Details über den Rumpf\\&."

#. type: Plain text
#: archlinux
msgid ""
"Additional sections will be inserted into the UKI, either automatically or "
"only if a specific option is provided\\&. See the discussions of B<--"
"cmdline=>, B<--os-release=>, B<--devicetree=>, B<--splash=>, B<--pcrpkey=>, "
"B<--uname=>, and B<--section=> below\\&."
msgstr ""
"Es werden zusätzliche Abschnitte in den UKI eingefügt, entweder automatisch "
"oder nur falls eine bestimmte Option bereitgestellt wird\\&. Siehe die "
"nachfolgende Besprechung von B<--cmdline=>, B<--os-release=>, B<--"
"devicetree=>, B<--splash=>, B<--pcrpkey=>, B<--uname=> und B<--section=>\\&."

#. type: Plain text
#: archlinux
msgid ""
"If PCR signing keys are provided via the B<--pcr-public-key=> and B<--pcr-"
"private-key=> options, PCR values that will be seen after booting with the "
"given kernel, initrd, and other sections, will be calculated, signed, and "
"embedded in the UKI\\&.  B<systemd-measure>(1)  is used to perform this "
"calculation and signing\\&."
msgstr ""
"Falls über die Optionen B<--pcr-public-key=> und B<--pcr-"
"private-key=> PCR-Signierschlüssel bereitgestellt werden, werden PCR-Werte, "
"die nach dem Systemstart mit dem angegebenen Kernel, der Initrd und anderen "
"Abschnitten auftauchen, berechnet, signiert und in das UKI eingebettet\\&. "
"Diese Berechnung und Signatur erfolgt mit B<systemd-measure>(1)\\&."

#. type: Plain text
#: archlinux
msgid ""
"The calculation of PCR values is done for specific boot phase paths\\&. "
"Those can be specified with B<--phases=> option\\&. If not specified, the "
"default provided by B<systemd-measure> is used\\&. It is also possible to "
"specify the B<--pcr-private-key=>, B<--pcr-public-key=>, and B<--phases=> "
"arguments more than once\\&. Signatures will be then performed with each of "
"the specified keys\\&. When both B<--phases=> and B<--pcr-private-key=> are "
"used, they must be specified the same number of times, and then the n-th "
"boot phase path set will be signed by the n-th key\\&. This can be used to "
"build different trust policies for different phases of the boot\\&."
msgstr ""
"Die Berechnung von PCR-Werten erfolgt für bestimmte "
"Systemstartphasenpfade\\&. Diese können mit der Option B<--phases=> "
"angegeben werden\\&. Falls nicht angegeben, wird die von B<systemd-"
"measure>(1) bereitgestellte Vorgabe verwandt\\&. Es ist auch möglich, die "
"Argumente B<--pcr-private-key=>, B<--pcr-public-key=> und B<--phases=> mehr "
"als einmal anzugeben\\&. Die Signaturen erfolgen dann mit jedem der "
"angegebenen Schlüssel\\&. Wird sowohl B<--phases=> als auch B<--pcr-private-"
"key=> verwandt, müssen sie beide gleich häufig angegeben werden und der n-te "
"Systemstartphasenpfad wird mit dem n-ten Schlüssel signiert\\&. Dies kann "
"zur Erstellung verschiedener Vertrauensrichtlinien für die verschiedenen Phasen "
"des Systemstarts verwandt werden\\&."

#. type: Plain text
#: archlinux
msgid ""
"If a SecureBoot signing key is provided via the B<--secureboot-private-key=> "
"option, the resulting PE binary will be signed as a whole, allowing the "
"resulting UKI to be trusted by SecureBoot\\&. Also see the discussion of "
"automatic enrollment in B<systemd-boot>(7)\\&."
msgstr ""
"Falls ein SecureBoot-Signaturschlüssel über die Option B<--secureboot-"
"private-key=> bereitgestellt ist, wird das resultierende PE-Programm als "
"Ganzes damit signiert\\&. Damit wird ermöglicht, dass SecureBoot dem "
"gesamten UKI vertraut\\&. Siehe auch die Erläuterung der automatischen "
"Registrierung in B<systemd-boot>(7)\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux
msgid ""
"Note that the I<LINUX> positional argument is mandatory\\&. The I<INITRD> "
"positional arguments are optional\\&. If more than one is specified, they "
"will all be combined into a single PE section\\&. This is useful to for "
"example prepend microcode before the actual initrd\\&."
msgstr ""
"Beachten Sie, dass das positionsabhängige Argument I<LINUX> verpflichtend "
"ist\\&. Die positionsabhängigen Argumente I<INITRD> sind optional\\&. Falls "
"mehr als eines angegeben ist, werden sie alle in einen einzigen PE-Abschnitt "
"kombiniert\\&. Dies ist zum Beispiel nützlich, um der eigentlichen Initrd "
"Microcode voranzustellen\\&."

#. type: Plain text
#: archlinux
msgid "The following options are understood:"
msgstr "Die folgenden Optionen werden verstanden:"

#. type: Plain text
#: archlinux
msgid "B<--cmdline=>I<TEXT>B<|>I<@PATH>"
msgstr "B<--cmdline=>I<TEXT>B<|>I<@PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify the kernel command line (the \"\\&.cmdline\" section)\\&. The "
"argument may be a literal string, or \"@\" followed by a path name\\&. If "
"not specified, no command line will be embedded\\&."
msgstr ""
"Gibt die Kernelbefehlszeile an (den Abschnitt »\\&.cmdline«)\\&. Das "
"Argument kann eine wörtliche Zeichenkette oder »@«, gefolgt von einem "
"Pfadnamen, sein\\&. Falls dies nicht angegeben ist, wird keine Befehlszeile "
"eingebettet\\&."

#. type: Plain text
#: archlinux
msgid "B<--os-release=>I<TEXT>B<|>I<@PATH>"
msgstr "B<--os-release=>I<TEXT>B<|>I<@PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify the os-release description (the \"\\&.osrel\" section)\\&. The "
"argument may be a literal string, or \"@\" followed by a path name\\&. If "
"not specified, the B<os-release>(5)  file will be picked up from the host "
"system\\&."
msgstr ""
"Gibt die Betriebssystem-Veröffentlichungsbeschreibung an (den Abschnitt "
"»\\&.osrel«)\\&. Das Argument kann eine wörtliche Zeichenkette oder »@«, "
"gefolgt von einem Pfadnamen, sein\\&. Falls dies nicht angegeben ist, wird "
"die Datei B<os-release>(5) vom Rechnersystem genommen\\&."

#. type: Plain text
#: archlinux
msgid "B<--devicetree=>I<PATH>"
msgstr "B<--devicetree=>I<PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify the devicetree description (the \"\\&.dtb\" section)\\&. The "
"argument is a path to a compiled binary DeviceTree file\\&. If not "
"specified, the section will not be present\\&."
msgstr ""
"Gibt die Devicetree-Beschreibung an (den Abschnitt »\\&.dtb«)\\&. Das "
"Argument ist ein Pfad zu einer kompilierten binären DeviceTree-Datei\\&. "
"Falls dies nicht angegeben ist, wird der Abschnitt nicht vorhanden sein\\&."

#. type: Plain text
#: archlinux
msgid "B<--splash=>I<PATH>"
msgstr "B<--splash=>I<PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify a picture to display during boot (the \"\\&.splash\" section)\\&. "
"The argument is a path to a BMP file\\&. If not specified, the section will "
"not be present\\&."
msgstr ""
"Gibt ein Bild an, das während des Systemstarts angezeigt werden soll (den "
"Abschnitt »\\&.splash«)\\&. Das Argument ist ein Pfad zu einer BMP-Datei\\&. "
"Falls dies nicht angegeben ist, wird der Abschnitt nicht vorhanden sein\\&."

#. type: Plain text
#: archlinux
msgid "B<--pcrpkey=>I<PATH>"
msgstr "B<--pcrpkey=>I<PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify a path to a public key to embed in the \"\\&.pcrpkey\" section\\&. "
"If not specified, and there\\*(Aqs exactly one B<--pcr-public-key=> "
"argument, that key will be used\\&. Otherwise, the section will not be "
"present\\&."
msgstr ""
"Gibt einen Pfad zu einem öffentlichen Schlüssel an, der im Abschnitt »\\&."
"pcrpkey« eingebettet werden soll\\&. Falls nicht angegeben und genau ein "
"Argument B<--pcr-public-key=> vorhanden ist, wird dieser Schlüssel "
"verwandt\\&. Andernfalls wird dieser Abschnitt nicht vorhanden sein\\&."

#. type: Plain text
#: archlinux
msgid "B<--uname=>I<VERSION>"
msgstr "B<--uname=>I<VERSION>"

#. type: Plain text
#: archlinux
msgid ""
"Specify the kernel version (as in B<uname -r>, the \"\\&.uname\" "
"section)\\&. If not specified, an attempt will be made to extract the "
"version string from the kernel image\\&. It is recommended to pass this "
"explicitly if known, because the extraction is based on heuristics and not "
"very reliable\\&. If not specified and extraction fails, the section will "
"not be present\\&."
msgstr ""
"Gibt die Kernelversion an (wie in B<uname -r>, dem Abschnitt »\\&."
"uname«)\\&. Falls nicht angegeben, wird versucht, die Versionszeichenkette "
"aus dem Kernelabbild auszulesen\\&. Es wird empfohlen, sie explizit zu "
"übergeben, wenn sie bekannt ist, da das Auslesen auf Heuristiken basiert und "
"nicht sehr zuverlässig ist\\&. Falls nicht angegeben und das Auslesen "
"fehlschlägt, wird der Abschnitt nicht vorhanden sein\\&."

#. type: Plain text
#: archlinux
msgid "B<--section=>I<NAME>B<:>I<TEXT>B<|>I<@PATH>"
msgstr "B<--section=>I<NAME>B<:>I<TEXT>B<|>I<@PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify an arbitrary additional section \"I<NAME>\"\\&. Note that the name "
"is used as-is, and if the section name should start with a dot, it must be "
"included in I<NAME>\\&. The argument may be a literal string, or \"@\" "
"followed by a path name\\&. This option may be specified more than once\\&. "
"Any sections specified in this fashion will be inserted (in order) before "
"the \"\\&.linux\" section which is always last\\&."
msgstr ""
"Gibt einen beliebigen, zusätzlichen Abschnitt »I<NAME>« an\\&. Beachten Sie, "
"dass der Name unverändert verwandt wird und den Punkt enthalten muss, falls "
"der Abschnittsname mit einem Punkt beginnen soll\\&. Das Argument kann eine "
"wörtliche Zeichenkette oder @, gefolgt von einem Pfadnamen, sein\\&. Diese "
"Option kann mehr als einmal angegeben werden\\&. Jeder auf diese Weise "
"angegebene Abschnitt wird (in der Reihenfolge) vor dem Abschnitt "
"»\\&.linux«, der immer der letzte ist, eingefügt\\&."

#. type: Plain text
#: archlinux
msgid "B<--pcr-private-key=>I<PATH>"
msgstr "B<--pcr-private-key=>I<PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify a private key to use for signing PCR policies\\&. This option may be "
"specified more than once, in which case multiple signatures will be made\\&."
msgstr ""
"Gibt einen privaten Schlüssel zum Signieren von PCR-Richtlinien an\\&. Diese "
"Option darf mehr als einmal angegeben werden\\&. Dann werden mehrere "
"Signaturen erstellt\\&."

#. type: Plain text
#: archlinux
msgid "B<--pcr-public-key=>I<PATH>"
msgstr "B<--pcr-public-key=>I<PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"Specify a public key to use for signing PCR policies\\&. This option may be "
"specified more than once, similarly to the B<--pcr-private-key=> option\\&. "
"If not present, the public keys will be extracted from the private keys\\&. "
"If present, the this option must be specified the same number of times as "
"the B<--pcr-private-key=> option\\&."
msgstr ""
"Gibt einen öffentlichen Schlüssel zum Signieren von PCR-Richtlinien an\\&. "
"Diese Option darf mehr als einmal angegeben werden, ähnlich wie die Option "
"B<--pcr-private-key=>\\&. Falls nicht vorhanden wird der öffentliche "
"Schlüssel aus dem privaten Schlüsseln abgeleitet\\&. Falls vorhanden, muss "
"diese Option genauso oft wie die Option B<--pcr-private-key=> angegeben "
"werden\\&."

#. type: Plain text
#: archlinux
msgid "B<--phases=>I<LIST>"
msgstr "B<--phases=>I<LISTE>"

#. type: Plain text
#: archlinux
msgid ""
"A comma or space-separated list of colon-separated phase paths to sign a "
"policy for\\&. If not present, the default of B<systemd-measure>(1)  will be "
"used\\&. When this argument is present, it must appear the same number of "
"times as the B<--pcr-private-key=> option\\&. Each set of boot phase paths "
"will be signed with the corresponding private key\\&."
msgstr ""
"Eine Kommata oder Leerraum-getrennte Liste von Doppelpunkt-getrennten "
"Phasenpfaden, für die eine Richtlinie signiert werden soll\\&. Falls nicht "
"vorhanden wird die Vorgabe von B<systemd-measure>(1) verwandt\\&. Wenn "
"dieses Argument vorhanden ist, muss es genauso oft wie die Option B<--pcr-"
"private-key=> auftauchen\\&. Jede Gruppe und Systemstartphasenpfade wird mit "
"dem entsprechenden privaten Schlüssel signiert\\&."

#. type: Plain text
#: archlinux
msgid "B<--pcr-banks=>I<PATH>"
msgstr "B<--pcr-banks=>I<PFAD>"

#. type: Plain text
#: archlinux
msgid ""
"A comma or space-separated list of PCR banks to sign a policy for\\&. If not "
"present, all known banks will be used (\"sha1\", \"sha256\", \"sha384\", "
"\"sha512\"), which will fail if not supported by the system\\&."
msgstr ""
"Eine Kommata- oder Leerraum-getrennte Liste von PCR-Bänken, für die eine "
"Richtlinie signiert werden soll\\&. Falls nicht vorhanden, werden alle "
"bekannten Bänke verwandt (»sha1«, »sha256«, »sha384«, »sha512«)\\&. Dies "
"wird fehlschlagen, wenn das vom System nicht unterstützt wird\\&."

#. type: Plain text
#: archlinux
msgid "B<--secureboot-private-key=>I<SB_KEY>"
msgstr "B<--secureboot-private-key=>I<SB_SCHLÜSSEL>"

#. type: Plain text
#: archlinux
msgid ""
"A path to a private key to use for signing of the resulting binary\\&. If "
"the B<--signing-engine=> option is used, this may also be an engine-specific "
"designation\\&."
msgstr ""
"Ein Pfad zu einem privaten Schlüssel, der zum Signieren des resultierenden "
"Programms verwandt wird\\&. Falls die Option B<--signing-engine=> verwandt "
"wird, kann dies auch eine Engine-spezifische Festsetzung sein\\&."

#. type: Plain text
#: archlinux
msgid "B<--secureboot-certificate=>I<SB_CERT>"
msgstr "B<--secureboot-certificate=>I<SB_ZERT>"

#. type: Plain text
#: archlinux
msgid ""
"A path to a certificate to use for signing of the resulting binary\\&. If "
"the B<--signing-engine=> option is used, this may also be an engine-specific "
"designation\\&."
msgstr ""
"Ein Pfad zu einem Zertifikat, das zum Signieren des resultierenden Programms "
"verwandt wird\\&. Falls die Option B<--signing-engine=> verwandt wird, kann "
"dies auch eine Engine-spezifische Festsetzung sein\\&."

#. type: Plain text
#: archlinux
msgid "B<--signing-engine=>I<ENGINE>"
msgstr "B<--signing-engine=>I<ENGINE>"

# FIXME to for → for
#. type: Plain text
#: archlinux
msgid ""
"An \"engine\" to for signing of the resulting binary\\&. This option is "
"currently passed verbatim to the B<--engine=> option of B<sbsign>(1)\\&."
msgstr ""
"Eine »Engine« zum Signieren des resultierenden Programms\\&. Diese Option "
"wird derzeit unverändert an die Option B<--engine=> von B<sbsign>(1) "
"weitergegeben\\&."

#. type: Plain text
#: archlinux
msgid "B<--sign-kernel>, B<--no-sign-kernel>"
msgstr "B<--sign-kernel>, B<--no-sign-kernel>"

#. type: Plain text
#: archlinux
msgid ""
"Override the detection of whether to sign the Linux binary itself before it "
"is embedded in the combined image\\&. If not specified, it will be signed if "
"a SecureBoot signing key is provided via the B<--secureboot-private-key=> "
"option and the binary has not already been signed\\&. If B<--sign-kernel> is "
"specified, and the binary has already been signed, the signature will be "
"appended anyway\\&."
msgstr ""
"Setzt die Erkennung, ob das Linux-Programm selbst vor dem Einbetten in das "
"kombinierte Abbild signiert werden soll, außer Kraft\\&. Falls nicht "
"angegeben, wird es signiert, falls ein SecureBoot-Signaturschlüssel über die "
"Option B<--secureboot-private-key=> bereitgestellt wird und das Programm "
"noch nicht signiert wurde\\&. Falls B<--sign-kernel> angegeben wurde und das "
"Programm bereits signiert wurde, wird die Signatur trotzdem angehängt\\&."

#. type: Plain text
#: archlinux
msgid "B<--tools=>I<DIRS>"
msgstr "B<--tools=>I<VERZ>"

#. type: Plain text
#: archlinux
msgid ""
"Specify one or more directories with helper tools\\&.  B<ukify> will look "
"for helper tools in those directories first, and if not found, try to load "
"them from I<$PATH> in the usual fashion\\&."
msgstr ""
"Gibt eines oder mehrere Verzeichnisse mit Hilfswerkzeugen an\\&. B<ukify> "
"wird zuerst in diesen Verzeichnissen nach Hilfswerkzeugen schauen und, falls "
"sie dort nicht gefunden werden, sie auf die übliche Weise aus I<$PATH> "
"laden\\&."

#. type: Plain text
#: archlinux
msgid "B<--measure>, B<--no-measure>"
msgstr "B<--measure>, B<--no-measure>"

# FIXME systmed-measure → systemd-measure and then B<systemd-measure> → B<systemd-measure>(1)
#. type: Plain text
#: archlinux
msgid ""
"Enable or disable a call to B<systmed-measure> to print pre-calculated PCR "
"values\\&. Defaults to false\\&."
msgstr ""
"Aktiviert oder deaktiviert einen Aufruf von B<systemd-measure>(1), um "
"vorberechnete PCR-Werte auszugeben\\&. Standardmäßig falsch\\&."

#. type: Plain text
#: archlinux
msgid "B<--output=>I<FILENAME>"
msgstr "B<--output=>I<DATEINAME>"

#. type: Plain text
#: archlinux
msgid ""
"The output filename\\&. If not specified, the name of the I<LINUX> argument, "
"with the suffix \"\\&.unsigned\\&.efi\" or \"\\&.signed\\&.efi\" will be "
"used, depending on whether signing for SecureBoot was performed\\&."
msgstr ""
"Der Ausgabedateiname\\&. Falls nicht angegeben, wird der Name des Arguments "
"I<LINUX> mit der Endung »\\&.unsigned\\&.efi« oder »\\&.signed\\&.efi« "
"verwandt, abhängig davon, ob die Signatur für SecureBoot durchgeführt "
"wurde\\&."

#. type: Plain text
#: archlinux
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux
msgid "Print a short help text and exit\\&."
msgstr "Zeigt einen kurzen Hilfetext an und beendet das Programm\\&."

#. type: Plain text
#: archlinux
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux
msgid "Print a short version string and exit\\&."
msgstr "Zeigt eine kurze Versionszeichenkette an und beendet das Programm\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux
msgid "B<Example\\ \\&1.\\ \\&Minimal invocation>"
msgstr "B<Beispiel\\ \\&1.\\ \\&Minimaler Aufruf>"

#. type: Plain text
#: archlinux
#, no-wrap
msgid ""
"ukify \\e\n"
"      /lib/modules/6\\&.0\\&.9-300\\&.fc37\\&.x86_64/vmlinuz \\e\n"
"      /some/path/initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img \\e\n"
"      --cmdline=\\*(Aqquiet rw\\*(Aq\n"
msgstr ""
"ukify \\e\n"
"      /lib/modules/6\\&.0\\&.9-300\\&.fc37\\&.x86_64/vmlinuz \\e\n"
"      /ein/Pfad/initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img \\e\n"
"      --cmdline=\\*(Aqquiet rw\\*(Aq\n"

#. type: Plain text
#: archlinux
msgid "This creates an unsigned UKI \\&./vmlinuz\\&.unsigned\\&.efi\\&."
msgstr "Dies erstellt ein unsigniertes UKI \\&./vmlinuz\\&.unsigned\\&.efi\\&."

#. type: Plain text
#: archlinux
msgid "B<Example\\ \\&2.\\ \\&All the bells and whistles>"
msgstr "B<Beispiel\\ \\&2.\\ \\&Mit allem Schnickschnack>"

#. type: Plain text
#: archlinux
#, no-wrap
msgid ""
"/usr/lib/systemd/ukify \\e\n"
"      /lib/modules/6\\&.0\\&.9-300\\&.fc37\\&.x86_64/vmlinuz \\e\n"
"      early_cpio \\e\n"
"      /some/path/initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img \\e\n"
"      --pcr-private-key=pcr-private-initrd-key\\&.pem \\e\n"
"      --pcr-public-key=pcr-public-initrd-key\\&.pem \\e\n"
"      --phases=\\*(Aqenter-initrd\\*(Aq \\e\n"
"      --pcr-private-key=pcr-private-system-key\\&.pem \\e\n"
"      --pcr-public-key=pcr-public-system-key\\&.pem \\e\n"
"      --phases=\\*(Aqenter-initrd:leave-initrd enter-initrd:leave-initrd:sysinit \\e\n"
"                enter-initrd:leave-initrd:sysinit:ready\\*(Aq \\e\n"
"      --pcr-banks=sha384,sha512 \\e\n"
"      --secureboot-private-key=sb\\&.key \\e\n"
"      --secureboot-certificate=sb\\&.cert \\e\n"
"      --sign-kernel \\e\n"
"      --cmdline=\\*(Aqquiet rw rhgb\\*(Aq\n"
msgstr ""
"/usr/lib/systemd/ukify \\e\n"
"      /lib/modules/6\\&.0\\&.9-300\\&.fc37\\&.x86_64/vmlinuz \\e\n"
"      early_cpio \\e\n"
"      /ein/Pfad/initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img \\e\n"
"      --pcr-private-key=pcr-private-initrd-key\\&.pem \\e\n"
"      --pcr-public-key=pcr-public-initrd-key\\&.pem \\e\n"
"      --phases=\\*(Aqenter-initrd\\*(Aq \\e\n"
"      --pcr-private-key=pcr-private-system-key\\&.pem \\e\n"
"      --pcr-public-key=pcr-public-system-key\\&.pem \\e\n"
"      --phases=\\*(Aqenter-initrd:leave-initrd enter-initrd:leave-initrd:sysinit \\e\n"
"                enter-initrd:leave-initrd:sysinit:ready\\*(Aq \\e\n"
"      --pcr-banks=sha384,sha512 \\e\n"
"      --secureboot-private-key=sb\\&.key \\e\n"
"      --secureboot-certificate=sb\\&.cert \\e\n"
"      --sign-kernel \\e\n"
"      --cmdline=\\*(Aqquiet rw rhgb\\*(Aq\n"

#. type: Plain text
#: archlinux
msgid ""
"This creates a signed UKI \\&./vmlinuz\\&.signed\\&.efi\\&. The initrd "
"section contains two concatenated parts, early_cpio and "
"initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img\\&. The policy embedded "
"in the \"\\&.pcrsig\" section will be signed for the initrd (the B<enter-"
"initrd> phase) with the key pcr-private-initrd-key\\&.pem, and for the main "
"system (phases B<leave-initrd>, B<sysinit>, B<ready>) with the key pcr-"
"private-system-key\\&.pem\\&. The Linux binary and the resulting combined "
"image will be signed with the SecureBoot key sb\\&.key\\&."
msgstr ""
"Dies erstellt ein signiertes UKI \\&./vmlinuz\\&.signed\\&.efi\\&. Der "
"Initrd-Abschnitt enthält zwei aneinandergehängte Teile, early_cpio und "
"initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img\\&. Die in dem Abschnitt "
"»\\&.pcrsig« eingebettete Richtlinie wird für die Initrd mit dem Schlüssel "
"pcr-private-initrd-key\\&.pem signiert (die Phase B<enter-initrd>) und für "
"das Hauptsystem (Phase B<leave-initrd>, B<sysinit>, B<ready>) mit dem "
"Schlüssel pcr-private-system-key\\&.pem\\&. Das Linux-Programm und das "
"resultierende kombinierte Abbild wird mit dem SecureBoot-Schlüssel sb\\&.key "
"signiert\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-boot>(7), B<objcopy>(1), "
"B<systemd-pcrphase.service>(1)"
msgstr ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-boot>(7), B<objcopy>(1), "
"B<systemd-pcrphase.service>(1)"

#. type: SH
#: archlinux
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux
msgid "Unified Kernel Image (UKI)"
msgstr "Vereinigtes Kernelabbild (UKI)"

#. type: Plain text
#: archlinux
msgid "\\%https://uapi-group.org/specifications/specs/unified_kernel_image/"
msgstr "\\%https://uapi-group.org/specifications/specs/unified_kernel_image/"
