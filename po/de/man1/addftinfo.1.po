# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-02-15 18:41+0100\n"
"PO-Revision-Date: 2021-03-20 14:45+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ADDFTINFO"
msgstr "ADDFTINFO"

#. type: TH
#: archlinux
#, no-wrap
msgid "29 March 2022"
msgstr "29. März 2022"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "groff 1.22.4"
msgstr "Groff 1.22.4"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "addftinfo - add information to troff font files for use with groff"
msgstr ""
"addftinfo - Informationen, die mit Groff verwendet werden sollen, zu Troff-"
"Schriftdateien hinzufügen"

#.  ====================================================================
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: SY
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "addftinfo"
msgstr "addftinfo"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-asc-height"
msgstr "-asc-height"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "n"
msgstr "n"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-body-depth"
msgstr "-body-depth"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-body-height"
msgstr "-body-height"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-cap-height"
msgstr "-cap-height"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-comma-depth"
msgstr "-comma-depth"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-desc-depth"
msgstr "-desc-depth"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-fig-height"
msgstr "-fig-height"

#. type: OP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-x-height"
msgstr "-x-height"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<res> I<unitwidth> I<font>"
msgstr "I<Auflösung> I<Einheitsbreite> I<Schrift>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--version>"
msgstr "B<--version>"

#.  ====================================================================
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<addftinfo> reads a troff font file and adds some additional font-metric "
"information that is used by the groff system."
msgstr ""
"B<addftinfo> liest eine Troff-Schriftdatei und fügt einige Informationen zur "
"Schriftmetrik hinzu, die vom Groff-System verwendet werden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The font file with the information added is written on the standard output."
msgstr ""
"Die Schriftdatei, zu der die Informationen hinzugefügt wurden, wird in die "
"Standardausgabe geschrieben."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The information added is guessed using some parametric information about the "
"font and assumptions about the traditional troff names for characters."
msgstr ""
"Die hinzugefügten Informationen werden anhand einiger parametrischer "
"Informationen zur Schrift und Annahmen zu traditionellen Troff-Zeichennamen "
"geschätzt."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The main information added is the heights and depths of characters."
msgstr ""
"Hauptsächlich werden Informationen zu Ober- und Unterlängen von Zeichen "
"hinzugefügt."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<res> and I<unitwidth> arguments should be the same as the "
"corresponding parameters in the DESC file; I<font> is the name of the file "
"describing the font; if I<font> ends with B<I> the font will be assumed to "
"be italic."
msgstr ""
"Die Argumente I<Auflösung> und I<Einheitsbreite> sollten dieselben wie die "
"korrespondierenden Parameter in der DESC-Datei sein; I<Schrift> ist der Name "
"der Datei, welche die Schrift beschreibt; falls I<Schrift> mit B<I> endet, "
"wird davon ausgegangen, dass die Schrift kursiv ist."

#.  ====================================================================
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-v> prints the version number."
msgstr "B<-v> gibt die Versionsnummer aus."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All other options change one of the parameters that are used to derive the "
"heights and depths."
msgstr ""
"Alle weiteren Optionen ändern einen der Parameter, die zur Ableitung der "
"Höhen und Tiefen verwendet werden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Like the existing quantities in the font file, each I<value> is in inches/"
"I<res> for a font whose point size is I<unitwidth>."
msgstr ""
"Wie die vorhandenen Größen in der Schriftdatei ist jeder Wert in Zoll/"
"I<Auflösung> für eine Schrift, deren Punktgröße I<Einheitsbreite> ist."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<param-option> must be one of:"
msgstr "I<Parameter-Option> muss eine der folgenden sein:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-x-height>"
msgstr "B<-x-height>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The height of lowercase letters without ascenders such as x."
msgstr "Die Höhe von Kleinbuchstaben ohne Oberlängen, wie beispielsweise x."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-fig-height>"
msgstr "B<-fig-height>"

# CHECK digits → Stellen, aber wozu?
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The height of figures (digits)."
msgstr "Die Höhe von Mediäval- und Versalziffern."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-asc-height>"
msgstr "B<-asc-height>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The height of characters with ascenders, such as b, d or l."
msgstr "Die Höhe von Zeichen mit Oberlängen, wie beispielsweise b, d oder l."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-body-height>"
msgstr "B<-body-height>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The height of characters such as parentheses."
msgstr "Die Höhe von Zeichen wie Klammern."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-cap-height>"
msgstr "B<-cap-height>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The height of uppercase letters such as A."
msgstr "Die Höhe von Großbuchstaben, wie beispielsweise A."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-comma-depth>"
msgstr "B<-comma-depth>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The depth of a comma."
msgstr "Die Tiefe des Kommas."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-desc-depth>"
msgstr "B<-desc-depth>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The depth of characters with descenders, such as p, q, or y."
msgstr "Die Höhe von Zeichen mit Unterlängen, wie beispielsweise p, q oder y."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-body-depth>"
msgstr "B<-body-depth>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The depth of characters such as parentheses."
msgstr "Die Tiefe von Zeichen wie Klammern."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<addftinfo> makes no attempt to use the specified parameters to guess the "
"unspecified parameters."
msgstr ""
"B<addftinfo> versucht nicht, anhand der angegebenen Parameter die nicht "
"angegebenen Parameter zu erraten."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If a parameter is not specified the default will be used."
msgstr "Falls ein Parameter nicht gesetzt ist, wird die Vorgabe verwendet."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The defaults are chosen to have the reasonable values for a Times font."
msgstr ""
"Die Voreinstellungen sind so gewählt, dass sie annehmbare Werte für eine "
"Times-Schrift ergeben."

#.  ====================================================================
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<groff_font>(5), B<groff>(1), B<groff_char>(7)"
msgstr "B<groff_font>(5), B<groff>(1), B<groff_char>(7)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "You can view a man page I<name>B<(>I<n>B<)> with"
msgstr "Sie können eine Handbuchseite I<name>B<(>I<n>B<)> mit"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<man>I< n name>\n"
msgstr "B<man>I< n Name>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "in text mode and with"
msgstr "im Textmodus und mit"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<groffer>I< n name>\n"
msgstr "B<groffer>I< n Name>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "in graphical mode (PDF, etc.)."
msgstr "im grafischen Modus (PDF usw.) betrachten."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "27 January 2021"
msgstr "27. Januar 2021"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "15 November 2022"
msgstr "15. November 2022"

#. type: TH
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "19 January 2023"
msgstr "19. Januar 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "18 November 2018"
msgstr "18. November 2018"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "7 February 2022"
msgstr "7. Februar 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "4 January 2023"
msgstr "4. Januar 2023"
