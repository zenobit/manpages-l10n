# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
# Helge Kreutzmann <debian@helgefjell.de>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.10.0\n"
"POT-Creation-Date: 2023-02-15 19:12+0100\n"
"PO-Revision-Date: 2021-10-14 21:59+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: IX
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "sane-usb"
msgstr "sane-usb"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "14 Jul 2008"
msgstr "14. Juli 2008"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sane-usb - USB configuration tips for SANE"
msgstr "sane-usb - USB-Konfigurationstipps für SANE"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manual page contains information on how to access scanners with a USB "
"interface. It focuses on two main topics: getting the scanner detected by "
"the operating system kernel and using it with SANE."
msgstr ""
"Diese Handbuchseite enthält Informationen darüber, wie auf Scanner mit einer "
"USB-Schnittstelle zugegriffen werden kann. Sie konzentriert sich auf zwei "
"Hauptthemen: Erkennen des Scanners durch den Betriebssystemkernel und dessen "
"Verwendung mit SANE."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This page applies to USB most backends and scanners, as they use the generic "
"sanei_usb interface. However, there is one exception: USB Scanners supported "
"by the B<sane-microtek2>(5)  backend need a special USB kernel driver."
msgstr ""
"Diese Seite ist für die meisten USB-Backends und -Scanner anwendbar, da sie "
"die generische sanei_usb-Schnittstelle benutzen. Es gibt allerdings eine "
"Ausnahme: USB-Scanner, die durch das Backend B<sane-microtek2>(5) "
"unterstützt werden, benötigen einen besonderen USB-Kerneltreiber."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "QUICK START"
msgstr "SCHNELLSTART"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is a short HOWTO-like section. For the full details, read the following "
"sections. The goal of this section is to get the scanner detected by B<sane-"
"find-scanner>(1)."
msgstr ""
"Dies ist ein kurzer, HOWTO-artiger Abschnitt. Für die vollständigen Details "
"lesen Sie die folgenden Abschnitte. Das Ziel dieses Abschnitts ist es, dafür "
"zu sorgen, dass B<sane-find-scanner>(1) den Scanner erkennt."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Run B<sane-find-scanner>(1).  If it lists your scanner with the correct "
"vendor and product ids, you are done. See section B<SANE ISSUES> for details "
"on how to go on."
msgstr ""
"Führen Sie B<sane-find-scanner>(1) aus. Falls es Ihren Scanner mit den "
"korrekten Lieferanten- und Produktkennungen auflistet, ist alles erledigt. "
"Siehe den Abschnitt B<SANE-PROBLEME> für Details dazu, wie Sie weiter "
"verfahren."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<sane-find-scanner>(1)  doesn't list your scanner? Does it work as root? If "
"yes, there is a permission issue.  See the B<LIBUSB> section for details."
msgstr ""
"B<sane-find-scanner>(1) listet Ihren Scanner nicht auf? Funktioniert es als "
"Root? Falls ja, gibt es ein Berechtigungsproblem. Siehe den Abschnitt "
"B<LIBUSB> für Details."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Nothing is found even as root? Check that your kernel supports USB and that "
"libusb is installed (see section B<LIBUSB>)."
msgstr ""
"Auch als Root wird nichts gefunden? Prüfen Sie, ob Ihr Kernel USB "
"unterstützt und ob Libusb installiert ist (siehe Abschnitt B<LIBUSB>)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "USB ACCESS METHODS"
msgstr "USB-ZUGRIFFSMETHODEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For accessing USB devices, the USB library libusb is used. There used to "
"exist another method to access USB devices: the kernel scanner driver. The "
"kernel scanner driver method is deprecated and shouldn't be used anymore. It "
"may be removed from SANE at any time. In Linux, the kernel scanner driver "
"has been removed in the 2.6.* kernel series. Only libusb access is "
"documented in this manual page."
msgstr ""
"Für den Zugriff auf USB-Geräte wird die USB-Bibliothek Libusb verwendet. Es "
"gab mal eine andere Methode zum Zugriff auf USB-Geräte: den Kernel-Scanner-"
"Treiber. Die Kernel-Scanner-Treiber-Methode ist veraltet und sollte nicht "
"mehr verwendet werden. Sie kann jederzeit von SANE entfernt werden. Unter "
"Linux wurde der Kernel-Scanner-Treiber in der 2.6.*-Kernel-Serie entfernt. "
"Diese Handbuchseite dokumentiert nur den Zugriff mit Libusb."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "LIBUSB"
msgstr "LIBUSB"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"SANE can only use libusb 0.1.6 or newer. It needs to be installed at build-"
"time. Modern Linux distributions and other operating systems come with "
"libusb."
msgstr ""
"SANE kann nur Libusb 0.1.6 oder neuer verwenden. Sie muss zur Bauzeit "
"installiert sein. Moderne Linux-Distributionen und andere Betriebssysteme "
"kommen mit Libusb."

# FIXME Is the part about usbdevfs still of any relevance? Is anybdoy still using such old kernels?
# FIXME /etc/fstab → I</etc/fstab>
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Libusb can only access your scanner if it's not claimed by the kernel "
"scanner driver. If you want to use libusb, unload the kernel driver (e.g. "
"rmmod scanner under Linux) or disable the driver when compiling a new "
"kernel. For Linux, your kernel needs support for the USB filesystem (usbfs). "
"For kernels older than 2.4.19, replace \"usbfs\" with \"usbdevfs\" because "
"the name has changed. This filesystem must be mounted. That's done "
"automatically at boot time, if /etc/fstab contains a line like this:"
msgstr ""
"Libusb kann nur auf Ihren Scanner zugreifen, falls er nicht vom Kernel-"
"Scanner-Treiber belegt ist. Falls Sie Libusb verwenden möchten, entladen Sie "
"den Kerneltreiber (z.B. »rmmod scanner« unter Linux) oder deaktivieren Sie "
"den Treiber beim Kompilieren eines neuen Kernels. Für Linux muss Ihr Kernel "
"das USB-Dateisystem (usbfs) unterstützen. Bei Kerneln älter als 2.4.19 "
"ersetzen Sie »usbfs« durch »usbdevfs«, da sich der Name geändert hat. Das "
"Dateisystem muss eingehängt sein. Dies erfolgt beim Systemstart automatisch, "
"falls I</etc/fstab> eine Zeile der folgenden Form enthält:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "none /proc/bus/usb usbfs defaults 0 0"
msgstr "none /proc/bus/usb usbfs defaults 0 0"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The permissions for the device files used by libusb must be adjusted for "
"user access. Otherwise only root can use SANE devices. For I<Linux>, the "
"devices are located in I</proc/bus/usb/> or in I</dev/bus/usb>, if you use "
"udev. There are directories named e.g. \"001\" (the bus name) containing "
"files \"001\", \"002\" etc. (the device files). The right device files can "
"be found out by running: I<scanimage -L:> as root. Setting permissions with "
"B<chmod>(1)  is not permanent, however. They will be reset after reboot or "
"replugging the scanner."
msgstr ""
"Die Berechtigungen für die von Libusb verwandten Geräte müssen für den "
"Benutzerzugriff angepasst werden. Andernfalls kann nur Root auf SANE-Geräte "
"zugreifen. Für I<Linux> befinden sich die Geräte in I</proc/bus/usb/> oder "
"in I</dev/bus/usb>, falls Sie Udev verwenden. Dies sind Verzeichnisse, die z."
"B. »001« (der Name des Busses) heißen und Dateien »001«, »002« usw. (die "
"Gerätedateien) enthalten. Die richtige Gerätedatei kann durch Ausführen von "
"I<scanimage -L:> als root herausgefunden werden. Das Setzen von "
"Berechtigungen mittels B<chmod>(1) ist allerdings nicht dauerhaft. Diese "
"werden nach einem Neustart oder nach dem erneuten Anstecken des Scanners "
"zurückgesetzt."

# FIXME It is unclear where to find the relative path I<tools/*>
# FIXME Is tools/udev still applicable? A user states it no longer exists
#. type: Plain text
#: archlinux
msgid ""
"Usually B<udev>(7)  or for older distributions the hotplug utilities are "
"used, which support dynamic setting of access permissions. SANE comes with "
"udev and hotplug scripts in the directory I<tools/udev> and I<tools/"
"hotplug>.  They can be used for setting permissions, see I</usr/share/doc/"
"sane/README.linux>, I<tools/README> and the I<README> in the I<tools/"
"hotplug> directory for more details."
msgstr ""
"Normalerweise wird B<udev>(7) oder bei älteren Distributionen das Hotplug-"
"Hilfswerkzeug verwendet, um dynamisches Setzen der Zugriffsberechtigungen zu "
"unterstützen. SANE enthält im Verzeichnis I<tools/udev> und I<tools/hotplug> "
"Udev- und Hotplug-Skripte. Diese können zum Setzen von Berechtigungen "
"verwendet werden, siehe I</usr/share/doc/sane/README.linux>, I<tools/README> "
"und die I<README> im Verzeichnis I<tools/hotplug> für weitere Details."

# FIXME chmod → B<chmod>(1)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For the B<BSDs>, the device files used by libusb are named I</dev/ugen*>.  "
"Use chmod to apply appropriate permissions."
msgstr ""
"Für die B<BSD>s heißen die von Libusb verwendeten Gerätedateien I</dev/"
"ugen*>. Verwenden Sie chmod, um die geeigneten Berechtigungen anzuwenden."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SANE ISSUES"
msgstr "SANE-PROBLEME"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This section assumes that your scanner is detected by B<sane-find-"
"scanner>(1).  It doesn't make sense to go on, if this is not the case. While "
"B<sane-find-scanner>(1)  is able to detect any USB scanner, actual scanning "
"will only work if the scanner is supported by a SANE backend. Information on "
"the level of support can be found on the SANE webpage (I<http://www.sane-"
"project.org/>), and the individual backend manpages."
msgstr ""
"Dieser Abschnitt nimmt an, dass Ihr Scanner durch B<sane-find-scanner>(1) "
"erkannt wurde. Es ist sinnlos, fortzufahren, falls dies nicht der Fall ist. "
"Während B<sane-find-scanner>(1) in der Lage ist, einen USB-Scanner zu "
"erkennen, wird das eigentliche Scannen nur funktionieren, falls Ihr Scanner "
"von einem SANE-Backend unterstützt wird. Information über den Grad der "
"Unterstützung können Sie auf der SANE-Webseite (I<http://www.sane-project."
"org/>) und in den einzelnen Backend-Handbuchseiten finden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Most backends can detect USB scanners automatically using \"usb\" "
"configuration file lines. This method allows one to identify scanners by the "
"USB vendor and product numbers.  The syntax for specifying a scanner this "
"way is:"
msgstr ""
"Die Backends können USB-Scanner automatisch mittels Zeilen der Form »usb« in "
"Konfigurationsdateien erkennen. Diese Methode erlaubt es Ihnen, Scanner über "
"den USB-Lieferanten und Produktnummern zu identifizieren. Die Syntax für die "
"Festlegung von Scannern auf diese Art lautet:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "usb I<VENDOR PRODUCT>"
msgstr "usb I<LIEFERANT PRODUKT>"

# FIXME: On my system (5.10 kernel) /proc/bus/usb does not exist
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"where I<VENDOR> is the USB vendor id, and I<PRODUCT> is the USB product id "
"of the scanner. Both ids are non-negative integer numbers in decimal or "
"hexadecimal format. The correct values for these fields can be found by "
"running B<sane-find-scanner>(1), looking into the syslog (e.g., I</var/log/"
"messages>)  or under Linux by issuing the command I<cat /proc/bus/usb/"
"devices>.  This is an example of a config file line:"
msgstr ""
"Hierbei ist I<LIEFERANT> die USB-Lieferantenkennung und I<PRODUKT> die USB-"
"Produktkennung des Scanners. Beide Kennungen sind nichtnegative dezimale "
"oder hexadezimale Ganzzahlen. Die korrekten Werte für diese Felder können "
"durch die Ausführung von B<sane-find-scanner>(1), dem Nachschauen im Syslog "
"(z.B. I</var/log/messages> oder unter Linux durch Ausführen des Befehls "
"I<cat /proc/bus/usb/devices> ermittelt werden. Eine Konfigurationszeile "
"könnte beispielsweise folgendermaßen aussehen:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "usb 0x055f 0x0006"
msgstr "usb 0x055f 0x0006"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"would have the effect that all USB devices in the system with a vendor id of "
"0x55f and a product id of 0x0006 would be probed and recognized by the "
"backend."
msgstr ""
"Dies hätte den Effekt, dass alle USB-Geräte im System mit einer "
"Lieferantenkennung 0x55f und einer Produktkennung 0x0006 untersucht und vom "
"Backend erkannt würden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If your scanner is not detected automatically, it may be necessary to edit "
"the appropriate backend configuration file before using SANE for the first "
"time.  For a detailed description of each backend's configuration file, "
"please refer to the relevant backend manual page (e.g.  B<sane-"
"mustek_usb>(5)  for Mustek USB scanners)."
msgstr ""
"Falls Ihr Scanner nicht automatisch erkannt wird, könnte es notwendig sein, "
"dass Sie die geeignete Backend-Konfigurationsdatei bearbeiten, bevor Sie "
"SANE erstmalig verwenden. Für eine detaillierte Beschreibung der "
"Konfigurationsdatei jedes Backends lesen Sie bitte die relevante "
"Handbuchseite des Backends (z.B. B<sane-mustek_usb>(5) für Mustek USB-"
"Scanner)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Do B<not> create a symlink from I</dev/scanner> to the USB device because "
"this link is used by the SCSI backends. The scanner may be confused if it "
"receives SCSI commands."
msgstr ""
"Legen Sie B<keinen> symbolischen Link von I</dev/scanner> auf das USB-Gerät "
"an, da dieser Link von den SCSI-Backends verwendet wird. Der Scanner könnte "
"verwirrt werden, wenn er SCSI-Befehle empfängt."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_SANEI_USB>"
msgstr "B<SANE_DEBUG_SANEI_USB>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for the USB I/O subsystem.  E.g., a value "
"of 128 requests all debug output to be printed.  Smaller levels reduce "
"verbosity. Values greater than 4 enable libusb debugging (if available). "
"Example: I<export SANE_DEBUG_SANEI_USB=4>."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für das USB-E/A-Subsystem. Beispielsweise "
"bewirkt ein Wert von 128 die Anzeige sämtlicher Debug-Ausgaben. Kleinere "
"Werte reduzieren die Ausführlichkeit. Werte größer als 4 ermöglichen das "
"Libusb-Debugging (falls verfügbar). Beispiel: I<export "
"SANE_DEBUG_SANEI_USB=4>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_USB_WORKAROUND>"
msgstr "B<SANE_USB_WORKAROUND>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If your scanner does not work when plugged into a USB3 port, try setting the "
"environment variable B<SANE_USB_WORKAROUND> to 1. This may work around "
"issues which happen with particular kernel versions. Example: I<export "
"SANE_USB_WORKAROUND=1.>"
msgstr ""
"Falls Ihr Scanner nicht funktioniert, wenn er in einen USB3-Port eingesteckt "
"wird, versuchen Sie, die Umgebungsvariable B<SANE_USB_WORKAROUND> auf 1 zu "
"setzen. Dies kann Probleme umgehen, die mit bestimmten Kernelversionen "
"auftreten. Beispiel: I<export SANE_USB_WORKAROUND=1>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<sane>(7), B<sane-find-scanner>(1), B<sane-\"backendname\">(5), B<sane-"
"scsi>(5)"
msgstr ""
"B<sane>(7), B<sane-find-scanner>(1), B<sane-\"backendname\">(5), B<sane-"
"scsi>(5)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Henning Meier-Geinitz E<lt>I<henning@meier-geinitz.de>E<gt>"
msgstr "Henning Meier-Geinitz E<lt>I<henning@meier-geinitz.de>E<gt>"

#. type: Plain text
#: debian-bullseye
msgid ""
"This manual page contains information on how to access scanners with a USB "
"interface. It focusses on two main topics: getting the scanner detected by "
"the operating system kernel and using it with SANE."
msgstr ""
"Diese Handbuchseite enthält Informationen darüber, wie auf Scanner mit einer "
"USB-Schnittstelle zugegriffen werden kann. Sie konzentriert sich auf zwei "
"Hauptthemen: Erkennen des Scanners durch den Betriebssystemkernel und dessen "
"Verwendung mit SANE."

#. type: Plain text
#: debian-bullseye
msgid ""
"This page applies to USB most backends and scanners, as they use the generic "
"sanei_usb interface. However, there is one exceptions: USB Scanners "
"supported by the microtek2 backend need a special USB kernel driver, see "
"B<sane-microtek2>(5)  for details."
msgstr ""
"Diese Seite ist für die meisten USB-Backends und -Scanner anwendbar, da sie "
"die generische sanei_usb-Schnittstelle benutzen. Es gibt allerdings eine "
"Ausnahme: USB-Scanner, die durch das Backend Microtek2 unterstützt werden, "
"benötigen einen besonderen USB-Kerneltreiber, siehe B<sane-microtek2>(5) für "
"Details."

# FIXME sane-find-scanner → B<sane-find-scanner>(1)
#. type: Plain text
#: debian-bullseye
msgid ""
"Run sane-find-scanner. If it lists your scanner with the correct vendor and "
"product ids, you are done. See section B<SANE ISSUES> for details on how to "
"go on."
msgstr ""
"Führen Sie B<sane-find-scanner>(1) aus. Falls es Ihren Scanner mit den "
"korrekten Lieferanten- und Produktkennungen auflistet, ist alles erledigt. "
"Siehe den Abschnitt B<SANE-PROBLEME> für Details dazu, wie Sie weiter "
"verfahren."

#. type: Plain text
#: debian-bullseye
msgid ""
"sane-find-scanner doesn't list your scanner? Does it work as root? If yes, "
"there is a permission issue. See the B<LIBUSB> section for details."
msgstr ""
"B<sane-find-scanner>(1) listet Ihren Scanner nicht auf? Funktioniert es als "
"Root? Falls ja, gibt es ein Berechtigungsproblem. Siehe den Abschnitt "
"B<LIBUSB> für Details."

#. type: Plain text
#: debian-bullseye
msgid ""
"The permissions for the device files used by libusb must be adjusted for "
"user access. Otherwise only root can use SANE devices. For I<Linux>, the "
"devices are located in /proc/bus/usb/ or in /dev/bus/usb, if you use udev. "
"There are directories named e.g. \"001\" (the bus name) containing files "
"\"001\", \"002\" etc. (the device files). The right device files can be "
"found out by running scanimage -L as root. Setting permissions with "
"\"chmod\" is not permanent, however. They will be reset after reboot or "
"replugging the scanner."
msgstr ""
"Die Berechtigungen für die von Libusb verwandten Geräte müssen für den "
"Benutzerzugriff angepasst werden. Andernfalls kann nur Root auf SANE-Geräte "
"zugreifen. Für I<Linux> befinden sich die Geräte in I</proc/bus/usb/> oder "
"in I</dev/bus/usb>, falls Sie Udev verwenden. Dies sind Verzeichnisse, die z."
"B. »001« (der Name des Busses) heißen und Dateien »001«, »002« usw. (die "
"Gerätedateien) enthalten. Die richtige Gerätedatei kann durch Ausführen von "
"I<scanimage -L:> als root herausgefunden werden. Das Setzen von "
"Berechtigungen mittels B<chmod>(1) ist allerdings nicht dauerhaft. Diese "
"werden nach einem Neustart oder nach dem erneuten Anstecken des Scanners "
"zurückgesetzt."

#. type: Plain text
#: debian-bullseye
msgid ""
"Usually udev or for older distributions the hotplug utilities are used, "
"which support dynamic setting of access permissions. SANE comes with udev "
"and hotplug scripts in the directory tools/udev and tools/hotplug. They can "
"be used for setting permissions, see /usr/share/doc/libsane/README.linux, "
"tools/README and the README in the tools/hotplug directory for more details."
msgstr ""
"Normalerweise wird B<udev>(7) oder bei älteren Distributionen das Hotplug-"
"Hilfswerkzeug verwendet, um dynamisches Setzen der Zugriffsberechtigungen zu "
"unterstützen. SANE enthält im Verzeichnis I<tools/udev> und I<tools/hotplug> "
"Udev- und Hotplug-Skripte. Diese können zum Setzen von Berechtigungen "
"verwendet werden, siehe I</usr/share/doc/sane/README.linux>, I<tools/README> "
"und die I<README> im Verzeichnis I<tools/hotplug> für weitere Details."

#. type: Plain text
#: debian-bullseye
msgid ""
"This section assumes that your scanner is detected by sane-find-scanner. It "
"doesn't make sense to go on, if this is not the case. While sane-find-"
"scanner is able to detect any USB scanner, actual scanning will only work if "
"the scanner is supported by a SANE backend. Information on the level of "
"support can be found on the SANE webpage (I<http://www.sane-project.org/>), "
"and the individual backend manpages."
msgstr ""
"Dieser Abschnitt nimmt an, dass Ihr Scanner durch B<sane-find-scanner>(1) "
"erkannt wurde. Es ist sinnlos, fortzufahren, falls dies nicht der Fall ist. "
"Während B<sane-find-scanner>(1) in der Lage ist, einen USB-Scanner zu "
"erkennen, wird das eigentliche Scannen nur funktionieren, falls Ihr Scanner "
"von einem SANE-Backend unterstützt wird. Information über den Grad der "
"Unterstützung können Sie auf der SANE-Webseite (I<http://www.sane-project."
"org/>) und in den einzelnen Backend-Handbuchseiten finden."

# FIXME: On my system (5.10 kernel) /proc/bus/usb does not exist
#. type: Plain text
#: debian-bullseye
msgid ""
"where I<VENDOR> is the USB vendor id, and I<PRODUCT> is the USB product id "
"of the scanner. Both ids are non-negative integer numbers in decimal or "
"hexadecimal format. The correct values for these fields can be found by "
"running sane-find-scanner, looking into the syslog (e.g., /var/log/messages) "
"or under Linux by issuing the command \"cat /proc/bus/usb/devices\".  This "
"is an example of a config file line:"
msgstr ""
"Hierbei ist I<LIEFERANT> die USB-Lieferantenkennung und I<PRODUKT> die USB-"
"Produktkennung des Scanners. Beide Kennungen sind nichtnegative dezimale "
"oder hexadezimale Ganzzahlen. Die korrekten Werte für diese Felder können "
"durch die Ausführung von B<sane-find-scanner>(1), dem Nachschauen im Syslog "
"(z.B. I</var/log/messages> oder unter Linux durch Ausführen des Befehls "
"I<cat /proc/bus/usb/devices> ermittelt werden. Eine Konfigurationszeile "
"könnte beispielsweise folgendermaßen aussehen:"

#. type: Plain text
#: debian-bullseye
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for the USB I/O subsystem.  E.g., a value "
"of 128 requests all debug output to be printed.  Smaller levels reduce "
"verbosity. Values greater than 4 enable libusb debugging (if available). "
"Example: export SANE_DEBUG_SANEI_USB=4."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für das USB-E/A-Subsystem. Beispielsweise "
"bewirkt ein Wert von 128 die Anzeige sämtlicher Debug-Ausgaben. Kleinere "
"Werte reduzieren die Ausführlichkeit. Werte größer als 4 ermöglichen das "
"Libusb-Debugging (falls verfügbar). Beispiel: I<export "
"SANE_DEBUG_SANEI_USB=4>."

# FIXME SANE_USB_WORKAROUND → B<SANE_USB_WORKAROUND>
#. type: Plain text
#: debian-bullseye
msgid ""
"If your scanner does not work when plugged into a USB3 port, try setting the "
"environment variable SANE_USB_WORKAROUND to 1. This may work around issues "
"which happen with particular kernel versions. Example: export "
"SANE_USB_WORKAROUND=1."
msgstr ""
"Falls Ihr Scanner nicht funktioniert, wenn er in einen USB3-Port eingesteckt "
"wird, versuchen Sie, die Umgebungsvariable B<SANE_USB_WORKAROUND> auf 1 zu "
"setzen. Dies kann Probleme umgehen, die bei bestimmten Kernelversionen "
"auftreten. Beispiel: export SANE_USB_WORKAROUND=1."

#. type: Plain text
#: debian-bullseye
msgid "Henning Meier-Geinitz E<lt>henning@meier-geinitz.deE<gt>"
msgstr "Henning Meier-Geinitz E<lt>henning@meier-geinitz.deE<gt>"

#. type: Plain text
#: debian-unstable
msgid ""
"Usually B<udev>(7)  or for older distributions the hotplug utilities are "
"used, which support dynamic setting of access permissions. SANE comes with "
"udev and hotplug scripts in the directory I<tools/udev> and I<tools/"
"hotplug>.  They can be used for setting permissions, see I</usr/share/doc/"
"libsane/README.linux>, I<tools/README> and the I<README> in the I<tools/"
"hotplug> directory for more details."
msgstr ""
"Normalerweise wird B<udev>(7) oder bei älteren Distributionen das Hotplug-"
"Hilfswerkzeug verwendet, um dynamisches Setzen der Zugriffsberechtigungen zu "
"unterstützen. SANE enthält im Verzeichnis I<tools/udev> und I<tools/hotplug> "
"Udev- und Hotplug-Skripte. Diese können zum Setzen von Berechtigungen "
"verwendet werden, siehe I</usr/share/doc/libsane/README.linux>, I<tools/"
"README> und die I<README> im Verzeichnis I<tools/hotplug> für weitere "
"Details."

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Usually B<udev>(7)  or for older distributions the hotplug utilities are "
"used, which support dynamic setting of access permissions. SANE comes with "
"udev and hotplug scripts in the directory I<tools/udev> and I<tools/"
"hotplug>.  They can be used for setting permissions, see I</usr/share/doc/"
"sane-backends/README.linux>, I<tools/README> and the I<README> in the "
"I<tools/hotplug> directory for more details."
msgstr ""
"Normalerweise wird B<udev>(7) oder bei älteren Distributionen das Hotplug-"
"Hilfswerkzeug verwendet, um dynamisches Setzen der Zugriffsberechtigungen zu "
"unterstützen. SANE enthält im Verzeichnis I<tools/udev> und I<tools/hotplug> "
"Udev- und Hotplug-Skripte. Diese können zum Setzen von Berechtigungen "
"verwendet werden, siehe I</usr/share/doc/sane-backends/README.linux>, "
"I<tools/README> und die I<README> im Verzeichnis I<tools/hotplug> für "
"weitere Details."

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Usually B<udev>(7)  or for older distributions the hotplug utilities are "
"used, which support dynamic setting of access permissions. SANE comes with "
"udev and hotplug scripts in the directory I<tools/udev> and I<tools/"
"hotplug>.  They can be used for setting permissions, see I</usr/share/doc/"
"packages/sane-backends/README.linux>, I<tools/README> and the I<README> in "
"the I<tools/hotplug> directory for more details."
msgstr ""
"Normalerweise wird B<udev>(7) oder bei älteren Distributionen das Hotplug-"
"Hilfswerkzeug verwendet, um dynamisches Setzen der Zugriffsberechtigungen zu "
"unterstützen. SANE enthält im Verzeichnis I<tools/udev> und I<tools/hotplug> "
"Udev- und Hotplug-Skripte. Diese können zum Setzen von Berechtigungen "
"verwendet werden, siehe I</usr/share/doc/packages/sane-backends/README."
"linux>, I<tools/README> und die I<README> im Verzeichnis I<tools/hotplug> "
"für weitere Details."
