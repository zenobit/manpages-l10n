# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.10.0\n"
"POT-Creation-Date: 2023-02-20 20:20+0100\n"
"PO-Revision-Date: 2022-11-06 12:07+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OOMD\\&.CONF"
msgstr "OOMD\\&.CONF"

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "oomd.conf"
msgstr "oomd.conf"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "oomd.conf, oomd.conf.d - Global B<systemd-oomd> configuration files"
msgstr ""
"oomd.conf, oomd.conf.d - Globale Konfigurationsdateien von B<systemd-oomd>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "/etc/systemd/oomd\\&.conf"
msgstr "/etc/systemd/oomd\\&.conf"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "/etc/systemd/oomd\\&.conf\\&.d/*\\&.conf"
msgstr "/etc/systemd/oomd\\&.conf\\&.d/*\\&.conf"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "/usr/lib/systemd/oomd\\&.conf\\&.d/*\\&.conf"
msgstr "/usr/lib/systemd/oomd\\&.conf\\&.d/*\\&.conf"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These files configure the various parameters of the B<systemd>(1)  userspace "
"out-of-memory (OOM) killer, B<systemd-oomd.service>(8)\\&. See B<systemd."
"syntax>(7)  for a general description of the syntax\\&."
msgstr ""
"Diese Dateien konfigurieren verschiedene Parameter von B<systemd-oomd."
"service>(8), dem Speichererknappheits- (OOM-)Killer im Anwendungsraum von "
"B<systemd>(1)\\&. Siehe B<systemd.syntax>(7) für eine allgemeine "
"Beschreibung der Syntax\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr "KONFIGURATIONSVERZEICHNISSE UND RANGFOLGE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""
"Die Standardkonfiguration wird während der Kompilierung gesetzt\\&. Daher "
"wird eine Konfiguration nur benötigt, wenn von diesen Vorgaben abgewichen "
"werden muss\\&. Anfänglich enthält die Hauptkonfigurationsdatei in /etc/"
"systemd/ die Vorgaben als auskommentierten Hinweis für den Administrator\\&. "
"Lokal können diese Einstellungen außer Kraft gesetzt werden, indem diese "
"Datei bearbeitet wird oder durch die Erstellung von Ergänzungen, wie "
"nachfolgend beschrieben\\&. Es wird empfohlen, Ergänzungen für lokale "
"Konfiguration zu verwenden, statt die Hauptkonfigurationsdatei zu "
"verändern\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""
"Zusätzlich zu der »Haupt«-Konfigurationsdatei, werden Ergänzungs-"
"Konfigurationsschnipsel aus /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/ und /etc/systemd/*\\&.conf\\&.d/ gelesen\\&. Diese "
"Ergänzungen haben Vorrang vor der Hauptkonfigurationsdatei und setzen diese "
"außer Kraft\\&. Dateien in den Konfigurationsunterverzeichnissen *\\&."
"conf\\&.d/ werden in lexikographischer Reihenfolge nach ihrem Dateinamen "
"sortiert, unabhängig davon, in welchem Unterverzeichnis sie sich "
"befinden\\&. Bei Optionen, die nur einen einzelnen Wert akzeptieren, hat der "
"Eintrag in der Datei, die als letztes in der Sortierung folgt, Vorrang, "
"falls mehrere Dateien die gleiche Option angeben\\&. Bei Optionen, die eine "
"Liste von Werten akzeptieren, werden Einträge gesammelt, wie sie in den "
"sortierten Dateien auftauchen\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""
"Wenn Pakete die Konfiguration anpassen müssen, können sie Ergänzungen unter /"
"usr/ installieren\\&. Dateien in /etc/ sind für den lokalen Administrator "
"reserviert, der diese Logik verwenden kann, um die durch die "
"Lieferantenpakete bereitgestellten Konfigurationsdateien außer Kraft zu "
"setzen\\&. Um Ergänzungen der Pakete außer Kraft zu setzen, müssen "
"Ergänzungen verwandt werden, da die Hauptkonfigurationsdatei die niedrigste "
"Priorität hat\\&. Es wird empfohlen, allen Dateinamen in diesen "
"Unterverzeichnissen eine zweistellige Zahl und einen Bindestrich "
"voranzustellen, um die Sortierung der Dateien zu vereinfachen\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""
"Um eine vom Lieferanten bereitgestellte Konfigurationsdatei zu deaktivieren, "
"wird empfohlen, einen Symlink nach /dev/null in dem "
"Konfigurationsverzeichnis in /etc/ mit dem gleichen Dateinamen wie die "
"Konfigurationsdatei des Lieferanten abzulegen\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "[OOM] SECTION OPTIONS"
msgstr "[OOM]-ABSCHNITT-OPTIONEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following options are available in the [OOM] section:"
msgstr "Die folgenden Optionen sind im Abschnit »[OOM]« verfügbar:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<SwapUsedLimit=>"
msgstr "I<SwapUsedLimit=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Sets the limit for memory and swap usage on the system before B<systemd-"
"oomd> will take action\\&. If the fraction of memory used and the fraction "
"of swap used on the system are both more than what is defined here, "
"B<systemd-oomd> will act on eligible descendant control groups with swap "
"usage greater than 5% of total swap, starting from the ones with the highest "
"swap usage\\&. Which control groups are monitored and what action gets taken "
"depends on what the unit has configured for I<ManagedOOMSwap=>\\&. Takes a "
"value specified in percent (when suffixed with \"%\"), permille (\"‰\") or "
"permyriad (\"‱\"), between 0% and 100%, inclusive\\&. Defaults to 90%\\&."
msgstr ""
"Setzt die Begrenzung für die Verwendung des Speichers und "
"Auslagerungsspeichers des Systems, bevor B<systemd-oomd> in Aktion tritt\\&. "
"Falls der Anteil an Speicherbenutzung und der Anteil an "
"Auslagerungsbenutzung des Systems größer als der hier definierte Wert ist, "
"wird B<systemd-oomd> auf geeignete Nachkommens-Steuergruppe agieren, bei "
"denen die Verwendung von Auslagerungsspeicher mehr als 5% des gesamten "
"Auslagerungsspeichers beträgt, beginnend mit derjenigen, die die höchste "
"Benutzung des Auslagerungsspeichers hat\\&. Welche Steuergruppen überwacht "
"und welche Aktionen vorgenommen werden, hängt davon ab, was die Unit für "
"I<ManagedOOMSwap=> konfiguriert hat\\&. Akzeptiert einen Wert, der als "
"Prozent (wenn ihm »%« angehängt wird), als Promille (»‰«) oder als "
"Prozehntausendstel (»◈«) zwischen (einschließlich) 0% und 100% angegeben "
"werden kann\\&. Standardmäßig 90%\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<DefaultMemoryPressureLimit=>"
msgstr "I<DefaultMemoryPressureLimit=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Sets the limit for memory pressure on the unit\\*(Aqs control group before "
"B<systemd-oomd> will take action\\&. A unit can override this value with "
"I<ManagedOOMMemoryPressureLimit=>\\&. The memory pressure for this property "
"represents the fraction of time in a 10 second window in which all tasks in "
"the control group were delayed\\&. For each monitored control group, if the "
"memory pressure on that control group exceeds the limit set for longer than "
"the duration set by I<DefaultMemoryPressureDurationSec=>, B<systemd-oomd> "
"will act on eligible descendant control groups, starting from the ones with "
"the most reclaim activity to the least reclaim activity\\&. Which control "
"groups are monitored and what action gets taken depends on what the unit has "
"configured for I<ManagedOOMMemoryPressure=>\\&. Takes a fraction specified "
"in the same way as I<SwapUsedLimit=> above\\&. Defaults to 60%\\&."
msgstr ""
"Setzt die Begrenzung für den Speicherdruck auf die Steuergruppe der Unit, "
"bevor B<systemd-oomd> in Aktion tritt\\&. Eine Unit kann diesen Wert mit "
"I<ManagedOOMMemoryPressureLimit=> außer Kraft setzen\\&. Der Speicherdruck "
"für diese Eigenschaft stellt den Bruchteil der Zeit in einem 10-Sekunden-"
"Zeitfenster dar, um den alle Prozesse in der Steuergruppe verzögert "
"wurden\\&. Für jede überwachte Steuergruppe wird B<systemd-oomd> auf "
"geeignete Nachkommen-Steuergruppen agieren, falls der Speicherdruck für "
"diese Steuergruppe die gesetzte Begrenzung für mehr als die durch "
"I<DefaultMemoryPressureDurationSec=> gesetzte Dauer überschreitet. Dabei "
"beginnt es mit denen, die über die meisten zurückgewinnbaren Aktivitäten "
"verfügen bis hin zu denen mit den geringsten zurückgewinnbaren "
"Aktivitäten\\&. Welche Steuergruppen überwacht und welche Aktionen "
"vorgenommen werden, hängt davon ab, was die Unit für "
"I<ManagedOOMMemoryPressure=> konfiguriert hat\\&. Akzeptiert einen Anteil, "
"der wie beim vorher beschriebenen I<SwapUsedLimit=> angegeben wird\\&. "
"Standardmäßig 60%\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<DefaultMemoryPressureDurationSec=>"
msgstr "I<DefaultMemoryPressureDurationSec=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Sets the amount of time a unit\\*(Aqs control group needs to have exceeded "
"memory pressure limits before B<systemd-oomd> will take action\\&. Memory "
"pressure limits are defined by I<DefaultMemoryPressureLimit=> and "
"I<ManagedOOMMemoryPressureLimit=>\\&. Must be set to 0, or at least 1 "
"second\\&. Defaults to 30 seconds when unset or 0\\&."
msgstr ""
"Setzt die Zeitdauer, die eine Steuergruppe der Unit die "
"Speicherdruckbeschränkung überschritten haben muss, bevor B<systemd-oomd> "
"Aktionen ergreift\\&. Speicherdruckbeschränkungen werden durch "
"I<DefaultMemoryPressureLimit=> und I<ManagedOOMMemoryPressureLimit=> "
"festgelegt\\&. Muss auf 0 oder mindestens eine 1 Sekunde gesetzt werden\\&. "
"Standardmäßig 30 Sekunden, wenn nicht gesetzt oder 0\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd.resource-control>(5), B<systemd-oomd.service>(8), "
"B<oomctl>(1)"
msgstr ""
"B<systemd>(1), B<systemd.resource-control>(5), B<systemd-oomd.service>(8), "
"B<oomctl>(1)"

#. type: TH
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"
