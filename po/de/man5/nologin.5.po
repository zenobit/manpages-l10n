# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Schulze <joey@infodrom.org>
# Michael Haardt <michael@moria.de>
# René Tschirley <gremlin@cs.tu-berlin.de>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.16.0\n"
"POT-Creation-Date: 2023-02-20 20:19+0100\n"
"PO-Revision-Date: 2023-01-11 13:17+0100\n"
"Last-Translator: Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "nologin"
msgstr "nologin"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "nologin - prevent unprivileged users from logging into the system"
msgstr "nologin - verhindert die Anmeldung nicht berechtigter Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the file I</etc/nologin> exists and is readable, B<login>(1)  will allow "
"access only to root.  Other users will be shown the contents of this file "
"and their logins will be refused.  This provides a simple way of temporarily "
"disabling all unprivileged logins."
msgstr ""
"Falls es die Datei I</etc/nologin> gibt und sie lesbar ist, erlaubt "
"B<login>(1) nur root eine Anmeldung am System. Anderen Benutzern wird der "
"Inhalt der Datei angezeigt und keine Anmeldung gestattet. Dies ist ein "
"einfacher Weg, die unprivilegierten Anmeldungen temporär zu deaktivieren."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</etc/nologin>"
msgstr "I</etc/nologin>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<login>(1), B<shutdown>(8)"
msgstr "B<login>(1), B<shutdown>(8)."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "NOLOGIN"
msgstr "NOLOGIN"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.10 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux-Handbuchseiten 6.02"
