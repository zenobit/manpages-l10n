# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020.
# Helge Kreutzmann <debian@helgefjell.de>, 2021,2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.12.1\n"
"POT-Creation-Date: 2023-02-15 19:11+0100\n"
"PO-Revision-Date: 2022-01-30 06:57+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: IX
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "sane-hpsj5s"
msgstr "sane-hpsj5s"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "13 Jul 2008"
msgstr "13. Juli 2008"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "sane-hpsj5s - SANE backend for HP ScanJet 5S sheet-fed scanner"
msgstr "sane-hpsj5s - SANE-Backend für den ScanJet 5S Einzugsscanner von HP"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"The B<sane-hpsj5s> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to a parallel port Hewlett-Packard ScanJet 5S "
"scanner."
msgstr ""
"Die Bibliothek B<sane-hpsj5s> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf den Parallelportscanner ScanJet 5S von Hewlett-"
"Packard."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"IMPORTANT: this is alpha code. Don't expect this to work correctly. Many "
"functions are missing, others contain errors. In some cases, your computer "
"might even hang. It cannot be excluded (although I consider it extremely "
"improbable) that your scanner will be damaged."
msgstr ""
"WICHTIG: Dies ist Alpha-Code. Gehen Sie nicht davon aus, dass er korrekt "
"funktioniert. Viele Funktionen fehlen, andere enthalten Fehler. In einigen "
"Fällen könnte sich Ihr Rechner sogar aufhängen. Es kann nicht ausgeschlossen "
"werden (obwohl ich es als extrem unwahrscheinlich betrachte), dass Ihr "
"Scanner beschädigt wird."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"LIMITATIONS: For now this backend works only on Linux.  This limitation is "
"due to dependence on the B<libieee1284>(3)  library. If your system supports "
"B<libieee1284>(3)  too, this backend should work. If you ported "
"B<libieee1284>(3)  for your platform, please let me know. Your system should "
"support B<EPP> (or B<EPP+ECP>)  mode to operate this scanner. Future "
"versions will support ECP and SPP (Nibble and Byte) modes also. It's planned "
"to support scanners not only at daisy-chain position 0, but anywhere. "
"Support for multiple scanners could be implemented too."
msgstr ""
"EINSCHRÄNKUNGEN: Derzeit funktioniert das Backend nur unter Linux. Diese "
"Einschränkung besteht aufgrund der Abhängigkeit von der Bibliothek "
"B<libieee1284>(3). Falls Ihr System auch B<libieee1284>(3) unterstützt, "
"sollte dieses Backend funktionieren. Falls Sie B<libieee1284>(3) auf Ihre "
"Plattform portierten, lassen Sie es mich bitte wissen. Ihr System sollte den "
"Modus B<EPP> (oder B<EPP+ECP>) unterstützen, um den Scanner zu betreiben. "
"Zukünftige Versionen werden auch die Modi ECP und SPP (Nibble und Byte) "
"unterstützen. Es ist geplant, die Scanner nicht nur an der nullten Position "
"der Reihenschaltung zu unterstützen, sondern an beliebigen. Es sollte auch "
"die Unterstützung für mehrere Scanner implementiert werden."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Current version implements only gray scale scanning. True Color and B/W "
"modes are not supported for now."
msgstr ""
"Derzeitige Versionen implementieren nur Graustufen-Scannen. Echte Farb- und "
"S/W-Modi werden derzeit nicht unterstützt."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"That said, TESTERS ARE WELCOME. Send your bug reports and comments to Max "
"Vorobiev E<lt>I<pcwizard@yandex.ru>E<gt>."
msgstr ""
"Mit anderen Worten: TESTER SIND WILLKOMMEN. Senden Sie Ihre Fehlerberichte "
"und Kommentare an Max Vorobiev E<lt>I<pcwizard@yandex.ru>E<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DEVICE NAMES"
msgstr "GERÄTENAMEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "This backend expects device names of the form:"
msgstr "Dieses Backend erwartet Gerätenamen der folgenden Form:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "I<special>"
msgstr "I<Spezialdatei>"

# in form → in a form
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Where I<special> is the parallel port name in form, B<libieee1284>(3)  "
"expects. It seems to be system dependent. Under Linux it's parport0, "
"parport1, etc."
msgstr ""
"Hierbei ist I<Spezialdatei> der Parallelportname in der Form, wie ihn "
"B<libieee1284>(3) erwartet. Er scheint systemabhängig zu sein. Unter Linux "
"ist es parport0, parport1 usw."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CONFIGURATION"
msgstr "KONFIGURATION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"The contents of the I<hpsj5s.conf> file is a list of parport names that "
"correspond to HP ScanJet 5S scanners.  Empty lines and lines starting with a "
"hash mark (#) are ignored.  Only one device name can be listed in I<hpsj5s."
"conf> for this moment. Future versions will support daisy chain selection."
msgstr ""
"Der Inhalt der Datei I<hpsj5s.conf> ist eine Liste von Parallel-Port-Namen, "
"die HP ScanJet 5S-Scannern entsprechen. Leere Zeilen und Zeilen, die mit "
"einer Raute (#) beginnen, werden ignoriert. Derzeit können nur Gerätenamen "
"in I<hpsj5s.conf> aufgeführt werden. Zukünftige Versionen werden auch die "
"Auswahl aus der Serienschaltung unterstützen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "TIPS"
msgstr "TIPPS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"It seems that HP ScanJet 5S scanner uses software noise correction. This "
"feature, along with gamma correction and calibration, are not implemented "
"for now.  They will be handled in future versions.  Native resolution for "
"this scanner is 300 DPI.  Other modes may present aliasing artifacts."
msgstr ""
"Es scheint, dass HP ScanJet 5S Scanner Sofware Rauschkorrektur verwendet. "
"Diese Funktion, zusammen mit Gamma-Korrektur und Kalibrierung, ist derzeit "
"nicht implementiert. Ich kümmere mich in zukünftigen Versionen darum. Die "
"natürliche Auflösung für diesen Scanner ist 300 DPI. Andere Modi könnten "
"Alias-Effekte zeigen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "I</etc/sane.d/hpsj5s.conf>"
msgstr "I</etc/sane.d/hpsj5s.conf>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"The backend configuration file (see also description of B<SANE_CONFIG_DIR> "
"below)."
msgstr ""
"Die Backend-Konfigurationsdatei (siehe auch die nachfolgende Beschreibung "
"von B<SANE_CONFIG_DIR>)."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-hpsj5s.a>"
msgstr "I</usr/lib/sane/libsane-hpsj5s.a>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "The static library implementing this backend."
msgstr "Die statische Bibliothek, die dieses Backend implementiert."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-hpsj5s.so>"
msgstr "I</usr/lib/sane/libsane-hpsj5s.so>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Die dynamische Bibliothek, die dieses Backend implementiert (auf Systemen "
"verfügbar, die dynamisches Laden unterstützen)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<SANE_CONFIG_DIR>"
msgstr "B<SANE_CONFIG_DIR>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  On *NIX systems, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I<tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Diese Umgebungsvariable gibt eine Liste von Verzeichnissen an, die die "
"Konfigurationsdatei enthalten können. Auf *NIX-Systemen sind die "
"Verzeichnisse durch Doppelpunkte (:) getrennt, unter OS/2 durch Semikola "
"(;). Falls diese Variable nicht gesetzt ist, wird in zwei "
"Standardverzeichnissen nach der Konfigurationsdatei gesucht: zuerst im "
"aktuellen Arbeitsverzeichnis (.) und dann in I</etc/sane.d>. Falls der Wert "
"der Umgebungsvariable mit dem Verzeichnis-Trennzeichen endet, dann werden "
"die Standardverzeichnisse nach den explizit angegebenen Verzeichnissen "
"durchsucht. Wenn Sie beispielsweise B<SANE_CONFIG_DIR> auf »/tmp/config:« "
"setzen, wird in den Verzeichnissen »tmp/config«, ».« und »/etc/sane.d« "
"gesucht (in dieser Reihenfolge)."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<SANE_DEBUG_HPSJ5S>"
msgstr "B<SANE_DEBUG_HPSJ5S>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für dieses Backend. Größere Werte erhöhen "
"die Ausführlichkeit der Ausgabe."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<sane>(7), B<libieee1284>(3)"
msgstr "B<sane>(7), B<libieee1284>(3)"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "I<http://hpsj5s.sourceforge.net>"
msgstr "I<http://hpsj5s.sourceforge.net>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "I<http://cyberelk.net/tim/libieee1284>"
msgstr "I<http://cyberelk.net/tim/libieee1284>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "Max Vorobiev"
msgstr "Max Vorobiev"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Man page mostly based on I<canon.man>."
msgstr "Die Handbuchseite basiert hauptsächlich auf I<canon.man>."

# FIXME B<EPP+ECP> ) → B<EPP+ECP>)
#. type: Plain text
#: debian-bullseye
msgid ""
"LIMITATIONS: For now this backend works only on Linux.  This limitation is "
"due to dependence on the libieee1284 library. If your system supports "
"libieee1284 too, this backend should work. If you ported libieee1284 for "
"your platform, please let me know. Your system should support B<EPP> (or "
"B<EPP+ECP> ) mode to operate this scanner. Future versions will support ECP "
"and SPP (Nibble and Byte) modes also. It's planned to support scanners not "
"only at daisy-chain position 0, but anywhere. Support for multiple scanners "
"could be implemented too."
msgstr ""
"EINSCHRÄNKUNGEN: Derzeit funktioniert das Backend nur unter Linux. Diese "
"Einschränkung besteht aufgrund der Abhängigkeit von der Bibliothek "
"Libieee1284. Falls Ihr System auch Libieee1284 unterstützt, sollte dieses "
"Backend funktionieren. Falls Sie Libieee1284 auf Ihre Plattform portierten, "
"lassen Sie es mich bitte wissen. Ihr System sollte den Modus B<EPP> (oder "
"B<EPP+ECP>) unterstützen, um den Scanner zu betreiben. Zukünftige Versionen "
"werden auch die Modi ECP und SPP (Nibble und Byte) unterstützen. Es ist "
"geplant, die Scanner nicht nur an der nullten Position der Reihenschaltung "
"zu unterstützen, sondern an beliebigen. Es sollte auch die Unterstützung für "
"mehrere Scanner implementiert werden."

#. type: Plain text
#: debian-bullseye
msgid ""
"That said, TESTERS ARE WELCOME. Send your bug reports and comments to Max "
"Vorobiev E<lt>pcwizard@yandex.ruE<gt>."
msgstr ""
"Mit anderen Worten: TESTER SIND WILLKOMMEN. Senden Sie Ihre Fehlerberichte "
"und Kommentare an Max Vorobiev E<lt>pcwizard@yandex.ruE<gt>."

#. type: Plain text
#: debian-bullseye
msgid ""
"Where I<special> is the parallel port name in form, libieee1284 expects. It "
"seems to be system dependent.  Under Linux it's parport0, parport1, etc."
msgstr ""
"Hierbei ist I<Spezialdatei> der Parallelportname in der Form, wie ihn "
"Libieee1284 erwartet. Er scheint systemabhängig zu sein. Unter Linux ist es "
"parport0, parport1 usw."

# FIXME The last sentence is very hard to read for non native speakers, the translation is more of a guess, "to jag in" is not in dictionaries?
#. type: Plain text
#: debian-bullseye
msgid ""
"It seems that HP ScanJet 5S scanner uses software noise correction. This "
"feature is not implemented for now. So does gamma correction and "
"calibration.  I'll handle it in future versions.  Native resolution for this "
"scanner is 300 DPI. Other modes could be jagged in some ways."
msgstr ""
"Es scheint, dass HP ScanJet 5S Scanner Sofware Rauschkorrektur verwendet. "
"Diese Funktion ist derzeit nicht implementiert. Genauso Gamma-Korrektur und "
"Kalibrierung. Ich kümmere mich in zukünftigen Versionen darum. Die "
"natürliche Auflösung für diesen Scanner ist 300 DPI. Andere Modi könnten "
"irgendwie reingebastelt werden."

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-hpsj5s.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-hpsj5s.a>"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-hpsj5s.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-hpsj5s.so>"

# FIXME /etc/sane.d → I</etc/sane.d>
#. type: Plain text
#: debian-bullseye
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  Under UNIX, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in /etc/"
"sane.d.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories \"tmp/config\", \".\", and "
"\"/etc/sane.d\" being searched (in this order)."
msgstr ""
"Diese Umgebungsvariable gibt eine Liste von Verzeichnissen an, die die "
"Konfigurationsdatei enthalten können. Unter UNIX sind die Verzeichnisse "
"durch Doppelpunkte (:) getrennt, unter OS/2 durch Semikola (;). Falls diese "
"Variable nicht gesetzt ist, wird in zwei Standardverzeichnissen nach der "
"Konfigurationsdatei gesucht: zuerst im aktuellen Arbeitsverzeichnis (.) und "
"dann in I</etc/sane.d>. Falls der Wert der Umgebungsvariable mit dem "
"Verzeichnis-Trennzeichen endet, dann werden die Standardverzeichnisse nach "
"den explizit angegebenen Verzeichnissen durchsucht. Wenn Sie beispielsweise "
"B<SANE_CONFIG_DIR> auf »/tmp/config:« setzen, wird in den Verzeichnissen "
"»tmp/config«, ».« und »/etc/sane.d« gesucht (in dieser Reihenfolge)."

#. type: Plain text
#: debian-bullseye
msgid "sane(7)"
msgstr "B<sane>(7)"

#. type: Plain text
#: debian-bullseye
msgid "http://hpsj5s.sourceforge.net"
msgstr "http://hpsj5s.sourceforge.net"

#. type: Plain text
#: debian-bullseye
msgid "http://cyberelk.net/tim/libieee1284"
msgstr "http://cyberelk.net/tim/libieee1284"

#. type: Plain text
#: debian-bullseye
msgid "Man page mostly based on canon.man"
msgstr "Die Handbuchseite basiert hauptsächlich auf canon.man."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</usr/lib64/sane/libsane-hpsj5s.a>"
msgstr "I</usr/lib64/sane/libsane-hpsj5s.a>"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</usr/lib64/sane/libsane-hpsj5s.so>"
msgstr "I</usr/lib64/sane/libsane-hpsj5s.so>"
