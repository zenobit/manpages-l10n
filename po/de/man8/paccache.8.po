# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2018, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-01-09 20:12+0100\n"
"PO-Revision-Date: 2022-10-24 11:40+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACCACHE"
msgstr "PACCACHE"

#. type: TH
#: archlinux
#, no-wrap
msgid "2023-01-04"
msgstr "4. Januar 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib 1\\&.8\\&.2"
msgstr "Pacman-contrib 1\\&.8\\&.2"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib Manual"
msgstr "Pacman-contrib-Handbuch"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid "paccache - flexible pacman cache cleaning utility"
msgstr ""
"paccache - flexibles Werkzeug zum Aufräumen des Pacman-Zwischenspeichers"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid "I<paccache> E<lt>operationE<gt> [options] [target \\&...]"
msgstr "B<paccache> E<lt>AktionE<gt> [Optionen] [Ziel …]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux
msgid ""
"I<paccache> removes old packages from the pacman cache directory\\&. By "
"default the last three versions of a package are kept\\&."
msgstr ""
"B<paccache> entfernt alte Pakete aus dem Zwischenspeicher von Pacman\\&. "
"Standardmäßig werden die letzten drei Versionen eines Pakets behalten\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPERATIONS"
msgstr "AKTIONEN"

#. type: Plain text
#: archlinux
msgid "B<-d, --dryrun>"
msgstr "B<-d, --dryrun>"

#. type: Plain text
#: archlinux
msgid "Perform a dry run, only finding candidate packages\\&."
msgstr ""
"führt einen »Trockenlauf« aus, wobei nur nach potenziell zu entfernenden "
"Paketen gesucht wird\\&."

#. type: Plain text
#: archlinux
msgid "B<-m, --move E<lt>dirE<gt>>"
msgstr "B<-m, --move E<lt>VerzeichnisE<gt>>"

#. type: Plain text
#: archlinux
msgid "Move candidate packages from the cache directory to I<dir>\\&."
msgstr ""
"verschiebt potenziell zu entfernende Pakete in das angegebene "
"I<Verzeichnis>\\&."

#. type: Plain text
#: archlinux
msgid "B<-r, --remove>"
msgstr "B<-r, --remove>"

#. type: Plain text
#: archlinux
msgid "Remove candidate packages from the cache directory\\&."
msgstr ""
"entfernt die Pakete aus dem Zwischenspeicher, die als potenziell zu "
"entfernen identifiziert wurden\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux
msgid "B<-a, --arch E<lt>archE<gt>>"
msgstr "B<-a, --arch E<lt>ArchitekturE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Scan for packages for a specific architecture\\&. Default is to scan for all "
"architectures\\&."
msgstr ""
"sucht nach Paketen für eine spezifische Architektur\\&. Standardmäßig wird "
"nach allen Architekturen gesucht\\&."

#. type: Plain text
#: archlinux
msgid "B<-c, --cachedir E<lt>dirE<gt>>"
msgstr "B<-c, --cachedir E<lt>VerzeichnisE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Specify a different cache directory\\&. This option can be used more than "
"once\\&. Default is to use the cache directory configured in I<pacman\\&."
"conf>\\&."
msgstr ""
"gibt ein anderes Zwischenspeicherverzeichnis an\\&. Diese Option kann "
"mehrmals angegeben werden\\&. Standardmäßig wird das in I<pacman\\&.conf> "
"konfigurierte Zwischenspeicherverzeichnis verwendet\\&."

#. type: Plain text
#: archlinux
msgid "B<-f, --force>"
msgstr "B<-f, --force>"

# FIXME I<mv> and I<rm> → B<mv>(1) and B<rm>(1)
#. type: Plain text
#: archlinux
msgid "Apply force to I<mv> and I<rm> operations\\&."
msgstr "erzwingt die Ausführung von B<mv>(1) und B<rm>(1)\\&."

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Display syntax and command-line options\\&."
msgstr "zeigt die Optionen der Syntax und Befehlszeile an\\&."

#. type: Plain text
#: archlinux
msgid "B<-i, --ignore E<lt>pkgsE<gt>>"
msgstr "B<-i, --ignore E<lt>PaketeE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Specify packages to ignore, comma-separated\\&. Alternatively \"-\" can be "
"used to read the package names from stdin, newline-delimited\\&."
msgstr ""
"gibt zu ignorierende Pakete an, durch Kommata getrennt\\&. Alternativ können "
"Sie »-« angeben, wodurch die Pakete aus der Standardeingabe gelesen werden, "
"durch Zeilenvorschübe getrennt\\&."

#. type: Plain text
#: archlinux
msgid "B<-k, --keep E<lt>numE<gt>>"
msgstr "B<-k, --keep E<lt>ZahlE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Specify how many versions of each package are kept in the cache directory, "
"default is 3\\&."
msgstr ""
"gibt an, wieviele Versionen eines Pakets im Zwischenspeicher behalten "
"werden\\&. Vorgabe ist 3\\&."

#. type: Plain text
#: archlinux
msgid "B<--min-atime E<lt>timeE<gt>>, B<--min-mtime E<lt>timeE<gt>>"
msgstr "B<--min-atime E<lt>ZeitE<gt>>, B<--min-mtime E<lt>ZeitE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Keep packages with an atime/mtime that is not older than the time given, "
"even if this means keeping more than specified through the I<--keep> "
"option\\&. Accepts arguments according to I<info \"Date input formats\">, "
"e\\&.g\\&.  I<30 days ago>\\&."
msgstr ""
"behält Pakete mit einer atime bzw. mtime, die nicht älter als die angegebene "
"I<Zeit> ist, selbst wenn dies bedeuten würde, dass mehr als das, was durch "
"die Option B<--keep> angegeben ist, behalten wird\\&. Es werden Argumente "
"gemäß I<info \"Date input formats\"> akzeptiert, beispielsweise I<30 days "
"ago>\\&."

#. type: Plain text
#: archlinux
msgid "B<--nocolor>"
msgstr "B<--nocolor>"

#. type: Plain text
#: archlinux
msgid "Do not colorize output\\&."
msgstr "stellt die Ausgabe nicht farbig dar.\\&."

#. type: Plain text
#: archlinux
msgid "B<-z, --null>"
msgstr "B<-z, --null>"

#. type: Plain text
#: archlinux
msgid "Use null delimiters for candidate names (only with -v and -vv)\\&."
msgstr ""
"verwendet Null-Trenner für die Namen potenziell zu entfernender Pakete (nur "
"mit -v und -vv)\\&."

#. type: Plain text
#: archlinux
msgid "B<-q, --quiet>"
msgstr "B<-q, --quiet>"

#. type: Plain text
#: archlinux
msgid "Minimize the output\\&."
msgstr "minimiert die Ausgabe\\&."

#. type: Plain text
#: archlinux
msgid "B<-u, --uninstalled>"
msgstr "B<-u, --uninstalled>"

#. type: Plain text
#: archlinux
msgid "Target uninstalled packages\\&."
msgstr "wirkt auf nicht installierte Pakete\\&."

#. type: Plain text
#: archlinux
msgid "B<-v, --verbose>"
msgstr "B<-v, --verbose>"

#. type: Plain text
#: archlinux
msgid "Increase verbosity, can be specified up to 3 times\\&."
msgstr ""
"erhöht die Ausführlichkeitsstufe der Ausgabe, kann bis zu drei Mal angegeben "
"werden\\&."

#. type: Plain text
#: archlinux
msgid "B<-V, --version>"
msgstr "B<-V, --version>"

#. type: Plain text
#: archlinux
msgid "Display version information\\&."
msgstr "zeigt Versionsinformationen an\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SYSTEMD TIMER"
msgstr "SYSTEMD-ZEITGEBER"

#. type: Plain text
#: archlinux
msgid ""
"The package cache can be cleaned periodically using the systemd timer "
"I<paccache\\&.timer>\\&. If the timer is enabled the cache will be cleaned "
"weekly with paccache\\(cqs default options\\&."
msgstr ""
"Der Paketzwischenspeicher kann periodisch mit dem Systemd-Timer "
"I<paccache\\&.timer> geleert werden\\&. Wenn der Timer freigegeben ist, wird "
"der Zwischenspeicher wöchentlich mit den Standardoptionen von Paccache "
"geleert\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux
msgid "B<pacman>(8), B<pacman.conf>(5), B<systemctl>(1)"
msgstr "B<pacman>(8), B<pacman.conf>(5), B<systemctl>(1)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, file an issue with as much detail as possible at https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman-contrib/-/issues/new\\&."
msgstr ""
"Fehler? Sie machen wohl Witze, es gibt keine Fehler in dieser Software\\&. "
"Nun ja, sollte unsere Annahme doch falsch sein, hinterlassen Sie einen "
"Fehlerbericht (auf Englisch) mit so vielen Details wie möglich auf https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman-contrib/-/issues/new\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux
msgid "Current maintainers:"
msgstr "Derzeitige Betreuer:"

#. type: Plain text
#: archlinux
msgid "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"
msgstr "E<.MT johannes@kyriasis.com>Johannes LöthbergE<.ME>"

#. type: Plain text
#: archlinux
msgid "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"
msgstr "E<.MT polyzen@archlinux.org>Daniel M. CapellaE<.ME>"

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the pacman-contrib\\&."
"git repository\\&."
msgstr ""
"Informationen zu weiteren Mitwirkenden erhalten Sie, wenn Sie den Befehl "
"B<git shortlog -s> im Repositorium pacman-contrib\\&.git aufrufen\\&."
