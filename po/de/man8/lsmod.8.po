# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2023-02-15 18:59+0100\n"
"PO-Revision-Date: 2022-04-20 20:45+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "LSMOD"
msgstr "LSMOD"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "06/30/2022"
msgstr "30.06.2022"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "kmod"
msgstr "kmod"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "lsmod"
msgstr "lsmod"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "lsmod - Show the status of modules in the Linux Kernel"
msgstr "lsmod - Zeigt den Status der Module im Linux-Kernel"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<lsmod>"
msgstr "B<lsmod>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME /proc/modules → I</proc/modules>
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<lsmod> is a trivial program which nicely formats the contents of the /proc/"
"modules, showing what kernel modules are currently loaded\\&."
msgstr ""
"B<lsmod> ist ein triviales Programm, das den Inhalt von I</proc/modules> "
"ansprechend formatiert und zeigt, welche Kernelmodule derzeit geladen sind."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manual page originally Copyright 2002, Rusty Russell, IBM "
"Corporation\\&. Maintained by Jon Masters and others\\&."
msgstr ""
"Diese Handbuchseite ist ursprünglich Copyright 2002, Rusty Russell, IBM "
"Corporation\\&. Betreut von Jon Masters und anderen\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# FIXME B<modinfo>(8) -> B<modinfo>(8),
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<insmod>(8), B<modprobe>(8), B<modinfo>(8)  B<depmod>(8)"
msgstr "B<insmod>(8), B<modprobe>(8), B<modinfo>(8), B<depmod>(8)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<Jon Masters> E<lt>\\&jcm@jonmasters\\&.org\\&E<gt>"
msgstr "B<Jon Masters> E<lt>\\&jcm@jonmasters\\&.org\\&E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Developer"
msgstr "Entwickler"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<Lucas De Marchi> E<lt>\\&lucas\\&.de\\&.marchi@gmail\\&.com\\&E<gt>"
msgstr "B<Lucas De Marchi> E<lt>\\&lucas\\&.de\\&.marchi@gmail\\&.com\\&E<gt>"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "01/08/2021"
msgstr "8. Januar 2021"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "12/10/2022"
msgstr "10. Dezember 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "01/29/2021"
msgstr "29. Januar 2021"
