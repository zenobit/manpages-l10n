# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-02-28 18:54+0100\n"
"PO-Revision-Date: 2023-03-01 18:44+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "ARCH-CHROOT"
msgstr "ARCH-CHROOT"

#. type: TH
#: archlinux
#, no-wrap
msgid "11/20/2022"
msgstr "20. November 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr "\\ \" "

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr "\\ \""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid "arch-chroot - enhanced chroot command"
msgstr "arch-chroot - erweiterter chroot-Befehl"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid "arch-chroot [options] chroot-dir [command] [arguments\\&...]"
msgstr "arch-chroot [Optionen] chroot-Verzeichnis [Befehl] [Argumente …]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME arch-chroot → B<arch-chroot>
#. type: Plain text
#: archlinux
msgid ""
"arch-chroot wraps the B<chroot>(1) command while ensuring that important "
"functionality is available, e\\&.g\\&. mounting I</dev/>, I</proc> and other "
"API filesystems, or exposing B<resolv.conf>(5) to the chroot\\&."
msgstr ""
"B<arch-chroot> ist ein Wrapper für den Befehl B<chroot>(1), wobei "
"sichergestellt wird, dass wichtige Funktionalität verfügbar bleibt, "
"beispielsweise das Einhängen von I</dev/>, I</proc> und anderen API-"
"Dateisystemen, oder die Offenlegung von B<resolv.conf>(5) zur chroot-"
"Umgebung\\&."

# FIXME arch-chroot → B<arch-chroot>
#. type: Plain text
#: archlinux
msgid "If I<command> is unspecified, arch-chroot will launch B</bin/bash>\\&."
msgstr ""
"Falls kein I<Befehl> angegeben ist, führt B<arch-chroot> den Befehl B</bin/"
"bash> aus\\&."

#. type: Plain text
#: archlinux
msgid "B<Note>"
msgstr "B<Hinweis>"

#. type: Plain text
#: archlinux
msgid ""
"The target chroot-dir B<should> be a mountpoint\\&. This ensures that tools "
"such as B<pacman>(8) or B<findmnt>(8) have an accurate hierarchy of the "
"mounted filesystems within the chroot\\&. If your chroot target is not a "
"mountpoint, you can bind mount the directory on itself to make it a "
"mountpoint, i\\&.e\\&.:"
msgstr ""
"Das chroot-Zielverzeichnis B<sollte> ein Einhängepunkt sein\\&. Dadurch ist "
"gewährleistet, dass Werkzeuge wie B<pacman>(8) oder B<findmnt>(8) innerhalb "
"des chroot-Verzeichnisses eine akkurate Hierarchie der eingehängten "
"Dateisysteme vorfinden\\&. Falls Ihr chroot-Zielverzeichnis kein "
"Einhängepunkt ist, können Sie das Verzeichnis per Bind-Einhängung auf sich "
"selbst zu einem Einhängepunkt machen, das heißt:"

#. type: Plain text
#: archlinux
msgid "I<mount --bind /your/chroot /your/chroot>"
msgstr "I<mount --bind /Ihr/chroot-Verzeichnis /Ihr/chroot-Verzeichnis>"

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux
msgid "B<-N>"
msgstr "B<-N>"

#. type: Plain text
#: archlinux
msgid ""
"Run in unshare mode\\&. This will use B<unshare>(1)  to create a new mount "
"and user namespace, allowing regular users to create new system "
"installations\\&."
msgstr ""
"bewirkt die Ausführung im Unshare-Modus\\&. Hierzu wird mittels "
"B<unshare>(1) ein neuer Einhänge- und Benutzernamensraum erzeugt, wodurch "
"reguläre Benutzer die Möglichkeit erhalten, System-Neuinstallationen "
"auszuführen\\&."

#. type: Plain text
#: archlinux
msgid "B<-u E<lt>userE<gt>[:group]>"
msgstr "B<-u E<lt>BenutzerE<gt>[:Gruppe]>"

#. type: Plain text
#: archlinux
msgid "Specify non-root user and optional group to use\\&."
msgstr ""
"gibt den zu verwendenden (nicht als Systemadministrator fungierenden) "
"Benutzer und optional dessen Gruppe an\\&."

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux
msgid "Output syntax and command line options\\&."
msgstr "gibt die Syntax und Befehlszeilenoptionen aus\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux
msgid "B<pacman>(8)"
msgstr "B<pacman>(8)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux
msgid ""
"Bugs can be reported on the bug tracker I<https://bugs\\&.archlinux\\&.org> "
"in the Arch Linux category and title prefixed with [arch-install-scripts] or "
"via arch-projects@archlinux\\&.org\\&."
msgstr ""
"Fehler können im Fehlererfassungssystem auf I<https://bugs\\&.archlinux\\&."
"org> in der Kategorie »Arch Linux« gemeldet werden\\&. Stellen Sie dem Titel "
"bitte [arch-install-scripts] voran\\&. Alternativ können Sie Fehler über die "
"E-Mail-Adresse E<.MT arch-projects@archlinux\\&.org>E<.ME> melden\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux
msgid "Maintainers:"
msgstr "Betreuer:"

#. type: Plain text
#: archlinux
msgid "Dave Reisner E<lt>dreisner@archlinux\\&.orgE<gt>"
msgstr "E<.MT dreisner@archlinux\\&.org>Dave ReisnerE<.ME>"

#. type: Plain text
#: archlinux
msgid "Eli Schwartz E<lt>eschwartz@archlinux\\&.orgE<gt>"
msgstr "E<.MT eschwartz@archlinux\\&.org>Eli SchwartzE<.ME>"

# FIXME git shortlog -s → B<git shortlog -s>
#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the arch-install-"
"scripts\\&.git repository\\&."
msgstr ""
"Informationen zu weiteren Mitwirkenden erhalten Sie, wenn Sie den Befehl "
"B<git shortlog -s> im Git-Repositorium arch-install-scripts\\&.git "
"aufrufen\\&."
