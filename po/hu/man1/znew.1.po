# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tímár András <timar_a@freemail.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 19:26+0100\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Tímár András <timar_a@freemail.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ZNEW"
msgstr "ZNEW"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "znew - recompress .Z files to .gz files"
msgstr "znew - újratömöríti a .Z fájlokat .gz fájlokká"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<znew> [ -ftv9PK] [ name.Z ...  ]"
msgstr "B<znew> [ -ftv9PK] [ név.Z ...  ]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Znew> recompresses files from .Z (compress) format to .gz (gzip) "
#| "format.  If you want to recompress a file already in gzip format, rename "
#| "the file to force a .Z extension then apply znew."
msgid ""
"The B<Znew> command recompresses files from .Z (compress) format to .gz "
"(gzip) format.  If you want to recompress a file already in gzip format, "
"rename the file to force a .Z extension then apply znew."
msgstr ""
"A I<znew> újratömöríti a .Z (compress) formátumú fájlokat .gz (gzip) "
"formátumú fájlokká. Ha olyan fájlt szeretnénk újratömöríteni, amely már "
"eleve gzip formátumú, nevezzük át a fájlt .Z kiterjesztésűre, és azután "
"használjuk a znew parancsot."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "KAPCSOLÓK"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Force recompression from .Z to .gz format even if a .gz file already exists."
msgstr ""
"Akkor is újratömöríti a .Z formátumú fájlt .gz formátumúra ha a .gz fájl már "
"létezik."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>"
msgstr "B<-t>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Tests the new files before deleting originals."
msgstr "Teszteli az új fájlokat, mielőtt letörölné a régieket."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Verbose. Display the name and percentage reduction for each file compressed."
msgstr ""
"Bőbeszédű. Kiírja minden egyes fájl nevét és a százalékos méretcsökkenést."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-9>"
msgstr "B<-9>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Use the slowest compression method (optimal compression)."
msgstr "A leglassabb tömörítési eljárást használja (optimális tömörítés)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Use pipes for the conversion to reduce disk space usage."
msgstr ""
"Csöveket (pipe) használ a konvertáláskor és így lemezhelyet takarít meg."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-K>"
msgstr "B<-K>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Keep a .Z file when it is smaller than the .gz file"
msgid "Keep a .Z file when it is smaller than the .gz file; implies B<-t>."
msgstr "Megtartja a .Z fájlt, ha az kisebb mint a .gz fájl."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "gzip(1), zmore(1), zdiff(1), zgrep(1), zforce(1), gzexe(1), compress(1)"
msgid ""
"B<gzip>(1), B<zmore>(1), B<zdiff>(1), B<zgrep>(1), B<zforce>(1), "
"B<gzexe>(1), B<compress(1)>"
msgstr ""
"gzip(1), zmore(1), zdiff(1), zgrep(1), zforce(1), gzexe(1), compress(1)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "HIBÁK"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Znew> does not maintain the time stamp with the -P option if "
#| "I<cpmod(1)> is not available and I<touch(1)> does not support the -r "
#| "option."
msgid ""
"If the B<-P> option is used, B<znew> does not maintain the timestamp if "
"B<touch>(1)  does not support the B<-r> option, and does not maintain "
"permissions if B<chmod>(1)  does not support the B<--reference> option."
msgstr ""
"A I<znew> nem tartja meg a fájlok dátumát és idejét a -P opció "
"használatakor, ha a I<cpmod(1)> nem elérhető, és a I<touch(1)> nem támogatja "
"a -r opciót."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"I<Znew> recompresses files from .Z (compress) format to .gz (gzip) format.  "
"If you want to recompress a file already in gzip format, rename the file to "
"force a .Z extension then apply znew."
msgstr ""
"A I<znew> újratömöríti a .Z (compress) formátumú fájlokat .gz (gzip) "
"formátumú fájlokká. Ha olyan fájlt szeretnénk újratömöríteni, amely már "
"eleve gzip formátumú, nevezzük át a fájlt .Z kiterjesztésűre, és azután "
"használjuk a znew parancsot."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "gzip(1), zmore(1), zdiff(1), zgrep(1), zforce(1), gzexe(1), compress(1)"
msgstr ""
"gzip(1), zmore(1), zdiff(1), zgrep(1), zforce(1), gzexe(1), compress(1)"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "I<Znew> does not maintain the time stamp with the -P option if "
#| "I<cpmod(1)> is not available and I<touch(1)> does not support the -r "
#| "option."
msgid ""
"If the B<-P> option is used, I<znew> does not maintain the timestamp if "
"I<touch>(1)  does not support the B<-r> option, and does not maintain "
"permissions if I<chmod>(1)  does not support the B<--reference> option."
msgstr ""
"A I<znew> nem tartja meg a fájlok dátumát és idejét a -P opció "
"használatakor, ha a I<cpmod(1)> nem elérhető, és a I<touch(1)> nem támogatja "
"a -r opciót."
