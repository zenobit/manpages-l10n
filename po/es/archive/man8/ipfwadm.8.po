# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Javier Peña Martínez <jpena@lander.es>, 1998.
# Pedro Pablo Fábrega Martínez <pfabrega@arrakis.es>, 1998.
# Carlos Arcos <webmaster@etea.net>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2020-11-24 18:45+01:00\n"
"PO-Revision-Date: 1998-10-15 00:21+0100\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: original/man8/ipfwadm.8:34
#, no-wrap
msgid "IPFWADM"
msgstr "IPFWADM"

#. type: TH
#: original/man8/ipfwadm.8:34
#, no-wrap
msgid "July 30, 1996"
msgstr "30 Julio 1996"

#. type: SH
#: original/man8/ipfwadm.8:35
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: original/man8/ipfwadm.8:37
msgid "ipfwadm - IP firewall and accounting administration"
msgstr "ipfwadm - Administración del cortafuegos y contabilidad IP"

#. type: SH
#: original/man8/ipfwadm.8:37
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: original/man8/ipfwadm.8:39
msgid "B<ipfwadm -A >command parameters [options]"
msgstr "B<ipfwadm -A >parámetros [opciones]"

#. type: Plain text
#: original/man8/ipfwadm.8:41
msgid "B<ipfwadm -I >command parameters [options]"
msgstr "B<ipfwadm -I >parámetros [opciones]"

#. type: Plain text
#: original/man8/ipfwadm.8:43
msgid "B<ipfwadm -O >command parameters [options]"
msgstr "B<ipfwadm -O >parámetros [opciones]"

#. type: Plain text
#: original/man8/ipfwadm.8:45
msgid "B<ipfwadm -F >command parameters [options]"
msgstr "B<ipfwadm -F >parámetros [opciones]"

#. type: Plain text
#: original/man8/ipfwadm.8:47
msgid "B<ipfwadm -M >[ -l | -s ] [options]"
msgstr "B<ipfwadm -M >[-l | -s] [opciones]"

#. type: SH
#: original/man8/ipfwadm.8:47
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: original/man8/ipfwadm.8:60
msgid ""
"B<Ipfwadm> is used to set up, maintain, and inspect the IP firewall and "
"accounting rules in the Linux kernel.  These rules can be divided into 4 "
"different categories: accounting of IP packets, the IP input firewall, the IP "
"output firewall, and the IP forwarding firewall.  For each of these "
"categories, a separate list of rules is maintained.  See I<ipfw>(4)  for more "
"details."
msgstr ""
"B<Ipfwadm> se utiliza para configurar, mantener e inspeccionar los "
"cortafuegos IP y las reglas de contabilidad del núcleo Linux.  Estas reglas "
"se pueden dividir en cuatro categorías diferentes: contabilidad de paquetes "
"IP, cortafuegos de entrada IP, cortafuegos de salida IP y cortafuegos de "
"reenvío.  Para cada una de estas categorías se mantiene una lista separada de "
"reglas."

#. type: SH
#: original/man8/ipfwadm.8:60
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: Plain text
#: original/man8/ipfwadm.8:64
msgid ""
"The options that are recognized by B<ipfwadm> can be divided into several "
"different groups."
msgstr ""
"Las opciones reconocidas por I<ipfwadm> se dividen en varios grupos "
"diferentes."

#. type: SS
#: original/man8/ipfwadm.8:64
#, no-wrap
msgid "CATEGORIES"
msgstr "CATEGORÍAS"

#. type: Plain text
#: original/man8/ipfwadm.8:67
msgid ""
"The following flags are used to select the category of rules to which the "
"given command applies:"
msgstr ""
"Las siguientes opciones se usan para seleccionar la categoría de reglas a las "
"que se aplican los comandos:"

#. type: TP
#: original/man8/ipfwadm.8:67
#, no-wrap
msgid "B<-A> [I<direction>]"
msgstr "B<-A>I< [dirección]>"

#. type: Plain text
#: original/man8/ipfwadm.8:81
msgid ""
"IP accounting rules.  Optionally, a I<direction> can be specified (I<in>, "
"I<out>, or I<both>), indicating whether only incoming or outgoing packets "
"should be counted.  The default direction is I<both>."
msgstr ""
"Reglas de contabilidad IP. Opcionalmente se puede especificar un I<sentido> "
"(I<in>, I<out>, I<both>), indicando si sólo se deben contar paquetes de "
"entrada, de salida o de ambos tipos. La opción por defecto es I<both>."

#. type: TP
#: original/man8/ipfwadm.8:81
#, no-wrap
msgid "B<-I>"
msgstr "B<-I>"

#. type: Plain text
#: original/man8/ipfwadm.8:84
msgid "IP input firewall rules."
msgstr "Reglas de entrada al cortafuegos IP."

#. type: TP
#: original/man8/ipfwadm.8:84
#, no-wrap
msgid "B<-O>"
msgstr "B<-O>"

#. type: Plain text
#: original/man8/ipfwadm.8:87
msgid "IP output firewall rules."
msgstr "Reglas de salida del cortafuegos IP."

#. type: TP
#: original/man8/ipfwadm.8:87
#, no-wrap
msgid "B<-F>"
msgstr "B<-F>"

#. type: Plain text
#: original/man8/ipfwadm.8:90
msgid "IP forwarding firewall rules."
msgstr "Reglas de reenvío del cortafuegos IP."

#. type: TP
#: original/man8/ipfwadm.8:90
#, no-wrap
msgid "B<-M>"
msgstr "B<-M>"

#. type: Plain text
#: original/man8/ipfwadm.8:99
msgid ""
"IP masquerading administration.  This category can only be used in "
"combination with the B<-l> (list) or B<-s> (set timeout values) command."
msgstr ""
"Administración de \"IP masquerading\". Esta categoría sólo se puede usar en "
"combinación con el comando B<-l> (list) o el comando B<-s> (fijar tiempo de "
"expiración)."

#. type: Plain text
#: original/man8/ipfwadm.8:101
msgid "Exactly one of these options has to be specified."
msgstr "Se tiene que especificar exactamente una de estas opciones."

#. type: SS
#: original/man8/ipfwadm.8:101
#, no-wrap
msgid "COMMANDS"
msgstr "COMANDOS"

#. type: Plain text
#: original/man8/ipfwadm.8:105
msgid ""
"The next options specify the specific action to perform.  Only one of them "
"can be specified on the command line, unless something else is listed in the "
"description."
msgstr ""
"Las siguientes opciones especifican la acción concreta que hay que realizar. "
"Sólo se puede especificar una de ellas en la línea de comandos, salvo que se "
"indique otra cosa en la descripción."

#. type: TP
#: original/man8/ipfwadm.8:105
#, no-wrap
msgid "B<-a> [I<policy>]"
msgstr "B<-a>I< [comportamiento]>"

#. type: Plain text
#: original/man8/ipfwadm.8:117
msgid ""
"Append one or more rules to the end of the selected list.  For the accounting "
"chain, no policy should be specified.  For firewall chains, it is required to "
"specify one of the following policies: I<accept>, I<deny>, I<reject>, or "
"I<masquerade>.  When the source and/or destination names resolve to more than "
"one address, a rule will be added for each possible address combination."
msgstr ""
"Añade una o más reglas al final de la lista seleccionada. Para la "
"contabilidad no se puede especificar ningún comportamiento. Para el "
"cortafuegos es necesario especificar una de las siguientes políticas: "
"I<accept>, I<masquerade> (válida sólo para reglas de reenvío), I<deny> o "
"I<reject>. Cuando los nombre de origen y/o destino se resuelven con más de "
"una dirección, se añadirá una regla para cada posible combinación."

#. type: TP
#: original/man8/ipfwadm.8:117
#, no-wrap
msgid "B<-i> [I<policy>]"
msgstr "B<-i>I< [comportamiento]>"

#. type: Plain text
#: original/man8/ipfwadm.8:123
msgid ""
"Insert one or more rules at the beginning of the selected list.  See the "
"description of the B<-a> command for more details."
msgstr ""
"Inserta una o más reglas al principio de la lista seleccionada. Vea la "
"descripción del comando B<-a> para más detalles."

#. type: TP
#: original/man8/ipfwadm.8:123
#, no-wrap
msgid "B<-d> [I<policy>]"
msgstr "B<-d>I< [comportamiento]>"

#. type: Plain text
#: original/man8/ipfwadm.8:131
msgid ""
"Delete one or more entries from the selected list of rules.  The semantics "
"are equal to those of the append/insert commands.  The specified parameters "
"should exactly match the parameters given with an append or insert command, "
"otherwise no match will be found and the rule will not be removed from the "
"list.  Only the first matching rule in the list will be deleted."
msgstr ""
"Borra una o más entradas al comienzo de la lista seleccionada. La semántica "
"es idéntica a la de los comandos anteriores. Los parámetros especificados "
"deben coincidir exactamente con los dados en los comandos añadir o insertar. "
"En otro caso, si no se ajusta a nada, no se borrará ninguna regla. Sólo se "
"borra la primera coincidencia."

#. type: TP
#: original/man8/ipfwadm.8:131
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: original/man8/ipfwadm.8:152
msgid ""
"List all the rules in the selected list.  This command may be combined with "
"the B<-z> (reset counters to zero) command.  In that case, the packet and "
"byte counters will be reset immediately after listing their current values.  "
"Unless the B<-x> option is present, packet and byte counters (if listed) will "
"be shown as I<number>K or I<number>M, where 1K means 1000 and 1M means 1000K "
"(rounded to the nearest integer value).  See also the B<-e> and B<-x> flags "
"for more capabilities."
msgstr ""
"Lista todas las reglas de la lista seleccionada. Este comando se puede "
"combinar con el comando B<-z> (reiniciar el contador a cero). En ese caso los "
"contadores de bytes y de paquetes se reinician inmediatamente tras mostrar "
"sus valores actuales. Salvo que la opción B<-x> esté presente, los contadores "
"de paquetes (si son listados)  se mostrarán como númeroK o númeroM donde 1K "
"significa 1000 y 1M significa 1000K (redondeado al valor entero más cercano). "
"Vea también las opciones B<-e> y B<-x> para ver más posibilidades."

#. type: TP
#: original/man8/ipfwadm.8:152
#, no-wrap
msgid "B<-z>"
msgstr "B<-z>"

#. type: Plain text
#: original/man8/ipfwadm.8:159
msgid ""
"Reset the packet and byte counters of all the rules in selected list.  This "
"command may be combined with the B<-l> (list) command."
msgstr ""
"Reinicia los contadores de bytes y de paquetes de todas las reglas de la "
"lista seleccionada. Este comando se puede combinar con el comando B<-l> "
"(list)."

#. type: TP
#: original/man8/ipfwadm.8:159
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: original/man8/ipfwadm.8:162
msgid "Flush the selected list of rules."
msgstr "Vacía la lista de reglas seleccionada."

#. type: TP
#: original/man8/ipfwadm.8:162
#, no-wrap
msgid "B<-p>I< policy>"
msgstr "B<-p>I< política>"

#. type: Plain text
#: original/man8/ipfwadm.8:180
msgid ""
"Change the default policy for the selected type of firewall.  The given "
"policy has to be one of I<accept>, I<deny>, I<reject>, or I<masquerade>.  The "
"default policy is used when no matching rule is found.  This operation is "
"only valid for IP firewalls, that is, in combination with the B<-I>, B<-O>, "
"or B<-F> flag."
msgstr ""
"Cambia la política por defecto para el tipo seleccionado de cortafuegos. La "
"política dada tiene que ser una de I<accept,> I<masquerade> (sólo válida para "
"reglas de reenvío), I<deny> o I<reject.> La política por defecto se utiliza "
"cuando no se encuentra ninguna regla que se ajuste. Esta operación sólo es "
"válida para cortafuegos IP, esto es, en combinación con las opciones B<-I>, "
"B<-O> o B<-F.>"

#. type: TP
#: original/man8/ipfwadm.8:180
#, no-wrap
msgid "B<-s>I< tcp tcpfin udp>"
msgstr "B<-s>I< tcp tcpfin udp>"

#. type: Plain text
#: original/man8/ipfwadm.8:191
msgid ""
"Change the timeout values used for masquerading.  This command always takes 3 "
"parameters, representing the timeout values (in seconds) for TCP sessions, "
"TCP sessions after receiving a FIN packet, and UDP packets, respectively.  A "
"timeout value 0 means that the current timeout value of the corresponding "
"entry is preserved.  This operation is only allowed in combination with the "
"B<-M> flag."
msgstr ""
"Cambia los valores de caducidad (timeout) usados en el enmascaramiento.  Este "
"comando siempre toma tres parámetros, que representan valores de caducidad "
"(en segundos) para sesiones TCP, sesiones TCP tras recibir un paquete FIN y "
"paquetes UDP, respectivamente.  Un valor de caducidad 0 significa que el "
"valor actual de caducidad, de la entrada correspondiente, se preserva.  Esta "
"operación sólo está permitida en combinación con la opción B<-M>."

#. type: TP
#: original/man8/ipfwadm.8:191
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: original/man8/ipfwadm.8:202
msgid ""
"Check whether this IP packet would be accepted, denied, or rejected by the "
"selected type of firewall.  This operation is only valid for IP firewalls, "
"that is, in combination with the B<-I>, B<-O>, or B<-F> flag."
msgstr ""
"Comprueba si este paquete IP sería aceptado, denegado o rechazado por el tipo "
"de cortafuegos seleccionado. Esta operación sólo es válida para cortafuegos "
"IP en combinación con las opciones B<-I>, B<-O> o B<-F>."

#. type: TP
#: original/man8/ipfwadm.8:202
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: original/man8/ipfwadm.8:206
msgid "Help.  Give a (currently very brief) description of the command syntax."
msgstr ""
"Ayuda. Da una descripción (actualmente breve) de la sintaxis del comando."

#. type: SS
#: original/man8/ipfwadm.8:206
#, no-wrap
msgid "PARAMETERS"
msgstr "PARÁMETROS"

#. type: Plain text
#: original/man8/ipfwadm.8:209
#, fuzzy
msgid ""
"The following parameters can be used in combination with the append, insert, "
"delete, or check commands:"
msgstr ""
"Los siguientes parámetros se pueden usar en combinación con los comandos B<-"
"a>, B<-i>, B<-d> o B<-c>:"

#. type: TP
#: original/man8/ipfwadm.8:209
#, no-wrap
msgid "B<-P >I<protocol>"
msgstr "B<-P >I<protocolo>"

#. type: Plain text
#: original/man8/ipfwadm.8:224
msgid ""
"The protocol of the rule or of the packet to check.  The specified protocol "
"can be one of I<tcp>, I<udp>, I<icmp>, or I<all>.  Protocol I<all> will match "
"with all protocols and is taken as default when this option is omitted.  "
"I<All> may not be used in in combination with the check command."
msgstr ""
"El protocolo de la regla o del paquete a comprobar.  El protocolo "
"especificado puede ser I<tcp>, I<udp>, I<icpm> o I<all.> El protocolo I<all> "
"se adaptará a todos los protocolos y se toma por defecto cuando se omite esta "
"opción.  I<all> no se puede usar en combinación con el comando B<-c.>"

#. type: TP
#: original/man8/ipfwadm.8:224
#, no-wrap
msgid "B<-S >I<address>[/I<mask>] [I<port> ...]"
msgstr "B<-S >I<dirección>[/I<máscara>] [I<puerto> ...]"

#. type: Plain text
#: original/man8/ipfwadm.8:237
msgid ""
"Source specification (optional).  I<Address> can be either a hostname, a "
"network name, or a plain IP address.  The I<mask> can be either a network "
"mask or a plain number, specifying the number of 1's at the left side of the "
"network mask.  Thus, a mask of I<24> is equivalent with I<255.255.255.0>."
msgstr ""
"Especificación de origen (obligatorio). La I<dirección> puede ser bien un "
"nombre de host, un nombre de red o una dirección IP concreta. La I<máscara> "
"puede ser una máscara de red o un número que indique el número de bits con "
"valor 1 a la izquierda de la máscara de red. Es decir, son equivalentes la "
"máscara I<255.255.255.0> y el número I<24>."

#. type: Plain text
#: original/man8/ipfwadm.8:251
msgid ""
"The source may include one or more port specifications or ICMP types.  Each "
"of them can either be a service name, a port number, or a (numeric) ICMP "
"type.  In the rest of this paragraph, a I<port> means either a port "
"specification or an ICMP type.  One of these specifications may be a range of "
"ports, in the format I<port>:I<port>.  Furthermore, the total number of ports "
"specified with the source and destination addresses should not be greater "
"than B<IP_FW_MAX_PORTS> (currently 10).  Here a port range counts as 2 ports."
msgstr ""
"El origen puede incluir una o más especificaciones de puertos o tipos ICMP. "
"Cada uno de ellos puede ser un nombre de servicio, número de puerto o un tipo "
"ICPM (numérico). En el resto de este párrafo, I<puerto> significa o una "
"especificación de puerto o un tipo ICPM. Una de estas especificaciones puede "
"ser un rango de puertos, con el formato I<puerto>:I<puerto>. Además, el "
"número total de puertos especificados con las direcciones origen y destino no "
"debe ser mayor que B<IP_FW_MAX_PORTS> (actualmente 10). Aquí un rango de "
"puertos cuenta como dos puertos."

#. type: Plain text
#: original/man8/ipfwadm.8:272
msgid ""
"Packets not being the first fragment of a TCP, UDP, or ICMP packet are always "
"accepted by the firewall.  For accounting purposes, these second and further "
"fragments are treated special, to be able to count them in some way.  The "
"port number 0xFFFF (65535) is used for a match with the second and further "
"fragments of TCP or UDP packets.  These packets will be treated for "
"accounting purposes as if both their port numbers are 0xFFFF.  The number "
"0xFF (255) is used for a match with the second and further fragments of ICMP "
"packets.  These packets will be treated for acounting purposes as if their "
"ICMP types are 0xFF.  Note that the specified command and protocol may imply "
"restrictions on the ports to be specified.  Ports may only be specified in "
"combination with the I<tcp>, I<udp>, or I<icmp> protocol."
msgstr ""
"Los paquetes que no son el primer fragmento de un paquete B<TCP>, B<UDP> o "
"B<ICMP> son siempre aceptados por el cortafuegos.  Por motivos de "
"contabilidad, estos segundos y posteriores fragmentos se tratan de forma "
"especial para poderlos contar de alguna forma. El puerto número 0xFFFF "
"(65535)  se usa para ajustarse con el segundo y siguientes fragmentos de "
"paquetes TCP o UDP. Estos paquetes se tratarán para propósitos de "
"contabilidad como si sus puertos fueran 0xFFFF. El número 0xFF (255) se usa "
"para ajustarse con el segundo y siguientes fragmentos para contabilidad de "
"paquetes ICPM. Estos paquetes se tratarán, para propósitos de contabilidad, "
"como si sus tipos ICPM fueran 0xFF. Observe que los comando y protocolo "
"especificados pueden implicar restricciones sobre el puerto que sea "
"especificado en combinación con los protocolos I<tcp>, I<udp> o I<icpm.> "
"También, cuando se especifica el comando B<-c>, se requiere exactamente un "
"puerto."

#. type: Plain text
#: original/man8/ipfwadm.8:278
msgid ""
"When this option is omitted, the default address/mask I<0.0.0.0/0> (matching "
"with any address) is used as source address.  This option is required in "
"combination with the check command, in which case also exactly one port has "
"to be specified."
msgstr ""

#. type: TP
#: original/man8/ipfwadm.8:278
#, no-wrap
msgid "B<-D >I<address>[/I<mask>] [I<port> ...]"
msgstr "B<-D >I<dirección>[/I<máscara>] [I<puerto> ...]>"

#. type: Plain text
#: original/man8/ipfwadm.8:290
msgid ""
"Destination specification (optional).  See the desciption of the B<-S> "
"(source) flag for a detailed description of the syntax, default values, and "
"other requirements.  Note that ICMP types are not allowed in combination with "
"the B<-D> flag: ICMP types can only be specified after the the B<-S> flag."
msgstr ""
"Especificaciones de destino (obligatorio). Vea la descripción de la opción B<-"
"S> (origen) para una descripción detallada de la sintaxis. Observe que los "
"tipos B<ICMP> no están permitidos en combinación con la opción B<-D>; los "
"tipos B<ICMP> sólo se pueden especificar tras la bandera B<-S>."

#. type: TP
#: original/man8/ipfwadm.8:290
#, no-wrap
msgid "B<-V >I<address>"
msgstr "B<-V >I<dirección>"

#. type: Plain text
#: original/man8/ipfwadm.8:302
msgid ""
"Optional address of an interface via which a packet is received, or via which "
"is packet is going to be sent.  I<Address> can be either a hostname or a "
"plain IP address.  When a hostname is specified, it should resolve to exactly "
"one IP address.  When this option is omitted, the address I<0.0.0.0> is "
"assumed, which has a special meaning and will match with any interface "
"address.  For the check command, this option is mandatory."
msgstr ""
"La dirección opcional de un interfaz a través del cual se envía o recibe un "
"paquete.  I<dirección> puede ser un nombre de host o una dirección IP. Cuando "
"se especifica un nombre de host, éste se debe resolver a exactamente una "
"dirección IP. Cuando se omite esta opción, se supone la dirección I<0.0.0.0>, "
"que tiene un significado especial y se ajustará a cualquier dirección de "
"interfaz. Para el comando B<-c>, esta opción es obligatoria."

#. type: TP
#: original/man8/ipfwadm.8:302
#, no-wrap
msgid "B<-W >I<name>"
msgstr "B<-W >I<nombre>"

#. type: Plain text
#: original/man8/ipfwadm.8:309
msgid ""
"Optional name of an interface via which a packet is received, or via which is "
"packet is going to be sent.  When this option is omitted, the empty string is "
"assumed, which has a special meaning and will match with any interface name.  "
"For the check command, this option is mandatory."
msgstr ""
"Nombre opcional de un interfaz a través del cual se envían o reciben "
"paquetes. Cuando se omite, se supone una cadena de caracteres vacía, que "
"tiene un significado especial y se ajustará a cualquier nombre de interfaz. "
"Para el comando B<-c>, esta opción es obligatoria."

#. type: SS
#: original/man8/ipfwadm.8:309
#, no-wrap
msgid "OTHER OPTIONS"
msgstr "OTRAS OPCIONES"

#. type: Plain text
#: original/man8/ipfwadm.8:311
msgid "The following additional options can be specified:"
msgstr "Se pueden especificar las siguientes opciones adicionales:"

#. type: TP
#: original/man8/ipfwadm.8:311
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: original/man8/ipfwadm.8:317
msgid ""
"Bidirectional mode.  The rule will match with IP packets in both directions.  "
"This option is only valid in combination with the append, insert, or delete "
"commands."
msgstr ""
"Modo Bidireccional.  La regla se ajustará con paquetes IP en ambas "
"direcciones. Esta opción sólo es válida en combinación con los comandos I<-"
"a>, I<-i> o I<-d>."

#. type: TP
#: original/man8/ipfwadm.8:317
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: original/man8/ipfwadm.8:329
msgid ""
"Extended output.  This option makes the list command also show the interface "
"address and the rule options (if any).  For firewall lists, also the packet "
"and byte counters (the default is to only show these counters for the "
"accounting rules) and the TOS masks will be listed.  When used in combination "
"with B<-M>, information related to delta sequence numbers will also be "
"listed.  This option is only valid in combination with the list command."
msgstr ""
"Salida extendida.  Esta opción hace al comando B<-l> mostrar también la "
"dirección del interfaz y las opciones de la regla (si existe). Para las "
"listas del cortafuegos, también se mostrarán los contadores de bytes y "
"paquetes (por defecto sólo se muestran los contadores para las reglas de "
"contabilidad) y se muestran las máscaras TOS. Cuando se usa en combinación "
"con B<-M>, también mostrará la información relacionada con la secuencia de "
"números delta. Esta opción sólo es válida en combinación con el comando B<-l>."

#. type: TP
#: original/man8/ipfwadm.8:329
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: original/man8/ipfwadm.8:335
#, fuzzy
msgid ""
"Only match TCP packets with the ACK bit set (this option will be ignored for "
"packets of other protocols).  This option is only valid in combination with "
"the append, insert, or delete command."
msgstr ""
"Ajustar sólo a paquetes TCP con el bit ACK activo.  Esta opción sólo es "
"válida en combinación con los comandos B<-a>, B<-i> o B<-d>, y el protocolo "
"TCP."

#. type: TP
#: original/man8/ipfwadm.8:335
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: original/man8/ipfwadm.8:352
msgid ""
"Masquerade packets accepted for forwarding.  When this option is set, packets "
"accepted by this rule will be masqueraded as if they originated from the "
"local host.  Furthermore, reverse packets will be recognized as such and they "
"will be demasqueraded automatically, bypassing the forwarding firewall.  This "
"option is only valid in forwarding firewall rules with policy I<accept> (or "
"when specifying I<accept> as default policy)  and can only be used when the "
"kernel is compiled with B<CONFIG_IP_MASQUERADE> defined."
msgstr ""
"Enmascaramiento de paquetes aceptados para reenvío.  Cuando se utiliza esta "
"opción, los paquetes aceptados por esta regla serán enmascarados como si "
"fueran originales del host local.  Además, los paquetes de respuesta serán "
"reconocidos como tales y serán desenmascarados automáticamente pasando el "
"cortafuegos de reenvío.  Esta opción es sólo válida para las reglas de "
"reenvío con comportamiento I<accept> (o cuando se haya especificado I<accept> "
"como el comportamiento por defecto), y sólo se puede usar cuando se compila "
"el núcleo con la opción B<CONFIG_IP_MASQUERADE>."

#. type: TP
#: original/man8/ipfwadm.8:352
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: original/man8/ipfwadm.8:358
msgid ""
"Numeric output.  IP addresses and port numbers will be printed in numeric "
"format.  By default, the program will try to display them as host names, "
"network names, or services (whenever applicable)."
msgstr ""
"Salida numérica.  Las direcciones IP y números se imprimirán en formato "
"numérico. Por defecto, el programa intentará mostrarlos como nombres de host, "
"nombres de red o servicios (cuando sea aplicable)."

#. type: TP
#: original/man8/ipfwadm.8:358
#, no-wrap
msgid "B<-o>"
msgstr "B<-o>"

#. type: Plain text
#: original/man8/ipfwadm.8:371
msgid ""
"Turn on kernel logging of matching packets.  When this option is set for a "
"rule, the Linux kernel will print some information of all matching packets "
"(like most IP header fields) via I<printk>().  This option will only be "
"effective when the Linux kernel is compiled with "
"B<CONFIG_IP_FIREWALL_VERBOSE> defined.  This option is only valid in "
"combination with the append, insert or delete command."
msgstr ""
"Activa el registro del núcleo de paquetes ajustados.  Cuando se pone esta "
"opción para una regla, el núcleo de Linux imprimirá cierta información básica "
"de todos los paquetes que se ajusten a ella mediante I<printk>().  Esta "
"opción sólo será efectiva cuando se compile el núcleo con la opción "
"B<CONFIG_IP_FIREWALL_VERBOSE>.  Esta opción sólo es válida en combinación con "
"los comandos B<-a>, B<-i> o B<-d>."

#. type: TP
#: original/man8/ipfwadm.8:371
#, no-wrap
msgid "B<-r >[I<port>]"
msgstr "B<-r >[I<puerto>]"

#. type: Plain text
#: original/man8/ipfwadm.8:385
msgid ""
"Redirect packets to a local socket.  When this option is set, packets "
"accepted by this rule will be redirected to a local socket, even if they were "
"sent to a remote host.  If the specified redirection port is 0, which is the "
"default value, the destination port of a packet will be used as the "
"redirection port.  This option is only valid in input firewall rules with "
"policy I<accept> and can only be used when the Linux kernel is compiled with "
"B<CONFIG_IP_TRANSPARENT_PROXY> defined."
msgstr ""
"Redirecciona paquetes a un conector (socket) local. Cuanto se utiliza esta "
"opción, los paquetes aceptados por la regla serán redireccionados a un "
"conector local, incluso si fueran redireccionados a un host remoto. Si el "
"puerto redireccionado es 0, que es el valor por defecto, se usará el puerto "
"destino del paquete como el puerto de redirección. Esta opción es sólo válida "
"en las reglas de entrada del cortafuegos con comportamiento I<accept> y sólo "
"puede ser utilizada cuando el núcleo de Linux está compilado con la opción "
"B<CONFIG_IP_TRANSPARENT_PROXY>."

#. type: TP
#: original/man8/ipfwadm.8:385
#, no-wrap
msgid "B<-t >I<andmask xormask>"
msgstr "B<-t >I<andmask xormask>"

#. type: Plain text
#: original/man8/ipfwadm.8:397
#, fuzzy
msgid ""
"Masks used for modifying the TOS field in the IP header.  When a packet is "
"accepted (with or without masquerading) by a firewall rule, its TOS field is "
"first bitwise and'ed with first mask and the result of this will be bitwise "
"xor'ed with the second mask.  The masks should be specified as hexadecimal 8-"
"bit values.  This option is only valid in combination with the append, insert "
"or delete command and will have no effect when used in combination with "
"accounting rules or firewall rules for rejecting or denying a packet."
msgstr ""
"Máscara utilizada para modificar el campo TOS en la cabecera IP. Cuando un "
"paquete se acepta (con o sin masquerade) por una regla del cortafuegos, a su "
"campo TOS primero se le hace un B<Y>-lógico con la máscara I<andmask> y al "
"resultado se le aplica un B<O>-lógico exclusivo con la máscara I<xormask>.  "
"La máscara se debe especificar en valores de 8 bits hexadecimales. Esta "
"opción sólo es válida en combinación con los comandos B<-a,> B<-i> o B<-d>, y "
"no tendrá efectos cuando se utilice en combinación con reglas de contabilidad "
"o de cortafuegos para rechazar o denegar un paquete."

#. type: TP
#: original/man8/ipfwadm.8:397
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: original/man8/ipfwadm.8:404
msgid ""
"Verbose output.  Print detailed information of the rule or packet to be "
"added, deleted, or checked.  This option will only have effect with the "
"append, insert, delete, or check command."
msgstr ""
"Salida detallada.  Imprime información detallada de la regla o paquete "
"añadido, borrado o comprobado.  Esta opción sólo tendrá efecto con los "
"comandos B<-a>, B<-i>, B<-d> o B<-c>."

#. type: TP
#: original/man8/ipfwadm.8:404
#, no-wrap
msgid "B<-x>"
msgstr "B<-x>"

#. type: Plain text
#: original/man8/ipfwadm.8:414
msgid ""
"Expand numbers.  Display the exact value of the packet and byte counters, "
"instead of only the rounded number in K's (multiples of 1000)  or M's "
"(multiples of 1000K).  This option will only have effect when the counters "
"are listed anyway (see also the B<-e> option)."
msgstr ""
"Expande números.  Muestra el valor exacto de los contadores de bytes y de "
"paquetes, en lugar de sólo los números redondeados a múltiplos de 1K o de 1M "
"(múltiplo de 1000K). Esta opción sólo tendrá efecto cuando se muestren los "
"contadores de cualquier forma (vea la opción B<-e>)."

#. type: TP
#: original/man8/ipfwadm.8:414
#, no-wrap
msgid "B<-y>"
msgstr "B<-y>"

#. type: Plain text
#: original/man8/ipfwadm.8:420
msgid ""
"Only match TCP packets with the SYN bit set and the ACK bit cleared (this "
"option will be ignored for packets of other protocols).  This option is only "
"valid in combination with the append, insert, or delete command."
msgstr ""
"Solo ajusta paquetes TCP con el bit SYN activado y el bit ACK desactivado. "
"Esta opción sólo es válida en combinación con los comandos B<-a>, B<-i> o B<-"
"d>, y el protocolo TCP."

#. type: SH
#: original/man8/ipfwadm.8:420
#, no-wrap
msgid "FILES"
msgstr "FICHEROS"

#. type: Plain text
#: original/man8/ipfwadm.8:422
msgid "I</proc/net/ip_acct>"
msgstr "I</proc/net/ip_acct>"

#. type: Plain text
#: original/man8/ipfwadm.8:424
msgid "I</proc/net/ip_input>"
msgstr "I</proc/net/ip_input>"

#. type: Plain text
#: original/man8/ipfwadm.8:426
msgid "I</proc/net/ip_output>"
msgstr "I</proc/net/ip_output>"

#. type: Plain text
#: original/man8/ipfwadm.8:428
msgid "I</proc/net/ip_forward>"
msgstr "I</proc/net/ip_forward>"

#.  .SH BUGS
#. type: Plain text
#: original/man8/ipfwadm.8:431
msgid "I</proc/net/ip_masquerade>"
msgstr "I</proc/net/ip_masquerade>"

#. type: SH
#: original/man8/ipfwadm.8:431
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: original/man8/ipfwadm.8:433
msgid "ipfw(4)"
msgstr "ipfw(4)"

#. type: SH
#: original/man8/ipfwadm.8:433
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: original/man8/ipfwadm.8:435
msgid "Jos Vos E<lt>jos@xos.nlE<gt>"
msgstr "Jos Vos E<lt>jos@xos.nlE<gt>"

#. type: Plain text
#: original/man8/ipfwadm.8:436
msgid "X/OS Experts in Open Systems BV, Amsterdam, The Netherlands"
msgstr "X/OS Experts in Open Systems BV, Amsterdam, The Netherlands"
