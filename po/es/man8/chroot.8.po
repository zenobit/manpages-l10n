# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marcos Fouces <marcos@debian.org>, 2021-2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2022-10-03 15:28+0200\n"
"PO-Revision-Date: 2022-10-04 00:07+0200\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "CHROOT"
msgstr "CHROOT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Septiembre de 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "chroot - run command or interactive shell with special root directory"
msgstr ""
"chroot - ejecuta una orden o una shell interactiva con un directorio raiz "
"especial"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NEWROOT \\/>[I<\\,COMMAND \\/>[I<\\,ARG\\/"
">]...]"
msgstr ""
"B<chroot> [I<\\,OPCIÓN\\/>] I<\\,NUEVODIRRAIZ \\/>[I<\\,ORDEN \\/>[I<\\,"
"ARG\\/>]...]"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<chroot> I<\\,OPTION\\/>"
msgstr "B<chroot> I<\\,OPCIÓN\\/>"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Run COMMAND with root directory set to NEWROOT."
msgstr "Ejecuta ORDEN con NUEVODIRRAIZ como directorio raiz"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<--groups>=I<\\,G_LIST\\/>"
msgstr "B<--groups>=I<\\,G_LISTA\\/>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "specify supplementary groups as g1,g2,..,gN"
msgstr "define grupos auxiliares como g1,g2... gN"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<--userspec>=I<\\,USER\\/>:GROUP"
msgstr "B<--userspec>=I<\\,USUARIO\\/>:GRUPO"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "specify user and group (ID or name) to use"
msgstr "define los nombres de usuario y grupo que se van a emplear"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<--skip-chdir>"
msgstr "B<--skip-chdir>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "do not change working directory to '/'"
msgstr "no cambia el directorio de trabajo a '/'"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "display this help and exit"
msgstr "muestra la ayuda y finaliza"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "output version information and exit"
msgstr "muestra la versión del programa y finaliza"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"If no command is given, run '\"$SHELL\" B<-i>' (default: '/bin/sh B<-i>')."
msgstr ""
"Si no se proporciona ninguna orden, ejecuta '\"$SHELL\" B<-i>' (por defecto: "
"'/bin/sh B<-i>')."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Written by Roland McGrath."
msgstr "Escrito por Roland McGrath."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "INFORMAR DE ERRORES"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Ayuda en línea de GNU Coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Informe cualquier error de traducción a E<lt>https://translationproject.org/"
"team/es.htmlE<gt>"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: debian-bullseye
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licencia GPLv3+: GNU GPL "
"versión 3 o posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Esto es software libre: usted es libre de cambiarlo y redistribuirlo.  NO "
"HAY GARANTÍA, en la medida permitida por la legislación."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: debian-bullseye
msgid "chroot(2)"
msgstr "chroot(2)"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chrootE<gt>"
msgstr ""
"Documentación completa en E<lt>https://www.gnu.org/software/coreutils/"
"chrootE<gt>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "or available locally via: info \\(aq(coreutils) chroot invocation\\(aq"
msgstr ""
"también disponible localmente ejecutando: info \\(aq(coreutils) chroot "
"invocation\\(aq"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Septiembre de 2022"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-unstable
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licencia GPLv3+: GNU GPL "
"versión 3 o posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-unstable
msgid "B<chroot>(2)"
msgstr "B<chroot>(2)"
