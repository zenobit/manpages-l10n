# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:07+0100\n"
"PO-Revision-Date: 2023-02-28 18:56+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "getline"
msgstr "getline"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "getline, getdelim - delimited string input"
msgstr "getline, getdelim - entrada de cadena delimitada"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<ssize_t getline(char **restrict >I<lineptr>B<, size_t *restrict >I<n>B<,>\n"
"B<                FILE *restrict >I<stream>B<);>\n"
"B<ssize_t getdelim(char **restrict >I<lineptr>B<, size_t *restrict >I<n>B<,>\n"
"B<                int >I<delim>B<, FILE *restrict >I<stream>B<);>\n"
msgstr ""
"B<ssize_t getline(char **restrict >I<lineptr>B<, size_t *restrict >I<n>B<,>\n"
"B<                FILE *restrict >I<stream>B<);>\n"
"B<ssize_t getdelim(char **restrict >I<lineptr>B<, size_t *restrict >I<n>B<,>\n"
"B<                int >I<delim>B<, FILE *restrict >I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<getline>(), B<getdelim>():"
msgstr "B<getline>(), B<getdelim>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Desde glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Antes de glibc 2.10:\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<getline()> reads an entire line, storing the address of the buffer "
#| "containing the text into I<*lineptr>.  The buffer is null-terminated and "
#| "includes the newline character, if a newline delimiter was found."
msgid ""
"B<getline>()  reads an entire line from I<stream>, storing the address of "
"the buffer containing the text into I<*lineptr>.  The buffer is null-"
"terminated and includes the newline character, if one was found."
msgstr ""
"B<getline()> lee una línea entera, almacenando la dirección del buffer que "
"contiene el texto en I<*lineptr>.  Al final del buffer se coloca el valor "
"null y además se incluye el carácter nueva línea si se encontró el "
"delimitador nueva línea."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If I<*lineptr> is set to NULL before the call, then B<getline>()  will "
"allocate a buffer for storing the line.  This buffer should be freed by the "
"user program even if B<getline>()  failed."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<*lineptr> is B<NULL>, the B<getline()> routine will allocate a "
#| "buffer for containing the line, which must be freed by the user program.  "
#| "Alternatively, before calling B<getline()>, I<*lineptr> can contain a "
#| "pointer to a B<malloc()>-allocated buffer I<*n> bytes in size. If the "
#| "buffer is not large enough to hold the line read in, B<getline()> resizes "
#| "the buffer to fit with B<realloc()>, updating I<*lineptr> and I<*n> as "
#| "necessary. In either case, on a successful call, I<*lineptr> and I<*n> "
#| "will be updated to reflect the buffer address and size respectively."
msgid ""
"Alternatively, before calling B<getline>(), I<*lineptr> can contain a "
"pointer to a B<malloc>(3)-allocated buffer I<*n> bytes in size.  If the "
"buffer is not large enough to hold the line, B<getline>()  resizes it with "
"B<realloc>(3), updating I<*lineptr> and I<*n> as necessary."
msgstr ""
"Si I<*lineptr> es B<NULL>, la rutina B<getline()> reservará un buffer para "
"contener la línea, que debe ser liberado por el programa de usuario.  "
"Alternativamente, antes de llamar a B<getline()>, I<*lineptr> puede contener "
"un puntero a un buffer reservado via B<malloc()> de tamaño I<*n> bytes. Si "
"el buffer no es lo suficientemente grande para almacenar la línea leída, "
"B<getline()> redimensiona el buffer para que quepa con B<realloc()>, "
"actualizando I<*lineptr> y I<*n> si es necesario. En cualquier caso, si la "
"llamada tiene éxito, I<*lineptr> y I<*n> serán actualizados para reflejar la "
"dirección del buffer y el tamaño respectivamente."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In either case, on a successful call, I<*lineptr> and I<*n> will be updated "
"to reflect the buffer address and allocated size respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getdelim>()  works like B<getline>(), except that a line delimiter other "
"than newline can be specified as the I<delimiter> argument.  As with "
"B<getline>(), a delimiter character is not added if one was not present in "
"the input before end of file was reached."
msgstr ""
"B<getdelim>() funciona como B<getline>(), salvo que se puede especificar "
"otro delimitador de línea distinto de nueva línea en el argumento "
"I<delimiter>. Como con B<getline>(), no se añade un carácter delimitador si "
"no hay ninguno presente en la entrada antes de que se alcanze el fin del "
"fichero."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "On success, B<getline()> and B<getdelim()> return the number of "
#| "characters read, including the delimiter character, but not including the "
#| "terminating null character. This value can be used to handle embedded "
#| "null characters in the line read."
msgid ""
"On success, B<getline>()  and B<getdelim>()  return the number of characters "
"read, including the delimiter character, but not including the terminating "
"null byte (\\[aq]\\e0\\[aq]).  This value can be used to handle embedded "
"null bytes in the line read."
msgstr ""
"En caso de éxito, B<getline()> y B<getdelim()> devuelven el número de "
"caracteres leídos, incluyendo el carácter delimitador, pero sin incluir el "
"carácter null del final. Este valor puede usarse para manejar los carácters "
"null embebidos en la línea leída."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Both functions return -1 on failure to read a line (including end of file "
#| "condition)."
msgid ""
"Both functions return -1 on failure to read a line (including end-of-file "
"condition).  In the event of a failure, I<errno> is set to indicate the "
"error."
msgstr ""
"Ambas funciones devuelven -1 si fallan al leer una línea (incluyendo la "
"condición de fin de fichero)."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If I<*lineptr> was set to NULL before the call, then the buffer should be "
"freed by the user program even on failure."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Bad arguments (I<n> or I<lineptr> is NULL, or I<stream> is not valid)."
msgstr ""
"Parámetros incorrectos (I<n> o I<lineptr> son NULL, o I<stream> no es "
"válido)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Allocation or reallocation of the line buffer failed."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getline>(),\n"
"B<getdelim>()"
msgstr ""
"B<getline>(),\n"
"B<getdelim>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Both getline() and getdelim() are GNU extensions.  They are available "
#| "since libc 4.6.27."
msgid ""
"Both B<getline>()  and B<getdelim>()  were originally GNU extensions.  They "
"were standardized in POSIX.1-2008."
msgstr ""
"Tanto getline() como getdelim() son extensiones de GNU.  Están disponibles "
"desde libc 4.6.27."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    FILE *stream;\n"
"    char *line = NULL;\n"
"    size_t len = 0;\n"
"    ssize_t nread;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    FILE *stream;\n"
"    char *line = NULL;\n"
"    size_t len = 0;\n"
"    ssize_t nread;\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>fileE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>fileE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    stream = fopen(argv[1], \"r\");\n"
"    if (stream == NULL) {\n"
"        perror(\"fopen\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    stream = fopen(argv[1], \"r\");\n"
"    if (stream == NULL) {\n"
"        perror(\"fopen\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    while ((nread = getline(&line, &len, stream)) != -1) {\n"
"        printf(\"Retrieved line of length %zd:\\en\", nread);\n"
"        fwrite(line, nread, 1, stdout);\n"
"    }\n"
msgstr ""
"    while ((nread = getline(&line, &len, stream)) != -1) {\n"
"        printf(\"Retrieved line of length %zd:\\en\", nread);\n"
"        fwrite(line, nread, 1, stdout);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    free(line);\n"
"    fclose(stream);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    free(line);\n"
"    fclose(stream);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: getline.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<read>(2), B<fgets>(3), B<fopen>(3), B<fread>(3), B<scanf>(3)"
msgstr "B<read>(2), B<fgets>(3), B<fopen>(3), B<fread>(3), B<scanf>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GETLINE"
msgstr "GETLINE"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 Noviembre 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<ssize_t getline(char **>I<lineptr>B<, size_t *>I<n>B<, FILE *>I<stream>B<);>\n"
msgstr "B<ssize_t getline(char **>I<lineptr>B<, size_t *>I<n>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<ssize_t getdelim(char **>I<lineptr>B<, size_t *>I<n>B<, int >I<delim>B<, FILE *>I<stream>B<);>\n"
msgstr "B<ssize_t getdelim(char **>I<lineptr>B<, size_t *>I<n>B<, int >I<delim>B<, FILE *>I<stream>B<);>\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Desde glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Antes de glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"If I<*lineptr> is set to NULL and I<*n> is set 0 before the call, then "
"B<getline>()  will allocate a buffer for storing the line.  This buffer "
"should be freed by the user program even if B<getline>()  failed."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<getline()> and B<getdelim()> return the number of "
#| "characters read, including the delimiter character, but not including the "
#| "terminating null character. This value can be used to handle embedded "
#| "null characters in the line read."
msgid ""
"On success, B<getline>()  and B<getdelim>()  return the number of characters "
"read, including the delimiter character, but not including the terminating "
"null byte (\\(aq\\e0\\(aq).  This value can be used to handle embedded null "
"bytes in the line read."
msgstr ""
"En caso de éxito, B<getline()> y B<getdelim()> devuelven el número de "
"caracteres leídos, incluyendo el carácter delimitador, pero sin incluir el "
"carácter null del final. Este valor puede usarse para manejar los carácters "
"null embebidos en la línea leída."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Both functions return -1 on failure to read a line (including end of file "
#| "condition)."
msgid ""
"Both functions return -1 on failure to read a line (including end-of-file "
"condition).  In the event of an error, I<errno> is set to indicate the cause."
msgstr ""
"Ambas funciones devuelven -1 si fallan al leer una línea (incluyendo la "
"condición de fin de fichero)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EJEMPLO"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    while ((nread = getline(&line, &len, stream)) != -1) {\n"
"        printf(\"Retrieved line of length %zu:\\en\", nread);\n"
"        fwrite(line, nread, 1, stdout);\n"
"    }\n"
msgstr ""
"    while ((nread = getline(&line, &len, stream)) != -1) {\n"
"        printf(\"Retrieved line of length %zu:\\en\", nread);\n"
"        fwrite(line, nread, 1, stdout);\n"
"    }\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 Diciembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Páginas de manual de Linux 6.02"
