# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Enrique Ferrero Puchades <enferpuc@olemail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-12-25 19:25+0100\n"
"PO-Revision-Date: 1999-05-28 19:53+0200\n"
"Last-Translator: Enrique Ferrero Puchades <enferpuc@olemail.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "FIXWPPS"
msgstr "FIXWPPS"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "fixwpps - filter to fix WP documents so PSUtils work"
msgstr ""
"fixwpps - filtro que prepara documentos de WP para que las PSUtils funcionen"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<fixwpps> E<lt> I<WordPerfect.ps> E<gt> I<Fixed.ps>"
msgstr "B<fixwpps> E<lt> I<WordPerfect.ps> E<gt> I<Fixed.ps>"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"I<Fixwpps> is a I<perl> filter which \"fixes\" PostScript from WordPerfect "
"5.0 and 5.1 so that it works correctly with Angus Duggan's B<psutils> "
"package."
msgstr ""
"I<Fixwpps> es un flitro I<perl> que \"prepara\" el PostScript generado por "
"WordPerfect 5.0 and 5.1 para que funcione correctamente con el paquete "
"B<psutils> de Angus Duggan."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Copyright (C) Angus J. C. Duggan 1991-1995"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"B<psbook>(1), B<psselect>(1), B<pstops>(1), B<epsffit>(1), B<psnup>(1), "
"B<psresize>(1), B<psmerge>(1), B<fixscribeps>(1), B<getafm>(1), "
"B<fixdlsrps>(1), B<fixfmps>(1), B<fixpsditps>(1), B<fixpspps>(1), "
"B<fixtpps>(1), B<fixwfwps>(1), B<fixwpps>(1), B<fixwwps>(1), "
"B<extractres>(1), B<includeres>(1), B<showchar>(1)"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "TRADEMARKS"
msgstr "MARCAS REGISTRADAS"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> es una marca registrada de Adobe Systems Incorporated."
