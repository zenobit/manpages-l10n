# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:10+0100\n"
"PO-Revision-Date: 2019-09-05 21:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. #-#-#-#-#  archlinux: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  debian-bullseye: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  %%%LICENSE_START(GPLv2_ONELINE)
#.  Licensed under GNU General Public License version 2 (GPLv2)
#.  %%%LICENSE_END
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  debian-unstable: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  fedora-38: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  fedora-rawhide: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  mageia-cauldron: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  opensuse-leap-15-5: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  %%%LICENSE_START(GPLv2_ONELINE)
#.  Licensed under GNU General Public License version 2 (GPLv2)
#.  %%%LICENSE_END
#.  shorthand for double quote that works everywhere.
#. type: ds q
#. #-#-#-#-#  opensuse-tumbleweed: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\N'34'"
msgstr "\\N'34'"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "hpsa"
msgstr "hpsa"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "hpsa - HP Smart Array SCSI driver"
msgstr "hpsa - драйвер SCSI для HP Smart Array"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "modprobe hpsa [ hpsa_allow_any=1 ]\n"
msgstr "modprobe hpsa [ hpsa_allow_any=1 ]\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<hpsa> is a SCSI driver for HP Smart Array RAID controllers."
msgstr "B<hpsa> — это драйвер SCSI для контроллеров RAID HP Smart Array."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Options"
msgstr "Параметры"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<hpsa_allow_any=1>: This option allows the driver to attempt to operate on "
"any HP Smart Array hardware RAID controller, even if it is not explicitly "
"known to the driver.  This allows newer hardware to work with older "
"drivers.  Typically this is used to allow installation of operating systems "
"from media that predates the RAID controller, though it may also be used to "
"enable B<hpsa> to drive older controllers that would normally be handled by "
"the B<cciss>(4)  driver.  These older boards have not been tested and are "
"not supported with B<hpsa>, and B<cciss>(4)  should still be used for these."
msgstr ""
"I<hpsa_allow_any=1>: Этот параметр разрешает драйверу попытаться управлять "
"аппаратурой любого контроллера RAID HP Smart Array, даже если его модель "
"неизвестна драйверу. Это позволяет новому оборудованию работать через старые "
"драйверы. Обычно, данный параметр используется для того, чтобы позволить "
"установить операционную систему с носителя, который был создан раньше "
"контроллера RAID, а также он может использоваться для обслуживания драйвером "
"B<hpsa> старых контроллеров, которые, обычно, работают через драйвер "
"B<cciss>(4). Такие старые карты не тестировались и не поддерживаются "
"B<hpsa>; для них пока всё же лучше использовать B<cciss>(4)."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Supported hardware"
msgstr "Поддерживаемое оборудование"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<hpsa> driver supports the following Smart Array boards:"
msgstr "Драйвер B<hpsa> поддерживает следующие карты Smart Array:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Smart Array P700M\n"
"    Smart Array P212\n"
"    Smart Array P410\n"
"    Smart Array P410i\n"
"    Smart Array P411\n"
"    Smart Array P812\n"
"    Smart Array P712m\n"
"    Smart Array P711m\n"
"    StorageWorks P1210m\n"
msgstr ""
"    Smart Array P700M\n"
"    Smart Array P212\n"
"    Smart Array P410\n"
"    Smart Array P410i\n"
"    Smart Array P411\n"
"    Smart Array P812\n"
"    Smart Array P712m\n"
"    Smart Array P711m\n"
"    StorageWorks P1210m\n"

#.  commit 135ae6edeb51979d0998daf1357f149a7d6ebb08
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Since Linux 4.14, the following Smart Array boards are also supported:"
msgstr "Начиная с Linux 4.14 также поддерживаются следующие платы Smart Array:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Smart Array 5300\n"
"    Smart Array 5312\n"
"    Smart Array 532\n"
"    Smart Array 5i\n"
"    Smart Array 6400\n"
"    Smart Array 6400 EM\n"
"    Smart Array 641\n"
"    Smart Array 642\n"
"    Smart Array 6i\n"
"    Smart Array E200\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E500\n"
"    Smart Array P400\n"
"    Smart Array P400i\n"
"    Smart Array P600\n"
"    Smart Array P700m\n"
"    Smart Array P800\n"
msgstr ""
"    Smart Array 5300\n"
"    Smart Array 5312\n"
"    Smart Array 532\n"
"    Smart Array 5i\n"
"    Smart Array 6400\n"
"    Smart Array 6400 EM\n"
"    Smart Array 641\n"
"    Smart Array 642\n"
"    Smart Array 6i\n"
"    Smart Array E200\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E500\n"
"    Smart Array P400\n"
"    Smart Array P400i\n"
"    Smart Array P600\n"
"    Smart Array P700m\n"
"    Smart Array P800\n"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Configuration details"
msgstr "Особенности настройки"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"To configure HP Smart Array controllers, use the HP Array Configuration "
"Utility (either B<hpacuxe>(8)  or B<hpacucli>(8))  or the Offline ROM-based "
"Configuration Utility (ORCA)  run from the Smart Array's option ROM at boot "
"time."
msgstr ""
"Для настройки контроллеров HP Smart Array используйте HP Array Configuration "
"Utility (B<hpacuxe>(8) или B<hpacucli>(8)) или Offline ROM-based "
"Configuration Utility (ORCA), которую можно запустить из ROM Smart Array при "
"старте машины."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Device nodes"
msgstr "Узлы устройства"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Logical drives are accessed via the SCSI disk driver (B<sd>(4)), tape drives "
"via the SCSI tape driver (B<st>(4)), and the RAID controller via the SCSI "
"generic driver (B<sg>(4)), with device nodes named I</dev/sd*>, I</dev/st*>, "
"and I</dev/sg*>, respectively."
msgstr ""
"Логические диски доступны через драйвер дисков SCSI (B<sd>(4)), ленточные "
"устройства — через драйвер лент SCSI (B<st>(4)), а контроллеры RAID — через "
"общий драйвер SCSI (B<sg>(4)); имена узлов устройств называются I</dev/sd*,> "
"I</dev/st*> и I</dev/sg*>, соответственно."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "HPSA-specific host attribute files in /sys"
msgstr "Файлы в /sys, относящиеся к атрибутам узла HPSA"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_host/host*/rescan>"
msgstr "I</sys/class/scsi_host/host*/rescan>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is a write-only attribute.  Writing to this attribute will cause the "
"driver to scan for new, changed, or removed devices (e.g., hot-plugged tape "
"drives, or newly configured or deleted logical drives, etc.)  and notify the "
"SCSI midlayer of any changes detected.  Normally a rescan is triggered "
"automatically by HP's Array Configuration Utility (either the GUI or the "
"command-line variety); thus, for logical drive changes, the user should not "
"normally have to use this attribute.  This attribute may be useful when hot "
"plugging devices like tape drives, or entire storage boxes containing "
"preconfigured logical drives."
msgstr ""
"Атрибут только для записи. Факт записи заставляет драйвер выполнить "
"сканирование на предмет появления новых, изменившихся или удалённых "
"устройств (например, подключаемых на ходу ленточных накопителей или только "
"что настроенных или удалённых логических дисков и т. д.) и уведомить "
"прослойку (midlayer)  SCSI об обнаруженных изменениях. Обычно, "
"пересканирование выполняется автоматически утилитой настройки HP Array (из "
"графического интерфейса или командной строки); поэтому при изменении "
"логических дисков, обычно, пользователь не должен использовать этот атрибут. "
"Данный атрибут полезен при подключении на ходу устройств, таких как "
"ленточные накопители или хранилищ с уже настроенными логическими дисками."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_host/host*/firmware_revision>"
msgstr "I</sys/class/scsi_host/host*/firmware_revision>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "This attribute contains the firmware version of the Smart Array."
msgstr "В данном атрибуте хранится версия микропрограммы Smart Array."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For example:"
msgstr "Пример:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_host/host4>\n"
"# B<cat firmware_revision>\n"
"7.14\n"
msgstr ""
"# B<cd /sys/class/scsi_host/host4>\n"
"# B<cat firmware_revision>\n"
"7.14\n"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "HPSA-specific disk attribute files in /sys"
msgstr "Файлы в /sys, относящиеся к атрибутам диска HPSA"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_disk/c:b:t:l/device/unique_id>"
msgstr "I</sys/class/scsi_disk/c:b:t:l/device/unique_id>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This attribute contains a 32 hex-digit unique ID for each logical drive."
msgstr ""
"В данном атрибуте хранится 32 шестнадцатеричных цифры уникального "
"идентификатора для каждого логического диска."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat unique_id>\n"
"600508B1001044395355323037570F77\n"
msgstr ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat unique_id>\n"
"600508B1001044395355323037570F77\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_disk/c:b:t:l/device/raid_level>"
msgstr "I</sys/class/scsi_disk/c:b:t:l/device/raid_level>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "This attribute contains the RAID level of each logical drive."
msgstr "В данном атрибуте хранится уровень RAID каждого логического диска."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat raid_level>\n"
"RAID 0\n"
msgstr ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat raid_level>\n"
"RAID 0\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_disk/c:b:t:l/device/lunid>"
msgstr "I</sys/class/scsi_disk/c:b:t:l/device/lunid>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This attribute contains the 16 hex-digit (8 byte) LUN ID by which a logical "
"drive or physical device can be addressed.  I<c>:I<b>:I<t>:I<l> are the "
"controller, bus, target, and lun of the device."
msgstr ""
"В данном атрибуте хранится 16 шестнадцатеричных цифры (8 байт) "
"идентификатора LUN, который может адресовать логический или физический диск. "
"Форматом I<c>:I<b>:I<t>:I<l> описывается контроллер, шина, целевое "
"устройство (target) и lun устройства."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat lunid>\n"
"0x0000004000000000\n"
msgstr ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat lunid>\n"
"0x0000004000000000\n"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Supported ioctl() operations"
msgstr "Поддерживаемые операции ioctl()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For compatibility with applications written for the B<cciss>(4)  driver, "
"many, but not all of the ioctls supported by the B<cciss>(4)  driver are "
"also supported by the B<hpsa> driver.  The data structures used by these "
"ioctls are described in the Linux kernel source file I<include/linux/"
"cciss_ioctl.h>."
msgstr ""
"Для совместимости с приложениями, написанными для драйвера B<cciss>(4), в "
"драйвере B<hpsa> поддерживаются многие ioctl из драйвера B<cciss>(4) (но не "
"все). Структуры данных, используемые в ioctl, описаны в файле исходного кода "
"ядра Linux I<include/linux/cciss_ioctl.h>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_DEREGDISK>, B<CCISS_REGNEWDISK>, B<CCISS_REGNEWD>"
msgstr "B<CCISS_DEREGDISK>, B<CCISS_REGNEWDISK>, B<CCISS_REGNEWD>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These three ioctls all do exactly the same thing, which is to cause the "
"driver to rescan for new devices.  This does exactly the same thing as "
"writing to the hpsa-specific host \"rescan\" attribute."
msgstr ""
"Это три ioctl выполняют одинаковую работу — заставляют драйвер искать новые "
"устройства. Это тоже самое, как если выполнить запись в hpsa-атрибут узла "
"«rescan»."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_GETPCIINFO>"
msgstr "B<CCISS_GETPCIINFO>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Returns PCI domain, bus, device, and function and \"board ID\" (PCI "
"subsystem ID)."
msgstr ""
"Возвращает домен PCI, шину, устройство, функцию и «board ID» (ID подсистемы "
"PCI)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_GETDRIVVER>"
msgstr "B<CCISS_GETDRIVVER>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Returns driver version in three bytes encoded as:"
msgstr "Возвращает версию драйвера в виде трёх байт в формате:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"(major_version E<lt>E<lt> 16) | (minor_version E<lt>E<lt> 8) |\n"
"    (subminor_version)\n"
msgstr ""
"(major_version E<lt>E<lt> 16) | (minor_version E<lt>E<lt> 8) |\n"
"    (subminor_version)\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_PASSTHRU>, B<CCISS_BIG_PASSTHRU>"
msgstr "B<CCISS_PASSTHRU>, B<CCISS_BIG_PASSTHRU>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Allows \"BMIC\" and \"CISS\" commands to be passed through to the Smart "
"Array.  These are used extensively by the HP Array Configuration Utility, "
"SNMP storage agents, and so on.  See I<cciss_vol_status> at E<.UR http://"
"cciss.sf.net> E<.UE> for some examples."
msgstr ""
"Разрешает передавать команды «BMIC» и «CISS» в Smart Array. Они часто "
"используются в HP Array Configuration Utility, SNMP-агентах хранилищ и т. д. "
"Примеры смотрите в E<.UR http://cciss.sf.net> E<.UE> в разделе о "
"I<cciss_vol_status>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<cciss>(4), B<sd>(4), B<st>(4), B<cciss_vol_status>(8), B<hpacucli>(8), "
"B<hpacuxe>(8)"
msgstr ""
"B<cciss>(4), B<sd>(4), B<st>(4), B<cciss_vol_status>(8), B<hpacucli>(8), "
"B<hpacuxe>(8)"

#. #-#-#-#-#  archlinux: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH AUTHORS
#.  Don Brace, Steve Cameron, Tom Lawler, Mike Miller, Scott Teel
#.  and probably some other people.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-38: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH AUTHORS
#.  Don Brace, Steve Cameron, Tom Lawler, Mike Miller, Scott Teel
#.  and probably some other people.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: hpsa.4.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"E<.UR http://cciss.sf.net> E<.UE ,> and I<Documentation/scsi/hpsa.txt> and "
"I<Documentation/ABI/testing/sysfs-bus-pci-devices-cciss> in the Linux kernel "
"source tree"
msgstr ""
"E<.UR http://cciss.sf.net> E<.UE ,> and I<Documentation/scsi/hpsa.txt> и "
"I<Documentation/ABI/testing/sysfs-bus-pci-devices-cciss> в дереве исходного "
"кода ядра Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "HPSA"
msgstr "HPSA"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Returns PCI domain, bus, device and function and \"board ID\" (PCI subsystem "
"ID)."
msgstr ""
"Возвращает домен PCI, шину, устройство, функцию и «board ID» (ID подсистемы "
"PCI)."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<cciss>(4), B<sd>(4), B<st>(4), B<cciss_vol_status>(8), B<hpacucli>(8), "
"B<hpacuxe>(8),"
msgstr ""
"B<cciss>(4), B<sd>(4), B<st>(4), B<cciss_vol_status>(8), B<hpacucli>(8), "
"B<hpacuxe>(8),"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
