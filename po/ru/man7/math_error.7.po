# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:15+0100\n"
"PO-Revision-Date: 2019-10-06 08:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Domain error"
msgid "math_error"
msgstr "Ошибка области"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "math_error - detecting errors from mathematical functions"
msgstr "math_error - определение ошибок при выполнении математических функций"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>math.hE<gt>>\n"
"B<#include E<lt>errno.hE<gt>>\n"
"B<#include E<lt>fenv.hE<gt>>\n"
msgstr ""
"B<#include E<lt>math.hE<gt>>\n"
"B<#include E<lt>errno.hE<gt>>\n"
"B<#include E<lt>fenv.hE<gt>>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"When an error occurs, most library functions indicate this fact by returning "
"a special value (e.g., -1 or NULL).  Because they typically return a "
"floating-point number, the mathematical functions declared in I<E<lt>math."
"hE<gt>> indicate an error using other mechanisms.  There are two error-"
"reporting mechanisms: the older one sets I<errno>; the newer one uses the "
"floating-point exception mechanism (the use of B<feclearexcept>(3)  and "
"B<fetestexcept>(3), as outlined below)  described in B<fenv>(3)."
msgstr ""
"При возникновении ошибки большинство библиотечных функций возвращают "
"специальное значение (например, -1 или NULL). Так как математические "
"функции, объявленные в I<E<lt>math.hE<gt>>, обычно, возвращают число с "
"плавающей запятой, то для выдачи ошибки используются другие способы. Есть "
"два варианта вернуть сообщение об ошибке: старый — изменяя I<errno>; новый — "
"используя механизм исключений плавающей запятой (с помощью "
"B<feclearexcept>(3) и B<fetestexcept>(3) как описано ниже), описанный в "
"B<fenv>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A portable program that needs to check for an error from a mathematical "
"function should set I<errno> to zero, and make the following call"
msgstr ""
"Переносимая программа, которой требуется проверка на ошибки в математических "
"функциях, должна обнулить I<errno> и выполнить вызов"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "feclearexcept(FE_ALL_EXCEPT);\n"
msgstr "feclearexcept(FE_ALL_EXCEPT);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "before calling a mathematical function."
msgstr "перед тем, как вызвать математическую функцию."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Upon return from the mathematical function, if I<errno> is nonzero, or the "
"following call (see B<fenv>(3))  returns nonzero"
msgstr ""
"По возврату из математической функции, если переменная I<errno> ненулевая, а "
"так же следующий вызов (смотрите B<fenv>(3)) вернул ненулевое значение"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"fetestexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW |\n"
"             FE_UNDERFLOW);\n"
msgstr ""
"fetestexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW |\n"
"             FE_UNDERFLOW);\n"

#.  enum
#.  {
#.  FE_INVALID = 0x01,
#.  __FE_DENORM = 0x02,
#.  FE_DIVBYZERO = 0x04,
#.  FE_OVERFLOW = 0x08,
#.  FE_UNDERFLOW = 0x10,
#.  FE_INEXACT = 0x20
#.  };
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "then an error occurred in the mathematical function."
msgstr "то ошибка произошла в математической функции."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The error conditions that can occur for mathematical functions are described "
"below."
msgstr "Условия возникновения математических ошибок приведены ниже."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Domain error"
msgstr "Ошибка области"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A I<domain error> occurs when a mathematical function is supplied with an "
"argument whose value falls outside the domain for which the function is "
"defined (e.g., giving a negative argument to B<log>(3)).  When a domain "
"error occurs, math functions commonly return a NaN (though some functions "
"return a different value in this case); I<errno> is set to B<EDOM>, and an "
"\"invalid\" (B<FE_INVALID>)  floating-point exception is raised."
msgstr ""
"I<Ошибка области> возникает, когда математической функции передаётся "
"аргумент, чьё значение выходит за границы области, ожидаемой функцией "
"(например, передача отрицательного значения в функцию B<log>(3)). Когда "
"возникает ошибка области, чаще всего, математические функции возвращают NaN "
"(хотя некоторые функции в этом случае возвращают другое значение); I<errno> "
"присваивается B<EDOM> возникает исключение плавающей запятой "
"«invalid» (B<FE_INVALID>)."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Pole error"
msgstr "Ошибка особой точки"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A I<pole error> occurs when the mathematical result of a function is an "
"exact infinity (e.g., the logarithm of 0 is negative infinity).  When a pole "
"error occurs, the function returns the (signed) value B<HUGE_VAL>, "
"B<HUGE_VALF>, or B<HUGE_VALL>, depending on whether the function result type "
"is I<double>, I<float>, or I<long double>.  The sign of the result is that "
"which is mathematically correct for the function.  I<errno> is set to "
"B<ERANGE>, and a \"divide-by-zero\" (B<FE_DIVBYZERO>)  floating-point "
"exception is raised."
msgstr ""
"I<Ошибка особой точки> возникает, когда результат математической функции "
"должен быть равен бесконечности (например, логарифм 0 равен отрицательной "
"бесконечности). Когда возникает ошибка особой точки функция возвращает "
"значение (со знаком) B<HUGE_VAL>, B<HUGE_VALF> или B<HUGE_VALL>, в "
"зависимости от типа результата функции — I<double>, I<float> или I<long "
"double>. Знак результата будет математически корректным для функции. "
"Переменной I<errno> присваивается значение B<ERANGE> и возникает исключение "
"плавающей запятой «divide-by-zero» (B<FE_DIVBYZERO>)."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Range error"
msgstr "Ошибка диапазона"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A I<range error> occurs when the magnitude of the function result means that "
"it cannot be represented in the result type of the function.  The return "
"value of the function depends on whether the range error was an overflow or "
"an underflow."
msgstr ""
"I<Ошибка диапазона> возникает, когда величина результата функции не может "
"быть представлена типом результата функции. Возвращаемое значение функции "
"зависит от того, было ли при ошибке диапазона переполнение или исчерпание."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A floating result I<overflows> if the result is finite, but is too large to "
"represented in the result type.  When an overflow occurs, the function "
"returns the value B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, depending on "
"whether the function result type is I<double>, I<float>, or I<long double>.  "
"I<errno> is set to B<ERANGE>, and an \"overflow\" (B<FE_OVERFLOW>)  floating-"
"point exception is raised."
msgstr ""
"Результат с плавающей запятой I<переполнен>, если он является конечным "
"значением, но слишком большим для представления типом результата. При "
"возникновении переполнения функция возвращает значение B<HUGE_VAL>, "
"B<HUGE_VALF> или B<HUGE_VALL>, в зависимости от того, каков тип результата "
"функции —  I<double>, I<float> или I<long double>. Переменной I<errno> "
"присваивается значение B<ERANGE> и возникает переполнение плавающей запятой "
"«overflow» (B<FE_OVERFLOW>)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "A floating result I<underflows> if the result is too small to be "
#| "represented in the result type.  If an underflow occurs, a mathematical "
#| "function typically returns 0.0 (C99 says a function shall return \"an "
#| "implementation-defined value whose magnitude is no greater than the "
#| "smallest normalized positive number in the specified type\").  I<errno> "
#| "may be set to B<ERANGE>, and an \"overflow\" (B<FE_UNDERFLOW>)  floating-"
#| "point exception may be raised."
msgid ""
"A floating result I<underflows> if the result is too small to be represented "
"in the result type.  If an underflow occurs, a mathematical function "
"typically returns 0.0 (C99 says a function shall return \"an implementation-"
"defined value whose magnitude is no greater than the smallest normalized "
"positive number in the specified type\").  I<errno> may be set to B<ERANGE>, "
"and an \"underflow\" (B<FE_UNDERFLOW>)  floating-point exception may be "
"raised."
msgstr ""
"Результат с плавающей запятой I<потерял значимость>, если он слишком мал для "
"представления типом результата. При возникновении исчерпаемости "
"математическая функция, обычно, возвращает 0.0 (в C99 сказано, что функция "
"должна возвращать «определяемое реализацией» значение, чья величина не "
"больше, чем самое маленькое положительное число заданного типа»). Переменной "
"I<errno> может быть присвоено значение B<ERANGE> и может возникать "
"переполнение плавающей запятой «overflow» (B<FE_UNDERFLOW>)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Some functions deliver a range error if the supplied argument value, or the "
"correct function result, would be I<subnormal>.  A subnormal value is one "
"that is nonzero, but with a magnitude that is so small that it can't be "
"presented in normalized form (i.e., with a 1 in the most significant bit of "
"the significand).  The representation of a subnormal number will contain one "
"or more leading zeros in the significand."
msgstr ""
"Некоторые функции возвращают ошибку диапазона, ели значение аргумента или "
"правильный результат функции был бы I<субнормальным>. Субнормальное значение "
"— ненулевое значение, но его величина так мала, что не может быть "
"представлена в нормализованном виде (т. е., есть 1 с самом значимом бите "
"значащей части). Представление субнормального числа будет содержать один или "
"более начальных нулей в значащей части."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#.  See CONFORMANCE in the glibc 2.8 (and earlier) source.
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<math_errhandling> identifier specified by C99 and POSIX.1 is not "
#| "supported by glibc.  This identifier is supposed to indicate which of the "
#| "two error-notification mechanisms (I<errno>, exceptions retrievable via "
#| "B<fettestexcept>(3))  is in use.  The standards require that at least one "
#| "be in use, but permit both to be available.  The current (version 2.8) "
#| "situation under glibc is messy.  Most (but not all) functions raise "
#| "exceptions on errors.  Some also set I<errno>.  A few functions set "
#| "I<errno>, but don't raise an exception.  A very few functions do "
#| "neither.  See the individual manual pages for details."
msgid ""
"The I<math_errhandling> identifier specified by C99 and POSIX.1 is not "
"supported by glibc.  This identifier is supposed to indicate which of the "
"two error-notification mechanisms (I<errno>, exceptions retrievable via "
"B<fetestexcept>(3))  is in use.  The standards require that at least one be "
"in use, but permit both to be available.  The current (glibc 2.8) situation "
"under glibc is messy.  Most (but not all) functions raise exceptions on "
"errors.  Some also set I<errno>.  A few functions set I<errno>, but don't "
"raise an exception.  A very few functions do neither.  See the individual "
"manual pages for details."
msgstr ""
"Идентификатор I<math_errhandling>, определённый в C99 и POSIX.1, не "
"поддерживается в glibc. Данный идентификатор предполагается для указания "
"какой из двух способов использован для уведомления об ошибке (I<errno>, "
"исключение, извлекаемое с помощью B<fettestexcept>(3)). Стандарты требуют, "
"чтобы использовался какой-то один, но допускают быть доступным обоим. На "
"данный момент (версия 2.8) ситуация в glibc запутанная. Большинство (но не "
"все) функций вызывают при ошибке исключение. Некоторые также изменяют "
"I<errno>. Несколько функций изменяют I<errno>, но не вызывают исключение. "
"Очень малое количество функций не делают ни того, ни другого. Всё это "
"описано в их справочных страницах."

#.  http://www.securecoding.cert.org/confluence/display/seccode/FLP32-C.+Prevent+or+detect+domain+and+range+errors+in+math+functions
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"To avoid the complexities of using I<errno> and B<fetestexcept>(3)  for "
"error checking, it is often advised that one should instead check for bad "
"argument values before each call.  For example, the following code ensures "
"that B<log>(3)'s argument is not a NaN and is not zero (a pole error) or "
"less than zero (a domain error):"
msgstr ""
"Чтобы при проверке ошибок избежать сложностей с использованием I<errno> и "
"B<fetestexcept>(3) часто советуют перед вызовом проверять аргументы на "
"некорректные значения. Например, следующий код проверяет, что аргумент "
"B<log>(3) не равен NaN и не равен нулю (ошибка особой точки) или меньше нуля "
"(ошибка области):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "double x, r;\n"
msgstr "double x, r;\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"if (isnan(x) || islessequal(x, 0)) {\n"
"    /* Deal with NaN / pole error / domain error */\n"
"}\n"
msgstr ""
"if (isnan(x) || islessequal(x, 0)) {\n"
"    /* обработка NaN / ошибки особой точки / ошибки области */\n"
"}\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "r = log(x);\n"
msgstr "r = log(x);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The discussion on this page does not apply to the complex mathematical "
"functions (i.e., those declared by I<E<lt>complex.hE<gt>>), which in general "
"are not required to return errors by C99 and POSIX.1."
msgstr ""
"Данная страница не применима к комплексным математическим функциям "
"(описанным в I<E<lt>complex.hE<gt>>), для которых в C99 and POSIX.1, обычно, "
"не требуется возвращать ошибки."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<gcc>(1)  I<-fno-math-errno> option causes the executable to employ "
"implementations of some mathematical functions that are faster than the "
"standard implementations, but do not set I<errno> on error.  (The B<gcc>(1)  "
"I<-ffast-math> option also enables I<-fno-math-errno>.)  An error can still "
"be tested for using B<fetestexcept>(3)."
msgstr ""
"Параметр B<gcc>(1) I<-fno-math-errno> заставляет в исполняемых файлах "
"вызывать реализации некоторых математических функций, которые быстрее "
"стандартных, но не изменяющие I<errno> при ошибке (параметр B<gcc>(1) I<-"
"ffast-math> также включает I<-fno-math-errno>.) Возникновение ошибки по-"
"прежнему можно проверить с помощью B<fetestexcept>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<gcc>(1), B<errno>(3), B<fenv>(3), B<fpclassify>(3), B<INFINITY>(3), "
"B<isgreater>(3), B<matherr>(3), B<nan>(3)"
msgstr ""
"B<gcc>(1), B<errno>(3), B<fenv>(3), B<fpclassify>(3), B<INFINITY>(3), "
"B<isgreater>(3), B<matherr>(3), B<nan>(3)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<info libc>"
msgstr "I<info libc>"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "MATH_ERROR"
msgstr "MATH_ERROR"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#.  See CONFORMANCE in the glibc 2.8 (and earlier) source.
#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The I<math_errhandling> identifier specified by C99 and POSIX.1 is not "
#| "supported by glibc.  This identifier is supposed to indicate which of the "
#| "two error-notification mechanisms (I<errno>, exceptions retrievable via "
#| "B<fettestexcept>(3))  is in use.  The standards require that at least one "
#| "be in use, but permit both to be available.  The current (version 2.8) "
#| "situation under glibc is messy.  Most (but not all) functions raise "
#| "exceptions on errors.  Some also set I<errno>.  A few functions set "
#| "I<errno>, but don't raise an exception.  A very few functions do "
#| "neither.  See the individual manual pages for details."
msgid ""
"The I<math_errhandling> identifier specified by C99 and POSIX.1 is not "
"supported by glibc.  This identifier is supposed to indicate which of the "
"two error-notification mechanisms (I<errno>, exceptions retrievable via "
"B<fetestexcept>(3))  is in use.  The standards require that at least one be "
"in use, but permit both to be available.  The current (version 2.8) "
"situation under glibc is messy.  Most (but not all) functions raise "
"exceptions on errors.  Some also set I<errno>.  A few functions set "
"I<errno>, but don't raise an exception.  A very few functions do neither.  "
"See the individual manual pages for details."
msgstr ""
"Идентификатор I<math_errhandling>, определённый в C99 и POSIX.1, не "
"поддерживается в glibc. Данный идентификатор предполагается для указания "
"какой из двух способов использован для уведомления об ошибке (I<errno>, "
"исключение, извлекаемое с помощью B<fettestexcept>(3)). Стандарты требуют, "
"чтобы использовался какой-то один, но допускают быть доступным обоим. На "
"данный момент (версия 2.8) ситуация в glibc запутанная. Большинство (но не "
"все) функций вызывают при ошибке исключение. Некоторые также изменяют "
"I<errno>. Несколько функций изменяют I<errno>, но не вызывают исключение. "
"Очень малое количество функций не делают ни того, ни другого. Всё это "
"описано в их справочных страницах."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"A floating result I<underflows> if the result is too small to be represented "
"in the result type.  If an underflow occurs, a mathematical function "
"typically returns 0.0 (C99 says a function shall return \"an implementation-"
"defined value whose magnitude is no greater than the smallest normalized "
"positive number in the specified type\").  I<errno> may be set to B<ERANGE>, "
"and an \"overflow\" (B<FE_UNDERFLOW>)  floating-point exception may be "
"raised."
msgstr ""
"Результат с плавающей запятой I<потерял значимость>, если он слишком мал для "
"представления типом результата. При возникновении исчерпаемости "
"математическая функция, обычно, возвращает 0.0 (в C99 сказано, что функция "
"должна возвращать «определяемое реализацией» значение, чья величина не "
"больше, чем самое маленькое положительное число заданного типа»). Переменной "
"I<errno> может быть присвоено значение B<ERANGE> и может возникать "
"переполнение плавающей запятой «overflow» (B<FE_UNDERFLOW>)."

#.  See CONFORMANCE in the glibc 2.8 (and earlier) source.
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The I<math_errhandling> identifier specified by C99 and POSIX.1 is not "
"supported by glibc.  This identifier is supposed to indicate which of the "
"two error-notification mechanisms (I<errno>, exceptions retrievable via "
"B<fettestexcept>(3))  is in use.  The standards require that at least one be "
"in use, but permit both to be available.  The current (version 2.8) "
"situation under glibc is messy.  Most (but not all) functions raise "
"exceptions on errors.  Some also set I<errno>.  A few functions set "
"I<errno>, but don't raise an exception.  A very few functions do neither.  "
"See the individual manual pages for details."
msgstr ""
"Идентификатор I<math_errhandling>, определённый в C99 и POSIX.1, не "
"поддерживается в glibc. Данный идентификатор предполагается для указания "
"какой из двух способов использован для уведомления об ошибке (I<errno>, "
"исключение, извлекаемое с помощью B<fettestexcept>(3)). Стандарты требуют, "
"чтобы использовался какой-то один, но допускают быть доступным обоим. На "
"данный момент (версия 2.8) ситуация в glibc запутанная. Большинство (но не "
"все) функций вызывают при ошибке исключение. Некоторые также изменяют "
"I<errno>. Несколько функций изменяют I<errno>, но не вызывают исключение. "
"Очень малое количество функций не делают ни того, ни другого. Всё это "
"описано в их справочных страницах."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
