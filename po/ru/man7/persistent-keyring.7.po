# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Kogan, Darima <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:21+0100\n"
"PO-Revision-Date: 2019-10-12 08:58+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Persistence"
msgid "persistent-keyring"
msgstr "Устойчивость"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-08"
msgstr "8 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "persistent-keyring - per-user persistent keyring"
msgstr "persistent-keyring - пользовательская постоянная связка ключей"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The persistent keyring is a keyring used to anchor keys on behalf of a "
"user.  Each UID the kernel deals with has its own persistent keyring that is "
"shared between all threads owned by that UID.  The persistent keyring has a "
"name (description) of the form I<_persistent.E<lt>UIDE<gt>> where "
"I<E<lt>UIDE<gt>> is the user ID of the corresponding user."
msgstr ""
"Постоянная связка ключей — это связка, используемая для привязки от имени "
"пользователя. Для каждого UID ядро создаёт отдельную постоянную связку "
"ключей, которая используется всеми нитями, принадлежащими этому UID. "
"Постоянная связка ключей имеет имя (описание) в виде I<_persistent."
"E<lt>UIDE<gt>>, I<E<lt>UIDE<gt>> — ID пользователя соответствующего "
"пользователя."

#.  FIXME The meaning of the preceding sentence isn't clear. What is meant?
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The persistent keyring may not be accessed directly, even by processes with "
"the appropriate UID.  Instead, it must first be linked to one of a process's "
"keyrings, before that keyring can access the persistent keyring by virtue of "
"its possessor permits.  This linking is done with the "
"B<keyctl_get_persistent>(3)  function."
msgstr ""
"Прямой доступ к постоянной связке ключей невозможен, даже процессам с "
"подходящим UID. Вместо этого сначала она должна быть прицеплена к одной из "
"связок ключей процесса, до этого связка ключей может получить доступ к "
"постоянной связке ключей согласно правам своего владельца. Эта связь "
"создаётся с помощью функции B<keyctl_get_persistent>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If a persistent keyring does not exist when it is accessed by the "
"B<keyctl_get_persistent>(3)  operation, it will be automatically created."
msgstr ""
"Если постоянная связка ключей не существует на момент вызова операции "
"B<keyctl_get_persistent>(3), то она автоматически создаётся."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Each time the B<keyctl_get_persistent>(3)  operation is performed, the "
#| "persistent key's expiration timer is reset to the value in:"
msgid ""
"Each time the B<keyctl_get_persistent>(3)  operation is performed, the "
"persistent keyring's expiration timer is reset to the value in:"
msgstr ""
"Каждый раз при выполнении операции B<keyctl_get_persistent>(3) срок действия "
"постоянного  ключа сбрасывается в значение:"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "/proc/sys/kernel/keys/persistent_keyring_expiry\n"
msgstr "/proc/sys/kernel/keys/persistent_keyring_expiry\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Should the timeout be reached, the persistent keyring will be removed and "
#| "everything it pins can then be garbage collected.  The key will then be "
#| "re-created on a subsequent call to B<keyctl_get_persistent>(3)."
msgid ""
"Should the timeout be reached, the persistent keyring will be removed and "
"everything it pins can then be garbage collected.  The keyring will then be "
"re-created on a subsequent call to B<keyctl_get_persistent>(3)."
msgstr ""
"По истечению срока действия постоянная связка ключей удаляется и все ссылки "
"на неё затем удаляются сборщиком мусора. После этого ключ будет пересоздан "
"при следующем вызове B<keyctl_get_persistent>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The persistent keyring is not directly searched by B<request_key>(2); it is "
"searched only if it is linked into one of the keyrings that is searched by "
"B<request_key>(2)."
msgstr ""
"В постоянной связке ключей напрямую невозможно искать с помощью "
"B<request_key>(2); в ней можно искать только, если она прицеплена к одной из "
"связок ключей, по которой выполняется B<request_key>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The persistent keyring is independent of B<clone>(2), B<fork>(2), "
"B<vfork>(2), B<execve>(2), and B<_exit>(2).  It persists until its "
"expiration timer triggers, at which point it is garbage collected.  This "
"allows the persistent keyring to carry keys beyond the life of the kernel's "
"record of the corresponding UID (the destruction of which results in the "
"destruction of the B<user-keyring>(7)  and the B<user-session-keyring>(7)).  "
"The persistent keyring can thus be used to hold authentication tokens for "
"processes that run without user interaction, such as programs started by "
"B<cron>(8)."
msgstr ""
"Постоянная связка ключей не зависит от B<clone>(2), B<fork>(2), B<vfork>(2), "
"B<execve>(2) и B<_exit>(2). Она существует до истечения срока действия "
"таймера, после чего удаляется сборщиком мусора. Это позволяет связке ключей "
"хранить ключи дольше жизни ядерной записи соответствующего UID (удаление "
"которой приводит к уничтожению B<user-keyring>(7) и B<user-session-"
"keyring>(7)). Таким образом, постоянную связку ключей можно использовать для "
"хранения токенов аутентификации для процессов, которые выполняются без "
"взаимодействия с пользователем, например программы, выполняемые B<cron>(8)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The persistent keyring is used to store UID-specific objects that themselves "
"have limited lifetimes (e.g., kerberos tokens).  If those tokens cease to be "
"used (i.e., the persistent keyring is not accessed), then the timeout of the "
"persistent keyring ensures that the corresponding objects are automatically "
"discarded."
msgstr ""
"Постоянная связка ключей используется для хранения объектов, для UID, "
"которые сами имеют ограниченный срок жизни (например, токены kerberos). Если "
"такие токены больше не используются (т. е., связка ключей недоступна), то "
"срок действия постоянной связки ключей позволяет автоматически удалять "
"соответствующие ей объекты."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Special operations"
msgstr "Специальные операции"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<keyutils> library provides the B<keyctl_get_persistent>(3)  function "
"for manipulating persistent keyrings.  (This function is an interface to the "
"B<keyctl>(2)  B<KEYCTL_GET_PERSISTENT> operation.)  This operation allows "
"the calling thread to get the persistent keyring corresponding to its own "
"UID or, if the thread has the B<CAP_SETUID> capability, the persistent "
"keyring corresponding to some other UID in the same user namespace."
msgstr ""
"Библиотека I<keyutils> для работы с постоянными связками ключей "
"предоставляет функцию B<keyctl_get_persistent>(3) (эта функция является "
"интерфейсом к операции B<keyctl>(2)  B<KEYCTL_GET_PERSISTENT>). Данная "
"операция позволяет вызывающей нити получить постоянную связку ключей, "
"соответствующую её UID или, если нить имеет мандат B<CAP_SETUID>, то "
"постоянная связка ключей соответствует какому-то другому UID в этом же "
"пространстве имён пользователя."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Each user namespace owns a keyring called I<.persistent_register> that "
#| "contains links to all of the persistent keys in that namespace.  (The I<."
#| "persistent_register> keyring can be seen when reading the contents of the "
#| "I</proc/keys> file for the UID 0 in the namespace.)  The "
#| "B<keyctl_get_persistent>(3)  operation looks for a key with a name of the "
#| "form I<_persistent.E<lt>UIDE<gt>> in that keyring, creates the key if it "
#| "does not exist, and links it into the keyring."
msgid ""
"Each user namespace owns a keyring called I<.persistent_register> that "
"contains links to all of the persistent keys in that namespace.  (The I<."
"persistent_register> keyring can be seen when reading the contents of the I</"
"proc/keys> file for the UID 0 in the namespace.)  The "
"B<keyctl_get_persistent>(3)  operation looks for a key with a name of the "
"form I<_persistent.>UID in that keyring, creates the key if it does not "
"exist, and links it into the keyring."
msgstr ""
"Каждое пространство имён пользователя имеет свою связку ключей с именем I<."
"persistent_register>, в которой содержатся ссылки на все постоянные ключи в "
"этом пространстве имён (связку ключей I<.persistent_register> можно увидеть "
"при чтении в пространстве имён содержимого файла I</proc/keys> для UID 0). "
"Операция B<keyctl_get_persistent>(3) ищет ключ с именем в виде I<_persistent."
"E<lt>UIDE<gt>> в этой связке ключей, создаёт ключ, если он не существует и "
"прицепляет его в связку ключей."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<keyctl>(1), B<keyctl>(3), B<keyctl_get_persistent>(3), B<keyrings>(7), "
"B<process-keyring>(7), B<session-keyring>(7), B<thread-keyring>(7), B<user-"
"keyring>(7), B<user-session-keyring>(7)"
msgstr ""
"B<keyctl>(1), B<keyctl>(3), B<keyctl_get_persistent>(3), B<keyrings>(7), "
"B<process-keyring>(7), B<session-keyring>(7), B<thread-keyring>(7), B<user-"
"keyring>(7), B<user-session-keyring>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "PERSISTENT-KEYRING"
msgstr "PERSISTENT-KEYRING"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-08-13"
msgstr "13 августа 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Each time the B<keyctl_get_persistent>(3)  operation is performed, the "
"persistent key's expiration timer is reset to the value in:"
msgstr ""
"Каждый раз при выполнении операции B<keyctl_get_persistent>(3) срок действия "
"постоянного  ключа сбрасывается в значение:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "    /proc/sys/kernel/keys/persistent_keyring_expiry\n"
msgstr "    /proc/sys/kernel/keys/persistent_keyring_expiry\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Should the timeout be reached, the persistent keyring will be removed and "
"everything it pins can then be garbage collected.  The key will then be re-"
"created on a subsequent call to B<keyctl_get_persistent>(3)."
msgstr ""
"По истечению срока действия постоянная связка ключей удаляется и все ссылки "
"на неё затем удаляются сборщиком мусора. После этого ключ будет пересоздан "
"при следующем вызове B<keyctl_get_persistent>(3)."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Each user namespace owns a keyring called I<.persistent_register> that "
"contains links to all of the persistent keys in that namespace.  (The I<."
"persistent_register> keyring can be seen when reading the contents of the I</"
"proc/keys> file for the UID 0 in the namespace.)  The "
"B<keyctl_get_persistent>(3)  operation looks for a key with a name of the "
"form I<_persistent.E<lt>UIDE<gt>> in that keyring, creates the key if it "
"does not exist, and links it into the keyring."
msgstr ""
"Каждое пространство имён пользователя имеет свою связку ключей с именем I<."
"persistent_register>, в которой содержатся ссылки на все постоянные ключи в "
"этом пространстве имён (связку ключей I<.persistent_register> можно увидеть "
"при чтении в пространстве имён содержимого файла I</proc/keys> для UID 0). "
"Операция B<keyctl_get_persistent>(3) ищет ключ с именем в виде I<_persistent."
"E<lt>UIDE<gt>> в этой связке ключей, создаёт ключ, если он не существует и "
"прицепляет его в связку ключей."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-03-13"
msgstr "13 марта 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
