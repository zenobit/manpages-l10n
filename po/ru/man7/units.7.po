# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:39+0100\n"
"PO-Revision-Date: 2023-03-04 14:17+0100\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "units"
msgstr "units"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-10"
msgstr "10 февраля 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "units - decimal and binary prefixes"
msgstr "units - десятичные и двоичные приставки"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Decimal prefixes"
msgstr "Десятичные приставки"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The SI system of units uses prefixes that indicate powers of ten.  A "
"kilometer is 1000 meter, and a megawatt is 1000000 watt.  Below the standard "
"prefixes."
msgstr ""
"В системе единиц СИ используются приставки, значения которых является "
"степенью числа. В километре 1000 метров, а в мегаватте — 1000000 ватт. Далее "
"показаны стандартные приставки."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Prefix"
msgstr "Приставка"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Имя"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "q"
msgstr "q"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "hecto"
msgid "quecto"
msgstr "гекто"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-30 = 0.000000000000000000000000000001"
msgstr "10\\[ha]-30 = 0.000000000000000000000000000001"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "r"
msgstr "r"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ronto"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-27 = 0.000000000000000000000000001"
msgstr "10\\[ha]-27 = 0.000000000000000000000000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "y"
msgstr "и"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "yocto"
msgstr "иокто"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-24 = 0.000000000000000000000001"
msgstr "10\\[ha]-24 = 0.000000000000000000000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "z"
msgstr "з"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "zepto"
msgstr "зепто"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-21 = 0.000000000000000000001"
msgstr "10\\[ha]-21 = 0.000000000000000000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "a"
msgstr "а"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "atto"
msgstr "атто"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-18 = 0.000000000000000001"
msgstr "10\\[ha]-18 = 0.000000000000000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "f"
msgstr "ф"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "femto"
msgstr "фемто"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-15 = 0.000000000000001"
msgstr "10\\[ha]-15 = 0.000000000000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "p"
msgstr "п"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "pico"
msgstr "пико"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-12 = 0.000000000001"
msgstr "10\\[ha]-12 = 0.000000000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "n"
msgstr "н"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "nano"
msgstr "нано"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-9  = 0.000000001"
msgstr "10\\[ha]-9  = 0.000000001"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[mc]"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "micro"
msgstr "микро"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-6  = 0.000001"
msgstr "10\\[ha]-6  = 0.000001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "m"
msgstr "м"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "milli"
msgstr "милли"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-3  = 0.001"
msgstr "10\\[ha]-3  = 0.001"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "c"
msgstr "с"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "centi"
msgstr "санти"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-2  = 0.01"
msgstr "10\\[ha]-2  = 0.01"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "d"
msgstr "д"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "deci"
msgstr "деци"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]-1  = 0.1"
msgstr "10\\[ha]-1  = 0.1"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "da"
msgstr "да"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "deka"
msgstr "дека"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha] 1  = 10"
msgstr "10\\[ha] 1  = 10"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "h"
msgstr "г"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "hecto"
msgstr "гекто"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha] 2  = 100"
msgstr "10\\[ha] 2  = 100"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "k"
msgstr "к"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "kilo"
msgstr "кило"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha] 3  = 1000"
msgstr "10\\[ha] 3  = 1000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "M"
msgstr "М"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "mega"
msgstr "мега"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha] 6  = 1000000"
msgstr "10\\[ha] 6  = 1000000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "G"
msgstr "Г"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "giga"
msgstr "гига"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha] 9  = 1000000000"
msgstr "10\\[ha] 9  = 1000000000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "T"
msgstr "Т"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "tera"
msgstr "тера"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]12  = 1000000000000"
msgstr "10\\[ha]12  = 1000000000000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "P"
msgstr "П"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "peta"
msgstr "пета"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]15  = 1000000000000000"
msgstr "10\\[ha]15  = 1000000000000000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "E"
msgstr "Э"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "exa"
msgstr "экса"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]18  = 1000000000000000000"
msgstr "10\\[ha]18  = 1000000000000000000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Z"
msgstr "З"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "zetta"
msgstr "зетта"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]21  = 1000000000000000000000"
msgstr "10\\[ha]21  = 1000000000000000000000"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Y"
msgstr "И"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "yotta"
msgstr "иотта"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]24  = 1000000000000000000000000"
msgstr "10\\[ha]24  = 1000000000000000000000000"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "R"
msgstr "R"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ronna"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]27  = 1000000000000000000000000000"
msgstr "10\\[ha]27  = 1000000000000000000000000000"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Q"
msgstr "Q"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "zetta"
msgid "quetta"
msgstr "зетта"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10\\[ha]30  = 1000000000000000000000000000000"
msgstr "10\\[ha]30  = 1000000000000000000000000000000"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The symbol for micro is the Greek letter mu, often written u in an ASCII "
"context where this Greek letter is not available."
msgstr ""
"Для обозначения micro используется греческая буква мю. В кодировке ASCII, "
"где она отсутствует, вместо неё часто используют латинскую u."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Binary prefixes"
msgstr "Двоичные префиксы"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The binary prefixes resemble the decimal ones, but have an additional "
"\\[aq]i\\[aq] (and \"Ki\" starts with a capital \\[aq]K\\[aq]).  The names "
"are formed by taking the first syllable of the names of the decimal prefix "
"with roughly the same size, followed by \"bi\" for \"binary\"."
msgstr ""
"Двоичные префиксы походят на десятичные, но имеют дополнительную \\[aq]i\\[aq] "
"(и «Ki» начинается с заглавной \\[aq]K\\[aq]). Имена формируются из первого "
"звука имени десятичных префиксов примерно того же размера, за которым "
"следует «bi» (от «binary» — «двоичный»)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Ki"
msgstr "КиБ"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "kibi"
msgstr "киби"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]10 = 1024"
msgstr "2\\[ha]10 = 1024"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Mi"
msgstr "МиБ"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "mebi"
msgstr "меби"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]20 = 1048576"
msgstr "2\\[ha]20 = 1048576"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Gi"
msgstr "ГиБ"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "gibi"
msgstr "гиби"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]30 = 1073741824"
msgstr "2\\[ha]30 = 1073741824"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Ti"
msgstr "ТиБ"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "tebi"
msgstr "теби"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]40 = 1099511627776"
msgstr "2\\[ha]40 = 1099511627776"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Pi"
msgstr "ПиБ"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "pebi"
msgstr "пеби"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]50 = 1125899906842624"
msgstr "2\\[ha]50 = 1125899906842624"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Ei"
msgstr "ЭиБ"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "exbi"
msgstr "эксби"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]60 = 1152921504606846976"
msgstr "2\\[ha]60 = 1152921504606846976"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Z"
msgid "Zi"
msgstr "З"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "zebi"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]70 = 1180591620717411303424"
msgstr "2\\[ha]70 = 1180591620717411303424"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Y"
msgid "Yi"
msgstr "И"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "yobi"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2\\[ha]80 = 1208925819614629174706176"
msgstr "2\\[ha]80 = 1208925819614629174706176"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Discussion"
msgstr "Обсуждение"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Before these binary prefixes were introduced, it was fairly common to use "
"k=1000 and K=1024, just like b=bit, B=byte.  Unfortunately, the M is capital "
"already, and cannot be capitalized to indicate binary-ness."
msgstr ""
"До введения двоичных префиксов довольно часто использовалось k=1000 и "
"K=1024, также как и b=bit (бит), B=byte (байт). К сожалению, заглавная M уже "
"занята, и не может быть использована как признак двоичности."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"At first that didn't matter too much, since memory modules and disks came in "
"sizes that were powers of two, so everyone knew that in such contexts "
"\"kilobyte\" and \"megabyte\" meant 1024 and 1048576 bytes, respectively.  "
"What originally was a sloppy use of the prefixes \"kilo\" and \"mega\" "
"started to become regarded as the \"real true meaning\" when computers were "
"involved.  But then disk technology changed, and disk sizes became arbitrary "
"numbers.  After a period of uncertainty all disk manufacturers settled on "
"the standard, namely k=1000, M=1000\\ k, G=1000\\ M."
msgstr ""
"Поначалу это не имело большого значения, модули памяти и диски имели размер, "
"равный степеням двойки, и все знали, что в таких случаях «килобайт» означает "
"1024, а «мегабайт» — 1048576 байтов. Впрочем, некоторая путаница связанная с "
"этим всё же была. После некоторых колебаний все производители дисков "
"установили стандарт, по которому k=1000, M=1000\\ к, G=1000\\ М."

#.  also common: 14.4k modem
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The situation was messy: in the 14k4 modems, k=1000; in the 1.44\\ MB "
"diskettes, M=1024000; and so on.  In 1998 the IEC approved the standard that "
"defines the binary prefixes given above, enabling people to be precise and "
"unambiguous."
msgstr ""
"Ситуация получилась запутанной: в модемах со скоростью 14k4 k=1000; в "
"дискетах 1.44\\ МБ M=1024000 и т. п. В 1998 году IEC принял стандарт, "
"который определяет двоичные префиксы как указано выше, что позволяет людям "
"быть точными и однозначными."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Thus, today, MB = 1000000\\ B and MiB = 1048576\\ B."
msgstr "Таким образом, сегодня МБ = 1000000 \\Б, а МиБ = 1048576 \\Б."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the free software world programs are slowly being changed to conform.  "
"When the Linux kernel boots and says"
msgstr ""
"В мире свободного программного обеспечения программы медленно переходят к "
"таким обозначениям. Когда ядро Linux при загрузке выводит"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "hda: 120064896 sectors (61473 MB) w/2048KiB Cache\n"
msgstr "hda: 120064896 sectors (61473 MB) w/2048KiB Cache\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "the MB are megabytes and the KiB are kibibytes."
msgstr "то MB — это мегабайты, а KiB — кибибайты."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"E<.UR https://www.bipm.org/\\:documents/\\:20126/\\:41483022/\\:SI-"
"Brochure-9.pdf> The International System of Units E<.UE .>"
msgstr ""

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "UNITS"
msgstr "UNITS"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-08-13"
msgstr "13 августа 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-24 = 0.000000000000000000000001"
msgstr "10^-24 = 0.000000000000000000000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-21 = 0.000000000000000000001"
msgstr "10^-21 = 0.000000000000000000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-18 = 0.000000000000000001"
msgstr "10^-18 = 0.000000000000000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-15 = 0.000000000000001"
msgstr "10^-15 = 0.000000000000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-12 = 0.000000000001"
msgstr "10^-12 = 0.000000000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-9  = 0.000000001"
msgstr "10^-9  = 0.000000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\(mc"
msgstr "мк"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-6  = 0.000001"
msgstr "10^-6  = 0.000001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-3  = 0.001"
msgstr "10^-3  = 0.001"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-2  = 0.01"
msgstr "10^-2  = 0.01"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^-1  = 0.1"
msgstr "10^-1  = 0.1"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^ 1  = 10"
msgstr "10^ 1  = 10"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^ 2  = 100"
msgstr "10^ 2  = 100"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^ 3  = 1000"
msgstr "10^ 3  = 1000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^ 6  = 1000000"
msgstr "10^ 6  = 1000000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^ 9  = 1000000000"
msgstr "10^ 9  = 1000000000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^12  = 1000000000000"
msgstr "10^12  = 1000000000000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^15  = 1000000000000000"
msgstr "10^15  = 1000000000000000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^18  = 1000000000000000000"
msgstr "10^18  = 1000000000000000000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^21  = 1000000000000000000000"
msgstr "10^21  = 1000000000000000000000"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "10^24  = 1000000000000000000000000"
msgstr "10^24  = 1000000000000000000000000"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The symbol for micro is the Greek letter mu, often written u in an ASCII "
"context where this Greek letter is not available.  See also"
msgstr ""
"Для обозначения micro используется греческая буква мю. В кодировке ASCII, "
"где она отсутствует, вместо неё часто используют латинскую u. Смотрите также"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "E<.UR http://physics.nist.gov\\:/cuu\\:/Units\\:/prefixes.html> E<.UE>"
msgstr "E<.UR http://physics.nist.gov\\:/cuu\\:/Units\\:/prefixes.html> E<.UE>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The binary prefixes resemble the decimal ones, but have an additional "
"\\(aqi\\(aq (and \"Ki\" starts with a capital \\(aqK\\(aq).  The names are "
"formed by taking the first syllable of the names of the decimal prefix with "
"roughly the same size, followed by \"bi\" for \"binary\"."
msgstr ""
"Двоичные префиксы походят на десятичные, но имеют дополнительную \\(aqi\\(aq "
"(и «Ki» начинается с заглавной \\(aqK\\(aq). Имена формируются из первого "
"звука имени десятичных префиксов примерно того же размера, за которым "
"следует «bi» (от «binary» — «двоичный»)."

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "2^10 = 1024"
msgstr "2^10 = 1024"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "2^20 = 1048576"
msgstr "2^20 = 1048576"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "2^30 = 1073741824"
msgstr "2^30 = 1073741824"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "2^40 = 1099511627776"
msgstr "2^40 = 1099511627776"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "2^50 = 1125899906842624"
msgstr "2^50 = 1125899906842624"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "2^60 = 1152921504606846976"
msgstr "2^60 = 1152921504606846976"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "See also"
msgstr "Смотрите также"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "E<.UR http://physics.nist.gov\\:/cuu\\:/Units\\:/binary.html> E<.UE>"
msgstr "E<.UR http://physics.nist.gov\\:/cuu\\:/Units\\:/binary.html> E<.UE>"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"E<.UR https://www.bipm.org/documents/20126/41483022/SI-Brochure-9.pdf> The "
"International System of Units E<.UE .>"
msgstr ""
