# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:40+0100\n"
"PO-Revision-Date: 2019-10-15 18:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "utime"
msgstr "utime"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-03"
msgstr "3 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "utime, utimes - change file last access and modification times"
msgstr "utime, utimes - изменить последнее время доступа и изменения к inode"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>utime.hE<gt>>\n"
msgstr "B<#include E<lt>utime.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int utime(const char *>I<filename>B<, const struct utimbuf *>I<times>B<);>\n"
msgid ""
"B<int utime(const char *>I<filename>B<,>\n"
"B<          const struct utimbuf *_Nullable >I<times>B<);>\n"
msgstr "B<int utime(const char *>I<filename>B<, const struct utimbuf *>I<times>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/time.hE<gt>>\n"
msgstr "B<#include E<lt>sys/time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int utimes(const char *>I<filename>B<, const struct timeval >I<times>B<[2]);>\n"
msgid ""
"B<int utimes(const char *>I<filename>B<,>\n"
"B<          const struct timeval >I<times>B<[_Nullable 2]);>\n"
msgstr "B<int utimes(const char *>I<filename>B<, const struct timeval >I<times>B<[2]);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<Note:> modern applications may prefer to use the interfaces described in "
"B<utimensat>(2)."
msgstr ""
"B<Замечание:> современным приложениям лучше использовать интерфейсы, "
"описанные в B<utimensat>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<utime>()  system call changes the access and modification times of "
#| "the inode specified by I<filename> to the I<actime> and I<modtime> fields "
#| "of I<times> respectively."
msgid ""
"The B<utime>()  system call changes the access and modification times of the "
"inode specified by I<filename> to the I<actime> and I<modtime> fields of "
"I<times> respectively.  The status change time (ctime) will be set to the "
"current time, even if the other time stamps don't actually change."
msgstr ""
"Системный вызов B<utime>() изменяет время доступа и изменения у inode, "
"указанного в I<filename> на значения полей I<actime> и I<modtime> из "
"структуры I<times>, соответственно."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<times> is NULL, then the access and modification times of the file are "
"set to the current time."
msgstr ""
"Если значение I<times> равно NULL, то время доступа и изменения файла "
"устанавливаются в текущее время."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Changing timestamps is permitted when: either the process has appropriate "
"privileges, or the effective user ID equals the user ID of the file, or "
"I<times> is NULL and the process has write permission for the file."
msgstr ""
"Изменение временных меток разрешено если: процесс имеет соответствующие "
"права или эффективный пользовательский идентификатор равен пользовательскому "
"идентификатору файла, или значение I<times> равно NULL и процесс имеет права "
"на запись в файл."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The I<utimbuf> structure is:"
msgstr "Структура I<utimbuf> выглядит так:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct utimbuf {\n"
"    time_t actime;       /* access time */\n"
"    time_t modtime;      /* modification time */\n"
"};\n"
msgstr ""
"struct utimbuf {\n"
"    time_t actime;       /* время доступа */\n"
"    time_t modtime;      /* время изменения */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<utime>()  system call allows specification of timestamps with a "
"resolution of 1 second."
msgstr ""
"Системный вызов B<utime>() позволяет указывать временные метки с точностью "
"до 1 секунды."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<utimes>()  system call is similar, but the I<times> argument refers to "
"an array rather than a structure.  The elements of this array are I<timeval> "
"structures, which allow a precision of 1 microsecond for specifying "
"timestamps.  The I<timeval> structure is:"
msgstr ""
"Системный вызов B<utimes>() выполняет подобное  действие, но аргумент "
"I<times> указывает на массив, а не на структуру. Элементы массива "
"представляют собой структуры I<timeval>, с помощью которых можно указывать "
"временные метки с точностью до 1 микросекунды . Структура I<timeval>:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timeval {\n"
"    long tv_sec;        /* seconds */\n"
"    long tv_usec;       /* microseconds */\n"
"};\n"
msgstr ""
"struct timeval {\n"
"    long tv_sec;        /* секунды */\n"
"    long tv_usec;       /* микросекунды */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<times[0]> specifies the new access time, and I<times[1]> specifies the new "
"modification time.  If I<times> is NULL, then analogously to B<utime>(), the "
"access and modification times of the file are set to the current time."
msgstr ""
"В I<times[0]> задаётся новое время доступа, а в I<times[1]> новое время "
"изменения. Если значение I<times> равно NULL, то аналогично B<utime>(), "
"время доступа и изменения файла устанавливаются в текущее время."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero returned.  On failure, -1 is returned and I<errno> is "
#| "set to indicate the error."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении возвращается 0. При ошибке возвращается -1, а "
"I<errno> присваивается значение ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Search permission is denied for one of the directories in the path prefix of "
"I<path> (see also B<path_resolution>(7))."
msgstr ""
"Запрещён поиск в одном из каталогов пути I<path> (см. также "
"B<path_resolution>(7))."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<times> is NULL, the caller's effective user ID does not match the owner of "
"the file, the caller does not have write access to the file, and the caller "
"is not privileged (Linux: does not have either the B<CAP_DAC_OVERRIDE> or "
"the B<CAP_FOWNER> capability)."
msgstr ""
"Значение I<times> равно NULL, эффективный пользовательский идентификатор "
"вызывающего процесса не совпадает с владельцем файла, вызывающий не имеет "
"права на запись в файл, и у вызывающего нет привилегий (Linux: не имеет "
"мандата B<CAP_DAC_OVERRIDE> или B<CAP_FOWNER>)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<filename> does not exist."
msgstr "Файл I<filename> не существует."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<times> is not NULL, the caller's effective UID does not match the owner of "
"the file, and the caller is not privileged (Linux: does not have the "
"B<CAP_FOWNER> capability)."
msgstr ""
"Значение I<times> не равно NULL, эффективный пользовательский идентификатор "
"вызывающего процесса не совпадает с владельцем файла и у вызывающего нет "
"привилегий (Linux: не имеет мандата B<CAP_FOWNER>)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<path> resides on a read-only filesystem."
msgstr ""
"I<path> располагается на файловой системе, доступной только для чтения."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<utime>(): SVr4, POSIX.1-2001.  POSIX.1-2008 marks B<utime>()  as obsolete."
msgstr ""
"B<utime>(): SVr4, POSIX.1-2001. В POSIX.1-2008 вызов B<utime>() помечен как "
"устаревший."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<utimes>(): 4.3BSD, POSIX.1-2001."
msgstr "B<utimes>(): 4.3BSD, POSIX.1-2001."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#.  In libc4 and libc5,
#.  .BR utimes ()
#.  is just a wrapper for
#.  .BR utime ()
#.  and hence does not allow a subsecond resolution.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Linux does not allow changing the timestamps on an immutable file, or "
"setting the timestamps to something other than the current time on an append-"
"only file."
msgstr ""
"В Linux нельзя изменять временные метки у недосягаемых (immutable) файлов "
"или задавать временные метки, отличные от текущего времени для файлов, в "
"которые можно только дописывать."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<chattr>(1), B<touch>(1), B<futimesat>(2), B<stat>(2), B<utimensat>(2), "
"B<futimens>(3), B<futimes>(3), B<inode>(7)"
msgstr ""
"B<chattr>(1), B<touch>(1), B<futimesat>(2), B<stat>(2), B<utimensat>(2), "
"B<futimens>(3), B<futimes>(3), B<inode>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "UTIME"
msgstr "UTIME"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>utime.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>utime.hE<gt>>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int utime(const char *>I<filename>B<, const struct utimbuf *>I<times>B<);>\n"
msgstr "B<int utime(const char *>I<filename>B<, const struct utimbuf *>I<times>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int utimes(const char *>I<filename>B<, const struct timeval >I<times>B<[2]);>\n"
msgstr "B<int utimes(const char *>I<filename>B<, const struct timeval >I<times>B<[2]);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<utime>()  system call changes the access and modification times of the "
"inode specified by I<filename> to the I<actime> and I<modtime> fields of "
"I<times> respectively."
msgstr ""
"Системный вызов B<utime>() изменяет время доступа и изменения у inode, "
"указанного в I<filename> на значения полей I<actime> и I<modtime> из "
"структуры I<times>, соответственно."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"I<times>[0] specifies the new access time, and I<times>[1] specifies the new "
"modification time.  If I<times> is NULL, then analogously to B<utime>(), the "
"access and modification times of the file are set to the current time."
msgstr ""
"В I<times>[0] задаётся новое время доступа, а в I<times>[1] новое время "
"изменения. Если значение I<times> равно NULL, то аналогично B<utime>(), "
"время доступа и изменения файла устанавливаются в текущее время."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
