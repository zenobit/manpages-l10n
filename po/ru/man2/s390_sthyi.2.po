# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:26+0100\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "s390_sthyi"
msgstr "s390_sthyi"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "s390_sthyi - emulate STHYI instruction"
msgstr "s390_sthyi - эмулирует инструкцию STHYI"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>asm/sthyi.hE<gt>>        /* Definition of B<STHYI_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>asm/sthyi.hE<gt>>        /* определения констант B<STHYI_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_s390_sthyi, unsigned long >I<function_code>B<,>\n"
"B<            void *>I<resp_buffer>B<, uint64_t *>I<return_code>B<,>\n"
"B<            unsigned long >I<flags>B<);>\n"
msgstr ""
"B<int syscall(SYS_s390_sthyi, unsigned long >I<function_code>B<,>\n"
"B<            void *>I<resp_buffer>B<, uint64_t *>I<return_code>B<,>\n"
"B<            unsigned long >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Glibc does not provide a wrapper for this system call; call it using "
#| "B<syscall>(2)."
msgid ""
"I<Note>: glibc provides no wrapper for B<s390_sthyi>(), necessitating the "
"use of B<syscall>(2)."
msgstr ""
"В glibc нет обёртки для данного системного вызова; запускайте его с помощью "
"B<syscall>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<s390_sthyi>()  system call emulates the STHYI (Store Hypervisor "
"Information) instruction.  It provides hardware resource information for the "
"machine and its virtualization levels.  This includes CPU type and capacity, "
"as well as the machine model and other metrics."
msgstr ""
"Системный вызов B<s390_sthyi>() эмулирует инструкцию STHYI (Store Hypervisor "
"Information). Он предоставляет информацию о ресурсах машины и её уровни "
"виртуализации. В частности, возвращается тип ЦП и ёмкость, а также модель "
"машины и другие свойства."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<function_code> argument indicates which function to perform.  The "
"following code(s) are supported:"
msgstr ""
"В аргументе I<function_code> указывается какую функцию нужно выполнить. "
"Поддерживаются следующие коды:"

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<STHYI_FC_CP_IFL_CAP>"
msgstr "B<STHYI_FC_CP_IFL_CAP>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Return CP (Central Processor) and IFL (Integrated Facility for Linux)  "
"capacity information."
msgstr ""
"Вернуть информацию о CP (Central Processor) и ёмкости IFL (Integrated "
"Facility for Linux)."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The I<resp_buffer> argument specifies the address of a response buffer.  "
"When the I<function_code> is B<STHYI_FC_CP_IFL_CAP>, the buffer must be one "
"page (4K) in size.  If the system call returns 0, the response buffer will "
"be filled with CPU capacity information.  Otherwise, the response buffer's "
"content is unchanged."
msgstr ""
"В аргументе I<resp_buffer> указывается адрес буфера результата. При "
"I<function_code> равном B<STHYI_FC_CP_IFL_CAP>, размер буфера должен быть "
"равен странице (4K). Если системный вызов вернул 0, то буфер результата "
"будет заполнен информацией о ёмкости ЦП. В противном случае содержимое "
"буфера результата не меняется."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<return_code> argument stores the return code of the STHYI instruction, "
"using one of the following values:"
msgstr ""
"В аргументе I<return_code> хранится код возврата инструкции STHYI; возможно "
"одно из следующих значений:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Success."
msgstr "Успешно."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "4"
msgstr "4"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Unsupported function code."
msgstr "Неподдерживаемый код функции."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For further details about I<return_code>, I<function_code>, and "
"I<resp_buffer>, see the reference given in NOTES."
msgstr ""
"Дополнительную информацию о I<return_code>, I<function_code> и "
"I<resp_buffer> смотрите по ссылкам, приведённым в ЗАМЕЧАНИЯХ."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<flags> argument is provided to allow for future extensions and "
"currently must be set to 0."
msgstr ""
"Аргумент I<flags> предназначен для будущих расширений, а пока его значение "
"должно быть равно 0."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success (that is: emulation succeeded), the return value of "
"B<s390_sthyi>()  matches the condition code of the STHYI instructions, which "
"is a value in the range [0..3].  A return value of 0 indicates that CPU "
"capacity information is stored in I<*resp_buffer>.  A return value of 3 "
"indicates \"unsupported function code\" and the content of I<*resp_buffer> "
"is unchanged.  The return values 1 and 2 are reserved."
msgstr ""
"При успешном выполнении (т. е. эмуляция прошла успешно), возвращаемое "
"B<s390_sthyi>() значение совпадает с кодом условия инструкций STHYI — "
"значением в диапазоне [0..3]. Возвращаемое значение 0 означает, что "
"информация о ёмкости ЦП сохранена в I<*resp_buffer>. Возвращаемое значение 3 "
"указывает на «неподдерживаемый код функции» и содержимое I<*resp_buffer> не "
"изменяется. Значения 1 и 2 зарезервированы."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"В случае ошибки возвращается -1, а I<errno> устанавливается в значение "
"ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The value specified in I<resp_buffer> or I<return_code> is not a valid "
"address."
msgstr ""
"Значение, указанное в I<resp_buffer> или I<return_code>, содержит "
"некорректный адрес."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The value specified in I<flags> is nonzero."
msgstr "Значение, указанное в I<flags>, не равно нулю."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Allocating memory for handling the CPU capacity information failed."
msgstr "Не удалось выделить память для обработки информации о ёмкости ЦП."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The value specified in I<function_code> is not valid."
msgstr "Некорректное значение в I<function_code>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "This system call is available since Linux 4.15."
msgstr "Данный системный вызов появился в Linux 4.15."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This Linux-specific system call is available only on the s390 architecture."
msgstr ""
"Данный системный вызов есть только в Linux и доступен только на архитектуре "
"s390."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"For details of the STHYI instruction, see E<.UR https://www.ibm.com\\:/"
"support\\:/knowledgecenter\\:/SSB27U_6.3.0\\:/com.ibm.zvm.v630.hcpb4\\:/"
"hcpb4sth.htm> the documentation page E<.UE .>"
msgstr ""
"Описание инструкции STHYI смотрите на E<.UR https://www.ibm.com\\:/"
"support\\:/knowledgecenter\\:/SSB27U_6.3.0\\:/com.ibm.zvm.v630.hcpb4\\:/"
"hcpb4sth.htm> странице документации E<.UE .>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When the system call interface is used, the response buffer doesn't have to "
"fulfill alignment requirements described in the STHYI instruction definition."
msgstr ""
"При использовании интерфейса системного вызова буфер результата не имеет "
"требований по выравниванию, описанных в инструкции STHYI."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The kernel caches the response (for up to one second, as of Linux 4.16).  "
"Subsequent system call invocations may return the cached response."
msgstr ""
"Ядро кэширует результат (в Linux 4.16 каждую секунду). Последующие запуски "
"системного вызова могут вернуть кэшированный ответ."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<syscall>(2)"
msgstr "B<syscall>(2)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "S390_STHYI"
msgstr "S390_STHYI"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-04-11"
msgstr "11 апреля 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<#include E<lt>asm/unistd.hE<gt>>\n"
msgstr "B<#include E<lt>asm/unistd.hE<gt>>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int s390_sthyi(unsigned long >I<function_code>B<, void *>I<resp_buffer>B<,>\n"
"B<               uint64_t *>I<return_code>B<, unsigned long >I<flags>B<);>\n"
msgstr ""
"B<int s390_sthyi(unsigned long >I<function_code>B<, void *>I<resp_buffer>B<,>\n"
"B<               uint64_t *>I<return_code>B<, unsigned long >I<flags>B<);>\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"The I<resp_buffer> argument specifies the address of a response buffer.  "
"When the I<function_code> is 0, the buffer must be one page (4K) in size.  "
"If the system call returns 0, the response buffer will be filled with CPU "
"capacity information.  Otherwise, the response buffer's content is unchanged."
msgstr ""
"В аргументе I<resp_buffer> указывается адрес буфера результата. При "
"I<function_code> равном 0, размер буфера должен быть равен странице (4K). "
"Если системный вызов вернул 0, то буфер результата будет заполнен "
"информацией о ёмкости ЦП. В противном случае содержимое буфера результата не "
"меняется."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"В случае ошибки возвращается -1 и значение I<errno> устанавливается "
"соответствующим образом."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Glibc does not provide a wrapper for this system call, use B<syscall>(2)  to "
"call it."
msgstr ""
"В glibc нет обёртки для данного системного вызова; запускайте его с помощью "
"B<syscall>(2)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2018-02-02"
msgstr "2 февраля 2018 г."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The I<resp_buffer> argument specifies the address of a response buffer.  "
#| "When the I<function_code> is 0, the buffer must be one page (4K) in "
#| "size.  If the system call returns 0, the response buffer will be filled "
#| "with CPU capacity information.  Otherwise, the response buffer's content "
#| "is unchanged."
msgid ""
"The I<resp_buffer> argument specifies the address of a response buffer.  If "
"the system call returns 0, the response buffer will be filled with CPU "
"capacity information.  Otherwise, the response buffer's content is unchanged."
msgstr ""
"В аргументе I<resp_buffer> указывается адрес буфера результата. При "
"I<function_code> равном 0, размер буфера должен быть равен странице (4K). "
"Если системный вызов вернул 0, то буфер результата будет заполнен "
"информацией о ёмкости ЦП. В противном случае содержимое буфера результата не "
"меняется."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "For details of the STHYI instruction, see E<.UR https://www.ibm.com\\:/"
#| "support\\:/knowledgecenter\\:/SSB27U_6.3.0\\:/com.ibm.zvm.v630.hcpb4\\:/"
#| "hcpb4sth.htm> the documentation page E<.UE .>"
msgid ""
"For details of the STHYI instruction, see E<.UR https://www.ibm.com\\:/"
"support\\:/knowledgecenter\\:/SSB27U_6.3.0\\:/com.ibm.zvm.v630.hcpb4\\:/"
"hcpb4sth.htm> E<.UE .>"
msgstr ""
"Описание инструкции STHYI смотрите на E<.UR https://www.ibm.com\\:/"
"support\\:/knowledgecenter\\:/SSB27U_6.3.0\\:/com.ibm.zvm.v630.hcpb4\\:/"
"hcpb4sth.htm> странице документации E<.UE .>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
