# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:33+0100\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "sysctl"
msgstr "sysctl"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sysctl - read/write system parameters"
msgstr "sysctl - читает/записывает параметры системы"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>unistd.hE<gt>>\n"
"B<#include E<lt>linux/sysctl.hE<gt>>\n"
msgstr ""
"B<#include E<lt>unistd.hE<gt>>\n"
"B<#include E<lt>linux/sysctl.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int _sysctl(struct __sysctl_args *>I<args>B<);>\n"
msgid "B<[[deprecated]] int _sysctl(struct __sysctl_args *>I<args>B<);>\n"
msgstr "B<int _sysctl(struct __sysctl_args *>I<args>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<This system call no longer exists on current kernels!> See NOTES."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<_sysctl>()  call reads and/or writes kernel parameters.  For example, "
"the hostname, or the maximum number of open files.  The argument has the form"
msgstr ""
"Системный вызов B<_sysctl> считывает и/или изменяет параметры ядра. К ним "
"относятся, например, имя машины или максимальное количество открытых файлов. "
"Параметр имеет следующую структуру:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct __sysctl_args {\n"
"    int    *name;    /* integer vector describing variable */\n"
"    int     nlen;    /* length of this vector */\n"
"    void   *oldval;  /* 0 or address where to store old value */\n"
"    size_t *oldlenp; /* available room for old value,\n"
"                        overwritten by actual size of old value */\n"
"    void   *newval;  /* 0 or address of new value */\n"
"    size_t  newlen;  /* size of new value */\n"
"};\n"
msgstr ""
"struct __sysctl_args {\n"
"    int    *name;    /* целочисленный вектор, описывающий\n"
"                        переменную */\n"
"    int     nlen;    /* длина этого вектора */\n"
"    void   *oldval;  /* 0 или адрес старого значения */\n"
"    size_t *oldlenp; /* размер старого значения, заменяется\n"
"                        реальным размером старого значения */\n"
"    void   *newval;  /* 0 или адрес нового значения */\n"
"    size_t  newlen;  /* размер нового значения */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This call does a search in a tree structure, possibly resembling a directory "
"tree under I</proc/sys>, and if the requested item is found calls some "
"appropriate routine to read or modify the value."
msgstr ""
"Этот вызов производит поиск в древовидной структуре, возможно, похожей на "
"структуру каталогов I</proc/sys>, и, если запрашиваемый элемент найден, "
"вызывает соответствующую процедуру, читающую или изменяющую значение."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Upon successful completion, B<_sysctl>()  returns 0.  Otherwise, a value of "
"-1 is returned and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<_sysctl>() возвращает 0. При ошибке возвращается "
"-1, а переменной I<errno> присваивается номер ошибки."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>, B<EPERM>"
msgstr "B<EACCES>, B<EPERM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"No search permission for one of the encountered \"directories\", or no read "
"permission where I<oldval> was nonzero, or no write permission where "
"I<newval> was nonzero."
msgstr ""
"Нет прав на поиск в одном из встретившихся «каталогов» или нет прав на "
"чтение, если I<oldval> не равно нулю; или нет прав на запись, если I<newval> "
"не равно нулю."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The invocation asked for the previous value by setting I<oldval> non-NULL, "
"but allowed zero room in I<oldlenp>."
msgstr ""
"Был сделан запрос предыдущего значения путём установки не-NULL значения "
"I<oldval>, но размер места под него в I<oldlenp> равен нулю."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<name> was not found."
msgstr "I<name> не найден."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<setns>()  system call first appeared in Linux in kernel 3.0; "
#| "library support was added to glibc in version 2.14."
msgid ""
"This system call first appeared in Linux 1.3.57.  It was removed in Linux "
"5.5; glibc support was removed in glibc 2.32."
msgstr ""
"Системный вызов B<setns>() впервые появился в ядре Linux версии 3.0; "
"поддержка в glibc добавлена в версии 2.14."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This call is Linux-specific, and should not be used in programs intended "
#| "to be portable.  A B<sysctl>()  call has been present in Linux since "
#| "version 1.3.57.  It originated in 4.4BSD.  Only Linux has the I</proc/"
#| "sys> mirror, and the object naming schemes differ between Linux and "
#| "4.4BSD, but the declaration of the B<sysctl>()  function is the same in "
#| "both."
msgid ""
"This call is Linux-specific, and should not be used in programs intended to "
"be portable.  It originated in 4.4BSD.  Only Linux has the I</proc/sys> "
"mirror, and the object naming schemes differ between Linux and 4.4BSD, but "
"the declaration of the B<sysctl>()  function is the same in both."
msgstr ""
"Этот вызов есть только в Linux и не должен использоваться в переносимых "
"программах. Вызов B<sysctl>() впервые появился в Linux 1.3.57. Впервые он "
"появился в 4.4BSD. Только в Linux существует зеркало I</proc/sys>, и схемы "
"именования объектов в Linux и 4.4BSD различаются, но объявление функции "
"B<sysctl>() одинаково в обеих системах."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Use of this system call was long discouraged: since Linux 2.6.24, uses of "
"this system call result in warnings in the kernel log, and in Linux 5.5, the "
"system call was finally removed.  Use the I</proc/sys> interface instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that on older kernels where this system call still exists, it is "
"available only if the kernel was configured with the "
"B<CONFIG_SYSCTL_SYSCALL> option.  Furthermore, glibc does not provide a "
"wrapper for this system call, necessitating the use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The object names vary between kernel versions, making this system call "
"worthless for applications."
msgstr ""
"Названия объектов различаются в разных версиях ядра, что делает данный вызов "
"в приложениях бесполезным."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Not all available objects are properly documented."
msgstr "Не все существующие объекты описаны соответствующим образом."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It is not yet possible to change operating system by writing to I</proc/sys/"
"kernel/ostype>."
msgstr ""
"В настоящее время невозможно изменить тип операционной системы путём записи "
"в файл I</proc/sys/kernel/ostype>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "#include E<lt>linux/sysctl.hE<gt>\n"
msgstr "#include E<lt>linux/sysctl.hE<gt>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "#define ARRAY_SIZE(arr)  (sizeof(arr) / sizeof((arr)[0]))\n"
msgstr "#define ARRAY_SIZE(arr)  (sizeof(arr) / sizeof((arr)[0]))\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "int _sysctl(struct __sysctl_args *args);\n"
msgstr "int _sysctl(struct __sysctl_args *args);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "#define OSNAMESZ 100\n"
msgstr "#define OSNAMESZ 100\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    int                   name[] = { CTL_KERN, KERN_OSTYPE };\n"
"    char                  osname[OSNAMESZ];\n"
"    size_t                osnamelth;\n"
"    struct __sysctl_args  args;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    int                   name[] = { CTL_KERN, KERN_OSTYPE };\n"
"    char                  osname[OSNAMESZ];\n"
"    size_t                osnamelth;\n"
"    struct __sysctl_args  args;\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = ARRAY_SIZE(name);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"
msgstr ""
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = ARRAY_SIZE(name);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "    osnamelth = sizeof(osname);\n"
msgstr "    osnamelth = sizeof(osname);\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"This machine is running %*s\\en\", (int) osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"Эта машина работает в %*s\\en\", (int) osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: sysctl.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SYSCTL"
msgstr "SYSCTL"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 ноября 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int _sysctl(struct __sysctl_args *>I<args>B<);>\n"
msgstr "B<int _sysctl(struct __sysctl_args *>I<args>B<);>\n"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The B<setns>()  system call first appeared in Linux in kernel 3.0; "
#| "library support was added to glibc in version 2.14."
msgid ""
"This system call first appeared in Linux 1.3.57.  It was removed in Linux "
"5.5; glibc support was removed in version 2.32."
msgstr ""
"Системный вызов B<setns>() впервые появился в ядре Linux версии 3.0; "
"поддержка в glibc добавлена в версии 2.14."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>linux/sysctl.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>linux/sysctl.hE<gt>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "int _sysctl(struct __sysctl_args *args );\n"
msgstr "int _sysctl(struct __sysctl_args *args );\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct __sysctl_args args;\n"
"    char osname[OSNAMESZ];\n"
"    size_t osnamelth;\n"
"    int name[] = { CTL_KERN, KERN_OSTYPE };\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct __sysctl_args args;\n"
"    char osname[OSNAMESZ];\n"
"    size_t osnamelth;\n"
"    int name[] = { CTL_KERN, KERN_OSTYPE };\n"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = sizeof(name)/sizeof(name[0]);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"
msgstr ""
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = sizeof(name)/sizeof(name[0]);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"This machine is running %*s\\en\", osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"Эта машина работает в %*s\\en\", osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""
"I<Замечание>: В glibc нет обёрточной функции для данного системного вызова; "
"смотрите ЗАМЕЧАНИЯ."

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<Do not use this system call!> See NOTES."
msgstr "B<Не используйте этот системный вызов!> Смотрите ЗАМЕЧАНИЯ."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This call is Linux-specific, and should not be used in programs intended to "
"be portable.  A B<sysctl>()  call has been present in Linux since version "
"1.3.57.  It originated in 4.4BSD.  Only Linux has the I</proc/sys> mirror, "
"and the object naming schemes differ between Linux and 4.4BSD, but the "
"declaration of the B<sysctl>()  function is the same in both."
msgstr ""
"Этот вызов есть только в Linux и не должен использоваться в переносимых "
"программах. Вызов B<sysctl>() впервые появился в Linux 1.3.57. Впервые он "
"появился в 4.4BSD. Только в Linux существует зеркало I</proc/sys>, и схемы "
"именования объектов в Linux и 4.4BSD различаются, но объявление функции "
"B<sysctl>() одинаково в обеих системах."

#.  See http://lwn.net/Articles/247243/
#.  Though comments in suggest that it is needed by old glibc binaries,
#.  so maybe it's not going away.
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Glibc does not provide a wrapper for this system call; call it using "
"B<syscall>(2).  Or rather...  I<don't> call it: use of this system call has "
"long been discouraged, and it is so unloved that B<it is likely to disappear "
"in a future kernel version>.  Since Linux 2.6.24, uses of this system call "
"result in warnings in the kernel log.  Remove it from your programs now; use "
"the I</proc/sys> interface instead."
msgstr ""
"В glibc отсутствует обёрточная функция для этого системного вызова; "
"вызывайте его через B<syscall>(2).  Или лучше I<совсем> не используйте: "
"использование данного системного вызова уже давно не рекомендуется и он так "
"всем не нравится, что, B<вероятно, исчезнет в новой версии ядра>. Начиная с "
"Linux 2.6.24, при использовании данного системного вызова записывается "
"предупреждение в журнал ядра. Удалите вызов из своих программ прямо сейчас; "
"вместо него используйте интерфейс I</proc/sys>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This system call is available only if the kernel was configured with the "
"B<CONFIG_SYSCTL_SYSCALL> option."
msgstr ""
"Данный системный вызов доступен только, если ядро было собрано с параметром "
"B<CONFIG_SYSCTL_SYSCALL>."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    memset(&args, 0, sizeof(struct __sysctl_args));\n"
"    args.name = name;\n"
"    args.nlen = sizeof(name)/sizeof(name[0]);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"
msgstr ""
"    memset(&args, 0, sizeof(struct __sysctl_args));\n"
"    args.name = name;\n"
"    args.nlen = sizeof(name)/sizeof(name[0]);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
