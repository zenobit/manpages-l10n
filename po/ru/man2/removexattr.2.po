# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:24+0100\n"
"PO-Revision-Date: 2019-10-06 09:20+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "removexattr"
msgstr "removexattr"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "removexattr, lremovexattr, fremovexattr - remove an extended attribute"
msgstr ""
"removexattr, lremovexattr, fremovexattr - удаление расширенных атрибутов"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr "B<#include E<lt>sys/xattr.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int removexattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<);>\n"
"B<int lremovexattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<);>\n"
"B<int fremovexattr(int >I<fd>B<, const char\\ *>I<name>B<);>\n"
msgstr ""
"B<int removexattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<);>\n"
"B<int lremovexattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<);>\n"
"B<int fremovexattr(int >I<fd>B<, const char\\ *>I<name>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Extended attributes are I<name>:I<value> pairs associated with inodes "
"(files, directories, symbolic links, etc.).  They are extensions to the "
"normal attributes which are associated with all inodes in the system (i.e., "
"the B<stat>(2)  data).  A complete overview of extended attributes concepts "
"can be found in B<xattr>(7)."
msgstr ""
"Расширенные атрибуты представляют собой пару I<имя>:I<значение> и "
"связываются с записями inode (файлы, каталоги, символьные ссылки и т.п.). "
"Они являются расширениями к обычным атрибутам, связанным со всеми записями "
"inode в системе (например, данные B<stat>(2)). Полное описание модели "
"расширенных атрибутов можно найти в B<xattr>(7)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<removexattr>()  removes the extended attribute identified by I<name> and "
"associated with the given I<path> in the filesystem."
msgstr ""
"Вызов B<removexattr>() удаляет расширенный атрибут с именем, заданным в "
"I<name> и связанный с заданным I<path> в файловой системе."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<lremovexattr>()  is identical to B<removexattr>(), except in the case of a "
"symbolic link, where the extended attribute is removed from the link itself, "
"not the file that it refers to."
msgstr ""
"Вызов B<lremovexattr>() идентичен B<removexattr>(), за исключением случая "
"работы с символьными ссылками; он удаляет расширенный атрибут на ссылке, а "
"не на файле, на который она указывает."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fremovexattr>()  is identical to B<removexattr>(), only the extended "
"attribute is removed from the open file referred to by I<fd> (as returned by "
"B<open>(2))  in place of I<path>."
msgstr ""
"Вызов B<fremovexattr>() идентичен B<removexattr>(), отличием является то, "
"что расширенный атрибут удаляется у открытого файла, на который указывает "
"I<fd> (возвращаемый B<open>(2)), а не на указанном в I<path>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"An extended attribute name is a null-terminated string.  The I<name> "
"includes a namespace prefix; there may be several, disjoint namespaces "
"associated with an individual inode."
msgstr ""
"Имя расширенного атрибута представляет собой строку, заканчивающуюся NULL. "
"Имя I<name> включает префикс пространства имён; их может быть несколько, "
"разрозненные пространства ассоциируются с разными inode."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On failure, -1 is returned and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set to indicate the error."
msgstr ""
"При успешном выполнении возвращается ноль. В случае ошибки возвращается -1, "
"а I<errno> устанавливается в соответствующее значение."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ENODATA>"
msgstr "B<ENODATA>"

#.  .RB ( ENOATTR
#.  is defined to be a synonym for
#.  .BR ENODATA
#.  in
#.  .IR <attr/attributes.h> .)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The named attribute does not exist."
msgstr "Указанный атрибут не существует."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSUP>"
msgstr "B<ENOTSUP>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Extended attributes are not supported by the filesystem, or are disabled."
msgstr ""
"Расширенные атрибуты не поддерживаются файловой системой или отключены."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "In addition, the errors documented in B<stat>(2)  can also occur."
msgstr "Также могут возникать ошибки, описанные в B<stat>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "These system calls have been available on Linux since kernel 2.4; glibc "
#| "support is provided since version 2.3."
msgid ""
"These system calls have been available since Linux 2.4; glibc support is "
"provided since glibc 2.3."
msgstr ""
"Данные системные вызовы доступны в Linux начиная с ядра версии 2.4; "
"поддержка в glibc появилась в версии 2.3."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#.  .SH AUTHORS
#.  Andreas Gruenbacher,
#.  .RI < a.gruenbacher@computer.org >
#.  and the SGI XFS development team,
#.  .RI < linux-xfs@oss.sgi.com >.
#.  Please send any bug reports or comments to these addresses.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These system calls are Linux-specific."
msgstr "Данные системные вызовы есть только в Linux."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<setxattr>(2), B<stat>(2), B<symlink>(7), B<xattr>(7)"
msgstr ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<setxattr>(2), B<stat>(2), B<symlink>(7), B<xattr>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "REMOVEXATTR"
msgstr "REMOVEXATTR"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 марта 2019 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set appropriately."
msgstr ""
"При успешном выполнении возвращается ноль. В случае ошибки возвращается -1, "
"а I<errno> устанавливается в соответствующее значение."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"These system calls have been available on Linux since kernel 2.4; glibc "
"support is provided since version 2.3."
msgstr ""
"Данные системные вызовы доступны в Linux начиная с ядра версии 2.4; "
"поддержка в glibc появилась в версии 2.3."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2015-05-07"
msgstr "7 мая 2015 г."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<ENOATTR>"
msgstr "B<ENOATTR>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The named attribute does not exist.  (B<ENOATTR> is defined to be a synonym "
"for B<ENODATA> in I<E<lt>attr/xattr.hE<gt>>.)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
