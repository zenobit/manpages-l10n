# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:02+0100\n"
"PO-Revision-Date: 2019-10-05 07:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ctermid>()"
msgid "ctermid"
msgstr "B<ctermid>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ctermid - get controlling terminal name"
msgstr "ctermid - возвращает имя управляющего терминала"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#.  POSIX also requires this function to be declared in <unistd.h>,
#.  and glibc does so if suitable feature test macros are defined.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *ctermid(char *>I<s>B<);>\n"
msgstr "B<char *ctermid(char *>I<s>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<ctermid>()"
msgid "B<ctermid>():"
msgstr "B<ctermid>()"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE\n"
msgstr "    _POSIX_C_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ctermid>()  returns a string which is the pathname for the current "
"controlling terminal for this process.  If I<s> is NULL, a static buffer is "
"used, otherwise I<s> points to a buffer used to hold the terminal pathname.  "
"The symbolic constant B<L_ctermid> is the maximum number of characters in "
"the returned pathname."
msgstr ""
"Функция B<ctermid>() возвращает строку с полным путём текущего управляющего "
"терминала процесса. Если I<s> равно NULL, то используется статический буфер, "
"в противном случае I<s> указывает на буфер для хранения пути терминала. "
"Символической константой B<L_ctermid> определяется максимальное количество "
"символов в возвращаемом пути."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The pointer to the pathname."
msgstr "Указатель на путь."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ctermid>()"
msgstr "B<ctermid>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, Svr4."
msgstr "POSIX.1-2001, POSIX.1-2008, Svr4."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The returned pathname may not uniquely identify the controlling terminal; it "
"may, for example, be I</dev/tty>."
msgstr ""
"Возвращаемый путь может быть не уникальным для идентификации управляющего "
"терминала; например, это может быть I</dev/tty>."

#. #-#-#-#-#  archlinux: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in 2.4.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  fedora-38: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in 2.4.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ctermid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  in glibc 2.3.x, x >= 4, the glibc headers threw an error
#.  if ctermid() was given an argument; fixed in glibc 2.4.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "It is not assured that the program can open the terminal."
msgstr ""
"Не выполняется проверка, что программа может открыть управляющий терминал."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ttyname>(3)"
msgstr "B<ttyname>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CTERMID"
msgstr "CTERMID"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 марта 2019 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<ctermid>(): _POSIX_C_SOURCE"
msgstr "B<ctermid>(): _POSIX_C_SOURCE"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 марта 2016 г."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The returned pathname may not uniquely identify the controlling terminal; "
#| "it may, for example, be I</dev/tty>."
msgid ""
"The path returned may not uniquely identify the controlling terminal; it "
"may, for example, be I</dev/tty>."
msgstr ""
"Возвращаемый путь может быть не уникальным для идентификации управляющего "
"терминала; например, это может быть I</dev/tty>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
