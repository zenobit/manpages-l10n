# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alex Nik <rage.iz.me@gmail.com>, 2013.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:19+0100\n"
"PO-Revision-Date: 2019-09-19 18:57+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<timer_gettime>(2)"
msgid "ntp_gettime"
msgstr "B<timer_gettime>(2)"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ntp_gettime, ntp_gettimex - get time parameters (NTP daemon interface)"
msgstr ""
"ntp_gettime, ntp_gettimex - возвращает параметры времени (интерфейс службы "
"NTP)"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/timex.hE<gt>>\n"
msgstr "B<#include E<lt>sys/timex.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int ntp_gettimex(struct ntptimeval *>I<ntv>B<);>\n"
msgid ""
"B<int ntp_gettime(struct ntptimeval *>I<ntv>B<);>\n"
"B<int ntp_gettimex(struct ntptimeval *>I<ntv>B<);>\n"
msgstr "B<int ntp_gettimex(struct ntptimeval *>I<ntv>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Both of these APIs return information to the caller via the I<ntv> argument, "
"a structure of the following type:"
msgstr ""
"Эти функции возвращают информацию вызывающему через аргумент I<ntv>, "
"структуру следующего вида:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct ntptimeval {\n"
#| "    struct timeval time;        /* Current time */\n"
#| "    long int maxerror;          /* Maximum error */\n"
#| "    long int esterror;          /* Estimated error */\n"
#| "    long int tai;               /* TAI offset */\n"
msgid ""
"struct ntptimeval {\n"
"    struct timeval time;    /* Current time */\n"
"    long maxerror;          /* Maximum error */\n"
"    long esterror;          /* Estimated error */\n"
"    long tai;               /* TAI offset */\n"
msgstr ""
"struct ntptimeval {\n"
"    struct timeval time;        /* текущее время */\n"
"    long int maxerror;          /* максимальная ошибка */\n"
"    long int esterror;          /* расчётная ошибка */\n"
"    long int tai;               /* смещение TAI */\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Further padding bytes allowing for future expansion */\n"
"};\n"
msgstr ""
"    /* байты-заполнители под будущее расширение */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The fields of this structure are as follows:"
msgstr "Поля этой структуры имеют следующее назначение:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<time>"
msgstr "I<time>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The current time, expressed as a I<timeval> structure:"
msgstr "Текущее время, представляется структурой I<timeval>:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timeval {\n"
"    time_t      tv_sec;   /* Seconds since the Epoch */\n"
"    suseconds_t tv_usec;  /* Microseconds */\n"
"};\n"
msgstr ""
"struct timeval {\n"
"    time_t      tv_sec;   /* кол-во секунд, начиная с Эпохи */\n"
"    suseconds_t tv_usec;  /* кол-во микросекунд */\n"
"};\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<maxerror>"
msgstr "I<maxerror>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Maximum error, in microseconds.  This value can be initialized by "
"B<ntp_adjtime>(3), and is increased periodically (on Linux: each second), "
"but is clamped to an upper limit (the kernel constant B<NTP_PHASE_MAX>, with "
"a value of 16,000)."
msgstr ""
"Максимальная ошибка, в микросекундах. Это значение может быть "
"инициализировано B<ntp_adjtime>(3), и периодически увеличивается (в Linux "
"каждую секунду), но не переходит верхний порог (константа ядра "
"B<NTP_PHASE_MAX>, равная 16000)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<esterror>"
msgstr "I<esterror>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Estimated error, in microseconds.  This value can be set via "
"B<ntp_adjtime>(3)  to contain an estimate of the difference between the "
"system clock and the true time.  This value is not used inside the kernel."
msgstr ""
"Расчётная ошибка, в микросекундах. Это значение может быть установлено с "
"помощью B<ntp_adjtime>(3) и содержит расчётную разницу между системными "
"часами и настоящим временем. Данное значение не используется внутри ядра."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<tai>"
msgstr "I<tai>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "TAI (Atomic International Time) offset."
msgstr "Смещение TAI (Atomic International Time, атомное международное время)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ntp_gettime>()  returns an I<ntptimeval> structure in which the I<time>, "
"I<maxerror>, and I<esterror> fields are filled in."
msgstr ""
"Функция B<ntp_gettime>() возвращает структуру I<ntptimeval> с заполненными "
"полями I<time>, I<maxerror> и I<esterror>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ntp_gettimex>()  performs the same task as B<ntp_gettime>(), but also "
"returns information in the I<tai> field."
msgstr ""
"Функция B<ntp_gettimex>() выполняет тоже что и B<ntp_gettime>() и "
"дополнительно возвращает информацию в поле I<tai>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#.  FIXME . the info page incorrectly describes the return values.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The return values for B<ntp_gettime>()  and B<ntp_gettimex>()  are as for "
"B<adjtimex>(2).  Given a correct pointer argument, these functions always "
"succeed."
msgstr ""
"Возвращаемые значения B<ntp_gettime>() и B<ntp_gettimex>() такие же как у "
"B<adjtimex>(2). При корректным переданном указателе эти функции всегда "
"выполняются без ошибок."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<ntp_gettime>()  function is available since glibc 2.1.  The "
"B<ntp_gettimex>()  function is available since glibc 2.12."
msgstr ""
"Функция B<ntp_gettime>() доступна в glibc с версии 2.1. Функция "
"B<ntp_gettimex>() доступна в glibc с версии 2.12."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<ntp_gettime>(),\n"
"B<ntp_gettimex>()"
msgstr ""
"B<ntp_gettime>(),\n"
"B<ntp_gettimex>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ntp_gettime>()  is described in the NTP Kernel Application Program "
"Interface.  B<ntp_gettimex>()  is a GNU extension."
msgstr ""
"Функция B<ntp_gettime>() описана в интерфейсе NTP Kernel Application "
"Program. Функция B<ntp_gettimex>() является расширением GNU."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<adjtimex>(2)  B<ntp_adjtime>(3), B<time>(7)"
msgid "B<adjtimex>(2), B<ntp_adjtime>(3), B<time>(7)"
msgstr "B<adjtimex>(2)  B<ntp_adjtime>(3), B<time>(7)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"E<.UR http://www.slac.stanford.edu/comp/unix/\\:package/\\:rtems/\\:src/\\:"
"ssrlApps/\\:ntpNanoclock/\\:api.htm> NTP \"Kernel Application Program "
"Interface\" E<.UE>"
msgstr ""
"E<.UR http://www.slac.stanford.edu/comp/unix/\\:package/\\:rtems/\\:src/\\:"
"ssrlApps/\\:ntpNanoclock/\\:api.htm> NTP \"Kernel Application Program "
"Interface\" E<.UE>"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "NTP_GETTIME"
msgstr "NTP_GETTIME"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 ноября 2020 г."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int ntp_gettime(struct ntptimeval *>I<ntv>B<);>\n"
msgstr "B<int ntp_gettime(struct ntptimeval *>I<ntv>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int ntp_gettimex(struct ntptimeval *>I<ntv>B<);>\n"
msgstr "B<int ntp_gettimex(struct ntptimeval *>I<ntv>B<);>\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 5.10. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct ntptimeval {\n"
"    struct timeval time;        /* Current time */\n"
"    long int maxerror;          /* Maximum error */\n"
"    long int esterror;          /* Estimated error */\n"
"    long int tai;               /* TAI offset */\n"
msgstr ""
"struct ntptimeval {\n"
"    struct timeval time;        /* текущее время */\n"
"    long int maxerror;          /* максимальная ошибка */\n"
"    long int esterror;          /* расчётная ошибка */\n"
"    long int tai;               /* смещение TAI */\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<adjtimex>(2)  B<ntp_adjtime>(3), B<time>(7)"
msgstr "B<adjtimex>(2)  B<ntp_adjtime>(3), B<time>(7)"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
