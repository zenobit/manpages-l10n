# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jarno Elonen <elonen@iki.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-06-16 17:20+0200\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Jarno Elonen <elonen@iki.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "pico"
msgstr "pico"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Version 5.09"
msgstr "Version 5.09"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Name"
msgstr "Nimi"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "pico - simple text editor in the style of the Alpine Composer"
msgstr "pico - yksinkertainen, Alpine Composerin tyylinen tekstieditori"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Syntax"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<pico> [ I<options> ] [ I<file> ]"
msgstr "B<pico> [ I<komentorivivalitsimet> ] [ I<tiedostot> ]"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Description"
msgstr "Kuvaus"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"I<Pico> is a simple, display-oriented text editor based on the Alpine "
"message system composer.  As with Alpine, commands are displayed at the "
"bottom of the screen, and context-sensitive help is provided.  As characters "
"are typed they are immediately inserted into the text."
msgstr ""
"I<Pico> on yksinkertainen, sähköpostiohjelma I<Alpine>:n viestieditoriin "
"(Pine Composer) pohjautuva, koko näytöllä toimiva (\"graafinen\") "
"tekstieditori.  Pico:ssa, kuten Alpinessäkin, on sisäänrakennettu "
"aputoiminto ja ohjelman komennot löytyvät ruudun alareunasta.  Tekstiin "
"tehdyt muutokset näkyvät ruudulla välittömästi."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Editing commands are entered using control-key combinations.  As a work-"
"around for communications programs that swallow certain control characters, "
"you can emulate a control key by pressing ESCAPE twice, followed by the "
"desired control character, e.g. \"ESC ESC c\" would be equivalent to "
"entering a ctrl-c.  The editor has five basic features: paragraph "
"justification, searching, block cut/paste, a spelling checker, and a file "
"browser."
msgstr ""
"I<Pico>:a käytetään control-näppäinyhdistelmillä.  Koska monet pääteohjelmat "
"eivät päästä läpi control-näppäimen painalluksia, ne voi korvata myös "
"painamalla ensin kaksi kertaa esc-näppäintä ja sen jälkeen haluttua "
"komentokirjainta.  Esim. \"ESC ESC c\" toimii samalla tavalla kuin \"CTRL-"
"c\".  Ohjelman tärkeimmät ominaisuudet ovat kappaleen tasaus, tekstin haku, "
"lohkojen leikkaus/liimaus, oikoluku ja tiedostoselain.  Näistä lisää "
"edempänä."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Paragraph justification (or filling) takes place in the paragraph that "
"contains the cursor, or, if the cursor is between lines, in the paragraph "
"immediately below.  Paragraphs are delimited by blank lines, or by lines "
"beginning with a space or tab.  Unjustification can be done immediately "
"after justification using the control-U key combination."
msgstr ""
"Kappaleen tasaus (Justification) suoritetaan sille kappaleelle, jonka "
"kohdalla kursori on.  Jos kursori on kappaleiden välissä, tasataan sen alla "
"oleva kappale.  Kappaleet erotetaan toisistaan joko tyhjällä rivillä tai "
"lisäämällä aina uuden kappaleen alkuun välilyöntejä tai sarkaimia "
"(tabulaattoreita).  Komennon voi peruuttaa painamalla \"CTRL-U\" heti "
"tasauksen jälkeen."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"String searches are not sensitive to case.  A search begins at the current "
"cursor position and wraps around the end of the text.  The most recent "
"search string is offered as the default in subsequent searches."
msgstr ""
"Merkkijonohaut eivät erottele pieniä ja isoja kirjaimia.  Haku alkaa "
"kursorista ja hyppää tekstin loputtua alkuun.  Peräkkäisiin hakuihin ohjelma "
"tarjoaa aina edellisessä haussa käytettyä merkkijonoa."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Blocks of text can be moved, copied or deleted with creative use of the "
"command for mark (ctrl-^), delete (ctrl-k) and undelete (ctrl-u).  The "
"delete command will remove text between the \"mark\" and the current cursor "
"position, and place it in the \"cut\" buffer.  The undelete command effects "
"a \"paste\" at the current cursor position."
msgstr ""
"Tekstilohkoja voi siirrellä, kopioida ja poistaa käyttämällä merkkaussta "
"(\"CTRL-^\"), leikkausta (\"CTRL-k\") ja liimausta (\"CTRL-u\").  "
"Leikkauskomento poistaa lohkon \"CTRL-^\":llä asetetun merkin ja osoittimen "
"välistä ja tallentaa sen liimausta varten muistiin.  \"CTRL-u\" liimaa "
"puskuriin tallennetun lohkon osoittimen kohdalle."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The spell checker examines all words in the text.  It then offers, in turn, "
"each misspelled word for correction while highlighting it in the text.  "
"Spell checking can be cancelled at any time.  Alternatively, I<pico> will "
"substitute for the default spell checking routine a routine defined by the "
"SPELL environment variable.  The replacement routine should read standard "
"input and write standard output."
msgstr ""
"Oikolukija tarkistaa tekstin sana kerrallaan.  Se pysähtyy jokaisen väärin "
"kirjoitetun sanan kohdalla, maalaa sen ja ehdottaa korjausta.  Oikoluvun voi "
"keskeyttää milloin tahansa.  I<Pico> osaa käyttää myös SPELL-"
"ympäristömuuttujalla määriteltyä oikolukurutiinia, jonka pitää lukea sanat "
"vakiostyötevirrasta ja kirjoittaa vastaavasti vakiotulostevirtaan."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The file browser is offered as an option in the \"Read File\" and \"Write "
"Out\" command prompts.  It is intended to help in searching for specific "
"files and navigating directory hierarchies.  Filenames with sizes and names "
"of directories in the current working directory are presented for "
"selection.  The current working directory is displayed on the top line of "
"the display while the list of available commands takes up the bottom two.  "
"Several basic file manipulation functions are supported: file renaming, "
"copying, and deletion."
msgstr ""
"Tiedostoselainta (File Browser) voi halutessaan käyttää tiedoston luku- ja "
"kirjoituskomentojen kanssa (\"Read File\" ja \"Write Out\").  Selain on "
"tarkoitettu helpottamaan tiedostojen etsimistä ja hakemistorakenteissa "
"liikkumista.  Selain näyttää kullakin hetkellä käytössä olevan hakemiston "
"nimen ruudun yläreunassa ja sen alla hakemistossa olevien tiedostojen nimet "
"ja koot.  Ruudun alareunassa näkyvät, kuten aina muulloinkin, käytössä "
"olevat näppäinkomennot.  Tiedostoselaimen avulla voi myös kopioida, poistaa "
"ja nimetä tiedostoja uudelleen."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "More specific help is available in I<pico>'s online help."
msgstr "Tarkempia ohjeita saa I<pico>:n sisäänrakennetun aputoiminnon avulla."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Options"
msgstr "Valitsimet"

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<+>I<n>"
msgstr "B<+>I<n>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Causes I<pico> to be started with the cursor located I<n> lines into the "
"file. (Note: no space between \"+\" sign and number)"
msgstr ""
"Siirtää kursoria käynnistyksen yhteydessä I<n> riviä alaspäin.  (Huom: \"+\"-"
"merkin ja numeron väliin ei tule välilyöntiä)"

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Display all files including those beginning with a period (.)."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Enable the option to Replace text matches found using the \"Where is\" "
"command. This now does nothing. Instead, the option is always turned on (as "
"if the -b flag had been specified)."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Rebind the \"delete\" key so the character the cursor is on is rubbed out "
"rather than the character to its left."
msgstr ""
"Muuttaa DELETE-näppäimen toimintaa siten, että se poistaa merkkejä kursorin "
"kohdalta eikä vasemmalta puolelta."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Enable file name completion."
msgstr "Kytkee päälle tiedostonimien automaattisen täytön (\"arvauksen\")."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Use function keys for commands.  This option supported only in conjunction "
"with UW Enhanced NCSA telnet."
msgstr ""
"Hyväksyy funktionäppäimien käytön komentojen syöttämiseen.  Ominaisuus "
"toimii vain UW Enhanced NCSA telnet:in kanssa."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "List valid command line options."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-j>"
msgstr "B<-j>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Enable \"Goto\" command in the file browser.  This enables the command to "
"permit explicitly telling I<pilot> which directory to visit."
msgstr ""
"Kytkee päälle tiedostoselaimen \"Goto\"-komennon.  Goto:n avulla "
"tiedostoselaimelle voi syöttää suoraan hakemistopolkuja."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-g>"
msgstr "B<-g>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Enable \"Show Cursor\" mode in file browser.  Cause cursor to be positioned "
"before the current selection rather than placed at the lower left of the "
"display."
msgstr ""
"Kursori siirretään tiedostoja selailtaessa valinnan kohdalle.  Tavallisesti "
"kursori on selauksen aikana ruudun vasemmassa alareunassa."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Causes \"Cut Text\" command to remove characters from the cursor position to "
"the end of the line rather than remove the entire line."
msgstr ""
"Muuttaa leikkaa-toiminnon (Cut Text) poistamaan rivin vain loppuosan, alkaen "
"kursorista.  Tavallisesti komento poistaa koko rivin."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"Enable mouse functionality.  This only works when I<pico> is run from within "
"an X Window System \"xterm\" window."
msgstr "Kytkee päälle hiirituen.  Ominaisuus toimii vain X Windowsilla."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-n>I<n>"
msgstr "B<-n>I<n>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The -nI<n> option enables new mail notification.  The I<n> argument is "
"optional, and specifies how often, in seconds, your mailbox is checked for "
"new mail.  For example, -n60 causes I<pico> to check for new mail once every "
"minute.  The default interval is 180 seconds, while the minimum allowed is "
"30. (Note: no space between \"n\" and the number)"
msgstr ""
"Käynnistää sähköpostintarkistajan.  Valinnaisella I<n> -osalla voi säätää "
"kuinka usein posti tarkistetaan.  Tarkistusväli annetaan sekunteina.  "
"Esimerkiksi komennolla -n60 I<pico> tarkistaa sähköpostin kerran "
"minuutissa.  Oletusarvo on 180 ja pienin sallittu luku 30.  (Huom: \"n\"-"
"kirjaimen ja numeron väliin ei tule välilyöntiä)"

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-o\\ >I<dir>"
msgstr "B<-o\\ >I<hakemisto>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Sets operating directory.  Only files within this directory are accessible.  "
"Likewise, the file browser is limited to the specified directory subtree."
msgstr ""
"Asettaa työhakemiston.  Käyttäjä pääsee komennon jälkeen käsiksi ainoastaan "
"annetussa hakemistossa ja sen alihakemistoissa oleviin tiedostoihin.  "
"Komento vaikuttaa myös tiedostoselaimeen."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-r>I<n>"
msgstr "B<-r>I<n>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Sets column used to limit the \"Justify\" command's right margin"
msgstr "Asettaa tasausta (Justify) rajaavan oikean reunan marginaalin paikan."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s\\ >I<speller>"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Specify an alternate program I<spell> to use when spell checking."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-t>"
msgstr "B<-t>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Enable \"tool\" mode.  Intended for when I<pico> is used as the editor "
"within other tools (e.g., Elm, Pnews).  I<Pico> will not prompt for save on "
"exit, and will not rename the buffer during the \"Write Out\" command."
msgstr ""
"Käynnistää I<pico>:n \"työkalu\"-tilassa.  Ominaisuus on tarkoittu "
"tilanteisiin, joissa I<pico>:a käytetään muiden ohjelmien (kuten Elm, "
"Pnews)  tekstieditorina.  Tällöin I<Pico> ei ehdota sulkeutuessaan "
"tallennusta eikä vaihda puskurin nimeä \"Write out\"-komennon aikana."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "View the file only, disallowing any editing."
msgstr "Sallii vain tiedoston katselun.  (ts.  estää muokkauksen)"

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-version>"
msgstr "B<-version>"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
#| msgid "Display version information and exit."
msgid "Print Pico version and exit."
msgstr "Näytä ohjelman versiotiedot ja poistu."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-w>"
msgstr "B<-w>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Disable word wrap (thus allow editing of long lines)."
msgstr ""
"Kytkee automaattisen rivinvaihdon pois päältä.  Sopii pitkien rivien "
"muokkaukseen."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-x>"
msgstr "B<-x>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Disable keymenu at the bottom of the screen."
msgstr "Poistaa komentolistan ruudun alareunasta."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-z>"
msgstr "B<-z>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Enable ^Z suspension of I<pico>."
msgstr "Sallii I<pico>:n väliaikaisen keskeyttämisen ^Z:n avulla."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Preserve the \"start\" and \"stop\" characters, typically Ctrl-Q and Ctrl-S, "
"which are sometimes used in communications paths to control data flow "
"between devices that operate at different speeds."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, fuzzy, no-wrap
#| msgid "B<-o\\ >I<dir>"
msgid "B<-Q\\ >I<quotestr>"
msgstr "B<-o\\ >I<hakemisto>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Set the quote string.  Especially useful when composing email, setting this "
"allows the quote string to be checked for when Justifying paragraphs.  A "
"common quote string is \"E<gt> \"."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-W\\ >I<word_separators>"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"If characters listed here appear in the middle of a word surrounded by "
"alphanumeric characters that word is split into two words. This is used by "
"the Forward and Backward word commands and by the spell checker."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-q>"
msgstr "B<-q>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Termcap or terminfo definition for input escape sequences are used in "
"preference to sequences defined by default.  This option is only available "
"if I<pico> was compiled with the TERMCAP_WINS define turned on."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-setlocale_ctype>"
msgstr "B<-setlocale_ctype>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Do setlocale(LC_CTYPE) if available. Default is to not do this setlocale."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-no_setlocale_collate>"
msgstr "B<-no_setlocale_collate>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Do not do setlocale(LC_COLLATE). Default is to do this setlocale."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Lastly, when a running I<pico> is disconnected (i.e., receives a SIGHUP), "
"I<pico> will save the current work if needed before exiting.  Work is saved "
"under the current filename with \".save\" appended.  If the current work is "
"unnamed, it is saved under the filename \"pico.save\"."
msgstr ""
"Kun ohjelmaa ajetaan \"disconnected\"-tilassa (eli se hyväksyy SIGHUP:in), "
"I<pico> tallentaa muokatun tekstin ennen sulkeutumista.  Tallennuksessa "
"käytetään tällöin nykyisen työtiedoston nimeä, jonka perään listään \"."
"save\".  Jos muokattavalla tekstillä ei ole nimeä, käytetään tiedostoa "
"\"pico.save\"."

#. type: SH
#: debian-bullseye debian-unstable
#, fuzzy, no-wrap
#| msgid "Ar port"
msgid "Color Support"
msgstr "Ar port"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"If your terminal supports colors, Pico can be configured to color text. "
"Users can configure the color of the text, the text in the key menu, the "
"titlebar, messages and prompt in the status line. As an added feature Pico "
"can also be used to configure the color of up to three different levels of "
"quoted text, and the signature of an email message. This is useful when Pico "
"is used as a tool (with the -t command line switch.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Pico can tell you the number of colors that your terminal supports, when "
"started with the switch -color_codes. In addition Pico will print a table "
"showing the numerical code of every color supported in that terminal. In "
"order to configure colors, one must use these numerical codes. For example, "
"0 is for black, so in order to configure a black color, one must use its "
"code, the number 0."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"In order to activate colors, one must use the option -ncolors with a "
"numerical value indicating the number of colors that your terminal supports, "
"for example, I<-ncolors 256> indicates that the user wishes to use a table "
"of 256 colors."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"All options that control color, are four letter options. Their last two "
"letters are either \"fc\" or \"bc\", indicating I<foreground color> and "
"I<bacground color>, respectively. The first two letters indicate the type of "
"text that is being configured, for example \"nt\" stands for I<normal text>, "
"so that -ntfc represents the color of the normal text, while -ntbc "
"represents the color of the background of normal text. Here is a complete "
"list of the color options supported by Pico."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-color_code>"
msgstr "B<-color_code>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"displays the number of colors supported by the terminal, and a table showing "
"the association of colors and numerical codes"
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-ncolors\\ >I<number>"
msgstr "B<-ncolors\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"activates color support in Pico, and tells Pico how many colors to use.  "
"Depending on your terminal I<number> could be 8, 16, or 256."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-ntfc\\ >I<num>"
msgstr "B<-ntfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color to be used to color normal text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-ntbc\\ >I<num>"
msgstr "B<-ntbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background for normal text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-rtfc\\ >I<num>"
msgstr "B<-rtfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of reverse text. Default: same as "
"background color of normal text (if specified.)"
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-rtbc\\ >I<num>"
msgstr "B<-rtbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background of reverse text. "
"Default: same as color of normal text (if specified.)"
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-tbfc\\ >I<num>"
msgstr "B<-tbfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of then color of text of the title bar.  "
"Default: same as foreground color of reverse text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-tbbc\\ >I<num>"
msgstr "B<-tbbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color in the background of the title bar."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-klfc\\ >I<num>"
msgstr "B<-klfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "specifies the number I<num> of the color of the text of the key label."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-klbc\\ >I<num>"
msgstr "B<-klbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color in the background of the key label."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-knfc\\ >I<num>"
msgstr "B<-knfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "specifies the number I<num> of the color of the text of the key name."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-knbc\\ >I<num>"
msgstr "B<-knbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background of the key name."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-stfc\\ >I<num>"
msgstr "B<-stfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the text of the status line."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-stbc\\ >I<num>"
msgstr "B<-stbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background of the status "
"line."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-prfc\\ >I<num>"
msgstr "B<-prfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "specifies the number I<num> of the color of the text of a prompt."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-prbc\\ >I<num>"
msgstr "B<-prbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "specifies the number I<num> of the color of the background of a prompt."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-q1fc\\ >I<num>"
msgstr "B<-q1fc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the text of level one of quoted "
"text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-q1bc\\ >I<num>"
msgstr "B<-q1bc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background of level one of "
"quoted text. If the option -q1bc is used, the default value of this option "
"is the background color or normal text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-q2fc\\ >I<num>"
msgstr "B<-q2fc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of text of level two of quoted text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-q2bc\\ >I<num>"
msgstr "B<-q2bc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background of level two of "
"quoted text. If the option -q1bc is used, the default value of this option "
"is the background color or normal text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-q3fc\\ >I<num>"
msgstr "B<-q3fc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of text of level three of quoted "
"text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-sbfc\\ >I<num>"
msgstr "B<-sbfc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of text of signature block text."
msgstr ""

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-sbbc\\ >I<num>"
msgstr "B<-sbbc\\ >I<numero>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"specifies the number I<num> of the color of the background of signature "
"block text."
msgstr ""

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Bugs"
msgstr "Bugit"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The manner in which lines longer than the display width are dealt is not "
"immediately obvious.  Lines that continue beyond the edge of the display are "
"indicated by a '$' character at the end of the line.  Long lines are "
"scrolled horizontally as the cursor moves through them."
msgstr ""

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Files"
msgstr "Tiedostot"

#. type: ta
#: debian-bullseye debian-unstable
#, no-wrap
msgid "1.75i"
msgstr "1.75i"

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"pico.save\tUnnamed interrupted work saved here.\n"
"*.save\tInterrupted work on a named file is saved here.\n"
msgstr ""
"pico.save\tNimeämättömät, keskeytetyt työt tallennetaan tänne.\n"
"*.save\tNimetyt, keskeytetyt työt tallennetaan tänne.\n"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Authors"
msgstr "Tekijät"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Michael Seibel E<lt>mikes@cac.washington.eduE<gt>"
msgstr "Michael Seibel E<lt>mikes@cac.washington.eduE<gt>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Laurence Lundblade E<lt>lgl@cac.washington.eduE<gt>"
msgstr "Laurence Lundblade E<lt>lgl@cac.washington.eduE<gt>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Pico was originally derived from MicroEmacs 3.6, by Dave G. Conroy."
msgstr ""
"Pico on alunperin johdettu MicroEmacsin versiosta 3.6 (tekijä Dave G.  "
"Conroy)."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Copyright 1989-2008 by the University of Washington."
msgstr "Copyright 1989-2008 by the University of Washington."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "See Also"
msgstr "Katso myös"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "alpine(1)"
msgstr "B<alpine>(1)"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Source distribution (part of the Alpine Message System):"
msgstr "Lähdekoodin jakelu (osana Alpine Message System:iä):"

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid "$Date: 2009-02-02 13:54:23 -0600 (Mon, 02 Feb 2009) $\n"
msgstr "$Date: 2009-02-02 13:54:23 -0600 (Mon, 02 Feb 2009) $\n"
