# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Carlos Augusto Horylka <horylka@conectiva.com.br>, 2000.
# Rafael Fontenelle <rafaelff@gnome.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:19+0100\n"
"PO-Revision-Date: 2021-06-13 03:43-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 40.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "null"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "null, zero - data sink"
msgstr "null, zero - matador de dados"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Data written to the I</dev/null> and I</dev/zero> special files is discarded."
msgstr ""
"Dados escritos nos arquivos especials B</dev/null> ou B</dev/zero> serão "
"descartados."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Reads from I</dev/null> always return end of file (i.e., B<read>(2)  "
#| "returns 0), whereas reads from I</dev/zero> always return bytes "
#| "containing zero (\\(aq\\e0\\(aq characters)."
msgid ""
"Reads from I</dev/null> always return end of file (i.e., B<read>(2)  returns "
"0), whereas reads from I</dev/zero> always return bytes containing zero "
"(\\[aq]\\e0\\[aq] characters)."
msgstr ""
"Leituras a partir de B</dev/null> sempre retornam fim de arquivo (isto é, "
"B<read>(2) retorna 0), enquanto leituras a partir de B</dev/zero> sempre "
"retornam o bytes contendo 0 (caracteres \\(aq\\e0\\(aq)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These devices are typically created by:"
msgstr "Estes dispositivos são tipicamente criados por:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 666 /dev/null c 1 3\n"
"mknod -m 666 /dev/zero c 1 5\n"
"chown root:root /dev/null /dev/zero\n"
msgstr ""
"mknod -m 666 /dev/null c 1 3\n"
"mknod -m 666 /dev/zero c 1 5\n"
"chown root:root /dev/null /dev/zero\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARQUIVOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</dev/null>"
msgstr "I</dev/null>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</dev/zero>"
msgstr "I</dev/zero>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If these devices are not writable and readable for all users, many programs "
"will act strangely."
msgstr ""
"Se estes dispositivos não podem ser escritos ou lidos por todos os usuários, "
"muitos programas podem agir estranhamente."

#.  commit 2b83868723d090078ac0e2120e06a1cc94dbaef0
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.31, reads from I</dev/zero> are interruptible by signals.  "
"(This change was made to help with bad latencies for large reads from I</dev/"
"zero>.)"
msgstr ""
"Desde o Linux 2.6.31, as leituras de I</dev/zero> podem ser interrompidas "
"por sinais. (Esta alteração foi feita para ajudar com latências ruins para "
"leituras grandes de I</dev/zero>.)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<full>(4)"
msgstr "B<chown>(1), B<mknod>(1), B<full>(4)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "NULL"
msgstr "NULL"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2015-07-23"
msgstr "23 julho 2015"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Reads from I</dev/null> always return end of file (i.e., B<read>(2)  returns "
"0), whereas reads from I</dev/zero> always return bytes containing zero "
"(\\(aq\\e0\\(aq characters)."
msgstr ""
"Leituras a partir de B</dev/null> sempre retornam fim de arquivo (isto é, "
"B<read>(2) retorna 0), enquanto leituras a partir de B</dev/zero> sempre "
"retornam o bytes contendo 0 (caracteres \\(aq\\e0\\(aq)."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 outubro 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
