# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:07+0100\n"
"PO-Revision-Date: 2001-06-02 19:20-0300\n"
"Last-Translator: Ricardo C.O.Freitas <english.quest@best-service.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getdtablesize>()"
msgid "getdtablesize"
msgstr "B<getdtablesize>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "getdtablesize - get descriptor table size"
msgid "getdtablesize - get file descriptor table size"
msgstr "getdtablesize - obtém o tamanho da tabela de descritores"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int getdtablesize(void);>"
msgid "B<int getdtablesize(void);>\n"
msgstr "B<int getdtablesize(void);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<getdtablesize>():"
msgstr "B<getdtablesize>():"

#.         || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.21:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    In glibc 2.19 and 2.20:\n"
#| "        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
#| "    Up to and including glibc 2.19:\n"
#| "        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
msgid ""
"    Since glibc 2.20:\n"
"        _DEFAULT_SOURCE || ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"    glibc 2.12 to glibc 2.19:\n"
"        _BSD_SOURCE || ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Desde o glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    No glibc 2.19 e 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
"    Até e incluindo o glibc 2.19:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<getdtablesize> returns the maximum number of files a process can have "
#| "open."
msgid ""
"B<getdtablesize>()  returns the maximum number of files a process can have "
"open, one more than the largest possible value for a file descriptor."
msgstr ""
"B<getdtablesize> retorna o número máximo de arquivos que um processo pode "
"ter aberto."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The current limit on the number of open files per process."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On Linux, B<getdtablesize>()  can return any of the errors described for "
"B<getrlimit>(2); see NOTES below."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getdtablesize>()"
msgstr "B<getdtablesize>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"SVr4, 4.4BSD (the B<getdtablesize>()  function first appeared in 4.2BSD).  "
"It is not specified in POSIX.1; portable applications should employ "
"I<sysconf(_SC_OPEN_MAX)> instead of this call."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#.  The libc4 and libc5 versions return
#.  .B OPEN_MAX
#.  (set to 256 since Linux 0.98.4).
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The glibc version of B<getdtablesize>()  calls B<getrlimit>(2)  and returns "
"the current B<RLIMIT_NOFILE> limit, or B<OPEN_MAX> when that fails."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "B<close>(2), B<dup>(2), B<open>(2)"
msgid "B<close>(2), B<dup>(2), B<getrlimit>(2), B<open>(2)"
msgstr "B<close>(2), B<dup>(2), B<open>(2)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GETDTABLESIZE"
msgstr "GETDTABLESIZE"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 junho 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<int getdtablesize(void);>"
msgstr "B<int getdtablesize(void);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.12:"
msgstr "Desde o glibc 2.12:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid ""
#| "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"/* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
"    || ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "Before glibc 2.10:"
msgid "Before glibc 2.12:"
msgstr "Antes do glibc 2.10:"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgid "_BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 março 2016"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgid "Before glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#.  The libc4 and libc5 versions return
#.  .B OPEN_MAX
#.  (set to 256 since Linux 0.98.4).
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<getdtablesize>()  is implemented as a libc library function.  The glibc "
"version calls B<getrlimit>(2)  and returns the current B<RLIMIT_NOFILE> "
"limit, or B<OPEN_MAX> when that fails."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 dezembro 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#.         || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.21:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    In glibc 2.19 and 2.20:\n"
#| "        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
#| "    Up to and including glibc 2.19:\n"
#| "        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
msgid ""
"    Since glibc 2.20:\n"
"        _DEFAULT_SOURCE || ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"    Glibc 2.12 to glibc 2.19:\n"
"        _BSD_SOURCE || ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Desde o glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    No glibc 2.19 e 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
"    Até e incluindo o glibc 2.19:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
