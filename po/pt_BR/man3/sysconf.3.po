# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rubens de Jesus Nogueira <darkseid99@usa.net>, 2000.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:33+0100\n"
"PO-Revision-Date: 2000-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sysconf>()"
msgid "sysconf"
msgstr "B<sysconf>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sysconf - get configuration information at run time"
msgstr "sysconf - obtém informações de configuração em tempo de execução"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<long sysconf(int >I<name>B<);>\n"
msgstr "B<long sysconf(int >I<name>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX allows an application to test at compile or run time whether certain "
"options are supported, or what the value is of certain configurable "
"constants or limits."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"At compile time this is done by including I<E<lt>unistd.hE<gt>> and/or "
"I<E<lt>limits.hE<gt>> and testing the value of certain macros."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"At run time, one can ask for numerical values using the present function "
"B<sysconf>().  One can ask for numerical values that may depend on the "
"filesystem in which a file resides using B<fpathconf>(3)  and "
"B<pathconf>(3).  One can ask for string values using B<confstr>(3)."
msgstr ""

#.  except that sysconf(_SC_OPEN_MAX) may change answer after a call
#.  to setrlimit( ) which changes the RLIMIT_NOFILE soft limit
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The values obtained from these functions are system configuration "
"constants.  They do not change during the lifetime of a process."
msgstr ""

#.  and 999 to indicate support for options no longer present in the latest
#.  standard. (?)
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"For options, typically, there is a constant B<_POSIX_FOO> that may be "
"defined in I<E<lt>unistd.hE<gt>>.  If it is undefined, one should ask at run "
"time.  If it is defined to -1, then the option is not supported.  If it is "
"defined to 0, then relevant functions and headers exist, but one has to ask "
"at run time what degree of support is available.  If it is defined to a "
"value other than -1 or 0, then the option is supported.  Usually the value "
"(such as 200112L) indicates the year and month of the POSIX revision "
"describing the option.  glibc uses the value 1 to indicate support as long "
"as the POSIX revision has not been published yet.  The B<sysconf>()  "
"argument will be B<_SC_FOO>.  For a list of options, see B<posixoptions>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For variables or limits, typically, there is a constant B<_FOO>, maybe "
"defined in I<E<lt>limits.hE<gt>>, or B<_POSIX_FOO>, maybe defined in "
"I<E<lt>unistd.hE<gt>>.  The constant will not be defined if the limit is "
"unspecified.  If the constant is defined, it gives a guaranteed value, and a "
"greater value might actually be supported.  If an application wants to take "
"advantage of values which may change between systems, a call to "
"B<sysconf>()  can be made.  The B<sysconf>()  argument will be B<_SC_FOO>."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "POSIX.1 variables"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"We give the name of the variable, the name of the B<sysconf>()  argument "
"used to inquire about its value, and a short description."
msgstr ""

#.  [for the moment: only the things that are unconditionally present]
#.  .TP
#.  .BR AIO_LISTIO_MAX " - " _SC_AIO_LISTIO_MAX
#.  (if _POSIX_ASYNCHRONOUS_IO)
#.  Maximum number of I/O operations in a single list I/O call.
#.  Must not be less than _POSIX_AIO_LISTIO_MAX.
#.  .TP
#.  .BR AIO_MAX " - " _SC_AIO_MAX
#.  (if _POSIX_ASYNCHRONOUS_IO)
#.  Maximum number of outstanding asynchronous I/O operations.
#.  Must not be less than _POSIX_AIO_MAX.
#.  .TP
#.  .BR AIO_PRIO_DELTA_MAX " - " _SC_AIO_PRIO_DELTA_MAX
#.  (if _POSIX_ASYNCHRONOUS_IO)
#.  The maximum amount by which a process can decrease its
#.  asynchronous I/O priority level from its own scheduling priority.
#.  Must be nonnegative.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Next, the POSIX.2 values:"
msgid "First, the POSIX.1 compatible values."
msgstr "A seguir, os valores de POSIX.2:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ARG_MAX> - B<_SC_ARG_MAX>"
msgstr "B<ARG_MAX> - B<_SC_ARG_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The maximum length of the arguments to the B<exec()> family of functions; "
#| "the corresponding macro is B<ARG_MAX>."
msgid ""
"The maximum length of the arguments to the B<exec>(3)  family of functions.  "
"Must not be less than B<_POSIX_ARG_MAX> (4096)."
msgstr ""
"O comprimento máximo dos argumentos para as famílias de funções B<exec()> ; "
"a macro correspondente é B<ARG_MAX>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<CHILD_MAX> - B<_SC_CHILD_MAX>"
msgstr "B<CHILD_MAX> - B<_SC_CHILD_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The number of simultaneous processes per user id, the corresponding macro "
#| "is B<_POSIX_CHILD_MAX>."
msgid ""
"The maximum number of simultaneous processes per user ID.  Must not be less "
"than B<_POSIX_CHILD_MAX> (25)."
msgstr ""
"O número de processos simultâneos por id de usuário, a macro correspondente "
"é B<_POSIX_CHILD_MAX>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<HOST_NAME_MAX> - B<_SC_HOST_NAME_MAX>"
msgstr "B<HOST_NAME_MAX> - B<_SC_HOST_NAME_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Maximum length of a hostname, not including the terminating null byte, as "
"returned by B<gethostname>(2).  Must not be less than "
"B<_POSIX_HOST_NAME_MAX> (255)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOGIN_NAME_MAX> - B<_SC_LOGIN_NAME_MAX>"
msgstr "B<LOGIN_NAME_MAX> - B<_SC_LOGIN_NAME_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Maximum length of a login name, including the terminating null byte.  Must "
"not be less than B<_POSIX_LOGIN_NAME_MAX> (9)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<NGROUPS_MAX> - B<_SC_NGROUPS_MAX>"
msgstr "B<NGROUPS_MAX> - B<_SC_NGROUPS_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Maximum number of supplementary group IDs."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "clock ticks - B<_SC_CLK_TCK>"
msgstr "clock ticks - B<_SC_CLK_TCK>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The number of clock ticks per second.  The corresponding variable is "
"obsolete.  It was of course called B<CLK_TCK>.  (Note: the macro "
"B<CLOCKS_PER_SEC> does not give information: it must equal 1000000.)"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<OPEN_MAX> - B<_SC_OPEN_MAX>"
msgstr "B<OPEN_MAX> - B<_SC_OPEN_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The maximum number of files that a process can have open at any time, the "
#| "corresponding macro is B<_POSIX_OPEN_MAX>."
msgid ""
"The maximum number of files that a process can have open at any time.  Must "
"not be less than B<_POSIX_OPEN_MAX> (20)."
msgstr ""
"O número máximo de arquivos que um processo pode manter abertos a qualquer "
"tempo, a macro correspondente é B<_POSIX_OPEN_MAX>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PAGESIZE> - B<_SC_PAGESIZE>"
msgstr "B<PAGESIZE> - B<_SC_PAGESIZE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Size of a page in bytes.  Must not be less than 1."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<PAGE_SIZE> - B<_SC_PAGE_SIZE>"
msgstr "B<PAGE_SIZE> - B<_SC_PAGE_SIZE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A synonym for B<PAGESIZE>/B<_SC_PAGESIZE>.  (Both B<PAGESIZE> and "
"B<PAGE_SIZE> are specified in POSIX.)"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<RE_DUP_MAX> - B<_SC_RE_DUP_MAX>"
msgstr "B<RE_DUP_MAX> - B<_SC_RE_DUP_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The number of repeated occurrences of a BRE permitted by B<regexec>(3)  and "
"B<regcomp>(3).  Must not be less than B<_POSIX2_RE_DUP_MAX> (255)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<STREAM_MAX> - B<_SC_STREAM_MAX>"
msgstr "B<STREAM_MAX> - B<_SC_STREAM_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The maximum number of streams that a process can have open at any time.  "
#| "The corresponding POSIX macro is B<STREAM_MAX>, the corresponding "
#| "standard C macro is B<FOPEN_MAX>."
msgid ""
"The maximum number of streams that a process can have open at any time.  If "
"defined, it has the same value as the standard C macro B<FOPEN_MAX>.  Must "
"not be less than B<_POSIX_STREAM_MAX> (8)."
msgstr ""
"O número máximo de fluxos que um processo pode manter abertos a qualquer "
"tempo. A macro POSIX correspondente é B<STREAM_MAX>, a macro com padrão C "
"correspondente é B<FOPEN_MAX>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SYMLOOP_MAX> - B<_SC_SYMLOOP_MAX>"
msgstr "B<SYMLOOP_MAX> - B<_SC_SYMLOOP_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The maximum number of symbolic links seen in a pathname before resolution "
"returns B<ELOOP>.  Must not be less than B<_POSIX_SYMLOOP_MAX> (8)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<TTY_NAME_MAX> - B<_SC_TTY_NAME_MAX>"
msgstr "B<TTY_NAME_MAX> - B<_SC_TTY_NAME_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The maximum length of terminal device name, including the terminating null "
"byte.  Must not be less than B<_POSIX_TTY_NAME_MAX> (9)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<TZNAME_MAX> - B<_SC_TZNAME_MAX>"
msgstr "B<TZNAME_MAX> - B<_SC_TZNAME_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The maximum number of bytes in a timezone name, the corresponding macro "
#| "is B<TZNAME_MAX>."
msgid ""
"The maximum number of bytes in a timezone name.  Must not be less than "
"B<_POSIX_TZNAME_MAX> (6)."
msgstr ""
"O número máximo de bytes em um nome de fuso horário, a macro correspondente "
"é B<TZNAME_MAX>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<_POSIX_VERSION> - B<_SC_VERSION>"
msgstr "B<_POSIX_VERSION> - B<_SC_VERSION>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "indicates the year and month the POSIX.1 standard was approved in the "
#| "format B<YYYYMML>;B<the> value B<199009L> indicates the most recent "
#| "revision, 1990."
msgid ""
"indicates the year and month the POSIX.1 standard was approved in the format "
"B<YYYYMML>; the value B<199009L> indicates the Sept. 1990 revision."
msgstr ""
"indica o ano e o mês em que o padrão POSIX.1 foi aprovado, no formato "
"B<YYYYMML>; o valor B<199009L> indica a revisão mais recente, 1990."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Next, the POSIX.2 values:"
msgid "POSIX.2 variables"
msgstr "A seguir, os valores de POSIX.2:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Next, the POSIX.2 values:"
msgid "Next, the POSIX.2 values, giving limits for utilities."
msgstr "A seguir, os valores de POSIX.2:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<BC_BASE_MAX> - B<_SC_BC_BASE_MAX>"
msgstr "B<BC_BASE_MAX> - B<_SC_BC_BASE_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "indicates the maximum I<obase> value accepted by the B<bc>(1)  utility."
msgstr "indica o valor máximo de I<obase> aceito pelo utilitário B<bc>(1)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<BC_DIM_MAX> - B<_SC_BC_DIM_MAX>"
msgstr "B<BC_DIM_MAX> - B<_SC_BC_DIM_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"indicates the maximum value of elements permitted in an array by B<bc>(1)."
msgstr ""
"indica o valor máximo dos elementos permitidos em uma matriz por B<bc>(1)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<BC_SCALE_MAX> - B<_SC_BC_SCALE_MAX>"
msgstr "B<BC_SCALE_MAX> - B<_SC_BC_SCALE_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "indicates the maximum I<scale> value allowed by B<bc>(1)."
msgstr "indica o valor máximo de I<scale> permitido por B<bc>(1)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<BC_STRING_MAX> - B<_SC_BC_STRING_MAX>"
msgstr "B<BC_STRING_MAX> - B<_SC_BC_STRING_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "indicates the maximum length of a string accepted by B<bc>(1)."
msgstr "indica o comprimento máximo de uma string aceita por B<bc>(1)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<COLL_WEIGHTS_MAX> - B<_SC_COLL_WEIGHTS_MAX>"
msgstr "B<COLL_WEIGHTS_MAX> - B<_SC_COLL_WEIGHTS_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"indicates the maximum numbers of weights that can be assigned to an entry of "
"the B<LC_COLLATE order> keyword in the locale definition file."
msgstr ""
"indica os números máximos de pesos que podem ser atribuídos a uma entrada da "
"palavra-chave de ordem B<LC_COLLATE> no arquivo de definição de 'locale'."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EXPR_NEST_MAX> - B<_SC_EXPR_NEST_MAX>"
msgstr "B<EXPR_NEST_MAX> - B<_SC_EXPR_NEST_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"is the maximum number of expressions which can be nested within parentheses "
"by B<expr>(1)."
msgstr ""
"é o número máximo de expressões que podem ser aninhadas dentro de parênteses "
"por B<expr>(1)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINE_MAX> - B<_SC_LINE_MAX>"
msgstr "B<LINE_MAX> - B<_SC_LINE_MAX>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The maximum length of a utility's input line length, either from standard "
#| "input or from a file. This includes length for a trailing newline.  The "
#| "corresponding macro is B<LINE_MAX>."
msgid ""
"The maximum length of a utility's input line, either from standard input or "
"from a file.  This includes space for a trailing newline."
msgstr ""
"O comprimento máximo de uma linha de entrada do utilitário de uma entrada "
"padrão ou de um arquivo. Isto inclui o comprimento de uma nova linha "
"seguinte. A macro correspondente é B<LINE_MAX>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The maximum number of repeated occurrences of a regular expression when the "
"interval notation B<\\e{m,n\\e}> is used."
msgstr ""
"O número máximo de ocorrências repetidas de uma expressão regular quando a "
"notação de intervalo B<\\e{m,n\\e}> é usada."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX2_VERSION> - B<_SC_2_VERSION>"
msgstr "B<POSIX2_VERSION> - B<_SC_2_VERSION>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "indicates the version of the POSIX.2 standard in the format of YYYYMML."
msgstr "indica a versão do padrão POSIX.2 no formato YYYYMML."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX2_C_DEV> - B<_SC_2_C_DEV>"
msgstr "B<POSIX2_C_DEV> - B<_SC_2_C_DEV>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"indicates whether the POSIX.2 C language development facilities are "
"supported."
msgstr ""
"indica se as facilidades de desenvolvimento na linguagem C do POSIX.2 são "
"suportadas."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX2_FORT_DEV> - B<_SC_2_FORT_DEV>"
msgstr "B<POSIX2_FORT_DEV> - B<_SC_2_FORT_DEV>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"indicates whether the POSIX.2 FORTRAN development utilities are supported."
msgstr ""
"indica se os utilitários de desenvolvimento em FORTRAN do POSIX.2 são "
"suportados."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX2_FORT_RUN> - B<_SC_2_FORT_RUN>"
msgstr "B<POSIX2_FORT_RUN> - B<_SC_2_FORT_RUN>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "indicates whether the POSIX.2 FORTRAN run-time utilities are supported."
msgstr ""
"indica se os utilitários de tempo de execução FORTRAN do POSIX.2 são "
"suportados."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<_POSIX2_LOCALEDEF> - B<_SC_2_LOCALEDEF>"
msgstr "B<_POSIX2_LOCALEDEF> - B<_SC_2_LOCALEDEF>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "indicates whether the POSIX.2 creation of locates via B<localedef>(1)  is "
#| "supported.  The corresponding macro is B<_POSIX2_LOCALEDEF>."
msgid ""
"indicates whether the POSIX.2 creation of locales via B<localedef>(1)  is "
"supported."
msgstr ""
"indica se a criação de locais do POSIX.2 através de B<localedef>(1) é "
"suportada. A macro correspondente é B<_POSIX2_LOCALEDEF>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<POSIX2_SW_DEV> - B<_SC_2_SW_DEV>"
msgstr "B<POSIX2_SW_DEV> - B<_SC_2_SW_DEV>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"indicates whether the POSIX.2 software development utilities option is "
"supported."
msgstr ""
"indica se a opção de utilitários de desenvolvimento de software do POSIX.2 é "
"suportada."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These values also exist, but may not be standard."
msgstr "Estes valores também existem, mas podem não ser padrões."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid " - B<_SC_PHYS_PAGES>"
msgstr " - B<_SC_PHYS_PAGES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The number of pages of physical memory.  Note that it is possible for the "
"product of this value and the value of B<_SC_PAGESIZE> to overflow."
msgstr ""
"O número de páginas de memória física. Note que é possível que o produto "
"deste valor com o valor de B<_SC_PAGESIZE> cause um estouro."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid " - B<_SC_AVPHYS_PAGES>"
msgstr " - B<_SC_AVPHYS_PAGES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The number of currently available pages of physical memory."
msgstr "O número de páginas da memória física disponíveis no momento."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid " - B<_SC_NPROCESSORS_CONF>"
msgstr " - B<_SC_NPROCESSORS_CONF>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The number of processors configured.  See also B<get_nprocs_conf>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid " - B<_SC_NPROCESSORS_ONLN>"
msgstr " - B<_SC_NPROCESSORS_ONLN>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The number of processors currently online (available).  See also "
"B<get_nprocs_conf>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The return value of B<sysconf>()  is one of the following:"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On error, -1 is returned and I<errno> is set to indicate the error (for "
"example, B<EINVAL>, indicating that I<name> is invalid)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<name> corresponds to a maximum or minimum limit, and that limit is "
"indeterminate, -1 is returned and I<errno> is not changed.  (To distinguish "
"an indeterminate limit from an error, set I<errno> to zero before the call, "
"and then check whether I<errno> is nonzero when -1 is returned.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<name> corresponds to an option, a positive value is returned if the "
"option is supported, and -1 is returned if the option is not supported."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Otherwise, the current value of the option or limit is returned.  This value "
"will not be more restrictive than the corresponding value that was described "
"to the application in I<E<lt>unistd.hE<gt>> or I<E<lt>limits.hE<gt>> when "
"the application was compiled."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<name> is invalid."
msgstr "I<name> é inválido."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sysconf>()"
msgstr "B<sysconf>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe"
msgid "MT-Safe env"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It is difficult to use B<ARG_MAX> because it is not specified how much of "
"the argument space for B<exec>(3)  is consumed by the user's environment "
"variables."
msgstr ""
"É difícil usar B<ARG_MAX> porque não é especificado quanto espaço de "
"argumento para B<exec>(3) é consumido pelas variáveis de ambiente do usuário."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Some returned values may be huge; they are not suitable for allocating "
"memory."
msgstr ""
"Alguns valores retornados podem ser enormes; eles não são adequados para "
"alocação de memória."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<bc>(1), B<expr>(1), B<getconf>(1), B<locale>(1), B<confstr>(3), "
"B<fpathconf>(3), B<pathconf>(3), B<posixoptions>(7)"
msgstr ""
"B<bc>(1), B<expr>(1), B<getconf>(1), B<locale>(1), B<confstr>(3), "
"B<fpathconf>(3), B<pathconf>(3), B<posixoptions>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "SYSCONF"
msgstr "SYSCONF"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-05-09"
msgstr "9 maio 2019"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#.  and 999 to indicate support for options no longer present in the latest
#.  standard. (?)
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For options, typically, there is a constant B<_POSIX_FOO> that may be "
"defined in I<E<lt>unistd.hE<gt>>.  If it is undefined, one should ask at run "
"time.  If it is defined to -1, then the option is not supported.  If it is "
"defined to 0, then relevant functions and headers exist, but one has to ask "
"at run time what degree of support is available.  If it is defined to a "
"value other than -1 or 0, then the option is supported.  Usually the value "
"(such as 200112L) indicates the year and month of the POSIX revision "
"describing the option.  Glibc uses the value 1 to indicate support as long "
"as the POSIX revision has not been published yet.  The B<sysconf>()  "
"argument will be B<_SC_FOO>.  For a list of options, see B<posixoptions>(7)."
msgstr ""

#. type: IP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On error, -1 is returned and I<errno> is set to indicate the cause of the "
"error (for example, B<EINVAL>, indicating that I<name> is invalid)."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-11-26"
msgstr "26 novembro 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Size of a page in bytes.  Must not be less than 1.  (Some systems use "
"PAGE_SIZE instead.)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"indicates the maximum numbers of weights that can be assigned to an entry of "
"the B<LC_COLLATE order> keyword in the locale definition file,"
msgstr ""
"indica os números máximos de pesos que podem ser atribuídos a uma entrada da "
"palavra-chave de ordem B<LC_COLLATE> no arquivo de definição de 'locale'."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"indicates whether the POSIX.2 creation of locates via B<localedef>(1)  is "
"supported."
msgstr ""
"indica se a criação de locais do POSIX.2 através de B<localedef>(1) é "
"suportada."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 dezembro 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: IP
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"
