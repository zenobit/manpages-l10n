# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marcelo M. de Abreu <mmabreu@terra.com.br>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-20 20:32+0100\n"
"PO-Revision-Date: 2001-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "strerror_r()"
msgid "strerror"
msgstr "strerror_r()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "strerror - return string describing error code"
msgid ""
"strerror, strerrorname_np, strerrordesc_np, strerror_r, strerror_l - return "
"string describing error number"
msgstr "strerror - retorna seqüencia descrevendo código de erro"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strerror(int >I<errnum>B<);>\n"
"B<const char *strerrorname_np(int >I<errnum>B<);>\n"
"B<const char *strerrordesc_np(int >I<errnum>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"
msgid ""
"B<int strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
"               /* XSI-compliant */\n"
msgstr "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"
msgid ""
"B<char *strerror_r(int >I<errnum>B<, char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<);>\n"
"               /* GNU-specific */\n"
msgstr "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"
msgstr "B<char *strerror_l(int >I<errnum>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<strerrorname_np>(), B<strerrordesc_np>():"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strerror_r>():"
msgstr "B<strerror_r>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    The XSI-compliant version is provided if:\n"
"        (_POSIX_C_SOURCE E<gt>= 200112L) && ! _GNU_SOURCE\n"
"    Otherwise, the GNU-specific version is provided.\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strerror>()  function returns a pointer to a string that describes the "
"error code passed in the argument I<errnum>, possibly using the "
"B<LC_MESSAGES> part of the current locale to select the appropriate "
"language.  (For example, if I<errnum> is B<EINVAL>, the returned description "
"will be \"Invalid argument\".)  This string must not be modified by the "
"application, but may be modified by a subsequent call to B<strerror>()  or "
"B<strerror_l>().  No other library function, including B<perror>(3), will "
"modify this string."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Like B<strerror>(), the B<strerrordesc_np>()  function returns a pointer to "
"a string that describes the error code passed in the argument I<errnum>, "
"with the difference that the returned string is not translated according to "
"the current locale."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<strerrorname_np>()  function returns a pointer to a string containing "
"the name of the error code passed in the argument I<errnum>.  For example, "
"given B<EPERM> as an argument, this function returns a pointer to the string "
"\"EPERM\"."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "strerror_r()"
msgstr "strerror_r()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strerror_r>()  function is similar to B<strerror>(), but is thread "
"safe.  This function is available in two versions: an XSI-compliant version "
"specified in POSIX.1-2001 (available since glibc 2.3.4, but not POSIX-"
"compliant until glibc 2.13), and a GNU-specific version (available since "
"glibc 2.0).  The XSI-compliant version is provided with the feature test "
"macros settings shown in the SYNOPSIS; otherwise the GNU-specific version is "
"provided.  If no feature test macros are explicitly defined, then (since "
"glibc 2.4)  B<_POSIX_C_SOURCE> is defined by default with the value 200112L, "
"so that the XSI-compliant version of B<strerror_r>()  is provided by default."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The XSI-compliant B<strerror_r>()  is preferred for portable applications.  "
"It returns the error string in the user-supplied buffer I<buf> of length "
"I<buflen>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<buflen> bytes are stored (the string may be truncated "
"if I<buflen> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\[aq]\\e0\\[aq])."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "strerror_l()"
msgstr "strerror_l()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<strerror_l>()  is like B<strerror>(), but maps I<errnum> to a locale-"
"dependent error message in the locale specified by I<locale>.  The behavior "
"of B<strerror_l>()  is undefined if I<locale> is the special locale object "
"B<LC_GLOBAL_LOCALE> or is not a valid locale object handle."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strerror()> function returns the appropriate description string, or "
#| "an unknown error message if the error code is unknown."
msgid ""
"The B<strerror>(), B<strerror_l>(), and the GNU-specific B<strerror_r>()  "
"functions return the appropriate error description string, or an \"Unknown "
"error nnn\" message if the error number is unknown."
msgstr ""
"A função B<strerror()> retorna a seqüencia de descrição apropriada, ou uma "
"mensagem de erro desconhecido se o código de erro não for conhecido."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strerror()> function returns the appropriate description string, or "
#| "an unknown error message if the error code is unknown."
msgid ""
"On success, B<strerrorname_np>()  and B<strerrordesc_np>()  return the "
"appropriate error description string.  If I<errnum> is an invalid error "
"number, these functions return NULL."
msgstr ""
"A função B<strerror()> retorna a seqüencia de descrição apropriada, ou uma "
"mensagem de erro desconhecido se o código de erro não for conhecido."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The XSI-compliant B<strerror_r>()  function returns 0 on success.  On error, "
"a (positive) error number is returned (since glibc 2.13), or -1 is returned "
"and I<errno> is set to indicate the error (before glibc 2.13)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 and POSIX.1-2008 require that a successful call to "
"B<strerror>()  or B<strerror_l>()  shall leave I<errno> unchanged, and note "
"that, since no function return value is reserved to indicate an error, an "
"application that wishes to check for errors should initialize I<errno> to "
"zero before the call, and then check I<errno> after the call."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The value of I<errnum> is not a valid error number."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Insufficient storage was supplied to contain the error description string."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSÕES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<strerror_l>()  function first appeared in glibc 2.6."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<strerrorname_np>()  and B<strerrordesc_np>()  functions first appeared "
"in glibc 2.32."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror>()"
msgstr "B<strerror>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:strerror"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strerrorname_np>(),\n"
"B<strerrordesc_np>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strerror_r>(),\n"
msgid ""
"B<strerror_r>(),\n"
"B<strerror_l>()"
msgstr "B<strerror_r>(),\n"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#.  FIXME . for later review when Issue 8 is one day released...
#.  A future POSIX.1 may remove strerror_r()
#.  http://austingroupbugs.net/tag_view_page.php?tag_id=8
#.  http://austingroupbugs.net/view.php?id=508
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<strerror>()  is specified by POSIX.1-2001, POSIX.1-2008, and C99.  "
"B<strerror_r>()  is specified by POSIX.1-2001 and POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strerror_l>()  is specified in POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The GNU-specific functions B<strerror_r>(), B<strerrorname_np>(), and "
"B<strerrordesc_np>()  are nonstandard extensions."
msgstr ""

#.  e.g., Solaris 8, HP-UX 11
#.  e.g., FreeBSD 5.4, Tru64 5.1B
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 permits B<strerror>()  to set I<errno> if the call encounters "
"an error, but does not specify what value should be returned as the function "
"result in the event of an error.  On some systems, B<strerror>()  returns "
"NULL if the error number is unknown.  On other systems, B<strerror>()  "
"returns a string something like \"Error nnn occurred\" and sets I<errno> to "
"B<EINVAL> if the error number is unknown.  C99 and POSIX.1-2008 require the "
"return value to be non-NULL."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The GNU C Library uses a buffer of 1024 characters for B<strerror>().  This "
"buffer size therefore should be sufficient to avoid an B<ERANGE> error when "
"calling B<strerror_r>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<strerrorname_np>()  and B<strerrordesc_np>()  are thread-safe and async-"
"signal-safe."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7)"
msgstr ""
"B<err>(3), B<errno>(3), B<error>(3), B<perror>(3), B<strsignal>(3), "
"B<locale>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "STRERROR"
msgstr "STRERROR"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 novembro 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int strerror_r(int >I<errnum>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
"            /* XSI-compliant */\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<char *strerror_r(int >I<errnum>B<, char *>I<buf>B<, size_t >I<buflen>B<);>\n"
"            /* GNU-specific */\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<strerrorname_np>(),\n"
"B<strerrordesc_np>():\n"
"    _GNU_SOURCE\n"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "The XSI-compliant version is provided if:"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "(_POSIX_C_SOURCE\\ E<gt>=\\ 200112L) && ! \\ _GNU_SOURCE"
msgstr "(_POSIX_C_SOURCE\\ E<gt>=\\ 200112L) && ! \\ _GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Otherwise, the GNU-specific version is provided."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The GNU-specific B<strerror_r>()  returns a pointer to a string containing "
"the error message.  This may be either a pointer to a string that the "
"function stores in I<buf>, or a pointer to some (immutable) static string "
"(in which case I<buf> is unused).  If the function stores a string in "
"I<buf>, then at most I<buflen> bytes are stored (the string may be truncated "
"if I<buflen> is too small and I<errnum> is unknown).  The string always "
"includes a terminating null byte (\\(aq\\e0\\(aq)."
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The XSI-compliant B<strerror_r>()  function returns 0 on success.  On error, "
"a (positive) error number is returned (since glibc 2.13), or -1 is returned "
"and I<errno> is set to indicate the error (glibc versions before 2.13)."
msgstr ""

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<strerror_r>(),\n"
msgstr "B<strerror_r>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<strerror_l>()"
msgstr "B<strerror_l>()"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#.  FIXME . for later review when Issue 8 is one day released...
#.  A future POSIX.1 may remove strerror_r()
#.  http://austingroupbugs.net/tag_view_page.php?tag_id=8
#.  http://austingroupbugs.net/view.php?id=508
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<strerror>()  is specified by POSIX.1-2001, POSIX.1-2008, C89, and C99.  "
"B<strerror_r>()  is specified by POSIX.1-2001 and POSIX.1-2008."
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "strerror - return string describing error code"
msgid ""
"strerror, strerror_r, strerror_l - return string describing error number"
msgstr "strerror - retorna seqüencia descrevendo código de erro"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<char *strerror(int >I<errnum>B<);>\n"
msgstr "B<char *strerror(int >I<errnum>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "The GNU-specific B<strerror_r>()  function is a nonstandard extension."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The GNU C Library uses a buffer of 1024 characters for B<strerror>().  This "
"buffer size therefore should be sufficient to avoid an B<ERANGE> error when "
"calling B<strerror_r>()  and B<strerror_l>()."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 dezembro 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
