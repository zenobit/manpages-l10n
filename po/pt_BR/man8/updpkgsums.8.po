# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rafael Fontenelle <rafaelff@gnome.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.1.0\n"
"POT-Creation-Date: 2023-01-09 20:30+0100\n"
"PO-Revision-Date: 2020-09-07 19:28-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "UPDPKGSUMS"
msgstr "UPDPKGSUMS"

#. type: TH
#: archlinux
#, no-wrap
msgid "2023-01-04"
msgstr "4 janeiro 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Pacman-contrib 1\\&.8\\&.0"
msgid "Pacman-contrib 1\\&.8\\&.2"
msgstr "Pacman-contrib 1\\&.8\\&.0"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib Manual"
msgstr "Manual do pacman-contrib"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux
msgid "updpkgsums - update checksums of a PKGBUILD file"
msgstr "updpkgsums - atualiza somas de verificação de um arquivo PKGBUILD"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux
msgid "I<updpkgsums> [options] [build file]"
msgstr "I<updpkgsums> [opções] [arquivo de compilação]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux
msgid ""
"I<updpkgsums> will perform an in place update of the checksums in the path "
"specified by [build file], defaulting to PKGBUILD in the current working "
"directory\\&."
msgstr ""
"I<updpkgsums> executará uma atualização local das somas de verificação no "
"caminho especificado por [arquivo de compilação], padronizando para PKGBUILD "
"no diretório de trabalho atual\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPÇÕES"

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Display syntax and command-line options\\&."
msgstr "Exibe a sintaxe e as opções de linha de comando\\&."

#. type: Plain text
#: archlinux
msgid "B<-m, --nocolor>"
msgstr "B<-m, --nocolor>"

#. type: Plain text
#: archlinux
msgid "Do not colorize output\\&."
msgstr "Não coloriza a saída\\&."

#. type: Plain text
#: archlinux
msgid "B<-V, --version>"
msgstr "B<-V, --version>"

#. type: Plain text
#: archlinux
msgid "Display version information and exit\\&."
msgstr "Exibe informações da versão e sai\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux
msgid "B<makepkg>(8), B<pkgbuild>(5)"
msgstr "B<makepkg>(8), B<pkgbuild>(5)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, file an issue with as much detail as possible at https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman-contrib/-/issues/new\\&."
msgstr ""
"Bugs? Você deve estar brincando; não há erros neste software\\&. Mas se por "
"acaso estivermos errados, envie um relatório de erro com o máximo de "
"detalhes possível para https://gitlab\\&.archlinux\\&.org/pacman/pacman-"
"contrib/-/issues/new\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORES"

#. type: Plain text
#: archlinux
msgid "Current maintainers:"
msgstr "Atuais mantenedores:"

#. type: Plain text
#: archlinux
msgid "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"
msgstr "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"

#. type: Plain text
#: archlinux
msgid "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"
msgstr "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the pacman-contrib\\&."
"git repository\\&."
msgstr ""
"Para contribuidores adicionais, use git shortlog -s no repositório do pacman-"
"contrib\\&.git\\&."
