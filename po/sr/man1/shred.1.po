# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-15 19:14+0100\n"
"PO-Revision-Date: 2021-09-03 20:06+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SHRED"
msgstr "SHRED"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2022"
msgstr "Новембар 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "ГНУ coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "shred - overwrite a file to hide its contents, and optionally delete it"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<shred> [I<\\,OPTION\\/>]... I<\\,FILE\\/>..."
msgstr "B<shred> [I<\\,ОПЦИЈА\\/>]... I<\\,ДАТОТЕКА\\/>..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Overwrite the specified FILE(s) repeatedly, in order to make it harder for "
"even very expensive hardware probing to recover the data."
msgstr ""
"Преписује наведену ДАТОТЕКУ(е) са понављањем, да би је учинио тежом чак и за "
"врло скуп хардверски покушај спашавања података."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If FILE is -, shred standard output."
msgstr "Ако је ДАТОТЕКА -, растура стандардни излаз."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Обавезни аргументи за дуге опције су обавезни и за кратке опције такође."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "change permissions to allow writing if necessary"
msgstr "мења овлашћења да омогући писање ако је потребно"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--iterations>=I<\\,N\\/>"
msgstr "B<-n>, B<--iterations>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "overwrite N times instead of the default (3)"
msgstr "преписује N пута уместо основних (3)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--random-source>=I<\\,FILE\\/>"
msgstr "B<--random-source>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "get random bytes from FILE"
msgstr "добавља насумичне бајтове из ДАТОТЕКЕ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--size>=I<\\,N\\/>"
msgstr "B<-s>, B<--size>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "shred this many bytes (suffixes like K, M, G accepted)"
msgstr "растура ове многе бајтове (прихватају се суфикси као K, M, G)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "deallocate and remove file after overwriting"
msgstr "премешта и уклања датотеку након преписивања;"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--remove>[=I<\\,HOW\\/>]"
msgstr "B<--remove>[=I<\\,КАКО\\/>]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "like B<-u> but give control on HOW to delete; See below"
msgstr "као B<-u> али пружа контролу за КАКО обрисати;  Видите испод"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "show progress"
msgstr "приказује напредак"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--exact>"
msgstr "B<-x>, B<--exact>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not round file sizes up to the next full block;"
msgstr "не заокружује величине датотека на следећи пуни блок;"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "this is the default for non-regular files"
msgstr "ово је основно за не-обичне датотеке"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "add a final overwrite with zeros to hide shredding"
msgstr "додаје крајње преписивање нулама да сакрије растурање"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "приказује ову помоћ и излази"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "исписује податке о издању и излази"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Delete FILE(s) if B<--remove> (B<-u>) is specified.  The default is not "
#| "to remove the files because it is common to operate on device files like "
#| "I<\\,/dev/hda\\/>, and those files usually should not be removed.  The "
#| "optional HOW parameter indicates how to remove a directory entry: "
#| "\\&'unlink' =E<gt> use a standard unlink call.  \\&'wipe' =E<gt> also "
#| "first obfuscate bytes in the name.  \\&'wipesync' =E<gt> also sync each "
#| "obfuscated byte to disk.  The default mode is 'wipesync', but note it can "
#| "be expensive."
msgid ""
"Delete FILE(s) if B<--remove> (B<-u>) is specified.  The default is not to "
"remove the files because it is common to operate on device files like I<\\,/"
"dev/hda\\/>, and those files usually should not be removed.  The optional "
"HOW parameter indicates how to remove a directory entry: \\&'unlink' =E<gt> "
"use a standard unlink call.  \\&'wipe' =E<gt> also first obfuscate bytes in "
"the name.  \\&'wipesync' =E<gt> also sync each obfuscated byte to the "
"device.  The default mode is 'wipesync', but note it can be expensive."
msgstr ""
"Брише ДАТОТЕКУ(Е) ако је наведено B<--remove> (B<-u>).  Основно је да не "
"уклања датотеке зато што је уобичајено да ради над датотекама уређаја као "
"I<\\,/dev/hda\\/>, а те датотеке обично не би требале да се уклањају. "
"Изборни параметар КАКО показује како се уклањају ставке директоријума: "
"\\&'unlink' =E<gt> користи стандардни позив поништавања везе. \\&'wipe' "
"=E<gt> такође прве замаскиране бајтове у називу. \\&'wipesync' =E<gt> такође "
"усклађује сваки замаскирани бајт на диск. Основни режим је „wipesync“, али "
"знајте да може бити скуп."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"CAUTION: shred assumes the file system and hardware overwrite data in "
"place.  Although this is common, many platforms operate otherwise.  Also, "
"backups and mirrors may contain unremovable copies that will let a shredded "
"file be recovered later.  See the GNU coreutils manual for details."
msgstr ""
"ПАЖЊА: B<shred> подразумева преписивање података система датотека и "
"хардвера. Иако је ово уобичајено, многе платформе раде другачије.  Такође, "
"резерве и огледала могу садржати неуклоњиве умношке који ће омогућити "
"каснији опоравак шредираних датотека.  Погледајте упутство Гнуовог "
"„coreutils“ за више појединости."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АУТОР"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Colin Plumb."
msgstr "Написао је Колин Пламб."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Помоћ на мрежи за ГНУ coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Грешке у преводу пријавите на E<lt>https://translationproject.org/team/sr."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АУТОРСКА ПРАВА"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ово је слободан софтвер: слободни сте да га мењате и расподељујете. Не "
"постоји НИКАКВА ГАРАНЦИЈА, у оквирима дозвољеним законом."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/shredE<gt>"
msgstr ""
"Сва документација се налази на E<lt>https://www.gnu.org/software/coreutils/"
"shredE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) shred invocation\\(aq"
msgstr ""
"или је доступна на рачунару путем наредбе „info \\(aq(coreutils) shred "
"invocation\\(aq“"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Септембра 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "ГНУ coreutils 8.32"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Delete FILE(s) if B<--remove> (B<-u>) is specified.  The default is not to "
"remove the files because it is common to operate on device files like I<\\,/"
"dev/hda\\/>, and those files usually should not be removed.  The optional "
"HOW parameter indicates how to remove a directory entry: \\&'unlink' =E<gt> "
"use a standard unlink call.  \\&'wipe' =E<gt> also first obfuscate bytes in "
"the name.  \\&'wipesync' =E<gt> also sync each obfuscated byte to disk.  The "
"default mode is 'wipesync', but note it can be expensive."
msgstr ""
"Брише ДАТОТЕКУ(Е) ако је наведено B<--remove> (B<-u>).  Основно је да не "
"уклања датотеке зато што је уобичајено да ради над датотекама уређаја као "
"I<\\,/dev/hda\\/>, а те датотеке обично не би требале да се уклањају. "
"Изборни параметар КАКО показује како се уклањају ставке директоријума: "
"\\&'unlink' =E<gt> користи стандардни позив поништавања везе. \\&'wipe' "
"=E<gt> такође прве замаскиране бајтове у називу. \\&'wipesync' =E<gt> такође "
"усклађује сваки замаскирани бајт на диск. Основни режим је „wipesync“, али "
"знајте да може бити скуп."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "Септембра 2022"

#. type: TH
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "January 2023"
msgstr "јануара 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Априла 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Октобра 2021"
