# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-28 19:19+0100\n"
"PO-Revision-Date: 2022-07-23 16:27+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-FSTEST"
msgstr "GRUB-FSTEST"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Фебруара 2023"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "ГРУБ 2.06"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Debug tool for filesystem driver."
msgid "grub-fstest - debug tool for GRUB filesystem drivers"
msgstr "Прочишћавачки алат за управљачке програме система датотека."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-fstest> [I<\\,OPTION\\/>...] I<\\,IMAGE_PATH COMMANDS\\/>"
msgstr "B<grub-fstest> [I<\\,ОПЦИЈА\\/>...] I<\\,ПУТАЊА_СЛИКЕ НАРЕДБЕ\\/>"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Debug tool for filesystem driver."
msgstr "Прочишћавачки алат за управљачке програме система датотека."

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Commands:"
msgstr "Наредбе:"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "blocklist FILE"
msgstr "blocklist ДАТОТЕКА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Display blocklist of FILE."
msgstr "Приказује списак блокова ДАТОТЕКЕ."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "cat FILE"
msgstr "cat ДАТОТЕКА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Copy FILE to standard output."
msgstr "Умножава ДАТОТЕКУ на стандардни излаз."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "cmp FILE LOCAL"
msgstr "cmp ДАТОТЕКА МЕСНА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Compare FILE with local file LOCAL."
msgstr "Упоређује ДАТОТЕКУ са месном датотеком МЕСНА."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "cp FILE LOCAL"
msgstr "cp ДАТОТЕКА МЕСНА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Copy FILE to local file LOCAL."
msgstr "Умножава ДАТОТЕКУ у месну датотеку МЕСНА."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "crc FILE"
msgstr "crc ДАТОТЕКА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Get crc32 checksum of FILE."
msgstr "Добавља „crc32“ проверу суме ДАТОТЕКЕ."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "hex FILE"
msgstr "hex ДАТОТЕКА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Show contents of FILE in hex."
msgstr "Приказује садржај ДАТОТЕКЕ хексадецимално."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "ls PATH"
msgstr "ls ПУТАЊА"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "List files in PATH."
msgstr "Исписује датотеке из ПУТАЊЕ."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "xnu_uuid DEVICE"
msgstr "xnu_uuid УРЕЂАЈ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Compute XNU UUID of the device."
msgstr "Упоређује ИксНУ УУИБ уређаја."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--diskcount>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--diskcount>=I<\\,БРОЈ\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Specify the number of input files."
msgstr "Наводи број улазних датотека."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--crypto>"
msgstr "B<-C>, B<--crypto>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Mount crypto devices."
msgstr "Прикачиње уређаје шифровања."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--debug>=I<\\,STRING\\/>"
msgstr "B<-d>, B<--debug>=I<\\,НИСКА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Set debug environment variable."
msgstr "Поставља променљиву окружења прочишћавања."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-K>, B<--zfs-key>=I<\\,FILE\\/>|prompt"
msgstr "B<-K>, B<--zfs-key>=I<\\,ДАТОТЕКА\\/>|prompt"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Load zfs crypto key."
msgstr "Учитава зсф крипто кључ."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--length>=I<\\,NUM\\/>"
msgstr "B<-n>, B<--length>=I<\\,БРОЈ\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Handle N bytes in output file."
msgstr "Рукује са N бајтова у излазној датотеци."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,НАЗИВ_УРЕЂАЈА\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Set root device."
msgstr "Поставља корени уређај."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--skip>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--skip>=I<\\,БРОЈ\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Skip N bytes from output file."
msgstr "Прескаче N бајтова из излазне датотеке."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--uncompress>"
msgstr "B<-u>, B<--uncompress>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Uncompress data."
msgstr "Распакује податке."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "исписује опширне поруке."

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print program version"
msgstr "исписује издање програма"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обавезни или изборни аргументи за дуге опције су такође обавезни или изборни "
"за било које одговарајуће кратке опције."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-fstest> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-fstest> programs are properly installed "
"at your site, the command"
msgstr ""
"Потпуна документација за B<grub-fstest> је одржавана као Тексинфо упутство.  "
"Ако су B<info> и B<grub-fstest> исправно инсталирани на вашем сајту, наредба"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<info grub-fstest>"
msgstr "B<info grub-fstest>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "grub-fstest (GRUB2) 2.06"
msgstr "grub-fstest (ГРУБ2) 2.06"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.06"
msgstr "ГРУБ2 2.06"
