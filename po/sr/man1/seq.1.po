# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-15 19:13+0100\n"
"PO-Revision-Date: 2021-09-03 20:06+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEQ"
msgstr "SEQ"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2022"
msgstr "Новембар 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "ГНУ coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "seq - print a sequence of numbers"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<seq> [I<\\,OPTION\\/>]... I<\\,LAST\\/>"
msgstr "B<seq> [I<\\,ОПЦИЈА\\/>]... I<\\,ПОСЛЕДЊИ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<seq> [I<\\,OPTION\\/>]... I<\\,FIRST LAST\\/>"
msgstr "B<seq> [I<\\,ОПЦИЈА\\/>]... I<\\,ПРВИ ПОСЛЕДЊИ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<seq> [I<\\,OPTION\\/>]... I<\\,FIRST INCREMENT LAST\\/>"
msgstr "B<seq> [I<\\,ОПЦИЈА\\/>]... I<\\,ПРВИ ПОВЕЋАЊЕ ПОСЛЕДЊИ\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print numbers from FIRST to LAST, in steps of INCREMENT."
msgstr "Исписује бројеве од ПРВОГ до ПОСЛЕДЊЕГ, у корацима ПОВЕЋАЊА."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Обавезни аргументи за дуге опције су обавезни и за кратке опције такође."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-f>, B<--format>=I<\\,ЗАПИС\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use printf style floating-point FORMAT"
msgstr "користи ЗАПИС покретног зареза у стилу „printf“-а"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--separator>=I<\\,STRING\\/>"
msgstr "B<-s>, B<--separator>=I<\\,НИСКА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use STRING to separate numbers (default: \\en)"
msgstr "користи НИСКУ за раздвајање бројева (основно: \\en)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--equal-width>"
msgstr "B<-w>, B<--equal-width>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "equalize width by padding with leading zeroes"
msgstr "изједначава ширину испуњавајући водећим нулама"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "приказује ову помоћ и излази"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "исписује податке о издању и излази"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If FIRST or INCREMENT is omitted, it defaults to 1.  That is, an omitted "
"INCREMENT defaults to 1 even when LAST is smaller than FIRST.  The sequence "
"of numbers ends when the sum of the current number and INCREMENT would "
"become greater than LAST.  FIRST, INCREMENT, and LAST are interpreted as "
"floating point values.  INCREMENT is usually positive if FIRST is smaller "
"than LAST, and INCREMENT is usually negative if FIRST is greater than LAST.  "
"INCREMENT must not be 0; none of FIRST, INCREMENT and LAST may be NaN.  "
"FORMAT must be suitable for printing one argument of type 'double'; it "
"defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point "
"decimal numbers with maximum precision PREC, and to %g otherwise."
msgstr ""
"Ако је ПРВИ или ПОВЕЋАЊЕ изостављено, прелази на 1.  Тако је, изостављено "
"ПОВЕЋАЊЕ прелази на 1 чак и када је ПОСЛЕДЊИ мањи од ПРВОГ. Низ бројева се "
"завршава када би збир текућег броја и ПОВЕЋАЊА постао већи од ПОСЛЕДЊЕГ. "
"ПРВИ, ПОВЕЋАЊЕ, и ПОСЛЕДЊИ се тумаче као вредности покретног зареза. "
"ПОВЕЋАЊЕ је обично позитивно ако је ПРВИ мањи од ПОСЛЕДЊЕГ, а ПОВЕЋАЊЕ је "
"обично негативно ако је ПРВИ већи од ПОСЛЕДЊЕГ. ПОВЕЋАЊЕ не сме бити 0; ни "
"ПРВИ, ни ПОВЕЋАЊЕ, ни ПОСЛЕДЊИ не може бити НаН. ЗАПИС мора бити подесан за "
"исписивање једног аргумента врсте „double“; подразумева „%.PRECf“ ако су "
"ПРВИ, ПОВЕЋАЊЕ и ПОСЛЕДЊИ сви децимални бројеви непокретног зареза са "
"највећом тачношћу ТАЧН, а „%g“ у супротном."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АУТОР"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Ulrich Drepper."
msgstr "Написао јеУлрих Дрепер."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Помоћ на мрежи за ГНУ coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Грешке у преводу пријавите на E<lt>https://translationproject.org/team/sr."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АУТОРСКА ПРАВА"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ово је слободан софтвер: слободни сте да га мењате и расподељујете. Не "
"постоји НИКАКВА ГАРАНЦИЈА, у оквирима дозвољеним законом."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/seqE<gt>"
msgstr ""
"Сва документација се налази на E<lt>https://www.gnu.org/software/coreutils/"
"seqE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) seq invocation\\(aq"
msgstr ""
"или је доступна на рачунару путем наредбе „info \\(aq(coreutils) seq "
"invocation\\(aq“"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Септембра 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "ГНУ coreutils 8.32"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "Септембра 2022"

#. type: TH
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "January 2023"
msgstr "јануара 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Априла 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Октобра 2021"
