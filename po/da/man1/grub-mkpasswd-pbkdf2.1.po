# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-06-18 15:07+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-MKPASSWD-PBKDF2"
msgstr "GRUB-MKPASSWD-PBKDF2"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "February 2023"
msgstr "februar 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r456.g65bc45963-1"
msgstr "GRUB 2:2.06.r456.g65bc45963-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-mkpasswd-pbkdf2 - generate hashed password for GRUB"
msgstr "grub-mkpasswd-pbkdf2 - opret adgangskodehash for GRUB"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkpasswd-pbkdf2> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mkpasswd-pbkdf2> [I<\\,TILVALG\\/>...] [I<\\,TILVALG\\/>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Generate PBKDF2 password hash."
msgstr "Opret PBKDF2-adgangskodehash."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>, B<--iteration-count>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--iteration-count>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Number of PBKDF2 iterations"
msgstr "Antal PBKDF2-iterationer"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-l>, B<--buflen>=I<\\,NUM\\/>"
msgstr "B<-l>, B<--buflen>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Length of generated hash"
msgstr "Længde af genereret hash"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>, B<--salt>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--salt>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Length of salt"
msgstr "Længde af saltnummer"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriske eller valgfri argumenter til lange tilvalg er også "
"obligatoriske henholdsvis valgfri til de tilsvarende korte."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapporter programfejl på engelsk til E<lt>bug-grub@gnu.orgE<gt>.\n"
"Oversættelsesfejl rapporteres til E<lt>dansk@dansk-gruppen.dkE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-mkpasswd-pbkdf2> is maintained as a "
"Texinfo manual.  If the B<info> and B<grub-mkpasswd-pbkdf2> programs are "
"properly installed at your site, the command"
msgstr ""
"Den fulde dokumentation for B<grub-mkpasswd-pbkdf2> vedligeholdes som "
"Texinfo-manual. Hvis B<info> og B<grub-mkpasswd-pbkdf2> programmerne er "
"korrekt installeret på din side, bør kommandoen"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-mkpasswd-pbkdf2>"
msgstr "B<info grub-mkpasswd-pbkdf2>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "November 2022"
msgstr "november 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u5"
msgstr "GRUB 2.06-3~deb11u5"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-8"
msgstr "GRUB 2.06-8"
