# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 19:06+0100\n"
"PO-Revision-Date: 2000-12-15 15:35+0100\n"
"Last-Translator: Przemek Borys <pborys@dione.ids.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "PSNUP"
msgstr "PSNUP"

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "May 2022"
msgstr "maj 2022"

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "psnup 2.09"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "psnup - put multiple pages of a PostScript document on to one page"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<psnup> [I<\\,OPTION\\/>...] I<\\,-NUP \\/>[I<\\,INFILE \\/>[I<\\,OUTFILE\\/"
">]]"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Put multiple pages of a PostScript document on to one page."
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-NUMBER>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "number of pages to impose on each output page"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgid "B<-p>, B<--paper>=I<\\,PAPER\\/>"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "output paper name or dimensions"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgid "B<-P>, B<--inpaper>=I<\\,PAPER\\/>"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "input paper name or dimensions"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgid "B<-m>, B<--margin>=I<\\,DIMENSION\\/>"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"width of margin around each output page [default 0pt]; useful for thumbnail "
"sheets, as the original page margins will be shrunk"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgid "B<-b>, B<--border>=I<\\,DIMENSION\\/>"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "width of border around each input page"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgid "B<-d>, B<--draw>[=I<\\,DIMENSION\\/>]"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"draw a line of given width around each page [relative to input page size; "
"argument defaults to 1pt; default is no line]"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--rotatedleft>"
msgstr "B<-l>, B<--rotatedleft>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "input pages are rotated left 90 degrees"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--rotatedright>"
msgstr "B<-r>, B<--rotatedright>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "input pages are rotated right 90 degrees"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--flip>"
msgstr "B<-f>, B<--flip>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "swap output pages' width and height"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--transpose>"
msgstr "B<-c>, B<--transpose>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "swap columns and rows (column-major order)"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgid "B<-t>, B<--tolerance>=I<\\,NUMBER\\/>"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "maximum wasted area in square pt [default: 100,000]"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "don't show page numbers being output"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"psnup aborts with an error if it cannot arrange the input pages so as to "
"waste less than the given tolerance."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The output paper size defaults to the input paper size; if that is not "
"given, the default given by the `paper' command is used."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "The input paper size defaults to the output paper size."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid ""
"In row-major order (the default), adjacent pages are placed in rows across "
"the paper; in column-major order, they are placed in columns down the page."
msgstr ""
"I<Psnup> normalnie używa rozkładu `row-major', gdzie przyległe strony są "
"umieszczane w wierszach na przestrzeni arkusza. Opcja I<-c> zmienia "
"uporządkowanie na `column-major', gdzie kolejne strony sąumieszczane w "
"kolumnach w dół aruksza."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<psnup> uses B<pstops> to impose multiple logical pages on to each physical "
"sheet of paper."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Paper sizes can be given either as a name (see B<paper(1)>)  or as "
"B<width>xB<height> (see B<psutils>(1)  for the available units)."
msgstr ""

#. type: SS
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "if OK,"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid ""
"The potential use of this utility is varied but one particular use is in "
"conjunction with B<psbook>(1).  For example, using groff to create a "
"PostScript document and lpr as the E<.SM UNIX > print spooler a typical "
"command line might look like this:"
msgstr ""
"Użycie tego narzędzia jest różnorodne, lecz jednym z nich jest połączenie z "
"B<psbook>(1). Na przykład, jeśli używasz groff do tworzenia dokumentu PS, a "
"lpr jako menedżera wydruku, to linia poleceń może wyglądać tak:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "groff -Tps -ms I<file> | psbook | psnup -2 | lpr"
msgstr "groff -Tps -ms I<plik> | psbook | psnup -2 | lpr"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"where file is a 4 page document this command will result in a two page "
"document printing two pages of I<file> per page and rearranges the page "
"order to match the input pages 4 and 1 on the first output page and pages 2 "
"then 3 of the input document on the second output page."
msgstr ""
"Gdzie plik jest 4-stronicowym dokumentem. Wynikiem polecenia będzie "
"dwustronicowy dokument, drukujący dwie strony I<pliku> na stronę i "
"rearanżujący kolejność stron aby odpowiadała stronom 4 i 1 wejścia na "
"pierwszej stronie wyjścia i stronom 2 i 3 wejścia na drugiej stronie wyjścia."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr "Copyright (C) Angus J. C. Duggan 1991-1995"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Copyright \\(co Reuben Thomas 2016-2022.  Released under the GPL version 3, "
"or (at your option) any later version."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr "ZNAKI TOWAROWE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> jest znakiem towarowym Adobe Systems Incorporated."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr "B<psutils>(1), B<paper>(1)"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Wydanie 1 Łata 17"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "psnup - multiple pages per sheet"
msgstr "psnup - wiele stron na arkusz"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<psnup> [ B<-w>I<width> ] [ B<-h>I<height> ] [ B<-p>I<paper> ] [ B<-"
"W>I<width> ] [ B<-H>I<height> ] [ B<-P>I<paper> ] [ B<-l> ] [ B<-r> ] [ B<-"
"f> ] [ B<-c> ] [ B<-m>I<margin> ] [ B<-b>I<border> ] [ B<-d>I<lwidth> ] [ B<-"
"s>I<scale> ] [ B<->I<nup> ] [ B<-q> ] [ I<infile> [ I<outfile> ] ]"
msgstr ""
"B<psnup> [ B<-w>I<szerokość> ] [ B<-h>I<wysokość> ] [ B<-p>I<papier> ] [ B<-"
"W>I<szerokość> ] [ B<-H>I<szerokość> ] [ B<-P>I<papier> ] [ B<-l> ] [ B<-"
"r> ] [ B<-f> ] [ B<-c> ] [ B<-m>I<margines> ] [ B<-b>I<ramka> ] [ B<-"
"d>I<lszerokość> ] [ B<-s>I<skala> ] [ B<->I<nup> ] [ B<-q> ] [ I<plik_we> "
"[ I<plik_wy> ] ]"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"I<Psnup> puts multiple logical pages onto each physical sheet of paper.  The "
"input PostScript file should follow the Adobe Document Structuring "
"Conventions."
msgstr ""
"I<Psnup> wstawia wiele stron logicznych na jeden fizyczny arkusz papieru. "
"Plik wejściowy powinien być zgodny z konwencjami strukturyzacji dokumentu "
"Adobe (DSC)."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The I<-w> option gives the paper width, and the I<-h> option gives the paper "
"height, normally specified in B<cm> or B<in>.  The I<-p> option can be used "
"as an alternative, to set the paper size to B<a0, a1, a2, a3, a4, a5, b5, "
"letter, legal, tabloid, statement,> executive, folio, quarto or B<10x14.> "
"The default paper size is normally B<a4,> but on a Debian system, /etc/"
"papersize is consulted.  The I<-W, -H,> and I<-P> options set the input "
"paper size, if it is different from the output size. This makes it easy to "
"impose pages of one size on a different size of paper."
msgstr ""
"Opcja I<-w> podaje szerokość papieru, a I<-h> jego wysokość. Są one "
"normalnie podawane w B<cm> lub B<in> (cale). Bez jednostki, używane są "
"punkty postscriptowe o rozmiarze 1/72 cala. Opcja I<-p> służy jako "
"alternatywny sposób ustawiania rozmiaru papieru na B<a0, a1, a2, a3, a4, a5, "
"b5, letter, legal, tabloid, statement,> executive, folio, quarto lub "
"B<10x14>. Domyślnym rozmiarem jest B<a4>. Opcje I<-W>, I<-H> i I<-P> "
"ustawiają rozmiar papieru wejściowego. Jest to wymagane, jeśli różni się on "
"od rozmiaru wyjściowego. Ułatwia to narzucanie stron z jednego rozmiaru na "
"drugi."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The I<-l> option should be used for pages which are in landscape orientation "
"(rotated 90 degrees anticlockwise). The I<-r> option should be used for "
"pages which are in seascape orientation (rotated 90 degrees clockwise), and "
"the I<-f> option should be used for pages which have the width and height "
"interchanged, but are not rotated."
msgstr ""
"Opcja I<-l> służy do [generowania] stron, które są w orientacji landscape "
"(obróconych o 90 stopni przeciwnie do ruchu wskazówek zegara). Opcja I<-r> "
"służy do [generowania] stron, które są w orientacji seascape (obróconych o "
"90 stopni w kierunku ruchu wskazówek zegara). Opcja I<-f> służy do "
"[generowania] stron, których wysokość i szerokość są wymienione, lecz nie są "
"obrócone."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"I<Psnup> normally uses `row-major' layout, where adjacent pages are placed "
"in rows across the paper.  The I<-c> option changes the order to `column-"
"major', where successive pages are placed in columns down the paper."
msgstr ""
"I<Psnup> normalnie używa rozkładu `row-major', gdzie przyległe strony są "
"umieszczane w wierszach na przestrzeni arkusza. Opcja I<-c> zmienia "
"uporządkowanie na `column-major', gdzie kolejne strony sąumieszczane w "
"kolumnach w dół aruksza."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"A margin to leave around the whole page can be specified with the I<-m> "
"option. This is useful for sheets of `thumbnail' pages, because the normal "
"page margins are reduced by putting multiple pages on a single sheet."
msgstr ""
"Margines wokół całej strony można ustawić opcją I<-m>. Jest to przydatne, "
"gdyż marginesy normalnych stron są redukowane przez wstawianie wielu stron "
"na jeden arkusz."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The I<-b> option is used to specify an additional margin around each page on "
"a sheet."
msgstr ""
"Opcja I<-b> jest używana do podawania dodatkowego marginesu wokół każdej "
"strony arkusza."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The I<-d> option draws a line around the border of each page, of the "
"specified width.  If the I<lwidth> parameter is omitted, a default linewidth "
"of 1 point is assumed. The linewidth is relative to the original page "
"dimensions, I<i.e.> it is scaled down with the rest of the page."
msgstr ""
"Opcja I<-d> rysuje wokół ramki każdej strony linię określonej grubości. "
"Jeśli parametr I<lszerokość> jest ominięty, używana jest szerokość 1 punktu. "
"Grubość linii jest liczona względem wymiarów oryginalnej strony, tj. może "
"być skalowana wraz z resztą strony."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The scale chosen by I<psnup> can be overridden with the I<-s> option. This "
"is useful to merge pages which are already reduced."
msgstr ""
"Skala, wybrana przez I<psnup> może być przeciążona opcją I<-s>. Jest to "
"przydatne do łączenia stron, które już są zredukowane."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The I<-nup> option selects the number of logical pages to put on each sheet "
"of paper. This can be any whole number; I<psnup> tries to optimise the "
"layout so that the minimum amount of space is wasted. If I<psnup> cannot "
"find a layout within its tolerance limit, it will abort with an error "
"message. The alternative form I<-i nup> can also be used, for compatibility "
"with other n-up programs."
msgstr ""
"Opcja I<-nup> wybiera liczbę stron logicznych, którą wstawić na każdy arkusz "
"papieru. Może to być dowolna liczba całkowita; I<psnup> próbuje "
"optymalizować rozkład tak, aby zmarnować jak najmniej miejsca. Jeśli "
"I<psnup> nie może znaleźć rozkładu w zakresie limitu tolerancji, przerwie "
"działanie z komunikatem o błędzie. Można użyć alternatywnej postaci I<-i "
"nup>, która jest wprowadzona dla kompatybilności z innymi programami n-up."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"I<Psnup> normally prints the page numbers of the pages re-arranged; the I<-"
"q> option suppresses this."
msgstr ""
"I<Psnup> normalnie drukuje numery przearanżowanych stron; opcja I<-q> "
"powstrzymuje to."

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"The potential use of this utility is varied but one particular use is in "
"conjunction with I<psbook(1).> For example, using groff to create a "
"PostScript document and lpr as the E<.SM UNIX > print spooler a typical "
"command line might look like this:"
msgstr ""
"Użycie tego narzędzia jest różnorodne, lecz jednym z nich jest połączenie z "
"B<psbook>(1). Na przykład, jeśli używasz groff do tworzenia dokumentu PS, a "
"lpr jako menedżera wydruku, to linia poleceń może wyglądać tak:"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Where file is a 4 page document this command will result in a two page "
"document printing two pages of I<file> per page and rearranges the page "
"order to match the input pages 4 and 1 on the first output page and pages 2 "
"then 3 of the input document on the second output page."
msgstr ""
"Gdzie plik jest 4-stronicowym dokumentem. Wynikiem polecenia będzie "
"dwustronicowy dokument, drukujący dwie strony I<pliku> na stronę i "
"rearanżujący kolejność stron aby odpowiadała stronom 4 i 1 wejścia na "
"pierwszej stronie wyjścia i stronom 2 i 3 wejścia na drugiej stronie wyjścia."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Copyright (C) Angus J. C. Duggan 1991-1995"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"B<psbook>(1), B<psselect>(1), B<pstops>(1), B<epsffit>(1), B<psnup>(1), "
"B<psresize>(1), B<psmerge>(1), B<fixscribeps>(1), B<getafm>(1), "
"B<fixdlsrps>(1), B<fixfmps>(1), B<fixpsditps>(1), B<fixpspps>(1), "
"B<fixtpps>(1), B<fixwfwps>(1), B<fixwpps>(1), B<fixwwps>(1), "
"B<extractres>(1), B<includeres>(1), B<showchar>(1)"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "I<Psnup> does not accept all DSC comments."
msgstr "I<Psnup> nie przyjmuje wszystkich komentarzy DSC."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "październik 2021"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "psnup 2.07"
msgstr ""

#. type: TP
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: mageia-cauldron
msgid ""
"B<Psnup> uses B<Pstops> to impose multiple logical pages on to each physical "
"sheet of paper."
msgstr ""

#. type: Plain text
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co Reuben Thomas 2016-2021.  Released under the GPL version 3, "
"or (at your option) any later version."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr "grudzień 2021"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "psnup 2.08"
msgstr ""
