# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2014, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2022-11-14 20:03+0100\n"
"PO-Revision-Date: 2021-03-08 22:34+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "UPTIME"
msgstr "UPTIME"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "październik 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: TH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "uptime - tell how long the system has been running"
msgstr "uptime - wskazuje jak długo system jest włączony"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<uptime> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]"
msgstr "B<uptime> [I<\\,OPCJA\\/>]... [I<\\,PLIK\\/>]"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Print the current time, the length of time the system has been up, the "
"number of users on the system, and the average number of jobs in the run "
"queue over the last 1, 5 and 15 minutes.  Processes in an uninterruptible "
"sleep state also contribute to the load average.  If FILE is not specified, "
"use I<\\,/var/run/utmp\\/>.  I<\\,/var/log/wtmp\\/> as FILE is common."
msgstr ""
"Wyświetla aktualny czas, jak długo system jest włączony, aktualną liczbę "
"użytkowników, przeciętną liczbę zadań w kolejce przez ostatnie: 1, 5 i 15 "
"minut. Procesy, które są w nieprzerywalnym stanie uśpienia również wliczają "
"się do średniego obciążenia (load). Jeśli nie podano I<PLIKU> używa plików "
"I<\\,/var/run/utmp\\/>. Często używanym I<PLIKIEM> jest I<\\,/var/log/wtmp\\/"
">."

#. type: TP
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Joseph Arceneaux, David MacKenzie, and Kaveh Ghazi."
msgstr "Napisane przez Josepha Arceneaux, Davida MacKenzie i Kaveha Ghazi."

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"O błędach tłumaczenia poinformuj przez E<lt>https://translationproject.org/"
"team/pl.htmlE<gt>"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/uptimeE<gt>"
msgstr ""
"Pełna dokumentacja E<lt>https://www.gnu.org/software/coreutils/uptimeE<gt>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) uptime invocation\\(aq"
msgstr ""
"lub lokalnie, za pomocą B<info \\(aq(coreutils) uptime invocation\\(aq>"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "wrzesień 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
