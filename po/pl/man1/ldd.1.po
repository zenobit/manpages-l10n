# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>
# Robert Luberda <robert@debian.org>, 2014.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-02-20 20:13+0100\n"
"PO-Revision-Date: 2022-08-07 09:05+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide
#, no-wrap
msgid "ldd"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "ldd - print shared object dependencies"
msgstr "ldd - wyświetla zależności od obiektów dzielonych"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<ldd> [I<option>]... I<file>...\n"
msgstr "B<ldd> [I<opcja>]... I<plik>...\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<ldd> prints the shared objects (shared libraries) required by each "
#| "program or shared object specified on the command line."
msgid ""
"B<ldd> prints the shared objects (shared libraries) required by each program "
"or shared object specified on the command line.  An example of its use and "
"output is the following:"
msgstr ""
"B<ldd> wyświetla obiekty dzielone (biblioteki dzielone) wymagane przez każdy "
"program lub obiekt dzielony podany w linii poleceń."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<ldd /bin/ls>\n"
"    linux-vdso.so.1 (0x00007ffcc3563000)\n"
"    libselinux.so.1 =E<gt> /lib64/libselinux.so.1 (0x00007f87e5459000)\n"
"    libcap.so.2 =E<gt> /lib64/libcap.so.2 (0x00007f87e5254000)\n"
"    libc.so.6 =E<gt> /lib64/libc.so.6 (0x00007f87e4e92000)\n"
"    libpcre.so.1 =E<gt> /lib64/libpcre.so.1 (0x00007f87e4c22000)\n"
"    libdl.so.2 =E<gt> /lib64/libdl.so.2 (0x00007f87e4a1e000)\n"
"    /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)\n"
"    libattr.so.1 =E<gt> /lib64/libattr.so.1 (0x00007f87e4817000)\n"
"    libpthread.so.0 =E<gt> /lib64/libpthread.so.0 (0x00007f87e45fa000)\n"
msgstr ""
"$ B<ldd /bin/ls>\n"
"    linux-vdso.so.1 (0x00007ffcc3563000)\n"
"    libselinux.so.1 =E<gt> /lib64/libselinux.so.1 (0x00007f87e5459000)\n"
"    libcap.so.2 =E<gt> /lib64/libcap.so.2 (0x00007f87e5254000)\n"
"    libc.so.6 =E<gt> /lib64/libc.so.6 (0x00007f87e4e92000)\n"
"    libpcre.so.1 =E<gt> /lib64/libpcre.so.1 (0x00007f87e4c22000)\n"
"    libdl.so.2 =E<gt> /lib64/libdl.so.2 (0x00007f87e4a1e000)\n"
"    /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)\n"
"    libattr.so.1 =E<gt> /lib64/libattr.so.1 (0x00007f87e4817000)\n"
"    libpthread.so.0 =E<gt> /lib64/libpthread.so.0 (0x00007f87e45fa000)\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the usual case, B<ldd> invokes the standard dynamic linker (see B<ld."
#| "so>(8))  with the B<LD_TRACE_LOADED_OBJECTS> environment variable set to "
#| "1, which causes the linker to display the library dependencies.  Be "
#| "aware, however, that in some circumstances, some versions of B<ldd> may "
#| "attempt to obtain the dependency information by directly executing the "
#| "program.  Thus, you should I<never> employ B<ldd> on an untrusted "
#| "executable, since this may result in the execution of arbitrary code.  A "
#| "safer alternative when dealing with untrusted executables is:"
msgid ""
"In the usual case, B<ldd> invokes the standard dynamic linker (see B<ld."
"so>(8))  with the B<LD_TRACE_LOADED_OBJECTS> environment variable set to 1.  "
"This causes the dynamic linker to inspect the program's dynamic "
"dependencies, and find (according to the rules described in B<ld.so>(8))  "
"and load the objects that satisfy those dependencies.  For each dependency, "
"B<ldd> displays the location of the matching object and the (hexadecimal) "
"address at which it is loaded.  (The I<linux-vdso> and I<ld-linux> shared "
"dependencies are special; see B<vdso>(7)  and B<ld.so>(8).)"
msgstr ""
"Zwyczajowo B<ldd> uruchamia standardowy konsolidator dynamiczny (patrz B<ld."
"so>(8) ze zmienną środowiska B<LD_TRACE_LOADED_OBJECTS> ustawioną na 1, co "
"powoduje, że konsolidator wypisze zależności od  bibliotek. Prosimy jednak "
"uważać - niektóre wersje  B<ldd> mogą próbować uzyskać te informacje, "
"uruchamiając program bezpośrednio. Dlatego I<nigdy> nie powinno się używać "
"B<ldd> z niezaufanym programem wykonywalnym, ponieważ może to owocować "
"wykonaniem dowolnego kodu. Bezpieczniejsza alternatywa radzenia sobie z "
"niezaufanymi programami wykonywalnymi to:"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Security"
msgstr "BEZPIECZEŃSTWO"

#.  The circumstances are where the program has an interpreter
#.  other than ld-linux.so. In this case, ldd tries to execute the
#.  program directly with LD_TRACE_LOADED_OBJECTS=1, with the
#.  result that the program interpreter gets control, and can do
#.  what it likes, or pass control to the program itself.
#.  Much more detail at
#.  http://www.catonmat.net/blog/ldd-arbitrary-code-execution/
#.  Mainline glibc's ldd allows this possibility (the line
#.       try_trace "$file"
#.  in glibc 2.15, for example), but many distro versions of
#.  ldd seem to remove that code path from the script.
#.  glibc commit eedca9772e99c72ab4c3c34e43cc764250aa3e3c
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide opensuse-tumbleweed
msgid ""
"Be aware that in some circumstances (e.g., where the program specifies an "
"ELF interpreter other than I<ld-linux.so>), some versions of B<ldd> may "
"attempt to obtain the dependency information by attempting to directly "
"execute the program, which may lead to the execution of whatever code is "
"defined in the program's ELF interpreter, and perhaps to execution of the "
"program itself.  (Before glibc 2.27, the upstream B<ldd> implementation did "
"this for example, although most distributions provided a modified version "
"that did not.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Thus, you should I<never> employ B<ldd> on an untrusted executable, since "
"this may result in the execution of arbitrary code.  A safer alternative "
"when dealing with untrusted executables is:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "$ B<objdump -p /path/to/program | grep NEEDED>\n"
msgstr "$ B<objdump -p /ścieżka/do/programu | grep NEEDED>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Note, however, that this alternative shows only the direct dependencies of "
"the executable, while B<ldd> shows the entire dependency tree of the "
"executable."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print the version number of B<ldd>."
msgstr "Drukuje numer wersji B<ldd>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Print all information, including, for example, symbol versioning information."
msgstr ""
"Wypisuje wszystkie informacje, włączając to na przykład informacje o "
"wersjach symboli."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unused>"
msgstr "B<-u>, B<--unused>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print unused direct dependencies.  (Since glibc 2.3.4.)"
msgstr "Wypisuje nieużywane bezpośrednie zależności. (Od glibc 2.3.4)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--data-relocs>"
msgstr "B<-d>, B<--data-relocs>"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Perform relocations and report any missing objects (ELF only)."
msgstr "Dokonuje relokacji i zgłasza wszelkie brakujące funkcje (tylko ELF)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--function-relocs>"
msgstr "B<-r>, B<--function-relocs>"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Perform relocations for both data objects and functions, and report any "
"missing objects or functions (ELF only)."
msgstr ""
"Dokonuje relokacji zarówno dla obiektów danych, jak i funkcji i zgłasza "
"listę nieobecnych obiektów lub funkcji (tylko ELF)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#
#. #-#-#-#-#  archlinux: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \[aq]/\[aq]; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \(aq/\(aq; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \[aq]/\[aq]; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#. #-#-#-#-#  fedora-38: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \[aq]/\[aq]; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \[aq]/\[aq]; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \(aq/\(aq; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ldd.1.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH NOTES
#.  The standard version of
#.  .B ldd
#.  comes with glibc2.
#.  Libc5 came with an older version, still present
#.  on some systems.
#.  The long options are not supported by the libc5 version.
#.  On the other hand, the glibc2 version does not support
#.  .B \-V
#.  and only has the equivalent
#.  .BR \-\-version .
#.  .LP
#.  The libc5 version of this program will use the name of a library given
#.  on the command line as-is when it contains a \(aq/\(aq; otherwise it
#.  searches for the library in the standard locations.
#.  To run it
#.  on a shared library in the current directory, prefix the name with "./".
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Usage information."
msgstr "Informacje o użyciu programu."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ldd> does not work on a.out shared libraries."
msgstr "B<ldd> nie działa na bibliotekach współdzielonych a.out."

#.  .SH AUTHOR
#.  David Engel.
#.  Roland McGrath and Ulrich Drepper.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ldd> does not work with some extremely old a.out programs which were built "
"before B<ldd> support was added to the compiler releases.  If you use B<ldd> "
"on one of these programs, the program will attempt to run with I<argc> = 0 "
"and the results will be unpredictable."
msgstr ""
"B<ldd> nie zadziała z bardzo starymi programami a.out, które zostały "
"skonsolidowane zanim dodano do kompilatora obsługę B<ldd>. Jeśli użyje się "
"B<ldd> na jednym z tych programów, to program będzie uruchomiony z I<argc> = "
"0, a wyniki tego będą nieprzewidywalne."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<pldd>(1), B<sprof>(1), B<ld.so>(8), B<ldconfig>(8)"
msgstr "B<pldd>(1), B<sprof>(1), B<ld.so>(8), B<ldconfig>(8)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "LDD"
msgstr "LDD"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 marca 2019 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<ldd> [I<option>]... I<file>..."
msgstr "B<ldd> [I<opcja>]... I<plik>..."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "B<ldd> prints the shared objects (shared libraries) required by each "
#| "program or shared object specified on the command line."
msgid ""
"B<ldd> prints the shared objects (shared libraries) required by each program "
"or shared object specified on the command line.  An example of its use and "
"output (using B<sed>(1)  to trim leading white space for readability in this "
"page)  is the following:"
msgstr ""
"B<ldd> wyświetla obiekty dzielone (biblioteki dzielone) wymagane przez każdy "
"program lub obiekt dzielony podany w linii poleceń."

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"$ B<ldd /bin/ls | sed \\(aqs/^ */    /\\(aq>\n"
"    linux-vdso.so.1 (0x00007ffcc3563000)\n"
"    libselinux.so.1 =E<gt> /lib64/libselinux.so.1 (0x00007f87e5459000)\n"
"    libcap.so.2 =E<gt> /lib64/libcap.so.2 (0x00007f87e5254000)\n"
"    libc.so.6 =E<gt> /lib64/libc.so.6 (0x00007f87e4e92000)\n"
"    libpcre.so.1 =E<gt> /lib64/libpcre.so.1 (0x00007f87e4c22000)\n"
"    libdl.so.2 =E<gt> /lib64/libdl.so.2 (0x00007f87e4a1e000)\n"
"    /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)\n"
"    libattr.so.1 =E<gt> /lib64/libattr.so.1 (0x00007f87e4817000)\n"
"    libpthread.so.0 =E<gt> /lib64/libpthread.so.0 (0x00007f87e45fa000)\n"
msgstr ""
"$ B<ldd /bin/ls | sed \\(aqs/^ */    /\\(aq>\n"
"    linux-vdso.so.1 (0x00007ffcc3563000)\n"
"    libselinux.so.1 =E<gt> /lib64/libselinux.so.1 (0x00007f87e5459000)\n"
"    libcap.so.2 =E<gt> /lib64/libcap.so.2 (0x00007f87e5254000)\n"
"    libc.so.6 =E<gt> /lib64/libc.so.6 (0x00007f87e4e92000)\n"
"    libpcre.so.1 =E<gt> /lib64/libpcre.so.1 (0x00007f87e4c22000)\n"
"    libdl.so.2 =E<gt> /lib64/libdl.so.2 (0x00007f87e4a1e000)\n"
"    /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)\n"
"    libattr.so.1 =E<gt> /lib64/libattr.so.1 (0x00007f87e4817000)\n"
"    libpthread.so.0 =E<gt> /lib64/libpthread.so.0 (0x00007f87e45fa000)\n"

#.  The circumstances are where the program has an interpreter
#.  other than ld-linux.so. In this case, ldd tries to execute the
#.  program directly with LD_TRACE_LOADED_OBJECTS=1, with the
#.  result that the program interpreter gets control, and can do
#.  what it likes, or pass control to the program itself.
#.  Much more detail at
#.  http://www.catonmat.net/blog/ldd-arbitrary-code-execution/
#.  Mainline glibc's ldd allows this possibility (the line
#.       try_trace "$file"
#.  in glibc 2.15, for example), but many distro versions of
#.  ldd seem to remove that code path from the script.
#.  glibc commit eedca9772e99c72ab4c3c34e43cc764250aa3e3c
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Be aware that in some circumstances (e.g., where the program specifies an "
"ELF interpreter other than I<ld-linux.so>), some versions of B<ldd> may "
"attempt to obtain the dependency information by attempting to directly "
"execute the program, which may lead to the execution of whatever code is "
"defined in the program's ELF interpreter, and perhaps to execution of the "
"program itself.  (In glibc versions before 2.27, the upstream B<ldd> "
"implementation did this for example, although most distributions provided a "
"modified version that did not.)"
msgstr ""

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"$ B<ldd /bin/ls>\n"
"        linux-vdso.so.1 (0x00007ffcc3563000)\n"
"        libselinux.so.1 =E<gt> /lib64/libselinux.so.1 (0x00007f87e5459000)\n"
"        libcap.so.2 =E<gt> /lib64/libcap.so.2 (0x00007f87e5254000)\n"
"        libc.so.6 =E<gt> /lib64/libc.so.6 (0x00007f87e4e92000)\n"
"        libpcre.so.1 =E<gt> /lib64/libpcre.so.1 (0x00007f87e4c22000)\n"
"        libdl.so.2 =E<gt> /lib64/libdl.so.2 (0x00007f87e4a1e000)\n"
"        /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)\n"
"        libattr.so.1 =E<gt> /lib64/libattr.so.1 (0x00007f87e4817000)\n"
"        libpthread.so.0 =E<gt> /lib64/libpthread.so.0 (0x00007f87e45fa000)\n"
msgstr ""
"$ B<ldd /bin/ls>\n"
"        linux-vdso.so.1 (0x00007ffcc3563000)\n"
"        libselinux.so.1 =E<gt> /lib64/libselinux.so.1 (0x00007f87e5459000)\n"
"        libcap.so.2 =E<gt> /lib64/libcap.so.2 (0x00007f87e5254000)\n"
"        libc.so.6 =E<gt> /lib64/libc.so.6 (0x00007f87e4e92000)\n"
"        libpcre.so.1 =E<gt> /lib64/libpcre.so.1 (0x00007f87e4c22000)\n"
"        libdl.so.2 =E<gt> /lib64/libdl.so.2 (0x00007f87e4a1e000)\n"
"        /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)\n"
"        libattr.so.1 =E<gt> /lib64/libattr.so.1 (0x00007f87e4817000)\n"
"        libpthread.so.0 =E<gt> /lib64/libpthread.so.0 (0x00007f87e45fa000)\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 grudnia 2022 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"
