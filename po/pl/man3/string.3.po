# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Paweł Wilk <siefca@pl.qmail.org>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2004.
# Robert Luberda <robert@debian.org>, 2014, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-02-20 20:32+0100\n"
"PO-Revision-Date: 2023-03-04 12:44+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "string"
msgstr "string"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-01-22"
msgstr "22 stycznia 2023 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"stpcpy, strcasecmp, strcat, strchr, strcmp, strcoll, strcpy, strcspn, "
"strdup, strfry, strlen, strncat, strncmp, strncpy, strncasecmp, strpbrk, "
"strrchr, strsep, strspn, strstr, strtok, strxfrm, index, rindex - string "
"operations"
msgstr ""
"stpcpy, strcasecmp, strcat, strchr, strcmp, strcoll, strcpy, strcspn, "
"strdup, strfry, strlen, strncat, strncmp, strncpy, strncasecmp, strpbrk, "
"strrchr, strsep, strspn, strstr, strtok, strxfrm, index, rindex - operacje "
"na łańcuchach znaków"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<#include E<lt>strings.hE<gt>>"
msgstr "B<#include E<lt>strings.hE<gt>>"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Compare the strings I<s1> and I<s2> ignoring case."
msgstr "Porównuje łańcuchy I<s1> i I<s2>, nie zważając na wielkość liter."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int strncasecmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"
msgstr "B<int strncasecmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Compare the first I<n> bytes of the strings I<s1> and I<s2> ignoring case."
msgstr ""
"Porównuje pierwszych I<n> bajtów łańcuchów I<s1> i I<s2>, nie zważając na "
"wielkość liter."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *index(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *index(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Identical to B<strchr>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Identical to B<strrchr>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>"
msgstr "B<#include E<lt>string.hE<gt>>"

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<char *stpcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr "B<char *stpcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Copy a string from I<src> to I<dest>, returning a pointer to the end of the "
"resulting string at I<dest>."
msgstr ""
"Kopiuje łańcuch z I<src> do I<dest> i zwraca wskaźnik do końca wynikowego "
"łańcucha I<dest>."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<char *strcat(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr "B<char *strcat(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Append the string I<src> to the string I<dest>, returning a pointer I<dest>."
msgstr ""
"Dołącza łańcuch I<src> do łańcucha I<dest> i zwraca wskaźnik do I<dest>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strchr(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *strchr(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Return a pointer to the first occurrence of the character I<c> in the string "
"I<s>."
msgstr "Zwraca wskaźnik do pierwszego wystąpienia znaku I<c> w łańcuchu I<s>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Compare the strings I<s1> with I<s2>."
msgstr "Porównuje łańcuchy I<s1> i I<s2>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int strcoll(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr "B<int strcoll(const char *>I<s1>B<, const char *>I<s2>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Compare the strings I<s1> with I<s2> using the current locale."
msgstr ""
"Porównuje łańcuchy I<s1> i I<s2> używając bieżących ustawień regionalnych."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<char *strcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr "B<char *strcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Copy the string I<src> to I<dest>, returning a pointer to the start of "
"I<dest>."
msgstr ""
"Kopiuje łańcuch z I<src> do I<dest> i zwraca wskaźnik do początku łańcucha "
"I<dest>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strcspn(const char *>I<s>B<, const char *>I<reject>B<);>"
msgstr "B<size_t strcspn(const char *>I<s>B<, const char *>I<reject>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Calculate the length of the initial segment of the string I<s> which does "
"not contain any of bytes in the string I<reject>,"
msgstr ""
"Oblicza długość początkowego segmentu łańcucha I<s> niezawierającego żadnego "
"z bajtów łańcucha I<reject>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strdup(const char *>I<s>B<);>"
msgstr "B<char *strdup(const char *>I<s>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Return a duplicate of the string I<s> in memory allocated using B<malloc>(3)."
msgstr ""
"Zwraca duplikat łańcucha I<n>, w pamięci nowo zaalokowanej przez B<malloc>(3)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strfry(char *>I<string>B<);>"
msgstr "B<char *strfry(char *>I<string>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Randomly swap the characters in I<string>."
msgstr "Przypadkowo zamienia kolejność znaków w łańcuchu I<string>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strlen(const char *>I<s>B<);>"
msgstr "B<size_t strlen(const char *>I<s>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Return the length of the string I<s>."
msgstr "Zwraca długość łańcucha I<s>."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strncat(char >I<dest>B<[restrict strlen(.>I<dest>B<) + .>I<n>B< + 1],>\n"
"B<       const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"
msgstr ""
"B<char *strncat(char >I<dest>B<[restrict strlen(.>I<dest>B<) + .>I<n>B< + 1],>\n"
"B<       const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Append at most I<n> bytes from the string I<src> to the string I<dest>, "
#| "returning a pointer to I<dest>."
msgid ""
"Append at most I<n> bytes from the unterminated string I<src> to the string "
"I<dest>, returning a pointer to I<dest>."
msgstr ""
"Dołącza co najwyżej I<n> bajtów łańcucha I<src> do łańcucha I<dest> i zwraca "
"wskaźnik do I<dest>."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int strncmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"
msgstr "B<int strncmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Compare at most I<n> bytes of the strings I<s1> and I<s2>."
msgstr "Porównuje co najwyżej I<n> bajtów łańcuchów I<s1> i I<s2>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strpbrk(const char *>I<s>B<, const char *>I<accept>B<);>"
msgstr "B<char *strpbrk(const char *>I<s>B<, const char *>I<accept>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Return a pointer to the first occurrence in the string I<s> of one of the "
"bytes in the string I<accept>."
msgstr ""
"Zwraca wskaźnik do pierwszego wystąpienia w łańcuchu I<s> jednego z bajtów "
"łańcucha I<accept>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Return a pointer to the last occurrence of the character I<c> in the string "
"I<s>."
msgstr "Zwraca wskaźnik do ostatniego wystąpienia znaku I<c> w łańcuchu I<s>."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<char *strsep(char **restrict >I<stringp>B<, const char *restrict >I<delim>B<);>"
msgstr "B<char *strsep(char **restrict >I<stringp>B<, const char *restrict >I<delim>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Extract the initial token in I<stringp> that is delimited by one of the "
"bytes in I<delim>."
msgstr ""
"Wyciąga z I<stringp> początkowe słowo, rozdzielone jednym z bajtów z "
"I<delim>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strspn(const char *>I<s>B<, const char *>I<accept>B<);>"
msgstr "B<size_t strspn(const char *>I<s>B<, const char *>I<accept>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Calculate the length of the starting segment in the string I<s> that "
"consists entirely of bytes in I<accept>."
msgstr ""
"Oblicza długość początkowego segmentu łańcucha I<s> składającego się tylko i "
"wyłącznie z bajtów łańcucha I<accept>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strstr(const char *>I<haystack>B<, const char *>I<needle>B<);>"
msgstr "B<char *strstr(const char *>I<haystack>B<, const char *>I<needle>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Find the first occurrence of the substring I<needle> in the string "
"I<haystack>, returning a pointer to the found substring."
msgstr ""
"Znajduje pierwsze wystąpienie podłańcucha I<needle> w łańcuchu I<haystack> i "
"zwraca wskaźnik do znalezionego podłańcucha."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<char *strtok(char *restrict >I<s>B<, const char *restrict >I<delim>B<);>"
msgstr "B<char *strtok(char *restrict >I<s>B<, const char *restrict >I<delim>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Extract tokens from the string I<s> that are delimited by one of the bytes "
"in I<delim>."
msgstr "Wyciąga z I<stringp> słowa, rozdzielone jednym z bajtów z I<delim>."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<size_t strxfrm(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<        size_t >I<n>B<);>\n"
msgstr ""
"B<size_t strxfrm(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<        size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Transforms I<src> to the current locale and copies the first I<n> bytes to "
"I<dest>."
msgstr ""
"Przekształca I<src> do bieżących ustawień regionalnych i kopiuje pierwszych "
"I<n> bajtów do I<dest>."

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Obsolete functions"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strncpy(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"
msgstr ""
"B<char *strncpy(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Copy at most I<n> bytes from string I<src> to I<dest>, returning a pointer "
"to the start of I<dest>."
msgstr ""
"Kopiuje co najwyżej I<n> bajtów łańcucha I<src> do I<dest> i zwraca wskaźnik "
"do początku łańcucha I<dest>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The string functions perform operations on null-terminated strings.  See the "
"individual man pages for descriptions of each function."
msgstr ""
"Funkcje łańcuchowe umożliwiają operacje na zakończonych znakiem null "
"łańcuchach. Opis każdej z funkcji znajduje się na indywidualnej dla danej "
"funkcji stronie podręcznika ekranowego."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<bstring>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), B<strchr>(3), "
"B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), B<strdup>(3), "
"B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), B<strncmp>(3), "
"B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
msgstr ""
"B<bstring>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), B<strchr>(3), "
"B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), B<strdup>(3), "
"B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), B<strncmp>(3), "
"B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "STRING"
msgstr "STRING"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 marca 2019 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int strncasecmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"
msgstr "B<int strncasecmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *stpcpy(char *>I<dest>B<, const char *>I<src>B<);>"
msgstr "B<char *stpcpy(char *>I<dest>B<, const char *>I<src>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strcat(char *>I<dest>B<, const char *>I<src>B<);>"
msgstr "B<char *strcat(char *>I<dest>B<, const char *>I<src>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strcpy(char *>I<dest>B<, const char *>I<src>B<);>"
msgstr "B<char *strcpy(char *>I<dest>B<, const char *>I<src>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strncat(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"
msgstr "B<char *strncat(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Append at most I<n> bytes from the string I<src> to the string I<dest>, "
"returning a pointer to I<dest>."
msgstr ""
"Dołącza co najwyżej I<n> bajtów łańcucha I<src> do łańcucha I<dest> i zwraca "
"wskaźnik do I<dest>."

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"
msgstr "B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"
msgstr "B<char *strncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>"
msgstr "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *strtok(char *>I<s>B<, const char *>I<delim>B<);>"
msgstr "B<char *strtok(char *>I<s>B<, const char *>I<delim>B<);>"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<size_t strxfrm(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"
msgstr "B<size_t strxfrm(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<index>(3), B<rindex>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), "
"B<strchr>(3), B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), "
"B<strdup>(3), B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), "
"B<strncmp>(3), B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), "
"B<strspn>(3), B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
msgstr ""
"B<index>(3), B<rindex>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), "
"B<strchr>(3), B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), "
"B<strdup>(3), B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), "
"B<strncmp>(3), B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), "
"B<strspn>(3), B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2014-01-04"
msgstr "4 stycznia 2014 r."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Compare the first I<n> bytes of the strings I<s1> and I<s2> ignoring case."
msgid ""
"Compare the first I<n> characters of the strings I<s1> and I<s2> ignoring "
"case."
msgstr ""
"Porównuje pierwszych I<n> bajtów łańcuchów I<s1> i I<s2>, nie zważając na "
"wielkość liter."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Append at most I<n> bytes from the string I<src> to the string I<dest>, "
#| "returning a pointer to I<dest>."
msgid ""
"Append at most I<n> characters from the string I<src> to the string I<dest>, "
"returning a pointer to I<dest>."
msgstr ""
"Dołącza co najwyżej I<n> bajtów łańcucha I<src> do łańcucha I<dest> i zwraca "
"wskaźnik do I<dest>."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Transforms I<src> to the current locale and copies the first I<n> bytes "
#| "to I<dest>."
msgid ""
"Transforms I<src> to the current locale and copies the first I<n> characters "
"to I<dest>."
msgstr ""
"Przekształca I<src> do bieżących ustawień regionalnych i kopiuje pierwszych "
"I<n> bajtów do I<dest>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-06"
msgstr "6 grudnia 2022 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<size_t strxfrm(char >I<dst>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<        size_t >I<n>B<);>\n"
msgstr ""
"B<size_t strxfrm(char >I<dst>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<        size_t >I<n>B<);>\n"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Transforms I<src> to the current locale and copies the first I<n> bytes to "
"I<dst>."
msgstr ""
"Przekształca I<src> do bieżących ustawień regionalnych i kopiuje pierwszych "
"I<n> bajtów do I<dst>."

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"B<bstring>(3), B<index>(3), B<rindex>(3), B<stpcpy>(3), B<strcasecmp>(3), "
"B<strcat>(3), B<strchr>(3), B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), "
"B<strcspn>(3), B<strdup>(3), B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), "
"B<strncat>(3), B<strncmp>(3), B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), "
"B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
msgstr ""
"B<bstring>(3), B<index>(3), B<rindex>(3), B<stpcpy>(3), B<strcasecmp>(3), "
"B<strcat>(3), B<strchr>(3), B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), "
"B<strcspn>(3), B<strdup>(3), B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), "
"B<strncat>(3), B<strncmp>(3), B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), "
"B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
