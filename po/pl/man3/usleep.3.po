# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Robert Luberda <robert@debian.org>, 2005, 2006, 2012, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-02-20 20:39+0100\n"
"PO-Revision-Date: 2019-08-15 23:55+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<usleep>()"
msgid "usleep"
msgstr "B<usleep>()"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "usleep - suspend execution for microsecond intervals"
msgstr "usleep - zawiesza wykonanie na czas wyrażony w mikrosekundach"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int usleep(useconds_t >I<usec>B<);>\n"
msgstr "B<int usleep(useconds_t >I<usec>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<usleep>():"
msgstr "B<usleep>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    Since glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Od glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Przed glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<usleep>()  function suspends execution of the calling thread for (at "
"least) I<usec> microseconds.  The sleep may be lengthened slightly by any "
"system activity or by the time spent processing the call or by the "
"granularity of system timers."
msgstr ""
"Funkcja B<usleep>() zawiesza wykonanie wywołującego procesu na (co najmniej) "
"I<usec> mikrosekund. Zawieszenie może być delikatnie wydłużone przez "
"jakąkolwiek aktywność systemu albo przez czas spędzony na przetwarzaniu "
"wywołania, albo z powodu ziarnistości (częstotliwości odświeżania) liczników "
"systemowych."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<usleep>()  function returns 0 on success.  On error, -1 is "
#| "returned, with I<errno> set to indicate the cause of the error."
msgid ""
"The B<usleep>()  function returns 0 on success.  On error, -1 is returned, "
"with I<errno> set to indicate the error."
msgstr ""
"Funkcja B<usleep>() w przypadku powodzenia zwraca 0. W razie wystąpienia "
"błędu zwracane jest -1 i ustawiana jest zmienna I<errno> wskazując na błąd."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Interrupted by a signal; see B<signal>(7)."
msgstr "Przerwane przez sygnał, patrz B<signal>(7)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<usec> is greater than or equal to 1000000.  (On systems where that is "
"considered an error.)"
msgstr ""
"I<usec> jest większe lub równe 1000000. (Dla systemów, dla których jest to "
"traktowane jako błąd)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<usleep>()"
msgstr "B<usleep>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 declares this function obsolete; use "
"B<nanosleep>(2)  instead.  POSIX.1-2008 removes the specification of "
"B<usleep>()."
msgstr ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 uznają tę funkcję jako przestarzałą, "
"sugerując używanie zamiast niej funkcji B<nanosleep>(2).  POSIX.1-2008  "
"usuwa opis funkcji B<usleep>()."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On the original BSD implementation, and in glibc before version 2.2.2, "
#| "the return type of this function is I<void>.  The POSIX version returns "
#| "I<int>, and this is also the prototype used since glibc 2.2.2."
msgid ""
"On the original BSD implementation, and before glibc 2.2.2, the return type "
"of this function is I<void>.  The POSIX version returns I<int>, and this is "
"also the prototype used since glibc 2.2.2."
msgstr ""
"W oryginalnej implementacji BSD oraz w wersjach wcześniejszych niż 2.2.2 "
"biblioteki glibc  typem zwracanym przez tę funkcję był I<void>. Wersja POSIX "
"zwraca I<int>, taki typ jest też zwracany przez glibc od wersji 2.2.2."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Only the B<EINVAL> error return is documented by SUSv2 and POSIX.1-2001."
msgstr "SUSv2 i POSIX.1-2001 dokumentują tylko błąd B<EINVAL>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The interaction of this function with the B<SIGALRM> signal, and with other "
"timer functions such as B<alarm>(2), B<sleep>(3), B<nanosleep>(2), "
"B<setitimer>(2), B<timer_create>(2), B<timer_delete>(2), "
"B<timer_getoverrun>(2), B<timer_gettime>(2), B<timer_settime>(2), "
"B<ualarm>(3)  is unspecified."
msgstr ""
"Interakcja tej funkcji z sygnałem SIGALRM oraz z innymi funkcjami "
"licznikowymi, takimi jak B<alarm>(2), B<sleep>(3), B<nanosleep>(2), "
"B<setitimer>(2), B<timer_create>(2), B<timer_delete>(2), "
"B<timer_getoverrun>(2), B<timer_gettime>(2), B<timer_settime>(2), "
"B<ualarm>(3) jest nieokreślona."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
#| "B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"
msgid ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<useconds_t>(3type), B<time>(7)"
msgstr ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "USLEEP"
msgstr "USLEEP"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.12:"
msgstr "Od glibc 2.12:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* Glibc od 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "Before glibc 2.12:"
msgstr "Przed glibc 2.12:"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye
msgid "_BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "_BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The B<usleep>()  function returns 0 on success.  On error, -1 is returned, "
"with I<errno> set to indicate the cause of the error."
msgstr ""
"Funkcja B<usleep>() w przypadku powodzenia zwraca 0. W razie wystąpienia "
"błędu zwracane jest -1 i ustawiana jest zmienna I<errno> wskazując na błąd."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On the original BSD implementation, and in glibc before version 2.2.2, the "
"return type of this function is I<void>.  The POSIX version returns I<int>, "
"and this is also the prototype used since glibc 2.2.2."
msgstr ""
"W oryginalnej implementacji BSD oraz w wersjach wcześniejszych niż 2.2.2 "
"biblioteki glibc  typem zwracanym przez tę funkcję był I<void>. Wersja POSIX "
"zwraca I<int>, taki typ jest też zwracany przez glibc od wersji 2.2.2."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The type I<useconds_t> is an unsigned integer type capable of holding "
"integers in the range [0,1000000].  Programs will be more portable if they "
"never mention this type explicitly.  Use"
msgstr ""
"Typ I<useconds_t> jest typem unsigned integer, zdolnym do przechowywania "
"liczb naturalnych z zakresu [0,1000000]. Programy będą bardziej przenośne, "
"gdy nigdy nie użyją wprost tego typu. Proszę użyć"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"\\&...\n"
"    unsigned int usecs;\n"
"\\&...\n"
"    usleep(usecs);\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"\\&...\n"
"    unsigned int usecs;\n"
"\\&...\n"
"    usleep(usecs);\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"
msgstr ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-5
msgid "Before glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "Przed glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
#| "B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"
msgid ""
"BR alarm (2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"
msgstr ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<sleep>(3), B<ualarm>(3), B<time>(7)"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Od glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* Glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Przed glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
