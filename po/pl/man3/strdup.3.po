# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014, 2017, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-02-20 20:32+0100\n"
"PO-Revision-Date: 2019-08-16 21:26+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "strdup"
msgstr "strdup"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strdup, strndup, strdupa, strndupa - duplicate a string"
msgstr "strdup, strndup, strdupa, strndupa - powielenie łańcucha"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strdup(const char *>I<s>B<);>\n"
msgstr "B<char *strdup(const char *>I<s>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strndup(const char >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr ""
"B<char *strndup(const char >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char >I<s>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strdup>():"
msgstr "B<strdup>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Od glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strndup>():"
msgstr "B<strndup>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Od glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Przed glibc 2.10:\n"
"        _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<strdupa>(), B<strndupa>():"
msgstr "B<strdupa>(), B<strndupa>():"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strdup>()  function returns a pointer to a new string which is a "
"duplicate of the string I<s>.  Memory for the new string is obtained with "
"B<malloc>(3), and can be freed with B<free>(3)."
msgstr ""
"Funkcja B<strdup>() zwraca wskaźnik do nowego łańcucha, który stanowi kopię "
"łańcucha I<s>. Pamięć dla nowego łańcucha jest przydzielana za pomocą "
"B<malloc>(3) i może być zwolniona za pomocą B<free>(3)."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The B<strndup>()  function is similar, but copies at most I<n> bytes.  If "
"I<s> is longer than I<n>, only I<n> bytes are copied, and a terminating null "
"byte (\\[aq]\\e0\\[aq]) is added."
msgstr ""
"Funkcja B<strndup>() jest podobna, lecz kopiuje co najwyżej I<n> znaków. "
"Jeśli I<s> jest dłuższe niż I<n>, kopiowane jest tylko I<n> znaków i "
"dodawany jest kończący znak null (\\[aq]\\e0\\[aq])."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<strdupa>()  and B<strndupa>()  are similar, but use B<alloca>(3)  to "
"allocate the buffer."
msgstr ""
"B<strdupa>() i B<strndupa>() są podobne, ale korzystają z B<alloca>(3) do "
"przydzielania pamięci na bufor."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, the B<strdup>()  function returns a pointer to the duplicated "
#| "string.  It returns NULL if insufficient memory was available, with "
#| "I<errno> set to indicate the cause of the error."
msgid ""
"On success, the B<strdup>()  function returns a pointer to the duplicated "
"string.  It returns NULL if insufficient memory was available, with I<errno> "
"set to indicate the error."
msgstr ""
"Funkcja B<strdup>() zwraca wskaźnik do skopiowanego łańcucha. Zwraca NULL, "
"gdy nie jest dostępna dostateczna ilość pamięci, i ustawia I<errno>, "
"wskazując na przyczynę błędu."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient memory available to allocate duplicate string."
msgstr ""
"Nie można przydzielić dostatecznej ilości pamięci potrzebnej dla kopii "
"łańcucha."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strdup>(),\n"
"B<strndup>(),\n"
"B<strdupa>(),\n"
"B<strndupa>()"
msgstr ""
"B<strdup>(),\n"
"B<strndup>(),\n"
"B<strdupa>(),\n"
"B<strndupa>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#.  4.3BSD-Reno, not (first) 4.3BSD.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<strdup>()  conforms to SVr4, 4.3BSD, POSIX.1-2001.  B<strndup>()  conforms "
"to POSIX.1-2008.  B<strdupa>()  and B<strndupa>()  are GNU extensions."
msgstr ""
"B<strdup>() jest zgodne z SVr4, 4.3BSD, POSIX.1-2001. B<strndup>() jest "
"zgodna z POSIX.1-2008. B<strdupa>() i  B<strndupa>() są rozszerzeniami GNU."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<alloca>(3), B<calloc>(3), B<free>(3), B<malloc>(3), B<realloc>(3), "
"B<string>(3), B<wcsdup>(3)"
msgstr ""
"B<alloca>(3), B<calloc>(3), B<free>(3), B<malloc>(3), B<realloc>(3), "
"B<string>(3), B<wcsdup>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "STRDUP"
msgstr "STRDUP"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr "6 marca 2019 r."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"
msgstr ""
"B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Od glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Od glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Przed glibc 2.10:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "B<strdupa>(), B<strndupa>(): _GNU_SOURCE"
msgstr "B<strdupa>(), B<strndupa>(): _GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strndup>()  function is similar, but copies at most I<n> bytes.  If "
"I<s> is longer than I<n>, only I<n> bytes are copied, and a terminating null "
"byte (\\(aq\\e0\\(aq) is added."
msgstr ""
"Funkcja B<strndup>() jest podobna, lecz kopiuje co najwyżej I<n> znaków. "
"Jeśli I<s> jest dłuższe niż I<n>, kopiowane jest tylko I<n> znaków i "
"dodawany jest kończący znak null (\\(aq\\e0\\(aq)."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<strdupa>()  and B<strndupa>()  are similar, but use B<alloca>(3)  to "
"allocate the buffer.  They are available only when using the GNU GCC suite, "
"and suffer from the same limitations described in B<alloca>(3)."
msgstr ""
"B<strdupa>() i B<strndupa>() są podobne, ale korzystają z B<alloca>(3) do "
"przydzielania pamięci na bufor. Są one dostępne wyłącznie, gdy używany jest "
"pakiet GNU GCC, i dotyczą ich te same ograniczenia, które opisano w "
"B<alloca>(3)."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success, the B<strdup>()  function returns a pointer to the duplicated "
"string.  It returns NULL if insufficient memory was available, with I<errno> "
"set to indicate the cause of the error."
msgstr ""
"Funkcja B<strdup>() zwraca wskaźnik do skopiowanego łańcucha. Zwraca NULL, "
"gdy nie jest dostępna dostateczna ilość pamięci, i ustawia I<errno>, "
"wskazując na przyczynę błędu."

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<strdup>(),\n"
"B<strndup>(),\n"
"B<strdupa>(),\n"
msgstr ""
"B<strdup>(),\n"
"B<strndup>(),\n"
"B<strdupa>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<strndupa>()"
msgstr "B<strndupa>()"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 5.10 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Linux man-pages 6.02"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Od glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
