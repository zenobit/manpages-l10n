# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-28 19:19+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "GRUB-OFPATHNAME"
msgstr "GRUB-OFPATHNAME"

#. type: TH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Februar 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrasjonsverktøy"

#. type: SH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "grub-ofpathname - find OpenBOOT path for a device"
msgstr ""

#. type: SH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<grub-ofpathname> I<\\,DEVICE\\/>"
msgstr "B<grub-ofpathname> I<\\,ENHET\\/>"

#. type: SH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: SH
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-ofpathname> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-ofpathname> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-ofpathname> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-ofpathname> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<info grub-ofpathname>"
msgstr "B<info grub-ofpathname>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "grub-ofpathname 2.06"
msgstr "grub-ofpathname 2.06"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.06"
msgstr "GRUB2 2.06"
