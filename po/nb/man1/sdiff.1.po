# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-15 19:12+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SDIFF"
msgstr "SDIFF"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "January 2023"
msgstr "Januar 2023"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "diffutils 3.9"
msgstr "diffutils 3.9"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Side-by-side merge of differences between FILE1 and FILE2."
msgid "sdiff - side-by-side merge of file differences"
msgstr "Sammenslåing av forskjeller mellom FIL1 og FIL2, side ved side."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sdiff> [I<OPTION>]... I<FILE1 FILE2>"
msgstr "B<sdiff> [I<VALG>]... I<FIL1 FIL2>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Side-by-side merge of differences between FILE1 and FILE2."
msgstr "Sammenslåing av forskjeller mellom FIL1 og FIL2, side ved side."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenter som er obligatoriske for lange valg, er også obligatoriske for "
"korte valg."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<FILE>"
msgstr "B<-o>, B<--output>=I<FIL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "operate interactively, sending output to FILE"
msgstr "Jobb interaktivt, og send utdata til FIL."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "consider upper- and lower-case to be the same"
msgstr "Ikke skill mellom store og små bokstaver."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>, B<--ignore-tab-expansion>"
msgstr "B<-E>, B<--ignore-tab-expansion>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes due to tab expansion"
msgstr "Ikke regn tabulator-forskjeller som endringer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-Z>, B<--ignore-trailing-space>"
msgstr "B<-Z>, B<--ignore-trailing-space>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore white space at line end"
msgstr "Ignorer blanktegn i slutten av linjer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--ignore-space-change>"
msgstr "B<-b>, B<--ignore-space-change>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes in the amount of white space"
msgstr "Ikke regn ulikt antall blanktegn som endringer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--ignore-all-space>"
msgstr "B<-W>, B<--ignore-all-space>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore all white space"
msgstr "Ignorer alle blanktegn."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--ignore-blank-lines>"
msgstr "B<-B>, B<--ignore-blank-lines>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes whose lines are all blank"
msgstr "Ignorer endringer hvis linjer er fullstendig tomme."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-I>, B<--ignore-matching-lines>=I<RE>"
msgstr "B<-I>, B<--ignore-matching-lines>=I<RE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes all whose lines match RE"
msgstr "Ignorer endringer hvis linjer samsvarer med RE."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-trailing-cr>"
msgstr "B<--strip-trailing-cr>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strip trailing carriage return on input"
msgstr "Fjern etterfølgende linjeskift-tegn fra inndata."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--text>"
msgstr "B<-a>, B<--text>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "treat all files as text"
msgstr "Behandle filer som tekst."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<NUM>"
msgstr "B<-w>, B<--width>=I<ANTALL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output at most NUM (default 130) print columns"
msgstr "Ikke skriv ut flere enn valgt ANTALL kolonner (standard: 130)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--left-column>"
msgstr "B<-l>, B<--left-column>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output only the left column of common lines"
msgstr "Bare skriv ut venstre kolonne med felles linjer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--suppress-common-lines>"
msgstr "B<-s>, B<--suppress-common-lines>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not output common lines"
msgstr "Ikke skriv ut felles linjer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--expand-tabs>"
msgstr "B<-t>, B<--expand-tabs>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "expand tabs to spaces in output"
msgstr "Utvid tabulatorer til mellomrom i utdata."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--tabsize>=I<NUM>"
msgstr "B<--tabsize>=I<NUMMER>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "tab stops at every NUM (default 8) print columns"
msgstr "Tabulatorer slutter ved hvert valgte kolonneNUMMER (standard: 8)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--minimal>"
msgstr "B<-d>, B<--minimal>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "try hard to find a smaller set of changes"
msgstr "Prøv hardt å finne en mindre rekke endringer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--speed-large-files>"
msgstr "B<-H>, B<--speed-large-files>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "assume large files, many scattered small changes"
msgstr "Forvent store filer med små, spredte endringer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--diff-program>=I<PROGRAM>"
msgstr "B<--diff-program>=I<PROGRAM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use PROGRAM to compare files"
msgstr "Bruk valgt PROGRAM til å sammenlikne filer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Vis denne hjelpeteksten og avslutt."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vis versjonsinformasjon og avslutt."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If a FILE is '-', read standard input.  Exit status is 0 if inputs are the "
"same, 1 if different, 2 if trouble."
msgstr ""
"Programmet leser fra standard inndata hvis FIL er «-». Avsluttende status er "
"0 hvis inndata er like, 1 hvis ulike og 2 hvis det oppstår problemer."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "OPPHAVSMANN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Thomas Lord."
msgstr "Skrevet av Thomas Lord."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Report bugs to: bug-diffutils@gnu.org"
msgstr ""
"Rapporter programfeil til: E<lt>bug-diffutils@gnu.orgE<gt>; rapporter "
"oversettelsesfeil til: E<lt>l10n-no@lister.huftis.orgE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"GNU diffutils home page: E<lt>https://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"Nettside for GNU diffutils: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Generell hjelp til bruk av GNU-programvare: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPPHAVSRETT"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er fri programvare. Du kan endre og dele den videre. Det stilles INGEN "
"GARANTI, i den grad dette tillates av gjeldende lovverk."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "cmp(1), diff(1), diff3(1)"
msgid "B<cmp>(1), B<diff>(1), B<diff3>(1)"
msgstr "B<cmp>(1), B<diff>(1), B<diff3>(1)"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The full documentation for B<sdiff> is maintained as a Texinfo manual.  If "
"the B<info> and B<sdiff> programs are properly installed at your site, the "
"command"
msgstr ""
"Den fullstendige dokumentasjonen for B<sdiff> opprettholdes som en Texinfo "
"manual. Dersom B<info> og B<sdiff> programmene er riktig installert på ditt "
"sted burde kommandoen"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<info sdiff>"
msgstr "B<info sdiff>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GNU"
msgstr "SDIFF"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "December 2018"
msgstr "Desember 2018"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "diffutils 3.7"
msgstr "diffutils 3.7"

#. type: Plain text
#: debian-bullseye
#, fuzzy
msgid "GNU sdiff - side-by-side merge of file differences"
msgstr "Sammenslåing av forskjeller mellom FIL1 og FIL2, side ved side."

#. type: Plain text
#: debian-bullseye
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-5
msgid "cmp(1), diff(1), diff3(1)"
msgstr "B<cmp>(1), B<diff>(1), B<diff3>(1)"

#. type: Plain text
#: debian-bullseye
msgid ""
"The full documentation for B<GNU> is maintained as a Texinfo manual.  If the "
"B<info> and B<GNU> programs are properly installed at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<sdiff> opprettholdes som en Texinfo "
"manual. Dersom B<info> og B<sdiff> programmene er riktig installert på ditt "
"sted burde kommandoen"

#. type: Plain text
#: debian-bullseye
msgid "B<info GNU>"
msgstr "B<info sdiff>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2021"
msgstr "August 2021"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "diffutils 3.8"
msgstr "diffutils 3.8"

#. type: Plain text
#: debian-unstable
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "February 2017"
msgid "May 2017"
msgstr "Februar 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "diffutils 3.6"
msgstr "diffutils 3.6"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"GNU diffutils home page: E<lt>http://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"Nettside for GNU diffutils: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>"
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Generell hjelp til bruk av GNU-programvare: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
