# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:04+0100\n"
"PO-Revision-Date: 2022-10-27 21:08+0200\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "success"
msgid "euidaccess"
msgstr "Succès."

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "euidaccess, eaccess - check effective user's permissions for a file"
msgstr ""
"euidaccess, eaccess - Vérifier les permissions utilisateur d'un fichier"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int euidaccess(const char *>I<pathname>B<, int >I<mode>B<);>\n"
"B<int eaccess(const char *>I<pathname>B<, int >I<mode>B<);>\n"
msgstr ""
"B<int euidaccess(const char *>I<pathname>B<, int >I<mode>B<);>\n"
"B<int eaccess(const char *>I<pathname>B<, int >I<mode>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Like B<access>(2), B<euidaccess>()  checks permissions and existence of the "
"file identified by its argument I<pathname>.  However, whereas B<access>(2)  "
"performs checks using the real user and group identifiers of the process, "
"B<euidaccess>()  uses the effective identifiers."
msgstr ""
"Comme B<access>(2), B<euidaccess>() vérifie les permissions et l'existence "
"du fichier identifié par I<pathname>. Cependant, là où B<access>(2) effectue "
"une vérification avec les identifiants réels d'utilisateur et de groupe du "
"processus, B<euidaccess>() utilise les identifiants effectifs."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<mode> is a mask consisting of one or more of B<R_OK>, B<W_OK>, B<X_OK>, "
"and B<F_OK>, with the same meanings as for B<access>(2)."
msgstr ""
"I<mode> est un masque constitué d'un ou plusieurs attributs B<R_OK>, "
"B<W_OK>, B<X_OK> et B<F_OK> qui ont le même sens qu'avec B<access>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<eaccess>()  is a synonym for B<euidaccess>(), provided for compatibility "
"with some other systems."
msgstr ""
"B<eaccess>() est un synonyme de B<euidaccess>(), fourni par compatibilité "
"avec d'autres systèmes."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"On success (all requested permissions granted), zero is returned.  On error "
"(at least one bit in I<mode> asked for a permission that is denied, or some "
"other error occurred), -1 is returned, and I<errno> is set to indicate the "
"error."
msgstr ""
"En cas de succès (toutes les permissions demandées sont autorisées), B<0> "
"est renvoyé. En cas d'erreur (au moins une permission de I<mode> est "
"interdite ou d'autres erreurs se sont produites), B<-1> est renvoyé et "
"I<errno> est positionné pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "As for B<access>(2)."
msgstr "Identique à B<access>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "The B<eaccess>()  function was added to glibc in version 2.4."
msgid "The B<eaccess>()  function was added in glibc 2.4."
msgstr "La fonction B<eaccess>() a été ajoutée à la version 2.4 de la glibc."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<euidaccess>(),\n"
"B<eaccess>()"
msgstr ""
"B<euidaccess>(),\n"
"B<eaccess>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#.  e.g., FreeBSD 6.1.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions are nonstandard.  Some other systems have an B<eaccess>()  "
"function."
msgstr ""
"Ces fonctions ne sont pas standards. Quelques autres systèmes possèdent la "
"fonction B<eaccess>()."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<Warning>: Using this function to check a process's permissions on a file "
"before performing some operation based on that information leads to race "
"conditions: the file permissions may change between the two steps.  "
"Generally, it is safer just to attempt the desired operation and handle any "
"permission error that occurs."
msgstr ""
"I<Attention> : l'utilisation de cette fonction pour vérifier les permissions "
"d'un processus sur un fichier avant d'effectuer quelques opérations basées "
"sur cette information conduit à une situation de compétition (« race "
"condition ») : les permissions du fichier peuvent changer entre les deux "
"étapes. Généralement, il est plus judicieux d'exécuter l'opération souhaitée "
"et de gérer toutes les erreurs de permissions qui se produisent."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This function always dereferences symbolic links.  If you need to check the "
"permissions on a symbolic link, use B<faccessat>(2)  with the flags "
"B<AT_EACCESS> and B<AT_SYMLINK_NOFOLLOW>."
msgstr ""
"Cette fonction déférence toujours les liens symboliques. Si vous devez "
"vérifier les permissions d'un lien symbolique, utilisez B<faccessat>(2) avec "
"les drapeaux B<AT_EACCESS> et B<AT_SYMLINK_NOFOLLOW>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<access>(2), B<chmod>(2), B<chown>(2), B<faccessat>(2), B<open>(2), "
"B<setgid>(2), B<setuid>(2), B<stat>(2), B<credentials>(7), "
"B<path_resolution>(7)"
msgstr ""
"B<access>(2), B<chmod>(2), B<chown>(2), B<faccessat>(2), B<open>(2), "
"B<setgid>(2), B<setuid>(2), B<stat>(2), B<credentials>(7), "
"B<path_resolution>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "EUIDACCESS"
msgstr "EUIDACCESS"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"On success (all requested permissions granted), zero is returned.  On error "
"(at least one bit in I<mode> asked for a permission that is denied, or some "
"other error occurred), -1 is returned, and I<errno> is set appropriately."
msgstr ""
"En cas de succès (toutes les permissions demandées sont autorisées), 0 est "
"renvoyé. En cas d'erreur (au moins une permission de I<mode> est interdite "
"ou d'autres erreurs se sont produites), -1 est renvoyé et I<errno> contient "
"le code d'erreur."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "The B<eaccess>()  function was added to glibc in version 2.4."
msgstr "La fonction B<eaccess>() a été ajoutée à la version 2.4 de la glibc."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
