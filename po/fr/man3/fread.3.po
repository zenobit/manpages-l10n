# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:06+0100\n"
"PO-Revision-Date: 2021-09-29 17:26+0200\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.2.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<read>"
msgid "fread"
msgstr "I<read>"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-12-29"
msgstr "29 décembre 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "fread, fwrite - binary stream input/output"
msgstr "fread, fwrite - Entrées/sorties binaires sur un flux"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<size_t fread(void *restrict >I<ptr>B<, size_t >I<size>B<, size_t >I<nmemb>B<,>\n"
#| "B<             FILE *restrict >I<stream>B<);>\n"
#| "B<size_t fwrite(const void *restrict >I<ptr>B<, size_t >I<size>B<, size_t >I<nmemb>B<,>\n"
#| "B<             FILE *restrict >I<stream>B<);>\n"
msgid ""
"B<size_t fread(void >I<ptr>B<[restrict .>I<size>B< * .>I<nmemb>B<],>\n"
"B<             size_t >I<size>B<, size_t >I<nmemb>B<,>\n"
"B<             FILE *restrict >I<stream>B<);>\n"
"B<size_t fwrite(const void >I<ptr>B<[restrict .>I<size>B< * .>I<nmemb>B<],>\n"
"B<             size_t >I<size>B<, size_t >I<nmemb>B<,>\n"
"B<             FILE *restrict >I<stream>B<);>\n"
msgstr ""
"B<size_t fread(void *restrict >I<ptr>B<, size_t >I<taille>B<, size_t >I<nbelem>B<,>\n"
"B<             FILE *restrict >I<flux>B<);>\n"
"B<size_t fwrite(const void *restrict >I<ptr>B<, size_t >I<taille>B<, size_t >I<nbelem>B<,>\n"
"B<             FILE *restrict >I<flux>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<fread>()  reads I<nmemb> items of data, each I<size> bytes "
"long, from the stream pointed to by I<stream>, storing them at the location "
"given by I<ptr>."
msgstr ""
"La fonction B<fread>() lit I<nbelem> éléments de données de I<taille> octets "
"chacun depuis le flux pointé par I<flux>, et les stocke à l'emplacement "
"pointé par I<ptr>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<fwrite>()  writes I<nmemb> items of data, each I<size> bytes "
"long, to the stream pointed to by I<stream>, obtaining them from the "
"location given by I<ptr>."
msgstr ""
"La fonction B<fwrite>() écrit I<nbelem> éléments de données de I<taille> "
"octets chacun dans le flux pointé par I<flux>, après les avoir récupérés "
"depuis l'emplacement pointé par I<ptr>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For nonlocking counterparts, see B<unlocked_stdio>(3)."
msgstr ""
"Pour des versions de ces fonctions ignorant les verrouillages, voir "
"B<unlocked_stdio>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, B<fread>()  and B<fwrite>()  return the number of items read or "
"written.  This number equals the number of bytes transferred only when "
"I<size> is 1.  If an error occurs, or the end of the file is reached, the "
"return value is a short item count (or zero)."
msgstr ""
"En cas de réussite, B<fread>() et B<fwrite>() renvoient le nombre d'éléments "
"lus ou écrits. Ce nombre n'est égal au nombre d'octets transférés que si "
"I<taille> est égal à 1. Si une erreur se produit, ou si la fin du fichier "
"est atteinte en lecture, le nombre renvoyé est plus petit que I<nbelem> et "
"peut même être nul."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The file position indicator for the stream is advanced by the number of "
"bytes successfully read or written."
msgstr ""
"L'indicateur de position du flux est incrémenté du nombre d'octets ayant été "
"lus ou écrits avec succès."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fread>()  does not distinguish between end-of-file and error, and callers "
"must use B<feof>(3)  and B<ferror>(3)  to determine which occurred."
msgstr ""
"B<fread>() ne fait pas la différence entre la fin de fichier et une erreur, "
"et l'appelant devra utiliser B<feof>(3) et B<ferror>(3) pour le savoir."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fread>(),\n"
"B<fwrite>()"
msgstr ""
"B<fread>(),\n"
"B<fwrite>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<fread>()  by parsing /bin/sh ELF "
"executable in binary mode and printing its magic and class:"
msgstr ""
"Le programme ci-dessous montre l'utilisation de B<fread>() pour parcourir "
"l'exécutable ELF /bin/sh en mode binaire et afficher son code magique et sa "
"classe :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out>\n"
"ELF magic: 0x7f454c46\n"
"Class: 0x02\n"
msgstr ""
"$ B<./a.out>\n"
"ELF magic: 0x7f454c46\n"
"Class: 0x02\n"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))\n"
msgstr "#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    FILE           *fp;\n"
"    size_t         ret;\n"
"    unsigned char  buffer[4];\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(void)\n"
#| "{\n"
#| "    FILE *fp = fopen(\"/bin/sh\", \"rb\");\n"
#| "    if (!fp) {\n"
#| "        perror(\"fopen\");\n"
#| "        return EXIT_FAILURE;\n"
#| "    }\n"
msgid ""
"    fp = fopen(\"/bin/sh\", \"rb\");\n"
"    if (!fp) {\n"
"        perror(\"fopen\");\n"
"        return EXIT_FAILURE;\n"
"    }\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    FILE *fp = fopen(\"/bin/sh\", \"rb\");\n"
"    if (!fp) {\n"
"        perror(\"fopen\");\n"
"        return EXIT_FAILURE;\n"
"    }\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    size_t ret = fread(buffer, sizeof(*buffer), ARRAY_SIZE(buffer), fp);\n"
#| "    if (ret != ARRAY_SIZE(buffer)) {\n"
#| "        fprintf(stderr, \"fread() failed: %zu\\en\", ret);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
msgid ""
"    ret = fread(buffer, sizeof(*buffer), ARRAY_SIZE(buffer), fp);\n"
"    if (ret != ARRAY_SIZE(buffer)) {\n"
"        fprintf(stderr, \"fread() failed: %zu\\en\", ret);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    size_t ret = fread(buffer, sizeof(*buffer), ARRAY_SIZE(buffer), fp);\n"
"    if (ret != ARRAY_SIZE(buffer)) {\n"
"        fprintf(stderr, \"fread() a échoué : %zu\\en\", ret);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"ELF magic: %#04x%02x%02x%02x\\en\", buffer[0], buffer[1],\n"
"           buffer[2], buffer[3]);\n"
msgstr ""
"    printf(\"ELF magic: %#04x%02x%02x%02x\\en\", buffer[0], buffer[1],\n"
"           buffer[2], buffer[3]);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    ret = fread(buffer, 1, 1, fp);\n"
"    if (ret != 1) {\n"
"        fprintf(stderr, \"fread() failed: %zu\\en\", ret);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    ret = fread(buffer, 1, 1, fp);\n"
"    if (ret != 1) {\n"
"        fprintf(stderr, \"fread() a échoué : %zu\\en\", ret);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    printf(\"Class: %#04x\\en\", buffer[0]);\n"
msgstr "    printf(\"Class: %#04x\\en\", buffer[0]);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    fclose(fp);\n"
msgstr "    fclose(fp);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: fread.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<read>(2), B<write>(2), B<feof>(3), B<ferror>(3), B<unlocked_stdio>(3)"
msgstr ""
"B<read>(2), B<write>(2), B<feof>(3), B<ferror>(3), B<unlocked_stdio>(3)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "FREAD"
msgstr "FREAD"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-08-13"
msgstr "13 août 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<size_t fread(void *>I<ptr>B<, size_t >I<size>B<, size_t >I<nmemb>B<, FILE *>I<stream>B<);>\n"
msgstr "B<size_t fread(void *>I<ptr>B<, size_t >I<taille>B<, size_t >I<nbelem>B<, FILE *>I<flux>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid ""
"B<size_t fwrite(const void *>I<ptr>B<, size_t >I<size>B<, size_t >I<nmemb>B<,>\n"
"B<              FILE *>I<stream>B<);>\n"
msgstr ""
"B<size_t fwrite(const void *>I<ptr>B<, size_t >I<taille>B<, size_t >I<nbelem>B<,>\n"
"B<              FILE *>I<flux>B<);>\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008, C89."
msgstr "POSIX.1-2001, POSIX.1-2008, C89."

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    FILE *fp = fopen(\"/bin/sh\", \"rb\");\n"
"    if (!fp) {\n"
"        perror(\"fopen\");\n"
"        return EXIT_FAILURE;\n"
"    }\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    FILE *fp = fopen(\"/bin/sh\", \"rb\");\n"
"    if (!fp) {\n"
"        perror(\"fopen\");\n"
"        return EXIT_FAILURE;\n"
"    }\n"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "    unsigned char buffer[4];\n"
msgstr "    unsigned char buffer[4];\n"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    size_t ret = fread(buffer, ARRAY_SIZE(buffer), sizeof(*buffer), fp);\n"
"    if (ret != sizeof(*buffer)) {\n"
"        fprintf(stderr, \"fread() failed: %zu\\en\", ret);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    size_t ret = fread(buffer, ARRAY_SIZE(buffer), sizeof(*buffer), fp);\n"
"    if (ret != sizeof(*buffer)) {\n"
"        fprintf(stderr, \"fread() a échoué : %zu\\en\", ret);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2015-07-23"
msgstr "23 juillet 2015"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
