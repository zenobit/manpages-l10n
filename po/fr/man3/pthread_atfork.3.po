# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2006.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Denis Barbier <barbier@debian.org>, 2004, 2006, 2010.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# François Micaux, 2002.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2009.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999-2002.
# Thomas Blein <tblein@tblein.eu>, 2011, 2014.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:22+0100\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "pthread_atfork"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid "pthread_atfork - register fork handlers"
msgstr ""
"#-#-#-#-#  pthread_atfork.3.po (glibc)  #-#-#-#-#\n"
"pthread_atfork - Enregistrer des gestionnaires à invoquer lors de l'appel à "
"fork(2)\n"
"#-#-#-#-#  pthread_atfork.3.po (perkamon)  #-#-#-#-#\n"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy, no-wrap
msgid ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)(void),>\n"
"B<                   void (*>I<child>B<)(void));>\n"
msgstr ""
"#-#-#-#-#  pthread_atfork.3.po (glibc)  #-#-#-#-#\n"
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)(void), void (*>I<child>B<)(void));>\n"
"#-#-#-#-#  pthread_atfork.3.po (perkamon)  #-#-#-#-#\n"
"B<int pthread_create(pthread_t *>I<thread>B<, const pthread_attr_t *>I<attr>B<,>\n"
"B<                   void *(*>I<start_routine>B<) (void *), void *>I<arg>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<pthread_atfork>()  function registers fork handlers that are to be "
"executed when B<fork>(2)  is called by any thread in a process.  The "
"handlers are executed in the context of the thread that calls B<fork>(2)."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Three kinds of handler can be registered:"
msgstr ""

#. type: IP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"I<prepare> specifies a handler that is executed in the parent process before "
"B<fork>(2)  processing starts."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<parent> specifies a handler that is executed in the parent process after "
"B<fork>(2)  processing completes."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<child> specifies a handler that is executed in the child process after "
"B<fork>(2)  processing completes."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Any of the three arguments may be NULL if no handler is needed in the "
"corresponding phase of B<fork>(2)  processing."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<pthread_atfork>()  returns zero.  On error, it returns an "
"error number.  B<pthread_atfork>()  may be called multiple times by a "
"process to register additional handlers.  The handlers for each phase are "
"called in a specified order: the I<prepare> handlers are called in reverse "
"order of registration; the I<parent> and I<child> handlers are called in the "
"order of registration."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Could not allocate memory to record the fork handler list entry."
msgstr ""

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"When B<fork>(2)  is called in a multithreaded process, only the calling "
"thread is duplicated in the child process.  The original intention of "
"B<pthread_atfork>()  was to allow the child process to be returned to a "
"consistent state.  For example, at the time of the call to B<fork>(2), other "
"threads may have locked mutexes that are visible in the user-space memory "
"duplicated in the child.  Such mutexes would never be unlocked, since the "
"threads that placed the locks are not duplicated in the child.  The intent "
"of B<pthread_atfork>()  was to provide a mechanism whereby the application "
"(or a library)  could ensure that mutexes and other process and thread state "
"would be restored to a consistent state.  In practice, this task is "
"generally too difficult to be practicable."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"After a B<fork>(2)  in a multithreaded process returns in the child, the "
"child should call only async-signal-safe functions (see B<signal-"
"safety>(7))  until such time as it calls B<execve>(2)  to execute a new "
"program."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"POSIX.1 specifies that B<pthread_atfork>()  shall not fail with the error "
"B<EINTR>."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid "B<fork>(2), B<atexit>(3), B<pthreads>(7)"
msgstr ""
"#-#-#-#-#  pthread_atfork.3.po (glibc)  #-#-#-#-#\n"
"B<fork>(2), B<pthread_mutex_lock>(3), B<pthread_mutex_unlock>(3).\n"
"#-#-#-#-#  pthread_atfork.3.po (perkamon)  #-#-#-#-#\n"
"B<prctl>(2), B<pthread_create>(3), B<pthreads>(7)"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-5
#, no-wrap
msgid "PTHREAD_ATFORK"
msgstr "PTHREAD_ATFORK"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "LinuxThreads"
msgstr "LinuxThreads"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid "pthread_atfork - register handlers to be called at fork(2) time"
msgstr ""
"#-#-#-#-#  pthread_atfork.3.po (glibc)  #-#-#-#-#\n"
"pthread_atfork - Enregistrer des gestionnaires à invoquer lors de l'appel à "
"fork(2)\n"
"#-#-#-#-#  pthread_atfork.3.po (perkamon)  #-#-#-#-#\n"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr "B<#include E<lt>pthread.hE<gt>>"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)"
"(void), void (*>I<child>B<)(void));>"
msgstr ""
"#-#-#-#-#  pthread_atfork.3.po (glibc)  #-#-#-#-#\n"
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)"
"(void), void (*>I<child>B<)(void));>\n"
"#-#-#-#-#  pthread_atfork.3.po (perkamon)  #-#-#-#-#\n"
"B<int pthread_create(pthread_t *>I<thread>B<, const pthread_attr_t "
"*>I<attr>B<,>\n"
"B<                   void *(*>I<start_routine>B<) (void *), void *>I<arg>B<);"
">\n"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<pthread_atfork> registers handler functions to be called just before and "
"just after a new process is created with B<fork>(2). The I<prepare> handler "
"will be called from the parent process, just before the new process is "
"created. The I<parent> handler will be called from the parent process, just "
"before B<fork>(2) returns. The I<child> handler will be called from the "
"child process, just before B<fork>(2) returns."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"One or several of the three handlers I<prepare>, I<parent> and I<child> can "
"be given as B<NULL>, meaning that no handler needs to be called at the "
"corresponding point."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<pthread_atfork> can be called several times to install several sets of "
"handlers. At B<fork>(2) time, the I<prepare> handlers are called in LIFO "
"order (last added with B<pthread_atfork>, first called before B<fork>), "
"while the I<parent> and I<child> handlers are called in FIFO order (first "
"added, first called)."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"To understand the purpose of B<pthread_atfork>, recall that B<fork>(2)  "
"duplicates the whole memory space, including mutexes in their current "
"locking state, but only the calling thread: other threads are not running in "
"the child process.  The mutexes are not usable after the B<fork> and must be "
"initialized with I<pthread_mutex_init> in the child process.  This is a "
"limitation of the current implementation and might or might not be present "
"in future versions."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<pthread_atfork> returns 0 on success and a non-zero error code on error."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "insufficient memory available to register the handlers."
msgstr ""

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
msgid "B<fork>(2), B<pthread_mutex_lock>(3), B<pthread_mutex_unlock>(3)."
msgstr ""
"#-#-#-#-#  pthread_atfork.3.po (glibc)  #-#-#-#-#\n"
"B<fork>(2), B<pthread_mutex_lock>(3), B<pthread_mutex_unlock>(3).\n"
"#-#-#-#-#  pthread_atfork.3.po (perkamon)  #-#-#-#-#\n"
"B<prctl>(2), B<pthread_create>(3), B<pthreads>(7)"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Link with I<-pthread>."
msgstr "Effectuez l'édition des liens avec l'option B<-pthread>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<pthread_atfork>()  function registers fork handlers that are to be "
"executed when B<fork>(2)  is called by this thread.  The handlers are "
"executed in the context of the thread that calls B<fork>(2)."
msgstr ""

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "*"
msgstr "–"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"I<prepare> specifies a handler that is executed before B<fork>(2)  "
"processing starts."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, B<pthread_atfork>()  returns zero.  On error, it returns an "
"error number.  B<pthread_atfork>()  may be called multiple times by a "
"thread, to register multiple handlers for each phase.  The handlers for each "
"phase are called in a specified order: the I<prepare> handlers are called in "
"reverse order of registration; the I<parent> and I<child> handlers are "
"called in the order of registration."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Could not allocate memory to record the form handler entry."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"When B<fork>(2)  is called in a multithreaded process, only the calling "
"thread is duplicated in the child process.  The original intention of "
"B<pthread_atfork>()  was to allow the calling thread to be returned to a "
"consistent state.  For example, at the time of the call to B<fork>(2), other "
"threads may have locked mutexes that are visible in the user-space memory "
"duplicated in the child.  Such mutexes would never be unlocked, since the "
"threads that placed the locks are not duplicated in the child.  The intent "
"of B<pthread_atfork>()  was to provide a mechanism whereby the application "
"(or a library)  could ensure that mutexes and other process and thread state "
"would be restored to a consistent state.  In practice, this task is "
"generally too difficult to be practicable."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"

#. type: IP
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"
