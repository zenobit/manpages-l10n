# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 19:59+0100\n"
"PO-Revision-Date: 2022-11-14 18:49+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.4.2\n"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "basename"
msgstr "basename"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "basename, dirname - parse pathname components"
msgstr "basename, dirname - Analyser les composants d'un chemin d'accès"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>libgen.hE<gt>>\n"
msgstr "B<#include E<lt>libgen.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *dirname(char *>I<path>B<);>\n"
"B<char *basename(char *>I<path>B<);>\n"
msgstr ""
"B<char *dirname(char *>I<chemin>B<);>\n"
"B<char *basename(char *>I<chemin>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Warning: there are two different functions B<basename>(); see below."
msgstr ""
"Attention\\ : il existe deux fonctions B<basename>() différentes\\ ; voir ci-"
"dessous."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "The functions B<dirname>()  and B<basename>()  break a null-terminated "
#| "pathname string into directory and filename components.  In the usual "
#| "case, B<dirname>()  returns the string up to, but not including, the "
#| "final \\(aq/\\(aq, and B<basename>()  returns the component following the "
#| "final \\(aq/\\(aq.  Trailing \\(aq/\\(aq characters are not counted as "
#| "part of the pathname."
msgid ""
"The functions B<dirname>()  and B<basename>()  break a null-terminated "
"pathname string into directory and filename components.  In the usual case, "
"B<dirname>()  returns the string up to, but not including, the final \\[aq]/"
"\\[aq], and B<basename>()  returns the component following the final \\[aq]/"
"\\[aq].  Trailing \\[aq]/\\[aq] characters are not counted as part of the "
"pathname."
msgstr ""
"Les fonctions B<dirname>() et B<basename>() décomposent un chemin d'accès, "
"représenté sous la forme d'une chaîne terminée par un caractère NULL, en ses "
"composants répertoire et nom de fichier. En général, B<dirname>() renvoie la "
"chaîne s'étendant jusqu'au dernier «\\ /\\ », sans l'inclure, et "
"B<basename>() renvoie la partie se trouvant après le dernier «\\ /\\ ». Les "
"caractères «\\ /\\ » en fin de chemin n'en font pas partie."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<path> does not contain a slash, B<dirname>()  returns the string \".\" "
"while B<basename>()  returns a copy of I<path>.  If I<path> is the string \"/"
"\", then both B<dirname>()  and B<basename>()  return the string \"/\".  If "
"I<path> is a null pointer or points to an empty string, then both "
"B<dirname>()  and B<basename>()  return the string \".\"."
msgstr ""
"Si I<chemin> ne contient pas de barre oblique, B<dirname>() renvoie la "
"chaîne «\\ .\\ » et B<basename>() renvoie une copie de la chaîne I<chemin>. "
"Si I<chemin> correspond à la chaîne «\\ /\\ », alors B<dirname>() et "
"B<basename>() renvoient toutes deux la chaîne «\\ /\\ ». Si I<chemin> est un "
"pointeur NULL ou pointe vers une chaîne vide, alors B<dirname>() et "
"B<basename>() renvoient toutes deux la chaîne «\\ .\\ »."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Concatenating the string returned by B<dirname>(), a \"/\", and the string "
"returned by B<basename>()  yields a complete pathname."
msgstr ""
"En mettant bout à bout la chaîne renvoyée par B<dirname>(), un «\\ /\\ » et "
"la chaîne renvoyée par B<basename>(), on obtient un chemin d'accès complet."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Both B<dirname>()  and B<basename>()  may modify the contents of I<path>, so "
"it may be desirable to pass a copy when calling one of these functions."
msgstr ""
"B<dirname>() et B<basename>() peuvent toutes deux modifier le contenu de "
"I<chemin> ; il est donc souhaitable de passer une copie de celui-ci lors "
"d'un appel à l'une de ces fonctions."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions may return pointers to statically allocated memory which may "
"be overwritten by subsequent calls.  Alternatively, they may return a "
"pointer to some part of I<path>, so that the string referred to by I<path> "
"should not be modified or freed until the pointer returned by the function "
"is no longer required."
msgstr ""
"Ces fonctions peuvent renvoyer des pointeurs vers de la mémoire allouée "
"statiquement qui peut être écrasée par des appels ultérieurs. Elles peuvent "
"aussi renvoyer un pointeur vers une partie de I<chemin>, de façon à ce que "
"la chaîne référencée par I<chemin> ne puisse être modifiée ou libérée que "
"lorsque le pointeur renvoyé par la fonction ne sera plus nécessaire."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following list of examples (taken from SUSv2)  shows the strings "
"returned by B<dirname>()  and B<basename>()  for different paths:"
msgstr ""
"La liste d'exemples suivante (prise dans SUSv2) montre les chaînes renvoyées "
"par B<dirname>() et B<basename>() pour différents chemins d'accès\\ :"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "path    "
msgstr "chemin    "

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "dirname"
msgstr "dirname"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/usr/lib"
msgstr "/usr/lib"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/usr"
msgstr "/usr"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "lib"
msgstr "lib"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/usr/   "
msgstr "/usr/   "

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/"
msgstr "/"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "usr"
msgstr "usr"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "usr     "
msgstr "usr     "

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "."
msgstr "."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/       "
msgstr "/       "

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\&.       "
msgstr "\\&.       "

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\&..      "
msgstr "\\&..      "

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ".."
msgstr ".."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Both B<dirname>()  and B<basename>()  return pointers to null-terminated "
"strings.  (Do not pass these pointers to B<free>(3).)"
msgstr ""
"Les fonctions B<dirname>() et B<basename>() renvoient des pointeurs sur des "
"chaînes terminées par un caractère NULL (ne pas passer ces pointeurs à "
"B<free>(3))."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<basename>(),\n"
"B<dirname>()"
msgstr ""
"B<basename>(),\n"
"B<dirname>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"There are two different versions of B<basename>()  - the POSIX version "
"described above, and the GNU version, which one gets after"
msgstr ""
"Il existe deux versions différentes de B<basename>()\\ : la version POSIX "
"décrite précédemment et la version GNU que l'on utilise avec"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<    #define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>string.hE<gt>>\n"
msgstr ""
"B<    #define _GNU_SOURCE>         /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The GNU version never modifies its argument, and returns the empty string "
"when I<path> has a trailing slash, and in particular also when it is \"/\".  "
"There is no GNU version of B<dirname>()."
msgstr ""
"La version GNU ne modifie jamais son argument et renvoie une chaîne vide "
"lorsque I<chemin> se termine par une barre oblique «\\ /\\ », et en "
"particulier aussi lorsqu'il vaut «\\ /\\ ». Il n'y a pas de version GNU de "
"B<dirname>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"With glibc, one gets the POSIX version of B<basename>()  when I<E<lt>libgen."
"hE<gt>> is included, and the GNU version otherwise."
msgstr ""
"Avec la glibc, on utilise la version POSIX de B<basename>() lorsque "
"I<E<lt>libgen.hE<gt>> est inclus et la version GNU dans le cas contraire."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the glibc implementation, the POSIX versions of these functions modify "
"the I<path> argument, and segfault when called with a static string such as "
"\"/usr/\"."
msgstr ""
"Dans l'implémentation de la glibc, les versions POSIX de ces fonctions "
"modifient l'argument I<chemin> et génèrent une erreur de segmentation "
"lorsqu'elles sont appelées avec une chaîne statique comme « /usr/ »."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Before glibc 2.2.1, the glibc version of B<dirname>()  did not correctly "
#| "handle pathnames with trailing \\(aq/\\(aq characters, and generated a "
#| "segfault if given a NULL argument."
msgid ""
"Before glibc 2.2.1, the glibc version of B<dirname>()  did not correctly "
"handle pathnames with trailing \\[aq]/\\[aq] characters, and generated a "
"segfault if given a NULL argument."
msgstr ""
"Avant la glibc\\ 2.2.1, la version de la glibc de la fonction B<dirname>() "
"ne gérait pas correctement les chemins se terminant par un caractère «\\ /\\ "
"» et générait une erreur de segmentation lorsqu'on lui passait un pointeur "
"NULL comme argument."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following code snippet demonstrates the use of B<basename>()  and "
"B<dirname>():"
msgstr ""
"L'extrait de code suivant montre l'utilisation de B<basename>() et "
"B<dirname>() :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"char *dirc, *basec, *bname, *dname;\n"
"char *path = \"/etc/passwd\";\n"
msgstr ""
"char *copie_rep, *copie_base, *nom_base, *nom_rep;\n"
"char *chemin = \"/etc/passwd\";\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"dirc = strdup(path);\n"
"basec = strdup(path);\n"
"dname = dirname(dirc);\n"
"bname = basename(basec);\n"
"printf(\"dirname=%s, basename=%s\\en\", dname, bname);\n"
msgstr ""
"copie_rep = strdup(chemin);\n"
"copie_base = strdup(chemin);\n"
"nom_rep = dirname(copie_rep);\n"
"nom_base = basename(copie_base);\n"
"printf(\"Nom répertoire = %s, Nom base = %s\\en\", nom_rep, nom_base);\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<basename>(1), B<dirname>(1)"
msgstr "B<basename>(1), B<dirname>(1)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "BASENAME"
msgstr "BASENAME"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 juin 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *dirname(char *>I<path>B<);>\n"
msgstr "B<char *dirname(char *>I<chemin>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<char *basename(char *>I<path>B<);>\n"
msgstr "B<char *basename(char *>I<chemin>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Warning: there are two different functions B<basename>()  - see below."
msgstr ""
"Attention\\ : il existe deux fonctions B<basename>() différentes – voir ci-"
"dessous."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The functions B<dirname>()  and B<basename>()  break a null-terminated "
"pathname string into directory and filename components.  In the usual case, "
"B<dirname>()  returns the string up to, but not including, the final \\(aq/"
"\\(aq, and B<basename>()  returns the component following the final \\(aq/"
"\\(aq.  Trailing \\(aq/\\(aq characters are not counted as part of the "
"pathname."
msgstr ""
"Les fonctions B<dirname>() et B<basename>() décomposent un chemin d'accès, "
"représenté sous la forme d'une chaîne terminée par un caractère NULL, en ses "
"composants répertoire et nom de fichier. En général, B<dirname>() renvoie la "
"chaîne s'étendant jusqu'au dernier «\\ /\\ », sans l'inclure, et "
"B<basename>() renvoie la partie se trouvant après le dernier «\\ /\\ ». Les "
"caractères «\\ /\\ » en fin de chemin n'en font pas partie."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Before glibc 2.2.1, the glibc version of B<dirname>()  did not correctly "
"handle pathnames with trailing \\(aq/\\(aq characters, and generated a "
"segfault if given a NULL argument."
msgstr ""
"Avant la glibc\\ 2.2.1, la version de la glibc de la fonction B<dirname>() "
"ne gérait pas correctement les chemins se terminant par un caractère «\\ /\\ "
"» et générait une erreur de segmentation lorsqu'on lui passait un pointeur "
"NULL comme argument."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
