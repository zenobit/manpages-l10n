# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: 2023-03-05 00:17+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ioctl_iflags"
msgstr "ioctl_iflags"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ioctl_iflags - ioctl() operations for inode flags"
msgstr "ioctl_iflags - opérations ioctl() pour les drapeaux d'inœud"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Various Linux filesystems support the notion of I<inode "
"flags>\\[em]attributes that modify the semantics of files and directories.  "
"These flags can be retrieved and modified using two B<ioctl>(2)  operations:"
msgstr ""
"Divers systèmes de fichiers sous Linux prennent en charge la notion de "
"I<drapeaux d'inœud> – des attributs qui modifient la sémantique des fichiers "
"et des répertoires. Ces attributs peuvent être récupérés et modifiés en "
"utilisant deux opérations B<ioctl>(2) :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int attr;\n"
"fd = open(\"pathname\", ...);\n"
msgstr ""
"int attr;\n"
"fd = open(\"pathname\", ...);\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Place current flags\n"
"                                       in \\[aq]attr\\[aq] */\n"
"attr |= FS_NOATIME_FL;              /* Tweak returned bit mask */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Update flags for inode\n"
"                                       referred to by \\[aq]fd\\[aq] */\n"
msgstr ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Positionner les drapeaux actuels\n"
"                                       dans \\[aq]attr\\[aq] */\n"
"attr |= FS_NOATIME_FL;              /* Modifier le masque de bits renvoyé */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Mettre à jour les drapeaux pour\n"
"                                       l'inœud auquel renvoie \\[aq]fd\\[aq] */\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<lsattr>(1)  and B<chattr>(1)  shell commands provide interfaces to "
"these two operations, allowing a user to view and modify the inode flags "
"associated with a file."
msgstr ""
"Les commandes B<lsattr>(1) et B<chattr>(1) fournissent des  interfaces avec "
"ces deux opérations, permettant à l'utilisateur de voir et de modifier les "
"drapeaux d'inœud associés à un fichier."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following flags are supported (shown along with the corresponding letter "
"used to indicate the flag by B<lsattr>(1)  and B<chattr>(1)):"
msgstr ""
"Les drapeaux suivants sont pris en charge (présentés ci-dessous avec la "
"lettre correspondante à utiliser pour indiquer le drapeau avec B<lsattr>(1) "
"et B<chattr>(1)) :"

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_APPEND_FL> \\[aq]a\\[aq]"
msgstr "B<FS_APPEND_FL> \\[aq]a\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file can be opened only with the B<O_APPEND> flag.  (This restriction "
"applies even to the superuser.)  Only a privileged process "
"(B<CAP_LINUX_IMMUTABLE>)  can set or clear this attribute."
msgstr ""
"Le fichier ne peut être ouvert qu'avec le drapeau B<O_APPEND> (cette "
"restriction s'applique même au superutilisateur). Seul un processus "
"privilégié (B<CAP_LINUX_IMMUTABLE>) peut positionner ou effacer l'attribut."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_COMPR_FL> \\[aq]c\\[aq]"
msgstr "B<FS_COMPR_FL> \\[aq]c\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Store the file in a compressed format on disk.  This flag is I<not> "
"supported by most of the mainstream filesystem implementations; one "
"exception is B<btrfs>(5)."
msgstr ""
"Stocker le fichier dans un format compressé sur le disque. Cet attribut "
"I<n'est pas> géré par la plupart des implémentations de systèmes de fichiers "
"en vogue, sauf B<btrfs>(5)."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_DIRSYNC_FL> \\[aq]D\\[aq] (since Linux 2.6.0)"
msgstr "B<FS_DIRSYNC_FL> \\[aq]D\\[aq] (depuis Linux 2.6.0)"

#. #-#-#-#-#  archlinux: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \[aq]e\[aq]"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \(aqe\(aq"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \[aq]e\[aq]"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  fedora-38: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \[aq]e\[aq]"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \[aq]e\[aq]"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \[aq]e\[aq]"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \(aqe\(aq"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_EXTENT_FL " \(aqe\(aq"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Write directory changes synchronously to disk.  This flag provides semantics "
"equivalent to the B<mount>(2)  B<MS_DIRSYNC> option, but on a per-directory "
"basis.  This flag can be applied only to directories."
msgstr ""
"Écrire les modifications de répertoire de manière synchronisée sur le "
"disque. Ce drapeau fournit une sémantique équivalente à l'option "
"B<MS_DIRSYNC> de B<mount>(2), mais sur une base individuelle à un "
"répertoire. Ce drapeau ne peut être appliqué qu'à des répertoires."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_IMMUTABLE_FL> \\[aq]i\\[aq]"
msgstr "B<FS_IMMUTABLE_FL> \\[aq]i\\[aq]"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The file is immutable: no changes are permitted to the file contents or "
"metadata (permissions, timestamps, ownership, link count, and so on).  (This "
"restriction applies even to the superuser.)  Only a privileged process "
"(B<CAP_LINUX_IMMUTABLE>)  can set or clear this attribute."
msgstr ""
"Le fichier est immuable : aucune modification du contenu ou des métadonnées "
"du fichier n'est autorisée (droits, horodatage, propriété, nombre de liens "
"et ainsi de suite) (cette restriction s'applique même au superutilisateur). "
"Seul un processus privilégié (B<CAP_LINUX_IMMUTABLE>) peut positionner ou "
"effacer cet attribut."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_JOURNAL_DATA_FL> \\[aq]j\\[aq]"
msgstr "B<FS_JOURNAL_DATA_FL> \\[aq]j\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Enable journaling of file data on B<ext3>(5)  and B<ext4>(5)  filesystems.  "
"On a filesystem that is journaling in I<ordered> or I<writeback> mode, a "
"privileged (B<CAP_SYS_RESOURCE>)  process can set this flag to enable "
"journaling of data updates on a per-file basis."
msgstr ""
"Activer la journalisation des fichiers sur les systèmes de fichiers "
"B<ext3>(5) et B<ext4>(5). Sur un système de fichiers effectuant une "
"journalisation en mode I<ordered> ou I<writeback>, un processus privilégié "
"(B<CAP_SYS_RESOURCE>) peut positionner ce drapeau pour activer la "
"journalisation des mises à jour des données sur une base individuelle à un "
"fichier."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_NOATIME_FL> \\[aq]A\\[aq]"
msgstr "B<FS_NOATIME_FL> \\[aq]A\\[aq]"

#. #-#-#-#-#  archlinux: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \[aq]\[aq]"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \(aq\(aq"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \[aq]\[aq]"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  fedora-38: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \[aq]\[aq]"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \[aq]\[aq]"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \[aq]\[aq]"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \(aq\(aq"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ioctl_iflags.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .TP
#.  .BR FS_NOCOMP_FL " \(aq\(aq"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Don't update the file last access time when the file is accessed.  This can "
"provide I/O performance benefits for applications that do not care about the "
"accuracy of this timestamp.  This flag provides functionality similar to the "
"B<mount>(2)  B<MS_NOATIME> flag, but on a per-file basis."
msgstr ""
"Ne pas mettre à jour la dernière date d'accès du fichier lors d'un accès au "
"fichier. Cela peut apporter de meilleures performances d'E/S pour des "
"applications qui n'ont pas besoin d'un horodatage rigoureux. Ce drapeau "
"fournit une fonctionnalité identique au drapeau B<MS_NOATIME> de "
"B<mount>(2), mais sur une base individuelle à un fichier."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_NOCOW_FL> \\[aq]C\\[aq] (since Linux 2.6.39)"
msgstr "B<FS_NOCOW_FL> \\[aq]C\\[aq] (depuis Linux 2.6.39)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file will not be subject to copy-on-write updates.  This flag has an "
"effect only on filesystems that support copy-on-write semantics, such as "
"Btrfs.  See B<chattr>(1)  and B<btrfs>(5)."
msgstr ""
"Le fichier ne sera pas sujet aux mises à jour de copie sur écriture. Ce "
"drapeau n'a d'effet que sur les systèmes de fichiers gérant la sémantique de "
"copie sur écriture tels que Btrfs. Voir B<chattr>(1) et B<btrfs>(5)."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_NODUMP_FL> \\[aq]d\\[aq]"
msgstr "B<FS_NODUMP_FL> \\[aq]d\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Don't include this file in backups made using B<dump>(8)."
msgstr "Ne pas inclure ce fichier dans les sauvegardes faites avec B<dump>(8)."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_NOTAIL_FL> \\[aq]t\\[aq]"
msgstr "B<FS_NOTAIL_FL> \\[aq]t\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This flag is supported only on Reiserfs.  It disables the Reiserfs tail-"
"packing feature, which tries to pack small files (and the final fragment of "
"larger files)  into the same disk block as the file metadata."
msgstr ""
"Ce drapeau n'est pris en charge que par ReiserFS. Il désactive la "
"fonctionnalité tail-packing de ReiserFS, qui essaie d'empaqueter de petits "
"fichiers (et le fragment de fin de fichiers plus gros) dans le même bloc de "
"disque que les métadonnées du fichier."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_PROJINHERIT_FL> \\[aq]P\\[aq] (since Linux 4.5)"
msgstr "B<FS_PROJINHERIT_FL> \\[aq]P\\[aq] (depuis Linux 4.5)"

#.  commit 040cb3786d9b25293b8b0b05b90da0f871e1eb9b
#.  Flag name was added in Linux 4.4
#.  FIXME Not currently supported because not in FS_FL_USER_MODIFIABLE?
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Inherit the quota project ID.  Files and subdirectories will inherit the "
"project ID of the directory.  This flag can be applied only to directories."
msgstr ""
"Hériter du quota de l'identifiant du projet. Les fichiers et les sous-"
"répertoires hériteront de l'identifiant du projet du répertoire. Ce drapeau "
"ne peut être appliqué qu'à des répertoires."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_SECRM_FL> \\[aq]s\\[aq]"
msgstr "B<FS_SECRM_FL> \\[aq]s\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mark the file for secure deletion.  This feature is not implemented by any "
"filesystem, since the task of securely erasing a file from a recording "
"medium is surprisingly difficult."
msgstr ""
"Marquer le fichier comme faisant partie des suppressions sécurisées. Cette "
"fonctionnalité n'est implémentée par aucun système de fichiers, puisque "
"l'effacement sécurisé d'un fichier d’un média d'enregistrement est "
"étonnamment difficile."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_SYNC_FL> \\[aq]S\\[aq]"
msgstr "B<FS_SYNC_FL> \\[aq]S\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Make file updates synchronous.  For files, this makes all writes synchronous "
"(as though all opens of the file were with the B<O_SYNC> flag).  For "
"directories, this has the same effect as the B<FS_DIRSYNC_FL> flag."
msgstr ""
"Marquer un fichier comme pouvant faire l'objet de synchronisation de mises à "
"jour. Pour les fichiers, cela rend synchrones toutes les écritures (comme "
"l’étaient toutes les ouvertures de fichier avec le drapeau B<O_SYNC>). Pour "
"les répertoires, cela a le même effet que le drapeau B<FS_DIRSYNC_FL>."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_TOPDIR_FL> \\[aq]T\\[aq]"
msgstr "B<FS_TOPDIR_FL> \\[aq]T\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mark a directory for special treatment under the Orlov block-allocation "
"strategy.  See B<chattr>(1)  for details.  This flag can be applied only to "
"directories and has an effect only for ext2, ext3, and ext4."
msgstr ""
"Marquer le fichier comme devant faire l'objet d'un traitement spécial dans "
"le cadre de la stratégie d'allocation de blocs Orlov. Voir B<chattr>(1) pour "
"les détails. Ce drapeau ne peut être appliqué qu'à des répertoires et il n'a "
"d'effet que sur ext2, ext3 et ext4."

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<FS_UNRM_FL> \\[aq]u\\[aq]"
msgstr "B<FS_UNRM_FL> \\[aq]u\\[aq]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Allow the file to be undeleted if it is deleted.  This feature is not "
"implemented by any filesystem, since it is possible to implement file-"
"recovery mechanisms outside the kernel."
msgstr ""
"Permettre au fichier d'être restauré s'il est effacé. Cette fonctionnalité "
"n'est implémentée par aucun système de fichiers, car il est possible "
"d'implémenter des mécanismes de restauration de fichiers en dehors du noyau."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In most cases, when any of the above flags is set on a directory, the flag "
"is inherited by files and subdirectories created inside that directory.  "
"Exceptions include B<FS_TOPDIR_FL>, which is not inheritable, and "
"B<FS_DIRSYNC_FL>, which is inherited only by subdirectories."
msgstr ""
"Dans la plupart des cas, lorsqu'un des drapeaux ci-dessus est positionné sur "
"un répertoire, le drapeau est transmis aux fichiers et aux sous-répertoires "
"créés dans ce répertoire. Les exceptions sont B<FS_TOPDIR_FL>, qui ne peut "
"pas être héritier, et B<FS_DIRSYNC_FL>, dont seuls les sous-répertoires "
"peuvent hériter."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Inode flags are a nonstandard Linux extension."
msgstr "Les drapeaux d'inœuds sont une extension non standard de Linux."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In order to change the inode flags of a file using the B<FS_IOC_SETFLAGS> "
"operation, the effective user ID of the caller must match the owner of the "
"file, or the caller must have the B<CAP_FOWNER> capability."
msgstr ""
"Pour modifier les attributs de l'inœud d'un fichier en utilisant l'opération "
"B<FS_IOC_SETFLAGS>, l'UID effectif du processus appelant doit correspondre à "
"celui du propriétaire du fichier, ou l’appelant doit avoir la capacité "
"B<CAP_FOWNER>."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The type of the argument given to the B<FS_IOC_GETFLAGS> and "
"B<FS_IOC_SETFLAGS> operations is I<int\\~*>, notwithstanding the implication "
"in the kernel source file I<include/uapi/linux/fs.h> that the argument is "
"I<long\\~*>."
msgstr ""
"Le type du paramètre donné aux opérations B<FS_IOC_GETFLAGS> et "
"B<FS_IOC_SETFLAGS> est I<int\\~*>, malgré le fait que dans le fichier source "
"I<include/uapi/linux/fs.h> du noyau le paramètre soit I<long\\~*>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<chattr>(1), B<lsattr>(1), B<mount>(2), B<btrfs>(5), B<ext4>(5), B<xfs>(5), "
"B<xattr>(7), B<mount>(8)"
msgstr ""
"B<chattr>(1), B<lsattr>(1), B<mount>(2), B<btrfs>(5), B<ext4>(5), B<xfs>(5), "
"B<xattr>(7), B<mount>(8)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "IOCTL_IFLAGS"
msgstr "IOCTL_IFLAGS"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-11-19"
msgstr "19 novembre 2019"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Various Linux filesystems support the notion of I<inode "
"flags>\\(emattributes that modify the semantics of files and directories.  "
"These flags can be retrieved and modified using two B<ioctl>(2)  operations:"
msgstr ""
"Divers systèmes de fichiers sous Linux prennent en charge la notion de "
"I<drapeaux d'inœud> – des attributs qui modifient la sémantique des fichiers "
"et des répertoires. Ces attributs peuvent être récupérés et modifiés en "
"utilisant deux opérations B<ioctl>(2) :"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Place current flags\n"
"                                       in \\(aqattr\\(aq */\n"
"attr |= FS_NOATIME_FL;              /* Tweak returned bit mask */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Update flags for inode\n"
"                                       referred to by \\(aqfd\\(aq */\n"
msgstr ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Positionner les drapeaux actuels\n"
"                                       dans \\(aqattr\\(aq */\n"
"attr |= FS_NOATIME_FL;              /* Modifier le masque de bits renvoyé */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Mettre à jour les drapeaux pour\n"
"                                       l'inœud auquel renvoie \\(aqfd\\(aq */\n"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_APPEND_FL> \\(aqa\\(aq"
msgstr "B<FS_APPEND_FL> \\(aqa\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_COMPR_FL> \\(aqc\\(aq"
msgstr "B<FS_COMPR_FL> \\(aqc\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_DIRSYNC_FL> \\(aqD\\(aq (since Linux 2.6.0)"
msgstr "B<FS_DIRSYNC_FL> \\(aqD\\(aq (depuis Linux 2.6.0)"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_IMMUTABLE_FL> \\(aqi\\(aq"
msgstr "B<FS_IMMUTABLE_FL> \\(aqi\\(aq"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"The file is immutable: no changes are permitted to the file contents or "
"metadata (permissions, timestamps, ownership, link count and so on).  (This "
"restriction applies even to the superuser.)  Only a privileged process "
"(B<CAP_LINUX_IMMUTABLE>)  can set or clear this attribute."
msgstr ""
"Le fichier est immuable : aucune modification du contenu ou des métadonnées "
"du fichier n'est autorisée (droits, horodatage, propriété, nombre de liens "
"et ainsi de suite) (cette restriction s'applique même au superutilisateur). "
"Seul un processus privilégié (B<CAP_LINUX_IMMUTABLE>) peut positionner ou "
"effacer cet attribut."

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_JOURNAL_DATA_FL> \\(aqj\\(aq"
msgstr "B<FS_JOURNAL_DATA_FL> \\(aqj\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_NOATIME_FL> \\(aqA\\(aq"
msgstr "B<FS_NOATIME_FL> \\(aqA\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_NOCOW_FL> \\(aqC\\(aq (since Linux 2.6.39)"
msgstr "B<FS_NOCOW_FL> \\(aqC\\(aq (depuis Linux 2.6.39)"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_NODUMP_FL> \\(aqd\\(aq"
msgstr "B<FS_NODUMP_FL> \\(aqd\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_NOTAIL_FL> \\(aqt\\(aq"
msgstr "B<FS_NOTAIL_FL> \\(aqt\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_PROJINHERIT_FL> \\(aqP\\(aq (since Linux 4.5)"
msgstr "B<FS_PROJINHERIT_FL> \\(aqP\\(aq (depuis Linux 4.5)"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_SECRM_FL> \\(aqs\\(aq"
msgstr "B<FS_SECRM_FL> \\(aqs\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_SYNC_FL> \\(aqS\\(aq"
msgstr "B<FS_SYNC_FL> \\(aqS\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_TOPDIR_FL> \\(aqT\\(aq"
msgstr "B<FS_TOPDIR_FL> \\(aqT\\(aq"

#. type: TP
#: debian-bullseye opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FS_UNRM_FL> \\(aqu\\(aq"
msgstr "B<FS_UNRM_FL> \\(aqu\\(aq"

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: debian-bullseye
msgid ""
"The type of the argument given to the B<FS_IOC_GETFLAGS> and "
"B<FS_IOC_SETFLAGS> operations is I<int\\ *>, notwithstanding the implication "
"in the kernel source file I<include/uapi/linux/fs.h> that the argument is "
"I<long\\ *>."
msgstr ""
"Le type du paramètre donné aux opérations B<FS_IOC_GETFLAGS> et "
"B<FS_IOC_SETFLAGS> est I<int\\ *>, malgré le fait que dans le fichier source "
"I<include/uapi/linux/fs.h> du noyau le paramètre soit I<long\\ *>."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
