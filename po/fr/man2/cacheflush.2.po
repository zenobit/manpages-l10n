# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-02-20 20:00+0100\n"
"PO-Revision-Date: 2023-02-22 10:01+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "cacheflush"
msgstr "cacheflush"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "cacheflush - flush contents of instruction and/or data cache"
msgstr "cacheflush - Vider le contenu des mémoires caches"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/cachectl.hE<gt>>\n"
msgstr "B<#include E<lt>sys/cachectl.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int cacheflush(void >I<addr>B<[.>I<nbytes>B<], int >I<nbytes>B<, int >I<cache>B<);>\n"
msgstr "B<int cacheflush(void >I<addr>B<[.>I<nbytes>B<], int >I<nbytes>B<, int >I<cache>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<Note>: On some architectures, there is no glibc wrapper for this system "
"call; see NOTES."
msgstr ""
"I<Note> : sur certaines architectures, il n'existe pas d'enveloppe glibc "
"pour cet appel système ; voir NOTES."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<cacheflush>()  flushes the contents of the indicated cache(s) for the user "
"addresses in the range I<addr> to I<(addr+nbytes-1)>.  I<cache> may be one "
"of:"
msgstr ""
"B<cacheflush>() vide le contenu des mémoires caches indiquées de l'espace "
"d'adressage utilisateur compris entre I<addr> et I<(addr+nbytes-1)>. La "
"mémoire I<cache> est l'une des suivantes\\ :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ICACHE>"
msgstr "B<ICACHE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Flush the instruction cache."
msgstr "Vider le cache d'instructions."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<DCACHE>"
msgstr "B<DCACHE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Write back to memory and invalidate the affected valid cache lines."
msgstr "Réécrire le cache en mémoire et invalider le cache concerné."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<BCACHE>"
msgstr "B<BCACHE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Same as B<(ICACHE|DCACHE)>."
msgstr "Identique à B<(ICACHE|DCACHE)>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<cacheflush>()  returns 0 on success.  On error, it returns -1 and sets "
"I<errno> to indicate the error."
msgstr ""
"B<cacheflush>() renvoie B<0> s'il réussit. En cas d'erreur, il renvoie B<-1> "
"et il positionne I<errno> pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Some or all of the address range I<addr> to I<(addr+nbytes-1)> is not "
"accessible."
msgstr ""
"Une partie de l'espace d'adressage entre I<addr> et I<(addr+nbytes-1)> n'est "
"pas accessible."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<cache> is not one of B<ICACHE>, B<DCACHE>, or B<BCACHE> (but see BUGS)."
msgstr ""
"Le paramètre I<cache> est différent de B<ICACHE>, B<DCACHE> ou B<BCACHE> "
"(mais voir BOGUES)."

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Historically, this system call was available on all MIPS UNIX variants "
"including RISC/os, IRIX, Ultrix, NetBSD, OpenBSD, and FreeBSD (and also on "
"some non-UNIX MIPS operating systems), so that the existence of this call in "
"MIPS operating systems is a de-facto standard."
msgstr ""
"Historiquement, cet appel système était disponible sur toutes les variantes "
"UNIX MIPS, notamment RISC/os, IRIX, Ultrix, NetBSD, OpenBSD et FreeBSD (et "
"aussi sur des systèmes d'exploitation MIPS non UNIX), ainsi l'existence de "
"cet appel dans les systèmes d'exploitation MIPS est de facto standard."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Caveat"
msgstr "Mise en garde"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<cacheflush>()  should not be used in programs intended to be portable.  On "
"Linux, this call first appeared on the MIPS architecture, but nowadays, "
"Linux provides a B<cacheflush>()  system call on some other architectures, "
"but with different arguments."
msgstr ""
"B<cacheflush>() ne devrait pas être utilisé dans des programmes conçus pour "
"être portables. Sur Linux, cet appel est apparu pour la première fois sur "
"l'architecture MIPS, mais Linux offre aujourd'hui un appel système "
"B<cacheflush>() sur d'autres architectures, mais avec des paramètres "
"différents."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Architecture-specific variants"
msgstr "Variantes dépendantes de l'architecture"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"glibc provides a wrapper for this system call, with the prototype shown in "
"SYNOPSIS, for the following architectures: ARC, CSKY, MIPS, and NIOS2."
msgstr ""
"La glibc fournit une enveloppe pour cet appel système, dont le prototype est "
"présenté en SYNOPSIS, pour les architectures suivantes : ARC, CSKY, MIPS et "
"NIOS2."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On some other architectures, Linux provides this system call, with different "
"arguments:"
msgstr ""
"Sur d'autres architectures, Linux fournit cet appel système avec des "
"paramètres différents :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "M68K:"
msgstr "M68K :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int cacheflush(unsigned long >I<addr>B<, int >I<scope>B<, int >I<cache>B<,>\n"
"B<               unsigned long >I<len>B<);>\n"
msgstr ""
"B<int cacheflush(unsigned long >I<addr>B<, int >I<scope>B<, int >I<cache>B<,>\n"
"B<               unsigned long >I<len>B<);>\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SH:"
msgstr "SH :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int cacheflush(unsigned long >I<addr>B<, unsigned long >I<len>B<, int >I<op>B<);>\n"
msgstr "B<int cacheflush(unsigned long >I<addr>B<, unsigned long >I<len>B<, int >I<op>B<);>\n"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NDS32:"
msgstr "NDS32 :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int cacheflush(unsigned int >I<start>B<, unsigned int >I<end>B<, int >I<cache>B<);>\n"
msgstr "B<int cacheflush(unsigned int >I<start>B<, unsigned int >I<end>B<, int >I<cache>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On the above architectures, glibc does not provide a wrapper for this system "
"call; call it using B<syscall>(2)."
msgstr ""
"Sur les architectures ci-dessus, la glibc ne fournit pas d'enveloppe autour "
"de cet appel système ; appelez-le avec B<syscall>(2)."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "GCC alternative"
msgstr "Alternative GCC"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Unless you need the finer grained control that this system call provides, "
"you probably want to use the GCC built-in function "
"B<__builtin___clear_cache>(), which provides a portable interface across "
"platforms supported by GCC and compatible compilers:"
msgstr ""
"Sauf si vous avez besoin du contrôle très fin fourni par cet appel système, "
"vous voudrez probablement utiliser la fonction intégrée à GCC, "
"B<__builtin___clear_cache>(), qui offre une interface portable sur toutes "
"les plateformes prises en charge par GCC et les compilateurs compatibles :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<void __builtin___clear_cache(void *>I<begin>B<, void *>I<end>B<);>\n"
msgstr "B<void __builtin___clear_cache(void *>I<begin>B<, void *>I<end>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On platforms that don't require instruction cache flushes, "
"B<__builtin___clear_cache>()  has no effect."
msgstr ""
"Sur les plateformes qui n'ont pas besoin de vider le cache d'instruction, "
"B<__builtin___clear_cache>() n'a aucun effet."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<Note>: On some GCC-compatible compilers, the prototype for this built-in "
"function uses I<char *> instead of I<void *> for the parameters."
msgstr ""
"I<Note> : sur certains compilateurs compatibles avec GCC, le prototype de "
"cette fonction intégrée utilise I<char *> au lieu de I<void *> pour les "
"paramètres."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Linux kernels older than Linux 2.6.11 ignore the I<addr> and I<nbytes> "
"arguments, making this function fairly expensive.  Therefore, the whole "
"cache is always flushed."
msgstr ""
"Les noyaux Linux avant Linux 2.6.11 ignorent les paramètres I<addr> et "
"I<nbytes>, ce qui rend cette fonction assez coûteuse. Par conséquent, le "
"cache entier est toujours vidé."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This function always behaves as if B<BCACHE> has been passed for the "
"I<cache> argument and does not do any error checking on the I<cache> "
"argument."
msgstr ""
"Cette fonction se comporte toujours comme si B<BCACHE> était passé en "
"paramètre I<cache> et elle ne vérifie pas les erreurs dans le paramètre "
"I<cache>."

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CACHEFLUSH"
msgstr "CACHEFLUSH"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-12-21"
msgstr "21 décembre 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<#include E<lt>asm/cachectl.hE<gt>>\n"
msgstr "B<#include E<lt>asm/cachectl.hE<gt>>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "B<int cacheflush(char *>I<addr>B<, int >I<nbytes>B<, int >I<cache>B<);>\n"
msgstr "B<int cacheflush(char *>I<addr>B<, int >I<nbytes>B<, int >I<cache>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"B<cacheflush>()  returns 0 on success or -1 on error.  If errors are "
"detected, I<errno> will indicate the error."
msgstr ""
"B<cacheflush>() renvoie B<0> s'il réussit. En cas d'échec, B<-1> est renvoyé "
"et I<errno> contient le code d'erreur."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: debian-bullseye opensuse-tumbleweed
msgid ""
"Glibc provides a wrapper for this system call, with the prototype shown in "
"SYNOPSIS, for the following architectures: ARC, CSKY, MIPS, and NIOS2."
msgstr ""
"La glibc fournit une enveloppe pour cet appel système, dont le prototype est "
"présenté en SYNOPSIS, pour les architectures suivantes : ARC, CSKY, MIPS et "
"NIOS2."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Linux kernels older than version 2.6.11 ignore the I<addr> and I<nbytes> "
"arguments, making this function fairly expensive.  Therefore, the whole "
"cache is always flushed."
msgstr ""
"Les noyaux Linux avant la version 2.6.11 ignorent les paramètres I<addr> et "
"I<nbytes>, ce qui rend cette fonction assez coûteuse. Par conséquent, le "
"cache entier est toujours vidé."

#. type: SH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 décembre 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr "Pages du manuel de Linux 6.02"
