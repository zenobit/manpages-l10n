# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Julien Cristau <jcristau@debian.org>
# Nicolas François <nicolas.francois@centraliens.net>, 2008-2010.
# Bastien Scher <bastien0705@gmail.com>, 2011-2013.
# David Prévot <david@tilapin.org>, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: coreutils manpages\n"
"POT-Creation-Date: 2023-02-15 18:45+0100\n"
"PO-Revision-Date: 2022-10-16 00:32+0200\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CHGRP"
msgstr "CHGRP"

#. type: TH
#: archlinux
#, no-wrap
msgid "November 2022"
msgstr "Novembre 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "chgrp - change group ownership"
msgstr "chgrp - Changer le groupe propriétaire d'un fichier"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chgrp> [I<\\,OPTION\\/>]... I<\\,GROUP FILE\\/>..."
msgstr "B<chgrp> [I<\\,OPTION\\/>] ... I<\\,GROUPE FICHIER\\/> ..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chgrp> [I<\\,OPTION\\/>]... I<\\,--reference=RFILE FILE\\/>..."
msgstr ""
"B<chgrp> [I<\\,OPTION\\/>] ... I<\\,--reference=FICHIER-R FICHIER\\/>..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Change the group of each FILE to GROUP.  With B<--reference>, change the "
"group of each FILE to that of RFILE."
msgstr ""
"Changer le groupe d'appartenance de chaque I<FICHIER> en I<GROUPE>. Avec "
"l'option B<--reference>, modifier le groupe de chaque fichier en celui de "
"I<FICHIER-R>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--changes>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "like verbose but report only when a change is made"
msgstr "utiliser le mode bavard en ne signalant que les modifications"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--silent>, B<--quiet>"
msgstr "B<-f>, B<--silent>, B<--quiet>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress most error messages"
msgstr "supprimer la plupart des messages d'erreur"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output a diagnostic for every file processed"
msgstr "afficher un diagnostic pour chaque fichier traité"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--dereference>"
msgstr "B<--dereference>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"affect the referent of each symbolic link (this is the default), rather than "
"the symbolic link itself"
msgstr ""
"affecter la cible de chaque lien symbolique (comportement par défaut) plutôt "
"que le lien symbolique lui-même"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--no-dereference>"
msgstr "B<-h>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"affect symbolic links instead of any referenced file (useful only on systems "
"that can change the ownership of a symlink)"
msgstr ""
"modifier les liens symboliques au lieu des fichiers référencés (utile "
"seulement sur les systèmes permettant de changer le propriétaire d'un lien "
"symbolique)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not treat '/' specially (the default)"
msgstr "ne pas traiter «\\ /\\ » de manière spéciale (par défaut)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>"
msgstr "B<--preserve-root>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "fail to operate recursively on '/'"
msgstr "ne pas opérer récursivement sur «\\ /\\ »"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--reference>=I<\\,RFILE\\/>"
msgstr "B<--reference>=I<\\,FICHIER-R\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use RFILE's group rather than specifying a GROUP value"
msgstr ""
"utiliser le groupe du I<FICHIER-R> plutôt que d'indiquer le nom du groupe"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "operate on files and directories recursively"
msgstr "modifier récursivement fichiers et répertoires"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following options modify how a hierarchy is traversed when the B<-R> "
"option is also specified.  If more than one is specified, only the final one "
"takes effect."
msgstr ""
"Les options suivantes modifient la façon dont est parcourue la hiérarchie "
"lorsque l'option B<-R> est également spécifiée. Si plus d'une option est "
"spécifiée, seule la dernière est prise en compte."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"if a command line argument is a symbolic link to a directory, traverse it"
msgstr ""
"si un paramètre de la ligne de commande est un lien symbolique vers un "
"répertoire, le parcourir"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>"
msgstr "B<-L>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "traverse every symbolic link to a directory encountered"
msgstr "parcourir chaque lien symbolique rencontré menant à un répertoire"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not traverse any symbolic links (default)"
msgstr "ne parcourir aucun lien symbolique (par défaut)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afficher l'aide-mémoire et quitter"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afficher les informations de version et quitter."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "chgrp staff /u"
msgstr "chgrp staff /u"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Change the group of /u to \"staff\"."
msgstr "attribue /u au groupe «\\ staff\\ »."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "chgrp -hR staff /u"
msgstr "chgrp -hR staff /u"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Change the group of /u and subfiles to \"staff\"."
msgstr ""
"attribue /u et ses sous-fichiers et sous-répertoires au groupe «\\ staff\\ »."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by David MacKenzie and Jim Meyering."
msgstr "Écrit par David MacKenzie et Jim Meyering."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Aide en ligne de GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction à E<lt>https://translationproject.org/"
"team/fr.htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ce programme est un logiciel libre. Vous pouvez le modifier et le "
"redistribuer. Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<chown>(1), B<chown>(2)"
msgstr "B<chown>(1), B<chown>(2)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chgrpE<gt>"
msgstr ""
"Documentation complète : E<lt>https://www.gnu.org/software/coreutils/"
"chgrpE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chgrp invocation\\(aq"
msgstr ""
"aussi disponible localement à l’aide de la commande : info \\(aq(coreutils) "
"chgrp invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Septembre 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "chown(1), chown(2)"
msgstr "B<chown>(1), B<chown>(2)"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "September 2022"
msgstr "Septembre 2022"

#. type: TH
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "January 2023"
msgstr "Janvier 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Avril 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Octobre 2021"
