# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2023-02-15 19:00+0100\n"
"PO-Revision-Date: 2022-08-20 17:26+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MCOOKIE"
msgstr "MCOOKIE"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "mcookie - generate magic cookies for xauth"
msgstr "mcookie - Créer des « cookies magiques » pour xauth"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mcookie> [options]"
msgstr "B<mcookie> [I<options>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<mcookie> generates a 128-bit random hexadecimal number for use with the X "
"authority system. Typical usage:"
msgstr ""
"B<mcookie> génère un nombre hexadécimal aléatoire de 128\\ bits qui peut "
"être utilisé avec le système d'authentification de X. Utilisation typique\\ :"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<xauth add :0 . >\\f(CRmcookie\\fR"
msgstr "B<xauth add :0 . >\\f(CRmcookie\\fR"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"\\f(CRThe \"random\" number generated is actually the MD5 message digest of "
"random information coming from one of the sources B<getrandom>\\f(CR(2) "
"system call, I</dev/urandom>\\f(CR, I</dev/random>\\f(CR, or the I<libc "
"pseudo-random functions>\\f(CR, in this preference order. See also the "
"option B<--file>\\f(CR.\\fR"
msgstr ""
"\\f(CRLe nombre « aléatoire » créé est en fait le condensé MD5 "
"d’informations aléatoires provenant d’une des sources : l'appel système "
"B<getrandom>\\f(CR(2), I</dev/urandom>\\f(CR, I</dev/random>\\f(CR ou les "
"I<fonctions pseudoaléatoires de libc>\\f(CR, dans cet ordre de préférence. "
"Voir aussi l'option B<--file>.\\f(CR.\\fR"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-f>, B<--file> I<file>"
msgstr "B<-f>, B<--file> I<fichier>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Use this I<file> as an additional source of randomness (for example I</dev/"
"urandom>). When I<file> is \\(aq-\\(aq, characters are read from standard "
"input."
msgstr ""
"Utiliser ce I<fichier> comme source supplémentaire d’aléa (par exemple I</"
"dev/urandom)>. Si I<fichier> est B<« - »>, les caractères sont lus sur "
"l'entrée standard."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-m>, B<--max-size> I<number>"
msgstr "B<-m>, B<--max-size> I<nombre>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Read from I<file> only this I<number> of bytes. This option is meant to be "
"used when reading additional randomness from a file or device."
msgstr ""
"Ne lire que I<nombre> octets du I<fichier>. Cette option est censée être "
"utilisée lors de la lecture d’un aléa supplémentaire à partir d’un fichier "
"ou périphérique."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<number> argument may be followed by the multiplicative suffixes "
"KiB=1024, MiB=1024*1024, and so on for GiB, TiB, PiB, EiB, ZiB and YiB (the "
"\"iB\" is optional, e.g., \"K\" has the same meaning as \"KiB\") or the "
"suffixes KB=1000, MB=1000*1000, and so on for GB, TB, PB, EB, ZB and YB."
msgstr ""
"L’argument I<nombre> peut être suivi des suffixes multiplicatifs KiB=1024, "
"MiB=1024*1024, etc., pour GiB, TiB, PiB, EiB, ZiB et YiB (la partie « iB » "
"est facultative, par exemple « K » est identique à « KiB ») ou des suffixes "
"KB=1000, MB=1000*1000, etc., pour GB, TB, PB, EB, ZB et YB."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Inform where randomness originated, with amount of entropy read from each "
"source."
msgstr ""
"Informer des sources d’aléa, avec la quantité d’entropie lue de chaque "
"source."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</dev/urandom>"
msgstr "I</dev/urandom>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</dev/random>"
msgstr "I</dev/random>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "It is assumed that none of the randomness sources will block."
msgstr "Aucune source d’aléa n’est censée bloquer."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<md5sum>(1), B<X>(7), B<xauth>(1), B<rand>(3)"
msgstr "B<md5sum>(1), B<X>(7), B<xauth>(1), B<rand>(3)"

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<mcookie> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<mcookie> fait partie du paquet util-linux qui peut être "
"téléchargé sur"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "December 2014"
msgstr "Décembre 2014"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<mcookie> generates a 128-bit random hexadecimal number for use with the X "
"authority system.  Typical usage:"
msgstr ""
"B<mcookie> génère un nombre hexadécimal aléatoire de 128\\ bits qui peut "
"être utilisé avec le système d'authentification de X. Utilisation typique\\ :"

#. type: Plain text
#: debian-bullseye
msgid "B<xauth add :0 . `mcookie`>"
msgstr "B<xauth add :0 . `mcookie`>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The \"random\" number generated is actually the MD5 message digest of random "
"information coming from one of the sources I<getrandom>()  system call, I</"
"dev/urandom>, I</dev/random>, or the I<libc pseudo-random functions>, in "
"this preference order. See also the option B<--file>."
msgstr ""
"Le nombre « aléatoire » créé est en fait le condensé MD5 d’informations "
"aléatoires provenant d’une des sources : l'appel système B<getrandom>(2), I</"
"dev/urandom>, I</dev/random> ou les I<fonctions pseudoaléatoires de libc>, "
"dans cet ordre de préférence. Voir aussi l'option B<--file>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-f>,B< --file >I<file>"
msgstr "B<-f>,B< --file >I<fichier>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Use this I<file> as an additional source of randomness (for example /dev/"
"urandom).  When I<file> is '-', characters are read from standard input."
msgstr ""
"Utiliser ce I<fichier> comme source supplémentaire d’aléa (par exemple I</"
"dev/urandom)>. Si I<fichier> est B<->, les caractères sont lus sur l'entrée "
"standard."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-m>,B< --max-size >I<number>"
msgstr "B<-m>, B<--max-size> I<nombre>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Read from I<file> only this I<number> of bytes.  This option is meant to be "
"used when reading additional randomness from a file or device."
msgstr ""
"Ne lire que I<nombre> octets du I<fichier>. Cette option est censée être "
"utilisée lors de la lecture d’un aléa supplémentaire à partir d’un fichier "
"ou périphérique."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-v>,B< --verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The mcookie command is part of the util-linux package and is available from "
"E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> "
"Linux Kernel Archive E<.UE .>"
msgstr ""
"La commande B<mcookie> fait partie du paquet util-linux, elle est disponible "
"sur E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/"
">l’archive du noyau LinuxE<.UE .>"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 février 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
