.\" Man page generated from reStructuredText.
.
.TH "BTRFS" "8" "Feb 24, 2023" "6.1.3" "BTRFS"
.SH NAME
btrfs \- a toolbox to manage btrfs filesystems
.
.nr rst2man-indent-level 0
.
.de1 rstReportMargin
\\$1 \\n[an-margin]
level \\n[rst2man-indent-level]
level margin: \\n[rst2man-indent\\n[rst2man-indent-level]]
-
\\n[rst2man-indent0]
\\n[rst2man-indent1]
\\n[rst2man-indent2]
..
.de1 INDENT
.\" .rstReportMargin pre:
. RS \\$1
. nr rst2man-indent\\n[rst2man-indent-level] \\n[an-margin]
. nr rst2man-indent-level +1
.\" .rstReportMargin post:
..
.de UNINDENT
. RE
.\" indent \\n[an-margin]
.\" old: \\n[rst2man-indent\\n[rst2man-indent-level]]
.nr rst2man-indent-level -1
.\" new: \\n[rst2man-indent\\n[rst2man-indent-level]]
.in \\n[rst2man-indent\\n[rst2man-indent-level]]u
..
.SH SYNOPSIS
.sp
\fBbtrfs\fP <command> [<args>]
.SH DESCRIPTION
.sp
The \fBbtrfs\fP utility is a toolbox for managing btrfs filesystems.  There are
command groups to work with subvolumes, devices, for whole filesystem or other
specific actions. See section \fICOMMANDS\fP\&.
.sp
There are also standalone tools for some tasks like \fBbtrfs\-convert\fP or
\fBbtrfstune\fP that were separate historically and/or haven\(aqt been merged to the
main utility. See section \fISTANDALONE TOOLS\fP for more details.
.sp
For other topics (mount options, etc) please refer to the separate manual
page btrfs(5)\&.
.SH COMMAND SYNTAX
.sp
Any command name can be shortened so long as the shortened form is unambiguous,
however, it is recommended to use full command names in scripts.  All command
groups have their manual page named \fBbtrfs\-<group>\fP\&.
.sp
For example: it is possible to run \fBbtrfs sub snaps\fP instead of
\fBbtrfs subvolume snapshot\fP\&.
But \fBbtrfs file s\fP is not allowed, because \fBfile s\fP may be interpreted
both as \fBfilesystem show\fP and as \fBfilesystem sync\fP\&.
.sp
If the command name is ambiguous, the list of conflicting options is
printed.
.sp
\fISizes\fP, both upon input and output, can be expressed in either SI or IEC\-I
units (see \fI\%numfmt(1)\fP)
with the suffix \fIB\fP appended.
All numbers will be formatted according to the rules of the \fIC\fP locale
(ignoring the shell locale, see \fI\%locale(7)\fP).
.sp
For an overview of a given command use \fBbtrfs command \-\-help\fP
or \fBbtrfs [command...] \-\-help \-\-full\fP to print all available options.
.SH COMMANDS
.INDENT 0.0
.TP
.B balance
Balance btrfs filesystem chunks across single or several devices.
See btrfs\-balance(8) for details.
.TP
.B check
Do off\-line check on a btrfs filesystem.
See btrfs\-check(8) for details.
.TP
.B device
Manage devices managed by btrfs, including add/delete/scan and so
on.  See btrfs\-device(8) for details.
.TP
.B filesystem
Manage a btrfs filesystem, including label setting/sync and so on.
See btrfs\-filesystem(8) for details.
.TP
.B inspect\-internal
Debug tools for developers/hackers.
See btrfs\-inspect\-internal(8) for details.
.TP
.B property
Get/set a property from/to a btrfs object.
See btrfs\-property(8) for details.
.TP
.B qgroup
Manage quota group(qgroup) for btrfs filesystem.
See btrfs\-qgroup(8) for details.
.TP
.B quota
Manage quota on btrfs filesystem like enabling/rescan and etc.
See btrfs\-quota(8) and btrfs\-qgroup(8) for details.
.TP
.B receive
Receive subvolume data from stdin/file for restore and etc.
See btrfs\-receive(8) for details.
.TP
.B replace
Replace btrfs devices.
See btrfs\-replace(8) for details.
.TP
.B rescue
Try to rescue damaged btrfs filesystem.
See btrfs\-rescue(8) for details.
.TP
.B restore
Try to restore files from a damaged btrfs filesystem.
See btrfs\-restore(8) for details.
.TP
.B scrub
Scrub a btrfs filesystem.
See btrfs\-scrub(8) for details.
.TP
.B send
Send subvolume data to stdout/file for backup and etc.
See btrfs\-send(8) for details.
.TP
.B subvolume
Create/delete/list/manage btrfs subvolume.
See btrfs\-subvolume(8) for details.
.UNINDENT
.SH STANDALONE TOOLS
.sp
New functionality could be provided using a standalone tool. If the functionality
proves to be useful, then the standalone tool is declared obsolete and its
functionality is copied to the main tool. Obsolete tools are removed after a
long (years) depreciation period.
.sp
Tools that are still in active use without an equivalent in \fBbtrfs\fP:
.INDENT 0.0
.TP
.B btrfs\-convert
in\-place conversion from ext2/3/4 filesystems to btrfs
.TP
.B btrfstune
tweak some filesystem properties on a unmounted filesystem
.TP
.B btrfs\-select\-super
rescue tool to overwrite primary superblock from a spare copy
.TP
.B btrfs\-find\-root
rescue helper to find tree roots in a filesystem
.UNINDENT
.sp
Deprecated and obsolete tools:
.INDENT 0.0
.TP
.B btrfs\-debug\-tree
moved to \fBbtrfs inspect\-internal dump\-tree\fP\&. Removed from
source distribution.
.TP
.B btrfs\-show\-super
moved to \fBbtrfs inspect\-internal dump\-super\fP, standalone
removed.
.TP
.B btrfs\-zero\-log
moved to \fBbtrfs rescue zero\-log\fP, standalone removed.
.UNINDENT
.sp
For space\-constrained environments, it\(aqs possible to build a single binary with
functionality of several standalone tools. This is following the concept of
busybox where the file name selects the functionality. This works for symlinks
or hardlinks. The full list can be obtained by \fBbtrfs help \-\-box\fP\&.
.SH EXIT STATUS
.sp
\fBbtrfs\fP returns a zero exit status if it succeeds. Non zero is returned in
case of failure.
.SH AVAILABILITY
.sp
\fBbtrfs\fP is part of btrfs\-progs.  Please refer to the documentation at
\fI\%https://btrfs.readthedocs.io\fP or wiki \fI\%http://btrfs.wiki.kernel.org\fP for further
information.
.SH SEE ALSO
.sp
btrfs(5),
btrfs\-balance(8),
btrfs\-check(8),
btrfs\-convert(8),
btrfs\-device(8),
btrfs\-filesystem(8),
btrfs\-inspect\-internal(8),
btrfs\-property(8),
btrfs\-qgroup(8),
btrfs\-quota(8),
btrfs\-receive(8),
btrfs\-replace(8),
btrfs\-rescue(8),
btrfs\-restore(8),
btrfs\-scrub(8),
btrfs\-send(8),
btrfs\-subvolume(8),
btrfstune(8),
mkfs.btrfs(8)
.\" Generated by docutils manpage writer.
.
