# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:11+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ioctl_fslabel"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "ioctl_fslabel - get or set a filesystem label"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/fs.hE<gt>>       /* Definition of B<*FSLABEL*> constants */\n"
"B<#include E<lt>sys/ioctl.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int ioctl(int >I<fd>B<, FS_IOC_GETFSLABEL, char >I<label>B<[FSLABEL_MAX]);>\n"
"B<int ioctl(int >I<fd>B<, FS_IOC_SETFSLABEL, char >I<label>B<[FSLABEL_MAX]);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If a filesystem supports online label manipulation, these B<ioctl>(2)  "
"operations can be used to get or set the filesystem label for the filesystem "
"on which I<fd> resides.  The B<FS_IOC_SETFSLABEL> operation requires "
"privilege (B<CAP_SYS_ADMIN>)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Possible errors include (but are not limited to) the following:"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<label> references an inaccessible memory area."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The specified label exceeds the maximum label length for the filesystem."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTTY>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This can appear if the filesystem does not support online label manipulation."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The calling process does not have sufficient permissions to set the label."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These B<ioctl>(2)  operations first appeared in Linux 4.18.  They were "
"previously known as B<BTRFS_IOC_GET_FSLABEL> and B<BTRFS_IOC_SET_FSLABEL> "
"and were private to Btrfs."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "This API is Linux-specific."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The maximum string length for this interface is B<FSLABEL_MAX>, including "
"the terminating null byte (\\[aq]\\e0\\[aq]).  Filesystems have differing "
"maximum label lengths, which may or may not include the terminating null.  "
"The string provided to B<FS_IOC_SETFSLABEL> must always be null-terminated, "
"and the string returned by B<FS_IOC_GETFSLABEL> will always be null-"
"terminated."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<ioctl>(2), B<blkid>(8)"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "IOCTL_FSLABEL"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-04-20"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "B<#include E<lt>sys/ioctl.hE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "B<#include E<lt>linux/fs.hE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"B<int ioctl(int >I<fd>B<, FS_IOC_GETFSLABEL, char >I<label>B<[FSLABEL_MAX]);>"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"B<int ioctl(int >I<fd>B<, FS_IOC_SETFSLABEL, char >I<label>B<[FSLABEL_MAX]);>"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"If a filesystem supports online label manipulation, these B<ioctl>(2)  "
"operations can be used to get or set the filesystem label for the filesystem "
"on which B<fd> resides.  The B<FS_IOC_SETFSLABEL> operation requires "
"privilege (B<CAP_SYS_ADMIN>)."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "Error can include (but are not limited to) the following:"
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-tumbleweed
msgid ""
"The maximum string length for this interface is B<FSLABEL_MAX>, including "
"the terminating null byte (\\(aq\\e0\\(aq).  Filesystems have differing "
"maximum label lengths, which may or may not include the terminating null.  "
"The string provided to B<FS_IOC_SETFSLABEL> must always be null-terminated, "
"and the string returned by B<FS_IOC_GETFSLABEL> will always be null-"
"terminated."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""
