# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:26+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "s390_guarded_storage"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"s390_guarded_storage - operations with z/Architecture guarded storage "
"facility"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>asm/guarded_storage.hE<gt> >/* Definition of B<GS_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>         >/* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_s390_guarded_storage, int >I<command>B<,>\n"
"B<            struct gs_cb *>I<gs_cb>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<s390_guarded_storage>(), "
"necessitating the use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<s390_guarded_storage>()  system call enables the use of the Guarded "
"Storage Facility (a z/Architecture-specific feature) for user-space "
"processes."
msgstr ""

#.  The description is based on
#.  http://www-05.ibm.com/de/linux-on-z-ws-us/agenda/pdfs/8_-_Linux_Whats_New_-_Stefan_Raspl.pdf
#.  and "z/Architecture Principles of Operation" obtained from
#.  http://publibfi.boulder.ibm.com/epubs/pdf/dz9zr011.pdf
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The guarded storage facility is a hardware feature that allows marking up to "
"64 memory regions (as of z14) as guarded; reading a pointer with a newly "
"introduced \"Load Guarded\" (LGG)  or \"Load Logical and Shift "
"Guarded\" (LLGFSG) instructions will cause a range check on the loaded value "
"and invoke a (previously set up)  user-space handler if one of the guarded "
"regions is affected."
msgstr ""

#.  The command description is copied from v4.12-rc1~139^2~56^2 commit message
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The I<command> argument indicates which function to perform.  The following "
"commands are supported:"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<GS_ENABLE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Enable the guarded storage facility for the calling task.  The initial "
"content of the guarded storage control block will be all zeros.  After "
"enablement, user-space code can use the \"Load Guarded Storage "
"Controls\" (LGSC) instruction (or the B<load_gs_cb>()  function wrapper "
"provided in the I<asm/guarded_storage.h> header) to load an arbitrary "
"control block.  While a task is enabled, the kernel will save and restore "
"the calling content of the guarded storage registers on context switch."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<GS_DISABLE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Disables the use of the guarded storage facility for the calling task.  The "
"kernel will cease to save and restore the content of the guarded storage "
"registers, the task-specific content of these registers is lost."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<GS_SET_BC_CB>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set a broadcast guarded storage control block to the one provided in the "
"I<gs_cb> argument.  This is called per thread and associates a specific "
"guarded storage control block with the calling task.  This control block "
"will be used in the broadcast command B<GS_BROADCAST>."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<GS_CLEAR_BC_CB>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Clears the broadcast guarded storage control block.  The guarded storage "
"control block will no longer have the association established by the "
"B<GS_SET_BC_CB> command."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<GS_BROADCAST>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Sends a broadcast to all thread siblings of the calling task.  Every sibling "
"that has established a broadcast guarded storage control block will load "
"this control block and will be enabled for guarded storage.  The broadcast "
"guarded storage control block is consumed; a second broadcast without a "
"refresh of the stored control block with B<GS_SET_BC_CB> will not have any "
"effect."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The I<gs_cb> argument specifies the address of a guarded storage control "
"block structure and is currently used only by the B<GS_SET_BC_CB> command; "
"all other aforementioned commands ignore this argument."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "On success, the return value of B<s390_guarded_storage>()  is 0."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<command> was B<GS_SET_BC_CB> and the copying of the guarded storage "
"control block structure pointed by the I<gs_cb> argument has failed."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The value provided in the I<command> argument was not valid."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<command> was one of B<GS_ENABLE> or B<GS_SET_BC_CB>, and the allocation of "
"a new guarded storage control block has failed."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The guarded storage facility is not supported by the hardware."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#.  916cda1aa1b412d7cf2991c3af7479544942d121, v4.12-rc1~139^2~56^2
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "This system call is available since Linux 4.12."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This Linux-specific system call is available only on the s390 architecture."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The guarded storage facility is available beginning with System z14."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The description of the guarded storage facility along with related "
"instructions and Guarded Storage Control Block and Guarded Storage Event "
"Parameter List structure layouts is available in \"z/Architecture Principles "
"of Operations\" beginning from the twelfth edition."
msgstr ""

#.  .PP
#.  For the example of using the guarded storage facility, see
#.  .UR https://developer.ibm.com/javasdk/2017/09/25/concurrent-scavenge-using-guarded-storage-facility-works/
#.  the article with the description of its usage in the Java Garbage Collection
#.  .UE
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The I<gs_cb> structure has a field I<gsepla> (Guarded Storage Event "
"Parameter List Address), which is a user-space pointer to a Guarded Storage "
"Event Parameter List structure (that contains the address of the "
"aforementioned event handler in the I<gseha> field), and its layout is "
"available as a B<gs_epl> structure type definition in the I<asm/"
"guarded_storage.h> header."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<syscall>(2)"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "S390_GUARDED_STORAGE"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2019-03-06"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<#include E<lt>asm/guarded_storage.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<int s390_guarded_storage(int >I<command>B<, struct gs_cb *>I<gs_cb>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"Glibc does not provide a wrapper for this system call, use B<syscall>(2)  to "
"call it."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""
