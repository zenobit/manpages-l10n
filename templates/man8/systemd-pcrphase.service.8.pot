# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-28 19:45+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-PCRPHASE\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd-pcrphase.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"systemd-pcrphase.service, systemd-pcrphase-sysinit.service, systemd-pcrphase-"
"initrd.service, systemd-pcrmachine.service, systemd-pcrfs-root.service, "
"systemd-pcrfs@.service, systemd-pcrphase - Measure boot phase into TPM2 PCR "
"11, machine ID and file system identity into PCR 15"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "systemd-pcrphase\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "systemd-pcrphase-sysinit\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "systemd-pcrphase-initrd\\&.service"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "systemd-pcrmachine\\&.service"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "systemd-pcrfs-root\\&.service"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "systemd-pcrfs@\\&.service"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "/usr/lib/systemd/systemd-pcrphase [I<STRING>]"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"systemd-pcrphase\\&.service, systemd-pcrphase-sysinit\\&.service, and "
"systemd-pcrphase-initrd\\&.service are system services that measure specific "
"strings into TPM2 PCR 11 during boot at various milestones of the boot "
"process\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"systemd-pcrmachine\\&.service is a system service that measures the machine "
"ID (see B<machine-id>(5)) into PCR 15\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"systemd-pcrfs-root\\&.service and systemd-pcrfs@\\&.service are services "
"that measure file system identity information (i\\&.e\\&. mount point, file "
"system type, label and UUID, partition label and UUID) into PCR 15\\&.  "
"systemd-pcrfs-root\\&.service does so for the root file system, systemd-"
"pcrfs@\\&.service is a template unit that measures the file system indicated "
"by its instance identifier instead\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"These services require B<systemd-stub>(7)  to be used in a unified kernel "
"image (UKI)\\&. They execute no operation when the stub has not been used to "
"invoke the kernel\\&. The stub will measure the invoked kernel and "
"associated vendor resources into PCR 11 before handing control to it; once "
"userspace is invoked these services then will extend TPM2 PCR 11 with "
"certain literal strings indicating phases of the boot process\\&. During a "
"regular boot process PCR 11 is extended with the following strings:"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"\"enter-initrd\" \\(em early when the initrd initializes, before activating "
"system extension images for the initrd\\&. It acts as a barrier between the "
"time where the kernel initializes and where the initrd starts operating and "
"enables system extension images, i\\&.e\\&. code shipped outside of the "
"UKI\\&. (This extension happens when systemd-pcrphase-initrd\\&.service is "
"started\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"\"leave-initrd\" \\(em when the initrd is about to transition into the host "
"file system\\&. It acts as barrier between initrd code and host OS code\\&. "
"(This extension happens when systemd-pcrphase-initrd\\&.service is "
"stopped\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"\"sysinit\" \\(em when basic system initialization is complete (which "
"includes local file systems having been mounted), and the system begins "
"starting regular system services\\&. (This extension happens when systemd-"
"pcrphase-sysinit\\&.service is started\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"\"ready\" \\(em during later boot-up, after remote file systems have been "
"activated (i\\&.e\\&. after remote-fs\\&.target), but before users are "
"permitted to log in (i\\&.e\\&. before systemd-user-sessions\\&.service)\\&. "
"It acts as barrier between the time where unprivileged regular users are "
"still prohibited to log in and where they are allowed to log in\\&. (This "
"extension happens when systemd-pcrphase\\&.service is started\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"\"shutdown\" \\(em when the system shutdown begins\\&. It acts as barrier "
"between the time the system is fully up and running and where it is about to "
"shut down\\&. (This extension happens when systemd-pcrphase\\&.service is "
"stopped\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"\"final\" \\(em at the end of system shutdown\\&. It acts as barrier between "
"the time the service manager still runs and when it transitions into the "
"final shutdown phase where service management is not available anymore\\&. "
"(This extension happens when systemd-pcrphase-sysinit\\&.service is "
"stopped\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"During a regular system lifecycle, PCR 11 is extended with the strings "
"\"enter-initrd\", \"leave-initrd\", \"sysinit\", \"ready\", \"shutdown\", "
"and \"final\"\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Specific phases of the boot process may be referenced via the series of "
"strings measured, separated by colons (the \"phase path\")\\&. For example, "
"the phase path for the regular system runtime is \"enter-initrd:leave-initrd:"
"sysinit:ready\", while the one for the initrd is just \"enter-initrd\"\\&. "
"The phase path for the boot phase before the initrd is an empty string; "
"because that\\*(Aqs hard to pass around a single colon (\":\") may be used "
"instead\\&. Note that the aforementioned six strings are just the default "
"strings and individual systems might measure other strings at other times, "
"and thus implement different and more fine-grained boot phases to bind "
"policy to\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"By binding policy of TPM2 objects to a specific phase path it is possible to "
"restrict access to them to specific phases of the boot process, for example "
"making it impossible to access the root file system\\*(Aqs encryption key "
"after the system transitioned from the initrd into the host root file "
"system\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Use B<systemd-measure>(1)  to pre-calculate expected PCR 11 values for "
"specific boot phases (via the B<--phase=> switch)\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"systemd-pcrfs-root\\&.service and systemd-pcrfs@\\&.service are "
"automatically pulled into the initial transaction by B<systemd-gpt-auto-"
"generator>(8)  for the root and /var/ file systems\\&.  B<systemd-fstab-"
"generator>(8)  will do this for all mounts with the B<x-systemd\\&.pcrfs> "
"mount option in /etc/fstab\\&."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The /usr/lib/systemd/system-pcrphase executable may also be invoked from the "
"command line, where it expects the word to extend into PCR 11, as well as "
"the following switches:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--bank=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Takes the PCR banks to extend the specified word into\\&. If not specified "
"the tool automatically determines all enabled PCR banks and measures the "
"word into all of them\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--tpm2-device=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Controls which TPM2 device to use\\&. Expects a device node path referring "
"to the TPM2 chip (e\\&.g\\&.  /dev/tpmrm0)\\&. Alternatively the special "
"value \"auto\" may be specified, in order to automatically determine the "
"device node of a suitable TPM2 device (of which there must be exactly "
"one)\\&. The special value \"list\" may be used to enumerate all suitable "
"TPM2 devices currently discovered\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid "B<--graceful>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"If no TPM2 firmware, kernel subsystem, kernel driver or device support is "
"found, exit with exit status 0 (i\\&.e\\&. indicate success)\\&. If this is "
"not specified any attempt to measure without a TPM2 device will cause the "
"invocation to fail\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<--machine-id>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Instead of measuring a word specified on the command line into PCR 11, "
"measure the host\\*(Aqs machine ID into PCR 15\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "B<--file-system=>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Instead of measuring a word specified on the command line into PCR 11, "
"measure identity information of the specified file system into PCR 15\\&. "
"The parameter must be the path to the established mount point of the file "
"system to measure\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-measure>(1), B<systemd-gpt-auto-"
"generator>(8), B<systemd-fstab-generator>(8)"
msgstr ""

#. type: TH
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"systemd-pcrphase.service, systemd-pcrphase-sysinit.service, systemd-pcrphase-"
"initrd.service, systemd-pcrphase - Measure boot phase into TPM2 PCR 11"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "/lib/systemd/system-pcrphase I<STRING>"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"systemd-pcrphase\\&.service, systemd-pcrphase-sysinit\\&.service and systemd-"
"pcrphase-initrd\\&.service are system services that measure specific strings "
"into TPM2 PCR 11 during boot at various milestones of the boot process\\&."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"These services require B<systemd-stub>(7)  to be used in a unified kernel "
"image (UKI) setup\\&. They execute no operation when invoked when the stub "
"has not been used to invoke the kernel\\&. The stub will measure the invoked "
"kernel and associated vendor resources into PCR 11 before handing control to "
"it; once userspace is invoked these services then will extend certain "
"literal strings indicating various phases of the boot process into TPM2 PCR "
"11\\&. During a regular boot process the following strings are extended into "
"PCR 11\\&."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\"enter-initrd\" is extended into PCR 11 early when the initrd initializes, "
"before activating system extension images for the initrd\\&. It is supposed "
"to act as barrier between the time where the kernel initializes, and where "
"the initrd starts operating and enables system extension images, i\\&.e\\&. "
"code shipped outside of the UKI\\&. (This string is extended at start of "
"systemd-pcrphase-initrd\\&.service\\&.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\"leave-initrd\" is extended into PCR 11 when the initrd is about to "
"transition into the host file system, i\\&.e\\&. when it achieved its "
"purpose\\&. It is supposed to act as barrier between kernel/initrd code and "
"host OS code\\&. (This string is extended at stop of systemd-pcrphase-"
"initrd\\&.service\\&.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\"sysinit\" is extended into PCR 11 when basic system initialization is "
"complete (which includes local file systems have been mounted), and the "
"system begins starting regular system services\\&. (This string is extended "
"at start of systemd-pcrphase-sysinit\\&.service\\&.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\"ready\" is extended into PCR 11 during later boot-up, after remote file "
"systems have been activated (i\\&.e\\&. after remote-fs\\&.target), but "
"before users are permitted to log in (i\\&.e\\&. before systemd-user-"
"sessions\\&.service)\\&. It is supposed to act as barrier between the time "
"where unprivileged regular users are still prohibited to log in and where "
"they are allowed to log in\\&. (This string is extended at start of systemd-"
"pcrphase\\&.service\\&.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\"shutdown\" is extended into PCR 11 when system shutdown begins\\&. It is "
"supposed to act as barrier between the time the system is fully up and "
"running and where it is about to shut down\\&. (This string is extended at "
"stop of systemd-pcrphase\\&.service\\&.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\"final\" is extended into PCR 11 at the end of system shutdown\\&. It is "
"supposed to act as barrier between the time the service manager still runs "
"and when it transitions into the final boot phase where service management "
"is not available anymore\\&. (This string is extended at stop of systemd-"
"pcrphase-sysinit\\&.service\\&.)"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"During a regular system lifecycle, the strings \"enter-initrd\" → \"leave-"
"initrd\" → \"sysinit\" → \"ready\" → \"shutdown\" → \"final\" are extended "
"into PCR 11, one after the other\\&."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"Specific phases of the boot process may be referenced via the series of "
"strings measured, separated by colons (the \"boot path\")\\&. For example, "
"the boot path for the regular system runtime is \"enter-initrd:leave-initrd:"
"sysinit:ready\", while the one for the initrd is just \"enter-initrd\"\\&. "
"The boot path for the the boot phase before the initrd, is an empty string; "
"because that\\*(Aqs hard to pass around a single colon (\":\") may be used "
"instead\\&. Note that the aforementioned six strings are just the default "
"strings and individual systems might measure other strings at other times, "
"and thus implement different and more fine-grained boot phases to bind "
"policy to\\&."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"By binding policy of TPM2 objects to a specific boot path it is possible to "
"restrict access to them to specific phases of the boot process, for example "
"making it impossible to access the root file system\\*(Aqs encryption key "
"after the system transitioned from the initrd into the host root file "
"system\\&."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The /lib/systemd/system-pcrphase executable may also be invoked from the "
"command line, where it expects the word to extend into PCR 11, as well as "
"the following switches:"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "B<systemd>(1), B<systemd-stub>(7), B<systemd-measure>(1)"
msgstr ""

#. type: Plain text
#: mageia-cauldron opensuse-tumbleweed
msgid "/usr/lib/systemd/system-pcrphase I<STRING>"
msgstr ""
