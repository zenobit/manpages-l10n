# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-28 18:55+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "GENFSTAB"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "11/20/2022"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "genfstab - generate output suitable for addition to an fstab file"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "genfstab [options] root"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"genfstab helps fill in an fstab file by autodetecting all the current mounts "
"below a given mountpoint and printing them in fstab-compatible format to "
"standard output\\&. It can be used to persist a manually mounted filesystem "
"hierarchy and is often used during the initial install and configuration of "
"an OS\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-f> E<lt>filterE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Restrict output to mountpoints matching the prefix I<filter>\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-L>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Use labels for source identifiers (shortcut for I<-t LABEL>)\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-p>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Exclude pseudofs mounts (default behavior)\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-P>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Include pseudofs mounts\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-t> E<lt>tagE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Use I<tag> for source identifiers (should be one of: I<LABEL>, I<UUID>, "
"I<PARTLABEL>, I<PARTUUID>)\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-U>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Use UUIDs for source identifiers (shortcut for I<-t UUID>)\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Output syntax and command line options\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<pacman>(8)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Bugs can be reported on the bug tracker I<https://bugs\\&.archlinux\\&.org> "
"in the Arch Linux category and title prefixed with [arch-install-scripts] or "
"via arch-projects@archlinux\\&.org\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Maintainers:"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Dave Reisner E<lt>dreisner@archlinux\\&.orgE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Eli Schwartz E<lt>eschwartz@archlinux\\&.orgE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the arch-install-"
"scripts\\&.git repository\\&."
msgstr ""
