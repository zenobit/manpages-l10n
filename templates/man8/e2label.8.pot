# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-15 18:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "E2LABEL"
msgstr ""

#. type: TH
#: archlinux debian-bullseye debian-unstable mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr ""

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "e2label - Change the label on an ext2/ext3/ext4 file system"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<e2label> I<device> [ I<volume-label> ]"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<e2label> will display or change the volume label on the ext2, ext3, or "
"ext4 file system located on I<device.>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the optional argument I<volume-label> is not present, B<e2label> will "
"simply display the current volume label."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"If the optional argument I<volume-label> is present, then B<e2label> will "
"set the volume label to be I<volume-label>.  Ext2 volume labels can be at "
"most 16 characters long; if I<volume-label> is longer than 16 characters, "
"B<e2label> will truncate it and print a warning message.  For other file "
"systems that support online label manipulation and are mounted B<e2label> "
"will work as well, but it will not attempt to truncate the I<volume-label> "
"at all."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It is also possible to set the volume label using the B<-L> option of "
"B<tune2fs>(8)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<e2label> was written by Theodore Ts'o (tytso@mit.edu)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<e2label> is part of the e2fsprogs package and is available from http://"
"e2fsprogs.sourceforge.net."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mke2fs>(8), B<tune2fs>(8)"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "E2fsprogs version 1.46.6"
msgstr ""

#. type: Plain text
#: debian-bullseye fedora-38 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"If the optional argument I<volume-label> is present, then B<e2label> will "
"set the volume label to be I<volume-label>.  Ext2 volume labels can be at "
"most 16 characters long; if I<volume-label> is longer than 16 characters, "
"B<e2label> will truncate it and print a warning message."
msgstr ""

#. type: TH
#: fedora-38 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr ""

#. type: TH
#: fedora-38 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.46.5"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "August 2021"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "E2fsprogs version 1.46.4"
msgstr ""
