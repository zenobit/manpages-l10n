# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-26 08:02+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "UKIFY"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "ukify"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "ukify - Combine kernel and initrd into a signed Unified Kernel Image"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B</usr/lib/systemd/ukify> I<LINUX> I<INITRD>... [OPTIONS...]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Note: this command is experimental for now\\&. While it is intended to "
"become a regular component of systemd, it might still change in behaviour "
"and interface\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"B<ukify> is a tool that combines a kernel and an initrd with a UEFI boot "
"stub to create a \\m[blue]B<Unified Kernel Image "
"(UKI)>\\m[]\\&\\s-2\\u[1]\\d\\s+2 \\(em a PE binary that can be executed by "
"the firmware to start the embedded linux kernel\\&. See B<systemd-stub>(7)  "
"for details about the stub\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Additional sections will be inserted into the UKI, either automatically or "
"only if a specific option is provided\\&. See the discussions of B<--"
"cmdline=>, B<--os-release=>, B<--devicetree=>, B<--splash=>, B<--pcrpkey=>, "
"B<--uname=>, and B<--section=> below\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"If PCR signing keys are provided via the B<--pcr-public-key=> and B<--pcr-"
"private-key=> options, PCR values that will be seen after booting with the "
"given kernel, initrd, and other sections, will be calculated, signed, and "
"embedded in the UKI\\&.  B<systemd-measure>(1)  is used to perform this "
"calculation and signing\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The calculation of PCR values is done for specific boot phase paths\\&. "
"Those can be specified with B<--phases=> option\\&. If not specified, the "
"default provided by B<systemd-measure> is used\\&. It is also possible to "
"specify the B<--pcr-private-key=>, B<--pcr-public-key=>, and B<--phases=> "
"arguments more than once\\&. Signatures will be then performed with each of "
"the specified keys\\&. When both B<--phases=> and B<--pcr-private-key=> are "
"used, they must be specified the same number of times, and then the n-th "
"boot phase path set will be signed by the n-th key\\&. This can be used to "
"build different trust policies for different phases of the boot\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"If a SecureBoot signing key is provided via the B<--secureboot-private-key=> "
"option, the resulting PE binary will be signed as a whole, allowing the "
"resulting UKI to be trusted by SecureBoot\\&. Also see the discussion of "
"automatic enrollment in B<systemd-boot>(7)\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Note that the I<LINUX> positional argument is mandatory\\&. The I<INITRD> "
"positional arguments are optional\\&. If more than one is specified, they "
"will all be combined into a single PE section\\&. This is useful to for "
"example prepend microcode before the actual initrd\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--cmdline=>I<TEXT>B<|>I<@PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify the kernel command line (the \"\\&.cmdline\" section)\\&. The "
"argument may be a literal string, or \"@\" followed by a path name\\&. If "
"not specified, no command line will be embedded\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--os-release=>I<TEXT>B<|>I<@PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify the os-release description (the \"\\&.osrel\" section)\\&. The "
"argument may be a literal string, or \"@\" followed by a path name\\&. If "
"not specified, the B<os-release>(5)  file will be picked up from the host "
"system\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--devicetree=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify the devicetree description (the \"\\&.dtb\" section)\\&. The "
"argument is a path to a compiled binary DeviceTree file\\&. If not "
"specified, the section will not be present\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--splash=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify a picture to display during boot (the \"\\&.splash\" section)\\&. "
"The argument is a path to a BMP file\\&. If not specified, the section will "
"not be present\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--pcrpkey=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify a path to a public key to embed in the \"\\&.pcrpkey\" section\\&. "
"If not specified, and there\\*(Aqs exactly one B<--pcr-public-key=> "
"argument, that key will be used\\&. Otherwise, the section will not be "
"present\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--uname=>I<VERSION>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify the kernel version (as in B<uname -r>, the \"\\&.uname\" "
"section)\\&. If not specified, an attempt will be made to extract the "
"version string from the kernel image\\&. It is recommended to pass this "
"explicitly if known, because the extraction is based on heuristics and not "
"very reliable\\&. If not specified and extraction fails, the section will "
"not be present\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--section=>I<NAME>B<:>I<TEXT>B<|>I<@PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify an arbitrary additional section \"I<NAME>\"\\&. Note that the name "
"is used as-is, and if the section name should start with a dot, it must be "
"included in I<NAME>\\&. The argument may be a literal string, or \"@\" "
"followed by a path name\\&. This option may be specified more than once\\&. "
"Any sections specified in this fashion will be inserted (in order) before "
"the \"\\&.linux\" section which is always last\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--pcr-private-key=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify a private key to use for signing PCR policies\\&. This option may be "
"specified more than once, in which case multiple signatures will be made\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--pcr-public-key=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify a public key to use for signing PCR policies\\&. This option may be "
"specified more than once, similarly to the B<--pcr-private-key=> option\\&. "
"If not present, the public keys will be extracted from the private keys\\&. "
"If present, the this option must be specified the same number of times as "
"the B<--pcr-private-key=> option\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--phases=>I<LIST>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"A comma or space-separated list of colon-separated phase paths to sign a "
"policy for\\&. If not present, the default of B<systemd-measure>(1)  will be "
"used\\&. When this argument is present, it must appear the same number of "
"times as the B<--pcr-private-key=> option\\&. Each set of boot phase paths "
"will be signed with the corresponding private key\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--pcr-banks=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"A comma or space-separated list of PCR banks to sign a policy for\\&. If not "
"present, all known banks will be used (\"sha1\", \"sha256\", \"sha384\", "
"\"sha512\"), which will fail if not supported by the system\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--secureboot-private-key=>I<SB_KEY>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"A path to a private key to use for signing of the resulting binary\\&. If "
"the B<--signing-engine=> option is used, this may also be an engine-specific "
"designation\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--secureboot-certificate=>I<SB_CERT>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"A path to a certificate to use for signing of the resulting binary\\&. If "
"the B<--signing-engine=> option is used, this may also be an engine-specific "
"designation\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--signing-engine=>I<ENGINE>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"An \"engine\" to for signing of the resulting binary\\&. This option is "
"currently passed verbatim to the B<--engine=> option of B<sbsign>(1)\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--sign-kernel>, B<--no-sign-kernel>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Override the detection of whether to sign the Linux binary itself before it "
"is embedded in the combined image\\&. If not specified, it will be signed if "
"a SecureBoot signing key is provided via the B<--secureboot-private-key=> "
"option and the binary has not already been signed\\&. If B<--sign-kernel> is "
"specified, and the binary has already been signed, the signature will be "
"appended anyway\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--tools=>I<DIRS>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Specify one or more directories with helper tools\\&.  B<ukify> will look "
"for helper tools in those directories first, and if not found, try to load "
"them from I<$PATH> in the usual fashion\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--measure>, B<--no-measure>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Enable or disable a call to B<systmed-measure> to print pre-calculated PCR "
"values\\&. Defaults to false\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--output=>I<FILENAME>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The output filename\\&. If not specified, the name of the I<LINUX> argument, "
"with the suffix \"\\&.unsigned\\&.efi\" or \"\\&.signed\\&.efi\" will be "
"used, depending on whether signing for SecureBoot was performed\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<Example\\ \\&1.\\ \\&Minimal invocation>"
msgstr ""

#. type: Plain text
#: archlinux
#, no-wrap
msgid ""
"ukify \\e\n"
"      /lib/modules/6\\&.0\\&.9-300\\&.fc37\\&.x86_64/vmlinuz \\e\n"
"      /some/path/initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img \\e\n"
"      --cmdline=\\*(Aqquiet rw\\*(Aq\n"
msgstr ""

#. type: Plain text
#: archlinux
msgid "This creates an unsigned UKI \\&./vmlinuz\\&.unsigned\\&.efi\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<Example\\ \\&2.\\ \\&All the bells and whistles>"
msgstr ""

#. type: Plain text
#: archlinux
#, no-wrap
msgid ""
"/usr/lib/systemd/ukify \\e\n"
"      /lib/modules/6\\&.0\\&.9-300\\&.fc37\\&.x86_64/vmlinuz \\e\n"
"      early_cpio \\e\n"
"      /some/path/initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img \\e\n"
"      --pcr-private-key=pcr-private-initrd-key\\&.pem \\e\n"
"      --pcr-public-key=pcr-public-initrd-key\\&.pem \\e\n"
"      --phases=\\*(Aqenter-initrd\\*(Aq \\e\n"
"      --pcr-private-key=pcr-private-system-key\\&.pem \\e\n"
"      --pcr-public-key=pcr-public-system-key\\&.pem \\e\n"
"      --phases=\\*(Aqenter-initrd:leave-initrd enter-initrd:leave-initrd:sysinit \\e\n"
"                enter-initrd:leave-initrd:sysinit:ready\\*(Aq \\e\n"
"      --pcr-banks=sha384,sha512 \\e\n"
"      --secureboot-private-key=sb\\&.key \\e\n"
"      --secureboot-certificate=sb\\&.cert \\e\n"
"      --sign-kernel \\e\n"
"      --cmdline=\\*(Aqquiet rw rhgb\\*(Aq\n"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This creates a signed UKI \\&./vmlinuz\\&.signed\\&.efi\\&. The initrd "
"section contains two concatenated parts, early_cpio and "
"initramfs-6\\&.0\\&.9-300\\&.fc37\\&.x86_64\\&.img\\&. The policy embedded "
"in the \"\\&.pcrsig\" section will be signed for the initrd (the B<enter-"
"initrd> phase) with the key pcr-private-initrd-key\\&.pem, and for the main "
"system (phases B<leave-initrd>, B<sysinit>, B<ready>) with the key pcr-"
"private-system-key\\&.pem\\&. The Linux binary and the resulting combined "
"image will be signed with the SecureBoot key sb\\&.key\\&."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-boot>(7), B<objcopy>(1), "
"B<systemd-pcrphase.service>(1)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux
msgid "Unified Kernel Image (UKI)"
msgstr ""

#. type: Plain text
#: archlinux
msgid "\\%https://uapi-group.org/specifications/specs/unified_kernel_image/"
msgstr ""
