# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:07+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "_Generic"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-12"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "_Generic - type-generic selection"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<_Generic(>I<expression>B<, type1: >e1B<, >... /*B<, default: >e */B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<_Generic>()  evaluates the path of code under the type selector that is "
"compatible with the type of the controlling I<expression>, or B<default:> if "
"no type is compatible."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "I<expression> is not evaluated."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is especially useful for writing type-generic macros, that will behave "
"differently depending on the type of the argument."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "C11 and later."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The following program demonstrates how to write a replacement for the "
"standard B<imaxabs>(3)  function, which being a function can't really "
"provide what it promises: seamlessly upgrading to the widest available type."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define my_imaxabs  _Generic(INTMAX_C(0),  \\e\n"
"    long:           labs,                  \\e\n"
"    long long:      llabs                  \\e\n"
" /* long long long: lllabs */              \\e\n"
")\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    off_t  a;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    a = -42;\n"
"    printf(\"imaxabs(%jd) == %jd\\en\", (intmax_t) a, my_imaxabs(a));\n"
"    printf(\"&imaxabs == %p\\en\", &my_imaxabs);\n"
"    printf(\"&labs    == %p\\en\", &labs);\n"
"    printf(\"&llabs   == %p\\en\", &llabs);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-11-12"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"The following code demonstrates how to write a macro similar to C++'s B<\\"
"%static_cast>(), which will allow casting safely between a limited set of "
"types.  It is useful for example when calling system calls or library "
"functions that use compatible structures, like for example B<bind>(2)  with "
"B<\\%sockaddr>(3type)."
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid "/* This code is in the public domain. */\n"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>netinet/in.hE<gt>\n"
"#include E<lt>sys/socket.hE<gt>\n"
"#include E<lt>sys/un.hE<gt>\n"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define sockaddr_cast(t, p)                            \\e\n"
"    _Generic(&*(p),                                    \\e\n"
"    struct sockaddr *:                                 \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr_in *:             (t) (p),     \\e\n"
"        struct sockaddr_in6 *:            (t) (p),     \\e\n"
"        struct sockaddr_un *:             (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    struct sockaddr **:                                \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr_in **:            (t) (p),     \\e\n"
"        struct sockaddr_in6 **:           (t) (p),     \\e\n"
"        struct sockaddr_un **:            (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    const struct sockaddr *:                           \\e\n"
"        _Generic((t) NULL,                             \\e\n"
"        const struct sockaddr_in *:       (t) (p),     \\e\n"
"        const struct sockaddr_in6 *:      (t) (p),     \\e\n"
"        const struct sockaddr_un *:       (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"                                                       \\e\n"
"    struct sockaddr_in *:                              \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr *:                (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    struct sockaddr_in **:                             \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr **:               (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    const struct sockaddr_in *:                        \\e\n"
"        _Generic((t) NULL,                             \\e\n"
"        const struct sockaddr *:          (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"                                                       \\e\n"
"    struct sockaddr_in6 *:                             \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr *:                (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    struct sockaddr_in6 **:                            \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr **:               (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    const struct sockaddr_in6 *:                       \\e\n"
"        _Generic((t) NULL,                             \\e\n"
"        const struct sockaddr *:          (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"                                                       \\e\n"
"    struct sockaddr_un *:                              \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr *:                (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    struct sockaddr_un **:                             \\e\n"
"        _Generic((typeof_unqual(t)) NULL,              \\e\n"
"        struct sockaddr **:               (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"    const struct sockaddr_un *:                        \\e\n"
"        _Generic((t) NULL,                             \\e\n"
"        const struct sockaddr *:          (t) (p),     \\e\n"
"        default:                              (p)),    \\e\n"
"                                                       \\e\n"
"    default:                                           \\e\n"
"        (p)                                            \\e\n"
"    )\n"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"socklen_t           slen;\n"
"struct sockaddr_un  sun;\n"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"slen = sizeof(ss);\n"
"getsockname(sfd, sockaddr_cast(struct sockaddr *, &sun), &slen);\n"
msgstr ""
