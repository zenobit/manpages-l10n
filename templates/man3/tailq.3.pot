# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:37+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TAILQ"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. TAILQ_FOREACH_FROM,
#. TAILQ_FOREACH_FROM_SAFE,
#. TAILQ_FOREACH_REVERSE_FROM,
#. TAILQ_FOREACH_REVERSE_FROM_SAFE,
#. TAILQ_FOREACH_REVERSE_SAFE,
#. TAILQ_FOREACH_SAFE,
#. TAILQ_SWAP
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"TAILQ_CONCAT, TAILQ_EMPTY, TAILQ_ENTRY, TAILQ_FIRST, TAILQ_FOREACH, "
"TAILQ_FOREACH_REVERSE, TAILQ_HEAD, TAILQ_HEAD_INITIALIZER, TAILQ_INIT, "
"TAILQ_INSERT_AFTER, TAILQ_INSERT_BEFORE, TAILQ_INSERT_HEAD, "
"TAILQ_INSERT_TAIL, TAILQ_LAST, TAILQ_NEXT, TAILQ_PREV, TAILQ_REMOVE - "
"implementation of a doubly linked tail queue"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/queue.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<TAILQ_ENTRY(TYPE);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<TAILQ_HEAD(HEADNAME, TYPE);>\n"
"B<TAILQ_HEAD TAILQ_HEAD_INITIALIZER(TAILQ_HEAD >I<head>B<);>\n"
"B<void TAILQ_INIT(TAILQ_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int TAILQ_EMPTY(TAILQ_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void TAILQ_INSERT_HEAD(TAILQ_HEAD *>I<head>B<,>\n"
"B<                         struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
"B<void TAILQ_INSERT_TAIL(TAILQ_HEAD *>I<head>B<,>\n"
"B<                         struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
"B<void TAILQ_INSERT_BEFORE(struct TYPE *>I<listelm>B<,>\n"
"B<                         struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
"B<void TAILQ_INSERT_AFTER(TAILQ_HEAD *>I<head>B<, struct TYPE *>I<listelm>B<,>\n"
"B<                         struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct TYPE *TAILQ_FIRST(TAILQ_HEAD *>I<head>B<);>\n"
"B<struct TYPE *TAILQ_LAST(TAILQ_HEAD *>I<head>B<, HEADNAME);>\n"
"B<struct TYPE *TAILQ_PREV(struct TYPE *>I<elm>B<, HEADNAME, TAILQ_ENTRY >I<NAME>B<);>\n"
"B<struct TYPE *TAILQ_NEXT(struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "TAILQ_FOREACH_FROM(struct TYPE *" var ", TAILQ_HEAD *" head ,
#.  .BI "                                TAILQ_ENTRY " NAME );
#.  .BI "TAILQ_FOREACH_REVERSE_FROM(struct TYPE *" var ", TAILQ_HEAD *" head ", HEADNAME,"
#.  .BI "                                TAILQ_ENTRY " NAME );
#.  .PP
#.  .BI "TAILQ_FOREACH_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ,
#.  .BI "                                TAILQ_ENTRY " NAME ,
#.  .BI "                                struct TYPE *" temp_var );
#.  .BI "TAILQ_FOREACH_FROM_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ,
#.  .BI "                                TAILQ_ENTRY " NAME ,
#.  .BI "                                struct TYPE *" temp_var );
#.  .BI "TAILQ_FOREACH_REVERSE_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ,
#.  .BI "                                HEADNAME, TAILQ_ENTRY " NAME ,
#.  .BI "                                struct TYPE *" temp_var );
#.  .BI "TAILQ_FOREACH_REVERSE_FROM_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ,
#.  .BI "                                HEADNAME, TAILQ_ENTRY " NAME ,
#.  .BI "                                struct TYPE *" temp_var );
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<TAILQ_FOREACH(struct TYPE *>I<var>B<, TAILQ_HEAD *>I<head>B<,>\n"
"B<                         TAILQ_ENTRY >I<NAME>B<);>\n"
"B<TAILQ_FOREACH_REVERSE(struct TYPE *>I<var>B<, TAILQ_HEAD *>I<head>B<, HEADNAME,>\n"
"B<                         TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void TAILQ_REMOVE(TAILQ_HEAD *>I<head>B<, struct TYPE *>I<elm>B<,>\n"
"B<                         TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "void TAILQ_SWAP(TAILQ_HEAD *" head1 ", TAILQ_HEAD *" head2 ", TYPE,"
#.  .BI "                                TAILQ_ENTRY " NAME );
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void TAILQ_CONCAT(TAILQ_HEAD *>I<head1>B<, TAILQ_HEAD *>I<head2>B<,>\n"
"B<                         TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "These macros define and operate on doubly linked tail queues."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In the macro definitions, I<TYPE> is the name of a user defined structure, "
"that must contain a field of type I<TAILQ_ENTRY>, named I<NAME>.  The "
"argument I<HEADNAME> is the name of a user defined structure that must be "
"declared using the macro B<TAILQ_HEAD>()."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Creation"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A tail queue is headed by a structure defined by the B<TAILQ_HEAD>()  "
"macro.  This structure contains a pair of pointers, one to the first element "
"in the queue and the other to the last element in the queue.  The elements "
"are doubly linked so that an arbitrary element can be removed without "
"traversing the queue.  New elements can be added to the queue after an "
"existing element, before an existing element, at the head of the queue, or "
"at the end of the queue.  A I<TAILQ_HEAD> structure is declared as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TAILQ_HEAD(HEADNAME, TYPE) head;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"where I<struct HEADNAME> is the structure to be defined, and I<struct TYPE> "
"is the type of the elements to be linked into the queue.  A pointer to the "
"head of the queue can later be declared as:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "struct HEADNAME *headp;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(The names I<head> and I<headp> are user selectable.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_ENTRY>()  declares a structure that connects the elements in the "
"queue."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_HEAD_INITIALIZER>()  evaluates to an initializer for the queue "
"I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<TAILQ_INIT>()  initializes the queue referenced by"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_EMPTY>()  evaluates to true if there are no items on the queue.  "
"I<head>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Insertion"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_INSERT_HEAD>()  inserts the new element I<elm> at the head of the "
"queue."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_INSERT_TAIL>()  inserts the new element I<elm> at the end of the "
"queue."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_INSERT_BEFORE>()  inserts the new element I<elm> before the element "
"I<listelm>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_INSERT_AFTER>()  inserts the new element I<elm> after the element "
"I<listelm>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Traversal"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_FIRST>()  returns the first item on the queue, or NULL if the queue "
"is empty."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_LAST>()  returns the last item on the queue.  If the queue is empty "
"the return value is NULL."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_PREV>()  returns the previous item on the queue, or NULL if this "
"item is the first."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_NEXT>()  returns the next item on the queue, or NULL if this item is "
"the last."
msgstr ""

#.  .PP
#.  .BR TAILQ_FOREACH_FROM ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the loop at
#.  .I var
#.  instead of the first element in the TAILQ referenced by
#.  .IR head .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_FOREACH>()  traverses the queue referenced by I<head> in the forward "
"direction, assigning each element in turn to I<var>.  I<var> is set to NULL "
"if the loop completes normally, or if there were no elements."
msgstr ""

#.  .PP
#.  .BR TAILQ_FOREACH_REVERSE_FROM ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH_REVERSE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the reverse loop at
#.  .I var
#.  instead of the last element in the TAILQ referenced by
#.  .IR head .
#.  .PP
#.  .BR TAILQ_FOREACH_SAFE ()
#.  and
#.  .BR TAILQ_FOREACH_REVERSE_SAFE ()
#.  traverse the list referenced by
#.  .I head
#.  in the forward or reverse direction respectively,
#.  assigning each element in turn to
#.  .IR var .
#.  However, unlike their unsafe counterparts,
#.  .BR TAILQ_FOREACH ()
#.  and
#.  .BR TAILQ_FOREACH_REVERSE ()
#.  permit to both remove
#.  .I var
#.  as well as free it from within the loop safely without interfering with the
#.  traversal.
#.  .PP
#.  .BR TAILQ_FOREACH_FROM_SAFE ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the loop at
#.  .I var
#.  instead of the first element in the TAILQ referenced by
#.  .IR head .
#.  .PP
#.  .BR TAILQ_FOREACH_REVERSE_FROM_SAFE ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH_REVERSE_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the reverse loop at
#.  .I var
#.  instead of the last element in the TAILQ referenced by
#.  .IR head .
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_FOREACH_REVERSE>()  traverses the queue referenced by I<head> in the "
"reverse direction, assigning each element in turn to I<var>."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Removal"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<TAILQ_REMOVE>()  removes the element I<elm> from the queue."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Other features"
msgstr ""

#.  .BR TAILQ_SWAP ()
#.  swaps the contents of
#.  .I head1
#.  and
#.  .IR head2 .
#.  .PP
#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_CONCAT>()  concatenates the queue headed by I<head2> onto the end of "
"the one headed by I<head1> removing all entries from the former."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<TAILQ_EMPTY>()  returns nonzero if the queue is empty, and zero if the "
"queue contains at least one entry."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_FIRST>(), B<TAILQ_LAST>(), B<TAILQ_PREV>(), and B<TAILQ_NEXT>()  "
"return a pointer to the first, last, previous, or next I<TYPE> structure, "
"respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<TAILQ_HEAD_INITIALIZER>()  returns an initializer that can be assigned to "
"the queue I<head>."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Not in POSIX.1, POSIX.1-2001, or POSIX.1-2008.  Present on the BSDs.  (TAILQ "
"functions first appeared in 4.4BSD)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<TAILQ_FOREACH>()  and B<TAILQ_FOREACH_REVERSE>()  don't allow I<var> to be "
"removed or freed within the loop, as it would interfere with the traversal.  "
"B<TAILQ_FOREACH_SAFE>()  and B<TAILQ_FOREACH_REVERSE_SAFE>(), which are "
"present on the BSDs but are not present in glibc, fix this limitation by "
"allowing I<var> to safely be removed from the list and freed from within the "
"loop without interfering with the traversal."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/queue.hE<gt>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"struct entry {\n"
"    int data;\n"
"    TAILQ_ENTRY(entry) entries;             /* Tail queue */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TAILQ_HEAD(tailhead, entry);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct entry *n1, *n2, *n3, *np;\n"
"    struct tailhead head;                   /* Tail queue head */\n"
"    int i;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    TAILQ_INIT(&head);                      /* Initialize the queue */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the head */\n"
"    TAILQ_INSERT_HEAD(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the tail */\n"
"    TAILQ_INSERT_TAIL(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n2 = malloc(sizeof(struct entry));      /* Insert after */\n"
"    TAILQ_INSERT_AFTER(&head, n1, n2, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    n3 = malloc(sizeof(struct entry));      /* Insert before */\n"
"    TAILQ_INSERT_BEFORE(n2, n3, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    TAILQ_REMOVE(&head, n2, entries);       /* Deletion */\n"
"    free(n2);\n"
"                                            /* Forward traversal */\n"
"    i = 0;\n"
"    TAILQ_FOREACH(np, &head, entries)\n"
"        np-E<gt>data = i++;\n"
"                                            /* Reverse traversal */\n"
"    TAILQ_FOREACH_REVERSE(np, &head, tailhead, entries)\n"
"        printf(\"%i\\en\", np-E<gt>data);\n"
"                                            /* TailQ deletion */\n"
"    n1 = TAILQ_FIRST(&head);\n"
"    while (n1 != NULL) {\n"
"        n2 = TAILQ_NEXT(n1, entries);\n"
"        free(n1);\n"
"        n1 = n2;\n"
"    }\n"
"    TAILQ_INIT(&head);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bullseye: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: tailq.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<insque>(3), B<queue>(7)"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-12-21"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GNU"
msgstr ""

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void TAILQ_CONCAT(TAILQ_HEAD *>I<head1>B<, TAILQ_HEAD *>I<head2>B<,>\n"
"B<                TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<struct TYPE *TAILQ_FIRST(TAILQ_HEAD *>I<head>B<);>\n"
msgstr ""

#.  .PP
#.  .BI "TAILQ_FOREACH_FROM(struct TYPE *" var ", TAILQ_HEAD *" head ", TAILQ_ENTRY " NAME ");"
#.  .PP
#.  .BI "TAILQ_FOREACH_FROM_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ", TAILQ_ENTRY " NAME ", struct TYPE *" temp_var ");"
#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<TAILQ_FOREACH(struct TYPE *>I<var>B<, TAILQ_HEAD *>I<head>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .PP
#.  .BI "TAILQ_FOREACH_REVERSE_FROM(struct TYPE *" var ", TAILQ_HEAD *" head ", HEADNAME, TAILQ_ENTRY " NAME ");"
#.  .PP
#.  .BI "TAILQ_FOREACH_REVERSE_FROM_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ", HEADNAME, TAILQ_ENTRY " NAME ", struct TYPE *" temp_var ");"
#.  .PP
#.  .BI "TAILQ_FOREACH_REVERSE_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ", HEADNAME, TAILQ_ENTRY " NAME ", TYPE *" temp_var ");"
#.  .PP
#.  .BI "TAILQ_FOREACH_SAFE(struct TYPE *" var ", TAILQ_HEAD *" head ", TAILQ_ENTRY " NAME ", struct TYPE *" temp_var ");"
#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<TAILQ_FOREACH_REVERSE(struct TYPE *>I<var>B<, TAILQ_HEAD *>I<head>B<, HEADNAME,>\n"
"B<                TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<TAILQ_HEAD(HEADNAME, TYPE);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<TAILQ_HEAD TAILQ_HEAD_INITIALIZER(TAILQ_HEAD >I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<void TAILQ_INIT(TAILQ_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void TAILQ_INSERT_AFTER(TAILQ_HEAD *>I<head>B<, struct TYPE *>I<listelm>B<,>\n"
"B<                struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void TAILQ_INSERT_BEFORE(struct TYPE *>I<listelm>B<, struct TYPE *>I<elm>B<,>\n"
"B<                TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void TAILQ_INSERT_HEAD(TAILQ_HEAD *>I<head>B<, struct TYPE *>I<elm>B<,>\n"
"B<                TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<void TAILQ_INSERT_TAIL(TAILQ_HEAD *>I<head>B<, struct TYPE *>I<elm>B<,>\n"
"B<                TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<struct TYPE *TAILQ_LAST(TAILQ_HEAD *>I<head>B<, HEADNAME);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<struct TYPE *TAILQ_NEXT(struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<struct TYPE *TAILQ_PREV(struct TYPE *>I<elm>B<, HEADNAME, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .PP
#.  .BI "void TAILQ_SWAP(TAILQ_HEAD *" head1 ", TAILQ_HEAD *" head2 ", TYPE, TAILQ_ENTRY " NAME ");"
#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "B<void TAILQ_REMOVE(TAILQ_HEAD *>I<head>B<, struct TYPE *>I<elm>B<, TAILQ_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"A tail queue is headed by a structure defined by the B<TAILQ_HEAD>()  "
"macro.  This structure contains a pair of pointers, one to the first element "
"in the tail queue and the other to the last element in the tail queue.  The "
"elements are doubly linked so that an arbitrary element can be removed "
"without traversing the tail queue.  New elements can be added to the tail "
"queue after an existing element, before an existing element, at the head of "
"the tail queue, or at the end of the tail queue.  A I<TAILQ_HEAD> structure "
"is declared as follows:"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"where I<struct HEADNAME> is the structure to be defined, and I<struct TYPE> "
"is the type of the elements to be linked into the tail queue.  A pointer to "
"the head of the tail queue can later be declared as:"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_HEAD_INITIALIZER>()  evaluates to an initializer for the "
"tail queue I<head>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_CONCAT>()  concatenates the tail queue headed by I<head2> "
"onto the end of the one headed by I<head1> removing all entries from the "
"former."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_EMPTY>()  evaluates to true if there are no items on the "
"tail queue."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_ENTRY>()  declares a structure that connects the elements "
"in the tail queue."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_FIRST>()  returns the first item on the tail queue or NULL "
"if the tail queue is empty."
msgstr ""

#.  .PP
#.  The macro
#.  .BR TAILQ_FOREACH_FROM ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the loop at
#.  .I var
#.  instead of the first element in the TAILQ referenced by
#.  .IR head .
#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_FOREACH>()  traverses the tail queue referenced by I<head> "
"in the forward direction, assigning each element in turn to I<var>.  I<var> "
"is set to NULL if the loop completes normally, or if there were no elements."
msgstr ""

#.  .PP
#.  The macro
#.  .BR TAILQ_FOREACH_REVERSE_FROM ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH_REVERSE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the reverse loop at
#.  .I var
#.  instead of the last element in the TAILQ referenced by
#.  .IR head .
#.  .PP
#.  The macros
#.  .BR TAILQ_FOREACH_SAFE ()
#.  and
#.  .BR TAILQ_FOREACH_REVERSE_SAFE ()
#.  traverse the list referenced by
#.  .I head
#.  in the forward or reverse direction respectively,
#.  assigning each element in turn to
#.  .IR var .
#.  However, unlike their unsafe counterparts,
#.  .BR TAILQ_FOREACH ()
#.  and
#.  .BR TAILQ_FOREACH_REVERSE ()
#.  permit to both remove
#.  .I var
#.  as well as free it from within the loop safely without interfering with the
#.  traversal.
#.  .PP
#.  The macro
#.  .BR TAILQ_FOREACH_FROM_SAFE ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the loop at
#.  .I var
#.  instead of the first element in the TAILQ referenced by
#.  .IR head .
#.  .PP
#.  The macro
#.  .BR TAILQ_FOREACH_REVERSE_FROM_SAFE ()
#.  behaves identically to
#.  .BR TAILQ_FOREACH_REVERSE_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found TAILQ element and begins the reverse loop at
#.  .I var
#.  instead of the last element in the TAILQ referenced by
#.  .IR head .
#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_FOREACH_REVERSE>()  traverses the tail queue referenced by "
"I<head> in the reverse direction, assigning each element in turn to I<var>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_INIT>()  initializes the tail queue referenced by I<head>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_INSERT_HEAD>()  inserts the new element I<elm> at the head "
"of the tail queue."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_INSERT_TAIL>()  inserts the new element I<elm> at the end "
"of the tail queue."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_INSERT_AFTER>()  inserts the new element I<elm> after the "
"element I<listelm>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_INSERT_BEFORE>()  inserts the new element I<elm> before "
"the element I<listelm>."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_LAST>()  returns the last item on the tail queue.  If the "
"tail queue is empty the return value is NULL."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_NEXT>()  returns the next item on the tail queue, or NULL "
"if this item is the last."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_PREV>()  returns the previous item on the tail queue, or "
"NULL if this item is the first."
msgstr ""

#.  .PP
#.  The macro
#.  .BR TAILQ_SWAP ()
#.  swaps the contents of
#.  .I head1
#.  and
#.  .IR head2 .
#. type: Plain text
#: debian-bullseye
msgid ""
"The macro B<TAILQ_REMOVE>()  removes the element I<elm> from the tail queue."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"B<TAILQ_FIRST>(), B<TAILQ_LAST>(), B<TAILQ_NEXT>(), and B<TAILQ_PREV>()  "
"return a pointer to the first, last, next or previous I<TYPE> structure, "
"respectively."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"Not in POSIX.1, POSIX.1-2001 or POSIX.1-2008.  Present on the BSDs.  (TAILQ "
"functions first appeared in 4.4BSD)."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The macros B<TAILQ_FOREACH>()  and B<TAILQ_FOREACH_REVERSE>()  don't allow "
"I<var> to be removed or freed within the loop, as it would interfere with "
"the traversal.  The macros B<TAILQ_FOREACH_SAFE>()  and "
"B<TAILQ_FOREACH_REVERSE_SAFE>(), which are present on the BSDs but are not "
"present in glibc, fix this limitation by allowing I<var> to safely be "
"removed from the list and freed from within the loop without interfering "
"with the traversal."
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"struct entry {\n"
"    int data;\n"
"    TAILQ_ENTRY(entry) entries;             /* Tail queue. */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct entry *n1, *n2, *n3, *np;\n"
"    struct tailhead head;                   /* Tail queue head. */\n"
"    int i;\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "    TAILQ_INIT(&head);                      /* Initialize the queue. */\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the head. */\n"
"    TAILQ_INSERT_HEAD(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the tail. */\n"
"    TAILQ_INSERT_TAIL(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n2 = malloc(sizeof(struct entry));      /* Insert after. */\n"
"    TAILQ_INSERT_AFTER(&head, n1, n2, entries);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    n3 = malloc(sizeof(struct entry));      /* Insert before. */\n"
"    TAILQ_INSERT_BEFORE(n2, n3, entries);\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"    TAILQ_REMOVE(&head, n2, entries);       /* Deletion. */\n"
"    free(n2);\n"
"                                            /* Forward traversal. */\n"
"    i = 0;\n"
"    TAILQ_FOREACH(np, &head, entries)\n"
"        np-E<gt>data = i++;\n"
"                                            /* Reverse traversal. */\n"
"    TAILQ_FOREACH_REVERSE(np, &head, tailhead, entries)\n"
"        printf(\"%i\\en\", np-E<gt>data);\n"
"                                            /* TailQ Deletion. */\n"
"    n1 = TAILQ_FIRST(&head);\n"
"    while (n1 != NULL) {\n"
"        n2 = TAILQ_NEXT(n1, entries);\n"
"        free(n1);\n"
"        n1 = n2;\n"
"    }\n"
"    TAILQ_INIT(&head);\n"
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""
