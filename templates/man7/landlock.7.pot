# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:13+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Landlock"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Landlock - unprivileged access-control"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Landlock is an access-control system that enables any processes to securely "
"restrict themselves and their future children.  Because Landlock is a "
"stackable Linux Security Module (LSM), it makes it possible to create safe "
"security sandboxes as new security layers in addition to the existing system-"
"wide access-controls.  This kind of sandbox is expected to help mitigate the "
"security impact of bugs, and unexpected or malicious behaviors in "
"applications."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A Landlock security policy is a set of access rights (e.g., open a file in "
"read-only, make a directory, etc.)  tied to a file hierarchy.  Such policy "
"can be configured and enforced by processes for themselves using three "
"system calls:"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<landlock_create_ruleset>(2)  creates a new ruleset;"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<landlock_add_rule>(2)  adds a new rule to a ruleset;"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<landlock_restrict_self>(2)  enforces a ruleset on the calling thread."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"To be able to use these system calls, the running kernel must support "
"Landlock and it must be enabled at boot time."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Landlock rules"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A Landlock rule describes an action on an object.  An object is currently a "
"file hierarchy, and the related filesystem actions are defined with access "
"rights (see B<landlock_add_rule>(2)).  A set of rules is aggregated in a "
"ruleset, which can then restrict the thread enforcing it, and its future "
"children."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Filesystem actions"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"These flags enable to restrict a sandboxed process to a set of actions on "
"files and directories.  Files or directories opened before the sandboxing "
"are not subject to these restrictions.  See B<landlock_add_rule>(2)  and "
"B<landlock_create_ruleset>(2)  for more context."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "A file can only receive these access rights:"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_EXECUTE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Execute a file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_WRITE_FILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Open a file with write access."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_READ_FILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Open a file with read access."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A directory can receive access rights related to files or directories.  The "
"following access right is applied to the directory itself, and the "
"directories beneath it:"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_READ_DIR>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Open a directory or list its content."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"However, the following access rights only apply to the content of a "
"directory, not the directory itself:"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_REMOVE_DIR>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Remove an empty directory or rename one."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_REMOVE_FILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Unlink (or rename) a file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_CHAR>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename or link) a character device."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_DIR>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename) a directory."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_REG>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename or link) a regular file."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_SOCK>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename or link) a UNIX domain socket."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_FIFO>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename or link) a named pipe."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_BLOCK>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename or link) a block device."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<LANDLOCK_ACCESS_FS_MAKE_SYM>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Create (or rename or link) a symbolic link."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Layers of file path access rights"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Each time a thread enforces a ruleset on itself, it updates its Landlock "
"domain with a new layer of policy.  Indeed, this complementary policy is "
"composed with the potentially other rulesets already restricting this "
"thread.  A sandboxed thread can then safely add more constraints to itself "
"with a new enforced ruleset."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"One policy layer grants access to a file path if at least one of its rules "
"encountered on the path grants the access.  A sandboxed thread can only "
"access a file path if all its enforced policy layers grant the access as "
"well as all the other system access controls (e.g., filesystem DAC, other "
"LSM policies, etc.)."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Bind mounts and OverlayFS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Landlock enables restricting access to file hierarchies, which means that "
"these access rights can be propagated with bind mounts (cf.  "
"B<mount_namespaces>(7))  but not with OverlayFS."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A bind mount mirrors a source file hierarchy to a destination.  The "
"destination hierarchy is then composed of the exact same files, on which "
"Landlock rules can be tied, either via the source or the destination path.  "
"These rules restrict access when they are encountered on a path, which means "
"that they can restrict access to multiple file hierarchies at the same time, "
"whether these hierarchies are the result of bind mounts or not."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"An OverlayFS mount point consists of upper and lower layers.  These layers "
"are combined in a merge directory, result of the mount point.  This merge "
"hierarchy may include files from the upper and lower layers, but "
"modifications performed on the merge hierarchy only reflect on the upper "
"layer.  From a Landlock policy point of view, each of the OverlayFS layers "
"and merge hierarchies is standalone and contains its own set of files and "
"directories, which is different from a bind mount.  A policy restricting an "
"OverlayFS layer will not restrict the resulted merged hierarchy, and vice "
"versa.  Landlock users should then only think about file hierarchies they "
"want to allow access to, regardless of the underlying filesystem."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Inheritance"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Every new thread resulting from a B<clone>(2)  inherits Landlock domain "
"restrictions from its parent.  This is similar to the B<seccomp>(2)  "
"inheritance or any other LSM dealing with tasks' B<credentials>(7).  For "
"instance, one process's thread may apply Landlock rules to itself, but they "
"will not be automatically applied to other sibling threads (unlike POSIX "
"thread credential changes, cf.  B<nptl>(7))."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"When a thread sandboxes itself, we have the guarantee that the related "
"security policy will stay enforced on all this thread's descendants.  This "
"allows creating standalone and modular security policies per application, "
"which will automatically be composed between themselves according to their "
"runtime parent policies."
msgstr ""

#. type: SS
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Ptrace restrictions"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A sandboxed process has less privileges than a non-sandboxed process and "
"must then be subject to additional restrictions when manipulating another "
"process.  To be allowed to use B<ptrace>(2)  and related syscalls on a "
"target process, a sandboxed process should have a subset of the target "
"process rules, which means the tracee must be in a sub-domain of the tracer."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Landlock was added in Linux 5.13."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Landlock is enabled by B<CONFIG_SECURITY_LANDLOCK>.  The I<lsm=lsm1,...,"
"lsmN> command line parameter controls the sequence of the initialization of "
"Linux Security Modules.  It must contain the string I<landlock> to enable "
"Landlock.  If the command line parameter is not specified, the "
"initialization falls back to the value of the deprecated I<security=> "
"command line parameter and further to the value of B<CONFIG_LSM>.  We can "
"check that Landlock is enabled by looking for I<landlock: Up and running.> "
"in kernel logs."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"It is currently not possible to restrict some file-related actions "
"accessible through these system call families: B<chdir>(2), B<truncate>(2), "
"B<stat>(2), B<flock>(2), B<chmod>(2), B<chown>(2), B<setxattr>(2), "
"B<utime>(2), B<ioctl>(2), B<fcntl>(2), B<access>(2).  Future Landlock "
"evolutions will enable to restrict them."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"We first need to create the ruleset that will contain our rules.  For this "
"example, the ruleset will contain rules that only allow read actions, but "
"write actions will be denied.  The ruleset then needs to handle both of "
"these kinds of actions.  See below for the description of filesystem actions."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"struct landlock_ruleset_attr attr = {0};\n"
"int ruleset_fd;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"attr.handled_access_fs =\n"
"        LANDLOCK_ACCESS_FS_EXECUTE |\n"
"        LANDLOCK_ACCESS_FS_WRITE_FILE |\n"
"        LANDLOCK_ACCESS_FS_READ_FILE |\n"
"        LANDLOCK_ACCESS_FS_READ_DIR |\n"
"        LANDLOCK_ACCESS_FS_REMOVE_DIR |\n"
"        LANDLOCK_ACCESS_FS_REMOVE_FILE |\n"
"        LANDLOCK_ACCESS_FS_MAKE_CHAR |\n"
"        LANDLOCK_ACCESS_FS_MAKE_DIR |\n"
"        LANDLOCK_ACCESS_FS_MAKE_REG |\n"
"        LANDLOCK_ACCESS_FS_MAKE_SOCK |\n"
"        LANDLOCK_ACCESS_FS_MAKE_FIFO |\n"
"        LANDLOCK_ACCESS_FS_MAKE_BLOCK |\n"
"        LANDLOCK_ACCESS_FS_MAKE_SYM;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"ruleset_fd = landlock_create_ruleset(&attr, sizeof(attr), 0);\n"
"if (ruleset_fd == -1) {\n"
"    perror(\"Failed to create a ruleset\");\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"We can now add a new rule to this ruleset thanks to the returned file "
"descriptor referring to this ruleset.  The rule will only allow reading the "
"file hierarchy I</usr>.  Without another rule, write actions would then be "
"denied by the ruleset.  To add I</usr> to the ruleset, we open it with the "
"I<O_PATH> flag and fill the I<struct landlock_path_beneath_attr> with this "
"file descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"struct landlock_path_beneath_attr path_beneath = {0};\n"
"int err;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"path_beneath.allowed_access =\n"
"        LANDLOCK_ACCESS_FS_EXECUTE |\n"
"        LANDLOCK_ACCESS_FS_READ_FILE |\n"
"        LANDLOCK_ACCESS_FS_READ_DIR;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"path_beneath.parent_fd = open(\"/usr\", O_PATH | O_CLOEXEC);\n"
"if (path_beneath.parent_fd == -1) {\n"
"    perror(\"Failed to open file\");\n"
"    close(ruleset_fd);\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
"err = landlock_add_rule(ruleset_fd, LANDLOCK_RULE_PATH_BENEATH,\n"
"                        &path_beneath, 0);\n"
"close(path_beneath.parent_fd);\n"
"if (err) {\n"
"    perror(\"Failed to update ruleset\");\n"
"    close(ruleset_fd);\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"We now have a ruleset with one rule allowing read access to I</usr> while "
"denying all other handled accesses for the filesystem.  The next step is to "
"restrict the current thread from gaining more privileges (e.g., thanks to a "
"set-user-ID binary)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0)) {\n"
"    perror(\"Failed to restrict privileges\");\n"
"    close(ruleset_fd);\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The current thread is now ready to sandbox itself with the ruleset."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"if (landlock_restrict_self(ruleset_fd, 0)) {\n"
"    perror(\"Failed to enforce ruleset\");\n"
"    close(ruleset_fd);\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
"close(ruleset_fd);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If the B<landlock_restrict_self>(2)  system call succeeds, the current "
"thread is now restricted and this policy will be enforced on all its "
"subsequently created children as well.  Once a thread is landlocked, there "
"is no way to remove its security policy; only adding more restrictions is "
"allowed.  These threads are now in a new Landlock domain, merge of their "
"parent one (if any) with the new ruleset."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Full working code can be found in E<.UR https://git.kernel.org/\\:pub/\\:scm/"
"\\:linux/\\:kernel/\\:git/\\:stable/\\:linux.git/\\:tree/\\:samples/\\:"
"landlock/\\:sandboxer.c> E<.UE>"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<landlock_create_ruleset>(2), B<landlock_add_rule>(2), "
"B<landlock_restrict_self>(2)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.UR https://landlock.io/> E<.UE>"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.02"
msgstr ""

#. type: IP
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Landlock is enabled by B<CONFIG_SECURITY_LANDLOCK>.  The I<lsm=lsm1,...,"
"lsmN> command line parameter controls the sequence of the initialization of "
"Linux Security Modules.  It must contain the string I<landlock> to enable "
"Landlock.  If the command line parameter is not specified, the "
"initialization falls back to the value of the deprecated I<security=> "
"command line parameter and further to the value of CONFIG_LSM.  We can check "
"that Landlock is enabled by looking for I<landlock: Up and running.> in "
"kernel logs."
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"We first need to create the ruleset that will contain our rules.  For this "
"example, the ruleset will contain rules that only allow read actions, but "
"write actions will be denied.  The ruleset then needs to handle both of "
"these kind of actions.  See below for the description of filesystem actions."
msgstr ""
