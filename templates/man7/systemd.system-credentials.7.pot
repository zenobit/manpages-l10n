# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-02-20 20:36+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD\\&.SYSTEM-CREDENTIALS"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd.system-credentials"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "systemd.system-credentials - System Credentials"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"\\m[blue]B<System and Service Credentials>\\m[]\\&\\s-2\\u[1]\\d\\s+2 are "
"data objects that may be passed into booted systems or system services as "
"they are invoked\\&. They can be acquired from various external sources, and "
"propagated into the system and from there into system services\\&. "
"Credentials may optionally be encrypted with a machine-specific key and/or "
"locked to the local TPM2 device, and are only decrypted when the consuming "
"service is invoked\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"System credentials may be used to provision and configure various aspects of "
"the system\\&. Depending on the consuming component credentials are only "
"used on initial invocations or are needed for all invocations\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Credentials may be used for any kind of data, binary or text, and may carry "
"passwords, secrets, certificates, cryptographic key material, identity "
"information, configuration, and more\\&."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "WELL KNOWN SYSTEM CREDENTIALS"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<firstboot\\&.keymap>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The console key mapping to set (e\\&.g\\&.  \"de\")\\&. Read by B<systemd-"
"firstboot>(1), and only honoured if no console keymap has been configured "
"before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<firstboot\\&.locale>, I<firstboot\\&.locale-message>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The system locale to set (e\\&.g\\&.  \"de_DE\\&.UTF-8\")\\&. Read by "
"B<systemd-firstboot>(1), and only honoured if no locale has been configured "
"before\\&.  I<firstboot\\&.locale> sets \"LANG\", while I<firstboot\\&."
"locale-message> sets \"LC_MESSAGES\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<firstboot\\&.timezone>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The system timezone to set (e\\&.g\\&.  \"Europe/Berlin\")\\&. Read by "
"B<systemd-firstboot>(1), and only honoured if no system timezone has been "
"configured before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<login\\&.issue>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The data of this credential is written to /etc/issue\\&.d/50-provision\\&."
"conf, if the file doesn\\*(Aqt exist yet\\&.  B<agetty>(8)  reads this file "
"and shows its contents at the login prompt of terminal logins\\&. See "
"B<issue>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Consumed by /usr/lib/tmpfiles\\&.d/provision\\&.conf, see B<tmpfiles."
"d>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<login\\&.motd>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The data of this credential is written to /etc/motd\\&.d/50-provision\\&."
"conf, if the file doesn\\*(Aqt exist yet\\&.  B<pam_motd>(8)  reads this "
"file and shows its contents as \"message of the day\" during terminal "
"logins\\&. See B<motd>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<network\\&.hosts>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The data of this credential is written to /etc/hosts, if the file "
"doesn\\*(Aqt exist yet\\&. See B<hosts>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "I<network\\&.dns>, I<network\\&.search_domains>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"DNS server information and search domains\\&. Read by B<systemd-resolved."
"service>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<passwd\\&.hashed-password\\&.root>, I<passwd\\&.plaintext-password\\&.root>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"May contain the password (either in UNIX hashed format, or in plaintext) for "
"the root users\\&. Read by both B<systemd-firstboot>(1)  and B<systemd-"
"sysusers>(1), and only honoured if no root password has been configured "
"before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<passwd\\&.shell\\&.root>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The path to the shell program (e\\&.g\\&.  \"/bin/bash\") for the root "
"user\\&. Read by both B<systemd-firstboot>(1)  and B<systemd-sysusers>(1), "
"and only honoured if no root shell has been configured before\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<ssh\\&.authorized_keys\\&.root>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The data of this credential is written to /root/\\&.ssh/authorized_keys, if "
"the file doesn\\*(Aqt exist yet\\&. This allows provisioning SSH access for "
"the system\\*(Aqs root user\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<sysusers\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Additional B<sysusers.d>(5)  lines to process during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<sysctl\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Additional B<sysctl.d>(5)  lines to process during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<tmpfiles\\&.extra>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Additional B<tmpfiles.d>(5)  lines to process during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"I<vconsole\\&.keymap>, I<vconsole\\&.keymap_toggle>, I<vconsole\\&.font>, "
"I<vconsole\\&.font_map>, I<vconsole\\&.font_unimap>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"Console settings to apply, see B<systemd-vconsole-setup.service>(8)  for "
"details\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid "I<vmm\\&.notify_socket>"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide
msgid ""
"This credential is parsed looking for an B<AF_VSOCK> or B<AF_UNIX> address "
"where to send a B<READY=1> notification datagram when the system has "
"finished booting\\&. See: B<sd_notify>(3)  This is useful for hypervisors/"
"VMMs or other processes on the host to receive a notification via VSOCK when "
"a virtual machine has finished booting\\&. Note that in case the hypervisor "
"does not support B<SOCK_DGRAM> over B<AF_VSOCK>, B<SOCK_SEQPACKET> will be "
"tried instead\\&. The credential payload for B<AF_VSOCK> should be in the "
"form: \"vsock:CID:PORT\"\\&."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<systemd>(1), B<kernel-command-line>(7)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "System and Service Credentials"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "\\%https://systemd.io/CREDENTIALS"
msgstr ""

#. type: TH
#: debian-bullseye debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 252"
msgstr ""
